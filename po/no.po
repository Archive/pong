# Norwegian translation of gnop (bokm�l dialect).
# Copyright (C) 2000 Free Software Foundation, Inc.
# Kjartan Maraas <kmaraas@gnome.org>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: gnop 0.1\n"
"POT-Creation-Date: 2001-07-03 00:42+0200\n"
"PO-Revision-Date: 2001-07-03 01:01+0200\n"
"Last-Translator: Kjartan Maraas <kmaraas@gnome.org>\n"
"Language-Team: Norwegian <no@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8-bit\n"

#: pong-edit/pong-edit.c:106
#, c-format
msgid "PonG-Edit: %s"
msgstr "PonG-redigering: %s"

#: pong-edit/pong-edit.c:108
msgid "PonG-Edit"
msgstr "PonG-redigering"

#: pong-edit/pong-edit.c:152
msgid "No filename specified, please choose \"File/Save As\""
msgstr "Ingen filnavn spesifisert, vennligst velg \"Fil/Lagre som\""

#: pong-edit/pong-edit.c:162 pong-edit/pong-edit.c:802
#, c-format
msgid "File '%s' cannot be saved"
msgstr "Fil '%s' kan ikke lagres"

#: pong-edit/pong-edit.c:185
msgid ""
"File not saved, continue?\n"
"(if you continue, changes will be lost)"
msgstr ""
"Fil ikke lagret, fortsett?\n"
"(hvis du fortsetter vil endringene g� tapt)"

#: pong-edit/pong-edit.c:188
msgid "Continue"
msgstr "Fortsett"

#: pong-edit/glade-strings.c:20 pong-edit/pong-edit.c:189
msgid "Save"
msgstr "Lagre"

#: pong-edit/pong-edit.c:324 pong-edit/pong-edit.c:419
msgid "File does not exist"
msgstr "Filen eksisterer ikke"

#: pong-edit/pong-edit.c:333 pong-edit/pong-edit.c:428
msgid "File cannot be loaded"
msgstr "Filen kan ikke lastes"

#: pong-edit/pong-edit.c:361 pong-edit/pong-edit.c:944
msgid ""
"The file that was loaded contains xml:lang\n"
"properties.  This would mean that it is a file\n"
"generated by xml-i18n-tools.  You probably want\n"
"to edit the .pong.in file and not the .pong file.\n"
"PonG-Edit cannot directly edit translated files\n"
"properly."
msgstr ""
"Filen som ble lastet inneholder xml:lang\n"
"egenskaper. Dette betyr at det er en fil\n"
"som er generert av xml-i18n-tools. Du vil sannsynligvis\n"
"redigere .pong.in filen og ikke .pong filen.\n"
"PonG-Edit kan ikke redigere oversatte filer direkte\n"
"p� en korrekt m�te."

#: pong-edit/glade-strings.c:19 pong-edit/pong-edit.c:387
msgid "Open"
msgstr "�pne"

#: pong-edit/pong-edit.c:441
msgid "Cannot import schemas"
msgstr "Kan ikke importere schemaer"

#: pong-edit/pong-edit.c:469
msgid ""
"Note that importing gconf schema files is not,\n"
"and will not ever be perfect.  It is only meant\n"
"as a timesaver when converting to pong,\n"
"or using pong to edit .schema files."
msgstr ""
"Merk at import av schemafiler fra gconf ikke er,\n"
"og aldri vil bli perfekt. Det er kun ment som\n"
"en tidsbesparende faktor ved konvertering\n"
"til pong, eller bruk av pong for � redigere\n"
"schemafiler."

#: pong-edit/pong-edit.c:479
msgid "Import schemas"
msgstr "Importer schemaer"

#: pong-edit/pong-edit.c:535
msgid "Select a level"
msgstr "Velg et niv�"

#: pong-edit/pong-edit.c:548
msgid "Select a level:"
msgstr "Velg et niv�:"

#: pong-edit/pong-edit.c:551
msgid "Default"
msgstr "Forvalgt"

#: pong-edit/pong-edit.c:600 pong-edit/pong-edit.c:658
msgid "Cannot create XML memory"
msgstr "Kan ikke opprette XML-minne"

#: pong-edit/pong-edit.c:610 pong/pong.c:161 pong/pong.c:197
msgid "Cannot load config dialog"
msgstr "Kan ikke laste konfigurasjonsdialogen"

#: pong-edit/pong-edit.c:626 pong/pong.c:170 pong/pong.c:206
msgid "Cannot run config dialog"
msgstr "Kan ikke kj�re konfigurasjonsdialogen"

#: pong-edit/pong-edit.c:670
msgid "Cannot parse XML buffer, strange!"
msgstr "Kan ikke tolke XML-buffer, rart!"

#: pong-edit/pong-edit.c:695
msgid ""
"Note that sensitivities do not work when\n"
"testing without gconf, it is meant only\n"
"to test the dialog layout"
msgstr ""
"Merk at sensitiviteter ikke virker hvis\n"
"du tester uten gconf. Det er kun ment\n"
"for testing av dialogboksutforming"

#: pong-edit/pong-edit.c:719
#, c-format
msgid "File %s exists, overwrite?"
msgstr "Filen %s eksisterer, overskriv?"

#: pong-edit/pong-edit.c:731
msgid "File cannot be written"
msgstr "Filen kan ikke skrives"

#: pong-edit/pong-edit.c:769
msgid "Export Schemas"
msgstr "Eksporer schemaer"

#: pong-edit/pong-edit.c:849
msgid "Save As"
msgstr "Lagre som"

#: pong-edit/pong-edit.c:900 pong/pongparser.c:1419
#, c-format
msgid "File '%s' does not exist"
msgstr "Filen '%s' eksisterer ikke!"

#: pong-edit/pong-edit.c:913
#, c-format
msgid "File '%s' cannot be loaded"
msgstr "Filen '%s' kan ikke lastes"

#: pong-edit/pong-edit.c:1068 pong-tool/pong-tool.c:61
#, c-format
msgid ""
"GConf init failed:\n"
"  %s"
msgstr ""
"GConf initialisering feilet:\n"
"  %s"

#: pong-edit/pong-edit.c:1090
msgid "You can only supply a single argument with the .pong file to use\n"
msgstr "Du kan kun oppgi et enkelt argument med .pong-filen som skal brukes\n"

#: pong-edit/glade-helper.c:80
msgid "(memory buffer)"
msgstr "(minnebuffer)"

#: pong-edit/glade-helper.c:112 pong-edit/glade-helper.c:134
#: pong-edit/glade-helper.c:154
msgid "(memory buffer"
msgstr "(minnebuffer"

#: pong-edit/glade-helper.c:183
#, c-format
msgid ""
"An error occured while loading the user interface\n"
"element %s%s from file %s.\n"
"Possibly the glade interface description was corrupted.\n"
"%s cannot continue and will exit now.\n"
"You should check your installation of %s or reinstall %s."
msgstr ""
"En feil oppsto under lasting av brukergrensesnitt-\n"
"elementet %s%s fra fil %s.\n"
"Sannsynligvis er glade-filen korrupt.\n"
"%s kan ikke fortsette og vil avslutte n�.\n"
"Du b�r sjekke installasjonen av %s eler installere %s p� nytt."

#: pong-edit/glade-helper.c:201
#, c-format
msgid ""
"Glade file is on crack! Make sure the correct file is installed!\n"
"file: %s widget: %s"
msgstr ""
"Glade-filen er p� crack! Forsikre deg om at korrekt fil er installert!\n"
"fil: %s widget: %s"

#: pong-edit/glade-helper.c:227
#, c-format
msgid ""
"An error occured while loading the user interface\n"
"element %s%s from file %s.\n"
"CList type widget should have %d columns.\n"
"Possibly the glade interface description was corrupted.\n"
"%s cannot continue and will exit now.\n"
"You should check your installation of %s or reinstall %s."
msgstr ""
"En feil oppsto under lasting av brukergrensesnitt-\n"
"elementet %s%s fra filen %s.\n"
"Widget av Clist-type skulle hatt %d kolonner.\n"
"Det er mulig at glade grensesnittbeskrivelsen er korrupt.\n"
"%s kan ikke fortsette og vil avslutte n�.\n"
"Du b�r sjekke installasjonen av %s eller reinstallere %s."

#: pong-edit/glade-helper.c:247
#, c-format
msgid ""
"Glade file is on crack! Make sure the correct file is installed!\n"
"file: %s widget: %s expected clist columns: %d"
msgstr ""
"Glade-filen er p� crack! Sjekk at riktig fil er installert!\n"
"fil: %s widget: %s forventet clist kolonner: %d"

#: pong-edit/glade-helper.c:261
#, c-format
msgid ""
"An error occured while loading the user interface\n"
"from file %s.\n"
"Possibly the glade interface description was not found.\n"
"%s cannot continue and will exit now.\n"
"You should check your installation of %s or reinstall %s."
msgstr ""
"En feil oppsto under lasting av grensesnitt fra\n"
"fil: %s.\n"
"Det er mulig at grensesnittbeskrivelsen ikke ble funnet.\n"
"%s kan ikke fortsette og vil avslutte n�.\n"
"Du b�r sjekke installasjonen av %s eller reinstallere %s."

#: pong-edit/glade-helper.c:274
#, c-format
msgid "No interface could be loaded, BAD! (file: %s)"
msgstr "Ingen grensesnitt kunne lastes. (fil: %s)"

#: pong-edit/glade-strings.c:89 pong-edit/panes.c:104 pong-edit/panes.c:953
msgid "Widget"
msgstr "Widget"

#: pong-edit/glade-strings.c:87 pong-edit/panes.c:943
msgid "Pane"
msgstr "Fane"

#: pong-edit/glade-strings.c:88 pong-edit/panes.c:948
msgid "Group"
msgstr "Gruppe"

#: pong-edit/glade-strings.c:92 pong-edit/panes.c:960
msgid "Plug"
msgstr "Plugg"

#: pong-edit/glade-strings.c:91 pong-edit/panes.c:967
msgid "Bonobo"
msgstr "Bonobo"

#: pong-edit/glade-strings.c:90 pong-edit/panes.c:974
msgid "Glade"
msgstr "Glade"

#: pong-edit/panes.c:979 pong/pong-check-group.gob:392
#: pong/pong-option-menu.gob:326 pong/pong-radio-group.gob:356
msgid "???"
msgstr "???"

#: pong-edit/glade-strings.c:148 pong-edit/panes.c:1088
msgid "<type>"
msgstr "<type>"

#.
#. * Translatable strings file generated by Glade.
#. * Add this file to your project's POTFILES.in.
#. * DO NOT compile it as part of your application.
#.
#: pong-edit/glade-strings.c:7
msgid "PonG Editor"
msgstr "PonG-redigering"

#: pong-edit/glade-strings.c:8
msgid "_New File"
msgstr "_Ny fil"

#: pong-edit/glade-strings.c:9
msgid "Import schemas into the pong file and add skeleton entries for them"
msgstr ""
"Importer schema til pong-filen og legg til skjelett-oppf�ringer for dem"

#: pong-edit/glade-strings.c:10
msgid "_Import schemas..."
msgstr "_Importer schemaer..."

#: pong-edit/glade-strings.c:11
msgid "Export GConf schema data to a file"
msgstr "Eksporter GConf schema-data til en fil"

#: pong-edit/glade-strings.c:12
msgid "Export schemas..."
msgstr "Eksporter schemaer..."

#: pong-edit/glade-strings.c:13
msgid "_Testing"
msgstr "_Tester"

#: pong-edit/glade-strings.c:14 pong-edit/glade-strings.c:21
msgid ""
"Try running this dialog and connect to GConf.  This will test the dialog as "
"it will be used in your application.  Note that this will be changing the "
"live GConf database."
msgstr "Pr�v � kj�re denne dialogen og koble til GConf. Dette vil teste dialogen som den blir brukt i applikasjonen. Merk at dette vil utf�re endringer p� en kj�rende GConf database."

#: pong-edit/glade-strings.c:15
msgid "Test with gconf"
msgstr "Test med gconf"

#: pong-edit/glade-strings.c:16 pong-edit/glade-strings.c:23
msgid ""
"Test the dialog without connecting to GConf.  Note that several things will "
"not be working as some features require connection to the GConf database.  "
"However this is good for testing the layout of widgets."
msgstr "Test dialogen uten � koble til GConf. Merk at flere ting ikke vil fungere da en mengde funksjoner krever tilkobling til GConf-databasen. Likevel er dette en bra m�te � teste plassering av widgeter."

#: pong-edit/glade-strings.c:17
msgid "Test without gconf"
msgstr "Test uten gconf"

#: pong-edit/glade-strings.c:18
msgid "Manual"
msgstr "H�ndbok"

#: pong-edit/glade-strings.c:22
msgid "Test (gconf)"
msgstr "Test (gconf)"

#: pong-edit/glade-strings.c:24
msgid "Test"
msgstr "Test"

#: pong-edit/glade-strings.c:25
msgid ""
"The prefix for the configuration keys.  This will be prepended to all keys "
"and should end with a slash.  For applications use the standard /apps/"
"application-binary-name/ convention."
msgstr "Prefiks for konfigurasjonsn�klene. Dette vil legges til foran alle n�kler og b�r slutte med en skr�strek. For applikasjoner bruk standarden /apps/ applikasjonens-bin�rfil-navn/ konvensjonen."

#: pong-edit/glade-strings.c:26
msgid "name under which the help file is created (e.g. \"panel\")"
msgstr "navn hjelpfilen blir opprettet med (f.eks. \"panel\")"

#: pong-edit/glade-strings.c:27
msgid "path to the help file (e.g. \"index.html\")"
msgstr "sti til hjelpfilen (f.eks. \"index.html\")"

#: pong-edit/glade-strings.c:28
msgid "Help name:"
msgstr "Hjelp-navn:"

#: pong-edit/glade-strings.c:29 pong-edit/glade-strings.c:179
msgid "Configuration prefix:"
msgstr "Konfigurasjonsprefiks:"

#: pong-edit/glade-strings.c:30
msgid "Help path:"
msgstr "Hjelp-sti:"

#: pong-edit/glade-strings.c:31
msgid "Dialog title:"
msgstr "Dialogtittel:"

#: pong-edit/glade-strings.c:32
msgid "The title of the preference dialog created."
msgstr "Tittel for brukervalg-dialogen som opprettes."

#: pong-edit/glade-strings.c:33
msgid ""
"The default GConf schema owner to use.  Usually the name of the application "
"binary."
msgstr "Standard GConf-schemaeier som skal brukes. Vanligvis navnet p� applikasjonens bin�rfil."

#: pong-edit/glade-strings.c:34
msgid "Schema owner:"
msgstr "Schema-eier:"

#: pong-edit/glade-strings.c:35
msgid "Configuration"
msgstr "Konfigurasjon"

#: pong-edit/glade-strings.c:36 pong-edit/glade-strings.c:72
msgid "Type"
msgstr "Type"

#: pong-edit/glade-strings.c:37
msgid "Widget name"
msgstr "Widgetnavn"

#: pong-edit/glade-strings.c:38
msgid "Configuration path"
msgstr "Konfigurasjonssti"

#: pong-edit/glade-strings.c:39 pong-edit/glade-strings.c:153
#: pong-edit/glade-strings.c:161 pong-edit/glade-strings.c:176
msgid "Add"
msgstr "Legg til"

#: pong-edit/glade-strings.c:40 pong-edit/glade-strings.c:78
#: pong-edit/glade-strings.c:95 pong-edit/glade-strings.c:154
#: pong-edit/glade-strings.c:162 pong-edit/glade-strings.c:177
msgid "Remove"
msgstr "Fjern"

#: pong-edit/glade-strings.c:41
msgid "GConf path to this key."
msgstr "GConf-sti til denne n�kkelen."

#: pong-edit/glade-strings.c:42
msgid "The name of the widget that this key corresponds to."
msgstr "Navnet p� widgetet som denne n�kkelen korresponderer til."

#: pong-edit/glade-strings.c:43
msgid ""
"If the widget can service several keys, this would select which value this "
"key corresponds to.  Usually can be left blank."
msgstr ""

#: pong-edit/glade-strings.c:44
msgid "Default:"
msgstr "Forvalg:"

#: pong-edit/glade-strings.c:45
msgid "Specifier:"
msgstr "Spesifikator:"

#: pong-edit/glade-strings.c:46 pong-edit/glade-strings.c:145
msgid "Widget:"
msgstr "Widget:"

#: pong-edit/glade-strings.c:47
msgid "Configuration path:"
msgstr "Konfigurasjonssti:"

#: pong-edit/glade-strings.c:48 pong-edit/glade-strings.c:166
msgid "The default value for this configuration key."
msgstr "Standardverdi for denne konfigurasjonsn�kkelen."

#: pong-edit/glade-strings.c:49 pong-edit/glade-strings.c:100
msgid "Basic"
msgstr "Grunnleggende"

#: pong-edit/glade-strings.c:50
msgid "Type:"
msgstr "Type:"

#: pong-edit/glade-strings.c:51 pong-edit/glade-strings.c:60
#: pong-edit/glade-strings.c:64 pong-edit/glade-strings.c:68
msgid "Int"
msgstr "Heltall"

#: pong-edit/glade-strings.c:52 pong-edit/glade-strings.c:61
#: pong-edit/glade-strings.c:65 pong-edit/glade-strings.c:69
msgid "Float"
msgstr "Flyt"

#: pong-edit/glade-strings.c:53 pong-edit/glade-strings.c:62
#: pong-edit/glade-strings.c:66 pong-edit/glade-strings.c:70
msgid "String"
msgstr "Streng"

#: pong-edit/glade-strings.c:54 pong-edit/glade-strings.c:63
#: pong-edit/glade-strings.c:67 pong-edit/glade-strings.c:71
msgid "Bool"
msgstr "Bool"

#: pong-edit/glade-strings.c:55
msgid "List"
msgstr "Liste"

#: pong-edit/glade-strings.c:56
msgid "Pair"
msgstr "Par"

#: pong-edit/glade-strings.c:57
msgid "Type of second pair element:"
msgstr "Type for andre element i par:"

#: pong-edit/glade-strings.c:58
msgid "Type of first pair element:"
msgstr "Type for f�rste element i par:"

#: pong-edit/glade-strings.c:59
msgid "Type of list items:"
msgstr "Type for listeoppf�ringer:"

#: pong-edit/glade-strings.c:73 pong-edit/glade-strings.c:150
#: pong-edit/glade-strings.c:159
msgid "Value"
msgstr "Verdi"

#: pong-edit/glade-strings.c:74
msgid "Sensitive widgets"
msgstr "F�lsomme widgeter"

#: pong-edit/glade-strings.c:75
msgid "Insensitive widgets"
msgstr "Uf�lsomme widgeter"

#: pong-edit/glade-strings.c:76
msgid "Add..."
msgstr "Legg til..."

#: pong-edit/glade-strings.c:77
msgid "Edit..."
msgstr "Rediger..."

#: pong-edit/glade-strings.c:79
msgid "Sensitivities"
msgstr "F�lsomhet"

#: pong-edit/glade-strings.c:80
msgid ""
"The GConf schema owner for this key, usually the name of the application "
"binary."
msgstr ""

#: pong-edit/glade-strings.c:81
msgid "Short description:"
msgstr "Kort beskrivelse:"

#: pong-edit/glade-strings.c:82
msgid "Owner:"
msgstr "Eier:"

#: pong-edit/glade-strings.c:83
msgid "Long description:"
msgstr "Lang beskrivelse:"

#: pong-edit/glade-strings.c:84
msgid "Schema"
msgstr "Schema"

#: pong-edit/glade-strings.c:85
msgid "Configuration keys"
msgstr "Konfigurasjonsn�kler"

#: pong-edit/glade-strings.c:86
msgid "Add:"
msgstr "Legg til:"

#: pong-edit/glade-strings.c:93 pong-edit/glade-strings.c:151
#: pong-edit/glade-strings.c:174
msgid "Move Up"
msgstr "Flytt opp"

#: pong-edit/glade-strings.c:94 pong-edit/glade-strings.c:152
#: pong-edit/glade-strings.c:175
msgid "Move Down"
msgstr "Flytt ned"

#: pong-edit/glade-strings.c:96 pong-edit/glade-strings.c:163
#: pong-edit/glade-strings.c:178
msgid "Name:"
msgstr "Navn:"

#: pong-edit/glade-strings.c:97 pong-edit/glade-strings.c:155
msgid "Label:"
msgstr "Etikett:"

#: pong-edit/glade-strings.c:98
msgid "A label to be set on this widget or prepended in front of it."
msgstr ""
"En etikett som skal settes p� dette widgetet eller legges til foran det."

#: pong-edit/glade-strings.c:99
msgid ""
"Name that uniquely identifies this widget.  This should correspond to the "
"Widget parameter for some configuration key."
msgstr ""

#: pong-edit/glade-strings.c:101
msgid "foo!"
msgstr "foo!"

#: pong-edit/glade-strings.c:102
msgid "label101"
msgstr "etikett101"

#: pong-edit/glade-strings.c:103
msgid "Pane has no other options"
msgstr "Fanen har ingen andre alternativer"

#: pong-edit/glade-strings.c:104
msgid "label63"
msgstr "etikett63"

#: pong-edit/glade-strings.c:105 pong-edit/glade-strings.c:108
#: pong-edit/glade-strings.c:133 pong-edit/glade-strings.c:139
#: pong-edit/glade-strings.c:143
msgid "Expand"
msgstr "Utvid"

#: pong-edit/glade-strings.c:106
msgid "Columns:"
msgstr "Kolonner:"

#: pong-edit/glade-strings.c:107
msgid "label64"
msgstr "etikett64"

#: pong-edit/glade-strings.c:109
msgid "Tooltip: "
msgstr "Verkt�ystips: "

#: pong-edit/glade-strings.c:110
msgid "Pong:Check:Group"
msgstr "Pong:Check:Group"

#: pong-edit/glade-strings.c:111
msgid "Pong:Radio:Group"
msgstr "Pong:Radio:Group"

#: pong-edit/glade-strings.c:112
msgid "Pong:Option:Menu"
msgstr "Pong:Option:Menu"

#: pong-edit/glade-strings.c:113
msgid "Pong:Color:Picker"
msgstr "Pong:Color:Picker"

#: pong-edit/glade-strings.c:114
msgid "Pong:Spin:Button"
msgstr "Pong:Spin:Button"

#: pong-edit/glade-strings.c:115
msgid "Pong:Slider"
msgstr "Pong:Slider"

#: pong-edit/glade-strings.c:116
msgid "Pong:List:Entry"
msgstr "Pong:List:Entry"

#: pong-edit/glade-strings.c:117
msgid "Gtk:Spin:Button"
msgstr "Gtk:Spin:Button"

#: pong-edit/glade-strings.c:118
msgid "Gnome:Number:Entry"
msgstr "Gnome:Number:Entry"

#: pong-edit/glade-strings.c:119
msgid "Gnome:Calculator"
msgstr "Gnome:Calculator"

#: pong-edit/glade-strings.c:120
msgid "Gtk:Entry"
msgstr "Gtk:Entry"

#: pong-edit/glade-strings.c:121
msgid "Gtk:Text"
msgstr "Gtk:Text"

#: pong-edit/glade-strings.c:122
msgid "Gtk:Combo"
msgstr "Gtk:Combo"

#: pong-edit/glade-strings.c:123
msgid "Gnome:Entry"
msgstr "Gnome:Entry"

#: pong-edit/glade-strings.c:124
msgid "Gnome:File:Entry"
msgstr "Gnome:File:Entry"

#: pong-edit/glade-strings.c:125
msgid "Gnome:Pixmap:Entry"
msgstr "Gnome:Pixmap:Entry"

#: pong-edit/glade-strings.c:126
msgid "Gnome:Icon:Entry"
msgstr "Gnome:Icon:Entry"

#: pong-edit/glade-strings.c:127
msgid "Gtk:Toggle:Button"
msgstr "Gtk:Toggle:Button"

#: pong-edit/glade-strings.c:128
msgid "Gtk:Check:Button"
msgstr "Gtk:Check:Button"

#: pong-edit/glade-strings.c:129
msgid "Gnome:Color:Picker"
msgstr "Gnome:Color:Picker"

#: pong-edit/glade-strings.c:130
msgid "Widget Type: "
msgstr "Widgettype: "

#: pong-edit/glade-strings.c:131 pong-edit/glade-strings.c:137
#: pong-edit/glade-strings.c:141 pong-edit/glade-strings.c:146
msgid "Align label"
msgstr "Juster etikett"

#: pong-edit/glade-strings.c:132
msgid "label65"
msgstr "etikett65"

#: pong-edit/glade-strings.c:134
msgid "Path: "
msgstr "Sti: "

#: pong-edit/glade-strings.c:135
msgid "Specifier: "
msgstr "Spesifikator: "

#: pong-edit/glade-strings.c:136
msgid "Plug type: "
msgstr "Type plugg: "

#: pong-edit/glade-strings.c:138
msgid "label66"
msgstr "etikett66"

#: pong-edit/glade-strings.c:140
msgid "Query string: "
msgstr "Sp�rringsstreng: "

#: pong-edit/glade-strings.c:142
msgid "label67"
msgstr "etikett67"

#: pong-edit/glade-strings.c:144
msgid "File:"
msgstr "Fil:"

#: pong-edit/glade-strings.c:147
msgid "label68"
msgstr "etikett68"

#: pong-edit/glade-strings.c:149
msgid "Label"
msgstr "Etikett"

#: pong-edit/glade-strings.c:156 pong-edit/glade-strings.c:164
msgid "Value:"
msgstr "Verdi:"

#: pong-edit/glade-strings.c:157
msgid "Options"
msgstr "Alternativer"

#: pong-edit/glade-strings.c:158 pong-edit/glade-strings.c:172
msgid "Name"
msgstr "Navn"

#: pong-edit/glade-strings.c:160
msgid "Translate"
msgstr "Oversett"

#: pong-edit/glade-strings.c:165
msgid "Value translatable"
msgstr "Verdi oversettbar"

#: pong-edit/glade-strings.c:167
msgid "Arguments"
msgstr "Argumenter"

#: pong-edit/glade-strings.c:168
msgid "All levels"
msgstr "Alle niv�er"

#: pong-edit/glade-strings.c:169
msgid "If no levels are selected the widget will appear in all user levels."
msgstr "Hvis ingen niv�er er valgt vil widgetet vises for alle brukerniv�er."

#: pong-edit/glade-strings.c:170
msgid "Levels"
msgstr "Niv�er"

#: pong-edit/glade-strings.c:171
msgid "Dialog panes"
msgstr "Dialogfaner"

#: pong-edit/glade-strings.c:173
msgid "Configuration prefix"
msgstr "Konfigurasjonsprefiks"

#: pong-edit/glade-strings.c:180
msgid "User levels"
msgstr "Brukerniv�er"

#: pong-edit/glade-strings.c:181
msgid "(c) 2000,2001 Eazel, Inc."
msgstr "� 2000,2001 Eazel, Inc."

#: pong-edit/glade-strings.c:182
msgid "PonG XML and GConf schema editor"
msgstr "PonG XML og GConf schema editor"

#: pong-edit/glade-strings.c:183
msgid "Edit Sensitivity"
msgstr "Rediger f�lsomhet"

#: pong-edit/glade-strings.c:184
msgid "Values:"
msgstr "Verdier:"

#: pong-edit/glade-strings.c:185
msgid "Less then"
msgstr "Mindre enn<"

#: pong-edit/glade-strings.c:186
msgid "Less then or equal to"
msgstr "Mindre enn eller lik"

#: pong-edit/glade-strings.c:187
msgid "Equal to"
msgstr "Lik"

#: pong-edit/glade-strings.c:188
msgid "Greater then or equal to"
msgstr "St�rre enn eller lik"

#: pong-edit/glade-strings.c:189
msgid "Greater then"
msgstr "St�rre enn"

#: pong-edit/glade-strings.c:190
msgid "Not equal to"
msgstr "Ikke lik"

#: pong-edit/glade-strings.c:191
msgid "And"
msgstr "Og"

#: pong-edit/glade-strings.c:192
msgid "Or"
msgstr "Eller"

#: pong-edit/glade-strings.c:193
msgid "Real value is: "
msgstr "Reell verdi er: "

#: pong-edit/glade-strings.c:194
msgid "Connector:"
msgstr "Tilkobler:"

#: pong-edit/glade-strings.c:195
msgid "These widgets become sensitive"
msgstr "Disse widgetene blir f�lsomme"

#: pong-edit/glade-strings.c:196 pong-edit/glade-strings.c:198
msgid "Select..."
msgstr "Velg..."

#: pong-edit/glade-strings.c:197
msgid "These widgets become insensitive"
msgstr "Disse widgetene blir uf�lsome"

#: pong-edit/glade-strings.c:199
msgid "Select Widget"
msgstr "Velg widget"

#: pong-edit/glade-strings.c:200
msgid ""
"Select a widget from the below list.  Only the pane interface widgets will "
"be shown however.  To enter other widgets you have to enter them directly."
msgstr ""

#: pong-edit/glade-strings.c:201
msgid "label99"
msgstr "etikett99"

#: pong/pongelement.c:622 pong/pongelement.c:923 pong/pongelement.c:1171
msgid "Invalid string widget type"
msgstr "Ugyldig type streng-widget"

#: pong/pongelement.c:745
msgid "Error while getting value from widget"
msgstr "Feil under henting av verdi fra widget"

#: pong/pongelement.c:823 pong/pongelement.c:1081
msgid "Invalid integer widget type"
msgstr "Ugyldig type heltall-widget"

#: pong/pongelement.c:872 pong/pongelement.c:1126
msgid "Invalid double widget type"
msgstr "Ugyldig type flyttall-widget"

#: pong/pongelement.c:950 pong/pongelement.c:1189
msgid "Invalid boolean widget type"
msgstr "Ugyldig type boolsk widget"

#: pong/pongelement.c:989 pong/pongelement.c:1226
msgid "Invalid list/pair widget type"
msgstr "Ugyldig liste/par widget-type"

#: pong/pongelement.c:1010
msgid "Error getting string from widget"
msgstr "Feil under henting av streng fra widget"

#: pong/pongelement.c:1250
msgid "Error setting widget value"
msgstr "Feil under setting av verdi for widget"

#: pong/pongelement.c:1371
msgid "Unsupported sensitivity value type"
msgstr "Ust�ttet type sensitivitetsverdi"

#: pong/pongpane.c:160
#, c-format
msgid ""
"GModule is not supported and a widget of type '%s' has not yet been "
"initialized!"
msgstr ""
"GModule er ikke st�ttet og et widget av type '%s' er ikke enn� initiert!"

#: pong/pongpane.c:175
#, c-format
msgid "Unknown widget %s found"
msgstr "Ukjent widget %s funnet"

#: pong/pongpane.c:187
#, c-format
msgid "Unknown widget '%s' found"
msgstr "Ukjent widget '%s' funnet"

#: pong/pongpane.c:194
#, c-format
msgid "Strange widget type '%s' found"
msgstr "Underlig widget-type '%s' funnet"

#: pong/pongpane.c:199
#, c-format
msgid "Non-widget object '%s' found"
msgstr "Objekt '%s' som ikke er et widget ble funnet"

#: pong/pongpane.c:470
#, c-format
msgid "Pane %d"
msgstr "Fane %d"

#: pong/pongpane.c:532
#, c-format
msgid "Group %d"
msgstr "Gruppe %d"

#: pong/pongpane.c:993
msgid "Invalid widget type found while creating dialog"
msgstr "Ugyldig widget-type funnet under oppretting av dialog"

#: pong/pongpane.c:1010 pong/pongpane.c:1024
msgid "Cannot create native widget!"
msgstr "Kan ikke opprette native widget!"

#: pong/pongpane.c:1082 pong/pongpane.c:1090 pong/pongpane.c:1098
msgid "Browse"
msgstr "Bla gjennom"

#: pong/pongpane.c:1106
msgid "Calculator"
msgstr "Kalkulator"

#: pong/pongpane.c:1137
msgid "Unsupported widget type found while creating dialog"
msgstr "Ust�ttet widget-type funnet under oppretting av dialog"

#. this should never happen really
#: pong/pongpane.c:1178
msgid "No plug_type specified for a plug widget"
msgstr "Ingen plug_type spesifisert for et plugg-widget"

#: pong/pongpane.c:1183
#, c-format
msgid "No path specified for a '%s' widget"
msgstr "Ingen sti spesifisert for en '%s'-widget"

#. can this even happen?
#: pong/pongpane.c:1190
#, c-format
msgid "No UI object.  Skipping '%s' widgets."
msgstr "Ingen UI-objekt. Hopper over '%s'-widgeter."

#: pong/pongpane.c:1202
#, c-format
msgid "No '%s' support in UI object.  Skipping '%s' widgets."
msgstr "Ingen '%s'-st�tte i UI-objekt. Hopper over '%s'-widgeter."

#: pong/pongpane.c:1210
#, c-format
msgid "Cannot create a '%s' widget %s %s"
msgstr "Kan ikke opprette et '%s'-widget %s %s"

#: pong/pongpane.c:1434 pong/pongpane.c:1440 pong/pongpane.c:1455
#: pong/pongpane.c:1460
#, c-format
msgid "Widget of name '%s' is already added!"
msgstr "Widget med navn '%s' er allerede lagt til!"

#: pong/pongpane.c:1625
msgid "No file or panes to make dialog from"
msgstr "Ingen fil eller faner � lage dialog fra"

#. Evil huh, but I want to
#. * keep the Revert
#. * translations so I don't want
#. * to just delete this line
#: pong/pongpane.c:1663
msgid "Revert"
msgstr "Forkast"

#: pong/pongparser.c:446
msgid "Type not set before Sensitivity"
msgstr "Type ikke satt f�r f�lsomhet"

#: pong/pongparser.c:770
msgid "Untyped Pong Element found"
msgstr "Pong-element uten type funnet"

#: pong/pongparser.c:776
msgid "Unnamed Pong Element found"
msgstr "Pong-element uten navn funnet"

#: pong/pongparser.c:813
msgid "Non auto apply is NOT currently implemented"
msgstr ""

#: pong/pongparser.c:866
msgid "Fields missing from Level tag"
msgstr "Felter mangler fra niv�-merke"

#: pong/pongparser.c:942
#, c-format
msgid "Unknown connection '%s' assuming OR"
msgstr "Ukjent forbindelse '%s' g�r ut fra ELLER"

#: pong/pongparser.c:955
#, c-format
msgid "Unknown comparison '%s' assuming equals"
msgstr "Ukjent sammenligning '%s' g�r ut fra \"er lik\""

#: pong/pongparser.c:1039
msgid "A glade widget needs to have 'File' and 'Widget' specified"
msgstr ""

#: pong/pongparser.c:1054
msgid "A bonobo widget needs to have 'Query' specified"
msgstr ""

#: pong/pongparser.c:1070
msgid "A plug widget needs to have 'Type' and 'Path' specified at least"
msgstr ""

#: pong/pongparser.c:1117
msgid "Columns must be at least 1"
msgstr "Verdi for kolonne m� v�re minst 1"

#: pong/pongparser.c:1145
msgid "Argument requires both name and value"
msgstr "Argument trenger b�de navn og verdi"

#: pong/pongparser.c:1193
msgid "Option requires a value"
msgstr "Alternativ trenger en verdi"

#: pong/pongparser.c:1482
#, c-format
msgid "Cannot find file '%s'"
msgstr "Kan ikke finne fil '%s'"

#: pong/pong-check-group.gob:182
msgid "Pong:Check:Group supports only boolwhen a 'specifier' is present"
msgstr "Pong:Check:Group st�tter bare bool n�r en 'spesifikator' er tilstede"

#: pong/pong-check-group.gob:198 pong/pong-check-group.gob:353
msgid "A specifier that doesn't exist requested on Pong:Check:Group"
msgstr ""
"Foresp�rsel etter en spesifikator som ikke eksisterer p� Pong:Check:Group"

#: pong/pong-check-group.gob:207 pong/pong-check-group.gob:362
msgid "Unsupported type on Pong:Check:Group"
msgstr "Ust�ttet type p� Pong:Check:Group"

#: pong/pong-check-group.gob:295 pong/pong-check-group.gob:299
msgid "Setting Pong:Check:Group to a wrong number of bools"
msgstr "Setter Pong:Check:Group til feil antall boolske verdier"

#: pong/pong-check-group.gob:338
msgid "Pong:Check:Group supports only bool when a 'specifier' is present"
msgstr "Pong:Check:Group st�tter kun bool n�r en 'spesifikator' er tilstede"

#: pong/pong-radio-group.gob:173
msgid "Pong:Radio:Group supports only boolwhen a 'specifier' is present"
msgstr "Pong:Radio:Group st�tter kun en bool n�r en 'spesifikator' er tilstede"

#: pong/pong-radio-group.gob:189 pong/pong-radio-group.gob:268
msgid "A specifier that doesn't exist requested on Pong:Radio:Group"
msgstr "Foresp�rsel om en spesifikator som ikke eksisterer p� Pong:Radio:Group"

#: pong/pong-radio-group.gob:198 pong/pong-radio-group.gob:278
msgid "Unsupported type on Pong:Radio:Group"
msgstr "Ust�ttet type p� Pong:Radio:Group"

#: pong/pong-radio-group.gob:253
msgid "Pong:Radio:Group supports only bool when a 'specifier' is present"
msgstr "Pong:Radio:Group st�tter kun bool n�r en 'spesifikator' er tilstede"

#: pong/pong-slider.gob:241 pong/pong-spin-button.gob:158
#: pong/pong-spin-button.gob:197
msgid "Unsupported type on Pong:Spin:Button"
msgstr "Ust�ttet type p� Pong:Spin:Button"

#: pong/pong-ui-glade.gob:97
#, c-format
msgid "Cannot load glade file: '%s'"
msgstr "Kan ikke laste glade-fil: '%s'"

#: pong/pong-ui-internal.gob:120
msgid "Cannot make dialog"
msgstr "Kan ikke lage dialog"

#: pong/pongutil.c:101
msgid "Unknown GConfValue type to compare"
msgstr "Ukjent GConfValue-type for sammenligning"

#: pong/pongutil.c:645 pong/pongutil.c:752
msgid "Invalid type"
msgstr "Ugyldig type"

#: pong/pongutil.c:936
msgid "Unsupported gconf value type to convert to string"
msgstr "Ust�ttet gconf-verditype for konvertering til streng"

#: pong/pongutil.c:998
msgid "Unsupported gconf value type to convert to text"
msgstr "Ust�ttet gconf-verditype for konvertering til tekst"

#: pong/pong-widget-interface.c:110
msgid "Widget specified no changed signal, trying \"changed\""
msgstr "Widget spesifiserte ikke-endret signal, pr�ver \"endret\""

#: pong/pong-widget-interface.c:136
msgid "get_value not implemented in widget"
msgstr "get_value ikke implementert i widget"

#: pong/pong-widget-interface.c:158
msgid "set_value not implemented in widget"
msgstr "set_value ikke implementert i widget"

#: pong/pong-widget-interface.c:193
msgid "add_options not implemented in widget"
msgstr "add_options ikke implementert i widget"

#: pong/pong-widget-interface.c:227
msgid "set_label not implemented in widget"
msgstr "set_label ikke implementert i widget"

#: pong/pong-widget-interface.c:266
msgid "set_arg not implemented in widget"
msgstr "set_arg ikke implementert i widget"

#: pong/pongwidgetutil.c:122
#, c-format
msgid "Can't set color to '%s'\n"
msgstr "Kan ikke sette farge til '%s'\n"

#: pong/pongwidgetutil.c:297
#, c-format
msgid "Cannot set object argument: %s"
msgstr "Kan ikke sette objektets argument: %s"

#: pong/pongwidgetutil.c:309 pong/pongwidgetutil.c:316
msgid "Invalid value being set"
msgstr "Ugyldig verdi satt"

#: pong/pongwidgetutil.c:362
msgid "Unsupported argument type being set"
msgstr "Ust�ttet argumenttype satt"

#: pong/pong-xml.gob:288 pong/pong-xml.gob:304
msgid "Could not construct object"
msgstr "Kunne ikke konstruere objekt"

#: pong/pong-xml.gob:402
#, c-format
msgid "Cannot load file '%s'"
msgstr "Kan ikke laste fil '%s'"

#: pong/pong-xml.gob:418
msgid "Cannot load from memory buffer"
msgstr "Kan ikke lese fra minnebuffer"

#: pong/pong-xml.gob:663
#, c-format
msgid "Cannot get any base path for user level %s"
msgstr "Kan ikke hente basissti for brukerniv� %s"

#: pong/pong-xml.gob:1076
msgid "Can't compare two values of different types"
msgstr "Kan ikke sammenligne to verdier med forskjellig type"

#: pong/pong-xml.gob:1101
msgid "Pairs or Lists can only be compared for equality or inequality"
msgstr "Par eller lister kan kun sammenlignes for likhet eller ulikhet"

#: pong/pong-xml.gob:1110
msgid "Invalid types in comparison"
msgstr "Ugyldige typer i sammenligning"

#: pong/pong-xml.gob:1175
#, c-format
msgid "Can't find widget named '%s'"
msgstr "Kan ikke finne widget med navn '%s'"

#: pong/pong-xml.gob:1180
msgid "Unsupported widget type"
msgstr "Ust�ttet widget-type"

#: pong-tool/pong-tool.c:34
msgid "Pong user level to use"
msgstr "Pong-brukerniv� som skal brukes"

#: pong-tool/pong-tool.c:34
msgid "LEVEL"
msgstr "NIV�"

#: pong-tool/pong-tool.c:35
msgid "Pong prefix to use"
msgstr "Pong-prefiks som skal brukes"

#: pong-tool/pong-tool.c:35
msgid "PREFIX"
msgstr "PREFIKS"

#: pong-edit/pong-gconf-schema-export.c:48 pong-tool/pong-tool.c:68
msgid "PonG init failed!"
msgstr "PonG-init feilet!"

#: pong-tool/pong-tool.c:70
msgid "PonG glade init failed!"
msgstr "PonG glade-initiering feilet!"

#: pong-tool/pong-tool.c:72
msgid "PonG bonobo init failed!"
msgstr "PonG bonobo-init feilet!"

#: pong-tool/pong-tool.c:80
msgid "You must supply a single argument with the .pong file to use\n"
msgstr "Du m� oppgi et enkelt argument med .pong-filen som skal brukes\n"

#: pong-tool/pong-tool.c:102
#, c-format
msgid "Can't load pong file: %s\n"
msgstr "Kan ikke laste pong-fil: %s\n"

#: pong-tool/pong-tool.c:117
msgid "Can't run dialog\n"
msgstr "Kan ikke kj�re dialog\n"

#: bonobo/pong-bonobo-widget.gob:384
msgid ""
"Unsupported bonobo property type, supported types are: string, long, short, "
"float, double and boolean"
msgstr "Bonobo egenskap-type som ikke er st�ttet. St�ttede typer er: string, long, short, float, double og boolean"

#: bonobo/pong-bonobo-widget.gob:526 bonobo/pong-bonobo-widget.gob:543
#: bonobo/pong-bonobo-widget.gob:552
msgid "Got the wrong type of value on getValue"
msgstr "Fikk ugyldig type verdi i getValue!"

#: bonobo/pong-bonobo-widget.gob:558
msgid "Usupported value type on getValue!"
msgstr "Ust�ttet type verdi satt p� getValue!"

#: bonobo/pong-bonobo-widget.gob:611
msgid "Got wrong type in pull_list"
msgstr "Fikk feil type i pull_list"

#: pong-edit/value-entry.gob:105 pong-edit/value-entry.gob:132
msgid "False"
msgstr "Usann"

#: pong-edit/value-entry.gob:130
msgid "True"
msgstr "Sann"

#: pong/pong-color-picker.gob:120 pong/pong-color-picker.gob:295
msgid "Unsupported type on Pong:Color:Picker"
msgstr "Ust�ttet type p� Pong:Color:Picker"

#: pong/pong-slider.gob:187
msgid "Unsupported type on Pong:Slider"
msgstr "Ust�ttet type p� Pong:Slider"

#: pong/pong-ui-plug-glade.gob:165
#, c-format
msgid "No widget specified for glade interface '%s'"
msgstr "Ingen widget spesifisert for glade-grensesnitt '%s'"

#: pong/pong-ui-plug-glade.gob:187
#, c-format
msgid "Cannot find glade file '%s'"
msgstr "Kan ikke finne glade-fil '%s'"

#: pong/pong-ui-plug-glade.gob:202
#, c-format
msgid "Cannot load glade file: %s (%s)"
msgstr "Kan ikke laste glade-fil: %s (%s)"

#: pong/pong-ui-plug-glade.gob:205
#, c-format
msgid "Cannot load glade file: %s"
msgstr "Kan ikke laste glade-fil: %s"

#: bonobo/pong-bonobo-entry.gob:323
msgid "Unsupported value type in get_pong_type!"
msgstr "Ust�ttet verdi-type i get_pong_type!"

#: bonobo/sample/GNOME_PonG_TestEntry.oaf.in.h:1
msgid "Pong test entry"
msgstr "Pong test-oppf�ring"

#: bonobo/sample/GNOME_PonG_TestEntry.oaf.in.h:2
msgid "Pong test entry factory"
msgstr "Pong factory for test-oppf�ring"

#: pong-edit/pong-gconf-schema-export.c:56
msgid ""
"You must supply two arguments, the .pong file to read and the .schemas file "
"to write."
msgstr ""
"Du m� oppgi to argumenter, .pong-filen som skal leses og .schema-filen som "
"skal skrives."

#: pong-edit/pong-gconf-schema-export.c:68
#, c-format
msgid "Cannot find %s"
msgstr "Kan ikke finne %s"

#: pong-edit/pong-gconf-schema-export.c:76
#, c-format
msgid "Cannot load %s"
msgstr "Kan ikke laste %s"

#: pong-edit/pong-gconf-schema-export.c:83
#, c-format
msgid "Cannot write %s"
msgstr "Kan ikke skrive %s"

#: pong/pong-list-entry.gob:93
msgid "Available"
msgstr "Tilgjengelig"

#: pong/pong-list-entry.gob:150
msgid "Selected"
msgstr "Velgt"

#: pong/pong-list-entry.gob:234 pong/pong-list-entry.gob:313
msgid "Unsupported type on Pong:List:Entry. Only list types are supported."
msgstr "Ust�ttet type p� Pong:List:Entry. Kun listetyper er st�ttet."

#: pong/pong-option-menu.gob:175 pong/pong-option-menu.gob:233
msgid "Unsupported type on Pong:Option:Menu"
msgstr "Ust�ttet type p� Pong:Option:Menu"

#: pong-edit/pong-edit.desktop.in.h:1
msgid "An editor for PonG files"
msgstr "Et redigeringsverkt�y for PonG-filer"

#: pong-edit/pong-edit.desktop.in.h:2
msgid "PonG Edit"
msgstr "PonG-redigering"

#~ msgid "Export Gettext Source"
#~ msgstr "Eksporter gettext kildekode"

#~ msgid "Export gettext strings as a .c sourcefile for use with gettext"
#~ msgstr ""
#~ "Eksporter gettext-strenger som en .c kildekodefil for bruk med gettext"

#~ msgid "Export gettext source..."
#~ msgstr "Eksporter gettext-kildekode..."

#~ msgid "Use gettext and not XML for i18n"
#~ msgstr "Bruk gettext og ikke XML for i18n"

#~ msgid "Gettext domain to use"
#~ msgstr "Gettext-domene som skal brukes"

#~ msgid "DOMAIN"
#~ msgstr "DOMENE"

#~ msgid "NOERROR"
#~ msgstr "INGENFEIL"

#~ msgid ""
#~ "Locale: %s\n"
#~ "Pong file: %s\n"
#~ "Translation file: %s\n"
#~ msgstr ""
#~ "Locale: %s\n"
#~ "Pong-fil: %s\n"
#~ "Oversettelsesfil: %s\n"

#~ msgid "Locale file exists, will merge.\n"
#~ msgstr "Locale-fil eksisterer, vil flette.\n"

#~ msgid "File %s exists, but is not a PonG Translation file, aborting"
#~ msgstr "Fil %s eksisterer, men er ikke en PonG-oversettelsesfil, avbryter"

#~ msgid "No backup made, filename %s contains a single quote"
#~ msgstr ""
#~ "Ingen sikkerhetskopi tatt, filnavn %s inneholder et enkelt sitattegn"

#~ msgid "Running: %s\n"
#~ msgstr "Kj�rer: %s\n"

#~ msgid "Failed to make backup!"
#~ msgstr "Kunne ikke lage sikkerhetskopi!"

#~ msgid "Can't load pong file: %s"
#~ msgstr "Kan ikke laste pong-fil: %s"

#~ msgid "Getting translations..."
#~ msgstr "Henter oversettelser..."

#~ msgid ""
#~ "\n"
#~ "Writing translations to %s..."
#~ msgstr ""
#~ "\n"
#~ "Skriver oversettelser til %s..."

#~ msgid ""
#~ "\n"
#~ "Done!\n"
#~ msgstr ""
#~ "\n"
#~ "Ferdig!\n"

#~ msgid "You must supply one argument, the .pong file to read."
#~ msgstr "Du m� oppgi ett enkelt argument, .pong-filen som skal leses."

#~ msgid "Unsupported property type"
#~ msgstr "Ust�ttet type egenskap"
