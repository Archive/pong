/* PonG: the Internal Bonobo Widget Changed Listener
 * Author: George Lebl
 * (c) 2001 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <gnome.h>
#include <bonobo.h>

#include "../pong/pong-i18n.h"

#include "../pong/pongutil.h"
#include "../pong/pong-strings.h"

#include "pong-interface.h"

#include "pong-bonobo-widget.h"
#include "pong-bonobo-widget-private.h" /* for protected methods */

#include "pong-changed-value-listener.h"

/*
 * Corba stuff
 */
typedef struct {
	POA_GNOME_PonG_ChangedValueListener servant;

	PongBonoboWidget *widget;
} impl_POA_GNOME_PonG_ChangedValueListener;

/*
 * Corba method prototypes
 */
static void
impl_GNOME_PonG_ChangedValueListener__destroy (GtkObject *obj,
					       impl_POA_GNOME_PonG_ChangedValueListener *servant);

GNOME_PonG_ChangedValueListener
impl_GNOME_PonG_ChangedValueListener__create (PongBonoboWidget *widget,
					      CORBA_Environment * ev);

static void
impl_GNOME_PonG_ChangedValueListener_changedValue (PortableServer_Servant _servant,
						   CORBA_Environment * ev);

/*
 * epv Initialization
 */
static PortableServer_ServantBase__epv impl_base_epv = {
	NULL,			/* _private data */
	NULL,			/* finalize routine */
	NULL			/* default_POA routine */
};
static POA_GNOME_PonG_ChangedValueListener__epv impl_epv = {
	NULL,			/* _private */
	impl_GNOME_PonG_ChangedValueListener_changedValue
};

static POA_GNOME_PonG_ChangedValueListener__vepv impl_vepv = {
	&impl_base_epv,
	&impl_epv
};

/*
 * Corba method implementations
 */
GNOME_PonG_ChangedValueListener
impl_GNOME_PonG_ChangedValueListener__create (PongBonoboWidget *widget,
					      CORBA_Environment * ev)
{
	GNOME_PonG_ChangedValueListener retval;
	impl_POA_GNOME_PonG_ChangedValueListener *servant;

	servant = g_new0 (impl_POA_GNOME_PonG_ChangedValueListener, 1);
	servant->servant.vepv = &impl_vepv;

	POA_GNOME_PonG_ChangedValueListener__init ((PortableServer_Servant) servant, ev);

	servant->widget = widget;

	CORBA_free (PortableServer_POA_activate_object (bonobo_poa (),
							servant, ev));
	if (BONOBO_EX (ev)) {
		POA_GNOME_PonG_ChangedValueListener__fini ((PortableServer_Servant) servant, ev);
		return CORBA_OBJECT_NIL;
	}


	retval = PortableServer_POA_servant_to_reference (bonobo_poa (),
							  servant, ev);
	if (BONOBO_EX (ev)) {
		impl_GNOME_PonG_ChangedValueListener__destroy (NULL,
							       servant);
		return CORBA_OBJECT_NIL;
	}


	gtk_signal_connect (GTK_OBJECT (widget), "destroy",
			    GTK_SIGNAL_FUNC (impl_GNOME_PonG_ChangedValueListener__destroy),
			    servant);

	return retval;
}

static void
impl_GNOME_PonG_ChangedValueListener__destroy (GtkObject *obj,
					       impl_POA_GNOME_PonG_ChangedValueListener *servant)
{
	PortableServer_ObjectId *objid;

	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	pong_bonobo_widget_whack_listener (servant->widget);
	servant->widget = NULL;

	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);

	POA_GNOME_PonG_ChangedValueListener__fini ((PortableServer_Servant) servant, &ev);

	g_free (servant);
	CORBA_exception_free (&ev);
}

static void
impl_GNOME_PonG_ChangedValueListener_changedValue (PortableServer_Servant _servant,
						   CORBA_Environment * ev)
{
	impl_POA_GNOME_PonG_ChangedValueListener *servant = 
		(impl_POA_GNOME_PonG_ChangedValueListener *)_servant;
	pong_bonobo_widget_changed (servant->widget);
}
