/* PonG: Bonobo support
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONG_BONOBO_H
#define PONG_BONOBO_H

#include <pong/pong.h>

#include <bonobo.h>

#include "pong-ui-plug-bonobo.h"
#include "pong-bonobo-entry.h"

#ifdef __cplusplus
extern "C" {
#endif

/* init bonobo support, all new PongXML's will be created
 * with bonobo support on by default after this, this does
 * not call bonobo_init, and you should init bonobo yourself */
gboolean	pong_bonobo_init		(void);

#ifdef __cplusplus
}
#endif

#endif /* PONG_BONOBO_H */
