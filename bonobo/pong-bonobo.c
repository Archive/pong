/* PonG: Bonobo support
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <bonobo.h>

#include "../pong/pong-i18n.h"

#include "../pong/pongutil.h"

#include "../pong/pong-xml.h"
#include "../pong/pong-ui-internal.h"

#include "pong-ui-plug-bonobo.h"

#include "pong-bonobo.h"

/**
 * pong_bonobo_init:
 *
 * Description:  Initializes pong and adds the bonobo plug as default.
 * You should call this if your pong files include bonobo controls.
 * You should also initialize bonobo in your main function then.
 *
 * Returns:  %TRUE on success or %FALSE on failiure
 **/
gboolean
pong_bonobo_init (void)
{
	if ( ! pong_init ())
		return FALSE;

	pong_add_plug_creator (pong_ui_plug_bonobo_new);

	return TRUE;
}
