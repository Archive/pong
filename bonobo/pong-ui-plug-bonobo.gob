%at{
/* PonG: the Internal Bonobo GOB UI object
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.99.2

%{
#include "config.h"
#include <gnome.h>
#include <bonobo.h>

#include "../pong/pong-i18n.h"

#include "../pong/pongpane.h"
#include "../pong/pongutil.h"
#include "../pong/pong-strings.h"

#include "../pong/pong-ui-plug.h"

#include "pong-bonobo-widget.h"

#include "pong-ui-plug-bonobo.h"
#include "pong-ui-plug-bonobo-private.h"

%}

class Pong:UI:Plug:Bonobo from Pong:UI:Plug {

	/* Note: Widgets are added to the gtk hash table in the panes
	 * so this is not neccessary, though I leave this in just
	 * for completeness. */
	/*
	override (Pong:UI:Plug)
	GtkWidget *
	get_widget (Pong:UI:Plug *ppp (check null type),
		    const char *name (check null))
		onerror NULL
	{
		Self *self = SELF (ppp);
		return NULL;
	}
	*/

	override (Pong:UI:Plug)
	gboolean
	supports (Pong:UI:Plug *ppp (check null type),
		  const char *plug_name (check null))
		onerror FALSE
	{
		if (pong_strcasecmp_no_locale (plug_name, PONG_S_Bonobo) == 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	override (Pong:UI:Plug)
	GtkWidget *
	make_widget (Pong:UI:Plug *ppp (check null type),
		     const char *name (check null),
		     const char *path (check null),
		     const char *specifier)
		onerror NULL
	{
		GtkWidget *widget;

		widget = pong_bonobo_widget_new (path);
		if (widget != NULL)
			gtk_widget_show (widget);

		return widget;
	}

	/**
	 * new:
	 *
	 * Description:  Makes a new bonobo PongUIPlug.  This is
	 * for use with the pong plug system.  Normally you'd
	 * just call #pong_bonobo_init, and not worry about this.
	 * You can use this if you have some really funky pong
	 * setup and don't want the bonobo plug to be set up
	 * always by default only on some ui objects.
	 *
	 * Returns:  New PongUIPlugBonobo object
	 **/
	public
	PongUIPlug *
	new (void)
	{
		return (PongUIPlug *) GET_NEW;
	}
}
