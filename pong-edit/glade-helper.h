/* Grapevine: GNOME Notifications
 *   Glade helper routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GLADE_HELPER_H
#define GLADE_HELPER_H

GladeXML *	glade_helper_load		(const char *file,
						 const char *widget,
						 GtkType expected_type,
						 gboolean dump_on_destroy);
GtkWidget *	glade_helper_load_widget	(const char *file,
						 const char *widget,
						 GtkType expected_type);

/* error checking get */
GtkWidget *	glade_helper_get 		(GladeXML *xml,
						 const char *name,
						 GtkType expected_type);
GtkWidget *	glade_helper_get_clist 		(GladeXML *xml,
						 const char *name,
						 GtkType expected_type,
						 int expected_columns);

#endif /* GLADE_HELPER_H */
