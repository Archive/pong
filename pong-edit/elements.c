/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000,2001 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "tree.h"
#include "util.h"
#include "pong-edit.h"
#include "glade-helper.h"
#include "value-entry.h"
#include "panes.h"
#include "element-sensitivities.h"

#include "elements.h"

static XMLTag *selected_element = NULL;

static gboolean widget_set = FALSE;
static gboolean ignore_widget_change = FALSE;
static gboolean owner_set = FALSE;
static gboolean ignore_owner_change = FALSE;

void change_widget		(GtkWidget *entry);
void change_specifier		(GtkWidget *entry);
void change_conf_path		(GtkWidget *entry);
void change_default		(GtkWidget *entry);

void change_owner		(GtkWidget *entry);
void change_short_desc		(GtkWidget *entry);
void change_long_desc		(GtkWidget *entry);

void focus_out_widget		(GtkWidget *entry,
				 GdkEvent *event);
void focus_out_owner		(GtkWidget *entry,
				 GdkEvent *event);

void elements_clist_select_row	(GtkCList *clist,
				 int row, int column,
				 GdkEvent *event);
void elements_clist_unselect_row(GtkCList *clist,
				 int row, int column,
				 GdkEvent *event);

void element_add		(GtkWidget *w);
void element_remove		(GtkWidget *w);

static GList *per_list_connections = NULL;

static struct {
	char *name;
	GConfValueType type;
} type_map[] = {
	{"Int", GCONF_VALUE_INT},
	{"Float", GCONF_VALUE_FLOAT},
	{"String", GCONF_VALUE_STRING},
	{"Bool", GCONF_VALUE_BOOL},
	{"List", GCONF_VALUE_LIST},
	{"Pair", GCONF_VALUE_PAIR},
	{NULL, 0}
};
#define NUM_OF_BASE_TYPES 4
#define NUM_OF_TYPES 6

static const char *
get_type_name (GConfValueType type)
{
	int i;
	for (i = 0; type_map[i].name != NULL; i++) {
		if (type_map[i].type == type)
			return type_map[i].name;
	}
	return NULL;
}

static char *
get_full_type_name (PongType type)
{
	GConfValueType basic_type, list_type, car_type, cdr_type;

	basic_type = pong_gconf_value_type_from_pong_type (type);
	list_type = pong_list_gconf_value_type_from_pong_type (type);
	car_type = pong_car_gconf_value_type_from_pong_type (type);
	cdr_type = pong_cdr_gconf_value_type_from_pong_type (type);

	if (basic_type == GCONF_VALUE_LIST) {
		return g_strdup_printf ("List (%s)",
					get_type_name (list_type));
	} else if (basic_type == GCONF_VALUE_PAIR) {
		return g_strdup_printf ("Pair (%s, %s)",
					get_type_name (car_type),
					get_type_name (cdr_type));
	} else {
		return g_strdup (get_type_name (basic_type));
	}

}

static int
get_type_index (GConfValueType type)
{
	int i;
	for (i = 0; type_map[i].name != NULL; i++) {
		if (type_map[i].type == type)
			return i;
	}
	return -1;
}

static void
element_type_updated (XMLTag *tag,
		      const char *key,
		      const char *text,
		      gpointer data)
{
	int the_row;

	the_row = pong_edit_find_tag_row (GTK_CLIST (elements_clist), tag);

	if (the_row >= 0) {
		const char *foo;
		foo = get_full_type_name (tree_element_get_pong_type (tag));
		gtk_clist_set_text (GTK_CLIST (elements_clist),
				    the_row, 0, foo);
	}
}

static void
element_things_updated (XMLTag *tag,
			const char *key,
			const char *text,
			gpointer data)
{
	int the_row;

	the_row = pong_edit_find_tag_row (GTK_CLIST (elements_clist), tag);

	if (the_row >= 0) {
		char *text_one;
		char *specifier, *widget, *conf_path;

		specifier = tag_get_text (tag, PONG_S_Specifier, NULL);
		widget = tag_get_text (tag, PONG_S_Widget, NULL);
		conf_path = tag_get_text (tag, PONG_S_ConfPath, NULL);

		if ( ! pong_string_empty (specifier) &&
		     ! pong_string_empty (widget))
			text_one = g_strdup_printf ("%s (%s)", widget, specifier);
		else if ( ! pong_string_empty (specifier) &&
			  pong_string_empty (widget))
			text_one = g_strdup_printf ("[%s] (%s)", conf_path, specifier);
		else if ( ! pong_string_empty (widget))
			text_one = g_strdup (widget);
		else
			text_one = g_strdup_printf ("[%s]", conf_path);

		gtk_clist_set_text (GTK_CLIST (elements_clist),
				    the_row, 1, text_one);
		gtk_clist_set_text (GTK_CLIST (elements_clist),
				    the_row, 2, conf_path);

		g_free (specifier);
		g_free (widget);
		g_free (conf_path);
		g_free (text_one);
	}
}

static void
element_killed (XMLTag *tag, gpointer data)
{
	int the_row;

	the_row = pong_edit_find_tag_row (GTK_CLIST (elements_clist), tag);

	if (the_row >= 0) {
		gtk_clist_remove (GTK_CLIST (elements_clist), the_row);
	}
}

static int
add_element_to_clist (XMLTag *element)
{
	int row;
	char *text[3];
	char *specifier, *widget, *conf_path;
	int con_id;

	text[0] = get_full_type_name (tree_element_get_pong_type (element));

	specifier = tag_get_text (element, PONG_S_Specifier, NULL);
	widget = tag_get_text (element, PONG_S_Widget, NULL);
	conf_path = tag_get_text (element, PONG_S_ConfPath, NULL);

	if ( ! pong_string_empty (specifier) &&
	     ! pong_string_empty (widget))
		text[1] = g_strdup_printf ("%s (%s)", widget, specifier);
	else if ( ! pong_string_empty (specifier) &&
		 pong_string_empty (widget))
		text[1] = g_strdup_printf ("[%s] (%s)", conf_path, specifier);
	else if ( ! pong_string_empty (widget))
		text[1] = g_strdup (widget);
	else
		text[1] = g_strdup_printf ("[%s]", conf_path);
	text[2] = conf_path;

	row = gtk_clist_append (GTK_CLIST (elements_clist), text);

	g_free (text[0]);
	g_free (text[1]);
	g_free (specifier);
	g_free (widget);
	g_free (conf_path);

	gtk_clist_set_row_data_full (GTK_CLIST (elements_clist), row,
				     tag_ref (element),
				     (GtkDestroyNotify)tag_unref);

	con_id = tag_connect_text (element, PONG_S_Type,
				   element_type_updated, NULL, NULL);
	connection_add (&per_list_connections, element, con_id);
	con_id = tag_connect_text (element, PONG_S_Specifier,
				   element_things_updated, NULL, NULL);
	connection_add (&per_list_connections, element, con_id);
	con_id = tag_connect_text (element, PONG_S_Widget,
				   element_things_updated, NULL, NULL);
	connection_add (&per_list_connections, element, con_id);
	con_id = tag_connect_text (element, PONG_S_ConfPath,
				   element_things_updated, NULL, NULL);
	connection_add (&per_list_connections, element, con_id);
	con_id = tag_connect_kill (element, element_killed, NULL, NULL);
	connection_add (&per_list_connections, element, con_id);

	return row;
}

static void
element_added (XMLTag *root, XMLTag *reference, gpointer data)
{
	const char *name = tag_peek_type (reference);

	if (name != NULL &&
	    pong_strcasecmp_no_locale (name, PONG_S_Element) == 0) {
		add_element_to_clist (reference);

		setup_pane_name_combo ();
	}
}

void
setup_elements_list (XMLTag *root)
{
	GList *list, *li;
	int con_id;

	gtk_clist_freeze (GTK_CLIST (elements_clist));

	list = tree_get_elements (xml_tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		add_element_to_clist (tag);
	}

	tree_free_tag_list (list);

	gtk_clist_thaw (GTK_CLIST (elements_clist));

	con_id = tag_connect_add (root, element_added, NULL, NULL);
	connection_add (&per_list_connections, root, con_id);

	setup_pane_name_combo ();
}

void
clear_elements_list (void)
{
	connection_disconnect_all (&per_list_connections);

	gtk_clist_unselect_all (GTK_CLIST (elements_clist));
	gtk_clist_clear (GTK_CLIST (elements_clist));
}

static void
change_type (GtkWidget *item, gpointer data)
{
	GtkWidget *def_entry;
	GtkWidget *option_menu;
	GtkWidget *list_option_menu;
	GtkWidget *car_option_menu;
	GtkWidget *cdr_option_menu;
	GConfValueType basic_type, list_type, car_type, cdr_type;
	PongType type;
	int i;

	if (selected_element == NULL)
		return;

	g_assert (app_xml != NULL);

	option_menu = glade_helper_get (app_xml, "basic_type",
					GTK_TYPE_OPTION_MENU);
	list_option_menu = glade_helper_get (app_xml, "list_type",
					     GTK_TYPE_OPTION_MENU);
	car_option_menu = glade_helper_get (app_xml, "paircar_type",
					    GTK_TYPE_OPTION_MENU);
	cdr_option_menu = glade_helper_get (app_xml, "paircdr_type",
					    GTK_TYPE_OPTION_MENU);

	i = pong_gtk_option_menu_get_history (GTK_OPTION_MENU (option_menu));
	basic_type = type_map[i].type;
	i = pong_gtk_option_menu_get_history (GTK_OPTION_MENU (list_option_menu));
	list_type = type_map[i].type;
	i = pong_gtk_option_menu_get_history (GTK_OPTION_MENU (car_option_menu));
	car_type = type_map[i].type;
	i = pong_gtk_option_menu_get_history (GTK_OPTION_MENU (cdr_option_menu));
	cdr_type = type_map[i].type;

	if (basic_type == GCONF_VALUE_LIST) {
		gtk_widget_set_sensitive (list_option_menu, TRUE);
	} else {
		gtk_widget_set_sensitive (list_option_menu, FALSE);
	}

	if (basic_type == GCONF_VALUE_PAIR) {
		gtk_widget_set_sensitive (car_option_menu, TRUE);
		gtk_widget_set_sensitive (cdr_option_menu, TRUE);
	} else {
		gtk_widget_set_sensitive (car_option_menu, FALSE);
		gtk_widget_set_sensitive (cdr_option_menu, FALSE);
	}

	type = pong_type_from_gconf_types (basic_type, list_type, car_type, cdr_type);

	tree_element_set_pong_type (selected_element, type);

	def_entry = glade_helper_get (app_xml, "basic_default",
				      TYPE_VALUE_ENTRY);
	value_entry_set_type (VALUE_ENTRY (def_entry), type);
}

static void
set_stripped_element_text (GtkWidget *entry, const char *key)
{
	char *text;

	if (selected_element == NULL)
		return;

	if (IS_VALUE_ENTRY (entry))
		text = value_entry_get_text (VALUE_ENTRY (entry));
	else
		text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);

	if (text != NULL)
		g_strstrip (text);

	if (pong_string_empty (text))
		tag_set_text (selected_element, key, NULL, NULL);
	else
		tag_set_text (selected_element, key, text, NULL);

	g_free (text);
}

void
change_widget (GtkWidget *entry)
{
	char *text;

	if (ignore_widget_change)
		return;

	text = gtk_entry_get_text (GTK_ENTRY (entry));

	if (pong_string_empty (text))
		widget_set = FALSE;
	else
		widget_set = TRUE;

	set_stripped_element_text (entry, PONG_S_Widget);

	setup_pane_name_combo ();
}

void
change_specifier (GtkWidget *entry)
{
	set_stripped_element_text (entry, PONG_S_Specifier);
}

void
change_default (GtkWidget *entry)
{
	set_stripped_element_text (entry, PONG_S_Default);
}

void
change_conf_path (GtkWidget *entry)
{
	gboolean success;
	char *text;

	if (selected_element == NULL)
		return;

	text = gtk_entry_get_text (GTK_ENTRY (entry));

	success = tree_element_set_conf_path (xml_tree, selected_element, text);

	pong_edit_set_red (entry, success ? FALSE : TRUE);

	if ( ! widget_set) {
		GtkWidget *w;

		ignore_widget_change = TRUE;

		w = glade_helper_get (app_xml, "basic_widget", GTK_TYPE_ENTRY);
		gtk_entry_set_text (GTK_ENTRY (w), text);

		ignore_widget_change = FALSE;
	}

	setup_pane_name_combo ();
}

void
change_owner (GtkWidget *entry)
{
	char *text;

	if (ignore_owner_change)
		return;

	text = gtk_entry_get_text (GTK_ENTRY (entry));

	if (pong_string_empty (text))
		owner_set = FALSE;
	else
		owner_set = TRUE;

	set_stripped_element_text (entry, PONG_S_SchemaOwner);
}

void
change_short_desc (GtkWidget *entry)
{
	set_stripped_element_text (entry, "_" PONG_S_SchemaShortDescription);
}

void
change_long_desc (GtkWidget *entry)
{
	set_stripped_element_text (entry, "_" PONG_S_SchemaLongDescription);
}

static GtkWidget *
do_type_option_menu (GConfValueType type, const char *widget)
{
	GtkWidget *w, *item;
	int i;

	w = glade_helper_get (app_xml, widget, GTK_TYPE_OPTION_MENU);
	i = get_type_index (type);
	gtk_option_menu_set_history (GTK_OPTION_MENU (w), i);
	item = pong_gtk_option_menu_get_item (GTK_OPTION_MENU (w), i);
	if (item != NULL)
		gtk_menu_item_activate (GTK_MENU_ITEM (item));

	return w;
}

static gboolean
setup_type (PongType pong_type)
{
	GtkWidget *w;
	GConfValueType basic_type;
	GConfValueType type;

	/* The type */
	basic_type = pong_gconf_value_type_from_pong_type (pong_type);
	w = do_type_option_menu (basic_type, "basic_type");
	if (w == NULL)
		return FALSE;

	/* The list type */
	type = pong_list_gconf_value_type_from_pong_type (pong_type);
	w = do_type_option_menu (type, "list_type");
	if (w == NULL)
		return FALSE;
	if (basic_type != GCONF_VALUE_LIST) {
		gtk_widget_set_sensitive (w, FALSE);
	}

	/* The pair car type */
	type = pong_car_gconf_value_type_from_pong_type (pong_type);
	w = do_type_option_menu (type, "paircar_type");
	if (w == NULL)
		return FALSE;
	if (basic_type != GCONF_VALUE_PAIR) {
		gtk_widget_set_sensitive (w, FALSE);
	}
	/* The pair cdr type */
	type = pong_cdr_gconf_value_type_from_pong_type (pong_type);
	w = do_type_option_menu (type, "paircdr_type");
	if (w == NULL)
		return FALSE;
	if (basic_type != GCONF_VALUE_PAIR) {
		gtk_widget_set_sensitive (w, FALSE);
	}

	return TRUE;
}

static void
setup_element_edit_area (XMLTag *element)
{
	GtkWidget *w;
	PongType pong_type;
	char *text;

	pong_type = tree_element_get_pong_type (element);

	/* type */
	if ( ! setup_type (pong_type))
		return;

	/* The widget name */
	w = glade_helper_get (app_xml, "basic_widget", GTK_TYPE_ENTRY);
	text = tag_get_text (element, PONG_S_Widget, NULL);
	if (pong_string_empty (text)) {
		g_free (text);
		text = tag_get_text (element, PONG_S_ConfPath, NULL);
		gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
		widget_set = FALSE;
	} else {
		gtk_entry_set_text (GTK_ENTRY (w), text);
		widget_set = TRUE;
	}
	g_free (text);

	/* The widget specifier name */
	w = glade_helper_get (app_xml, "basic_specifier", GTK_TYPE_ENTRY);
	text = tag_get_text (element, PONG_S_Specifier, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The conf path */
	w = glade_helper_get (app_xml, "basic_conf_path", GTK_TYPE_ENTRY);
	text = tag_get_text (element, PONG_S_ConfPath, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The default */
	w = glade_helper_get (app_xml, "basic_default", TYPE_VALUE_ENTRY);
	text = tag_get_text (element, PONG_S_Default, NULL);
	value_entry_set_type (VALUE_ENTRY (w), PONG_TYPE_STRING);
	value_entry_set_text (VALUE_ENTRY (w), pong_sure_string (text));
	g_free (text);
	/* we set the type afterwards so that if the above triggered a change
	 * in database it was a text one and thus non-destructive one */
	value_entry_set_type (VALUE_ENTRY (w), pong_type);

	/* Schema stuff */
	/* The owner */
	w = glade_helper_get (app_xml, "owner_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (element, PONG_S_SchemaOwner, NULL);
	g_free (text);
	if (pong_string_empty (text)) {
		XMLTag *root;
		root = tree_root (xml_tree);

		g_free (text);
		text = tag_get_text (root, PONG_S_SchemaOwner, NULL);
		gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
		owner_set = FALSE;

		tag_unref (root);
	} else {
		gtk_entry_set_text (GTK_ENTRY (w), text);
		owner_set = TRUE;
	}
	g_free (text);

	/* the short desc */
	w = glade_helper_get (app_xml, "short_desc_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (element, PONG_S_SchemaShortDescription, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* the short desc */
	w = glade_helper_get (app_xml, "long_desc_text", GTK_TYPE_TEXT);
	text = tag_get_text (element, PONG_S_SchemaLongDescription, NULL);
	gtk_editable_delete_text (GTK_EDITABLE (w), 0, -1);
	gtk_text_insert (GTK_TEXT (w),
			 NULL /*font*/,
			 NULL /*fore*/,
			 NULL /*back*/,
			 pong_sure_string (text),
			 -1 /*length*/);
	g_free (text);

	/* Sensitivities */
	setup_element_sensitivities_list (element);
}

static void
clear_element_edit_area (void)
{
	GtkWidget *w;

	/* type */
	if ( ! setup_type (PONG_TYPE_INT))
		return;

	/* The widget name */
	w = glade_helper_get (app_xml, "basic_widget", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The widget specifier name */
	w = glade_helper_get (app_xml, "basic_specifier", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The conf path */
	w = glade_helper_get (app_xml, "basic_conf_path", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The default */
	w = glade_helper_get (app_xml, "basic_default", TYPE_VALUE_ENTRY);
	value_entry_set_type (VALUE_ENTRY (w), PONG_TYPE_STRING);
	value_entry_set_text (VALUE_ENTRY (w), "");

	/* Schema stuff */
	/* The owner */
	w = glade_helper_get (app_xml, "owner_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* the short desc */
	w = glade_helper_get (app_xml, "short_desc_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* the short desc */
	w = glade_helper_get (app_xml, "long_desc_text", GTK_TYPE_TEXT);
	gtk_editable_delete_text (GTK_EDITABLE (w), 0, -1);

	/* Sensitivities */
	clear_element_sensitivities_list ();
}

static GtkWidget *
signals_connect_type_option_menu (const char *widget)
{
	GtkWidget *w, *item;
	int i;

	w = glade_helper_get (app_xml, widget, GTK_TYPE_OPTION_MENU);

	/* setup the type changing to work */
	for(i = 0; i < NUM_OF_TYPES; i++) {
		item = pong_gtk_option_menu_get_item (GTK_OPTION_MENU (w), i);
		if (item == NULL)
			break;
		gtk_signal_connect_after (GTK_OBJECT (item), "activate",
					  GTK_SIGNAL_FUNC (change_type),
					  NULL);
	}

	return w;
}

void
element_add (GtkWidget *w)
{
	XMLTag *tag;

	if (xml_tree != NULL)
		tag = tree_add_element (xml_tree);
	else
		tag = NULL;

	clist_select_thing (GTK_CLIST (elements_clist), tag);

	tag_unref (tag);
}

void
element_remove (GtkWidget *w)
{
	if (selected_element != NULL)
		tag_kill (selected_element);
}

void
signals_connect_element_edit_area (void)
{
	GtkWidget *w;

	/* The type */
	w = signals_connect_type_option_menu ("basic_type");
	if (w == NULL)
		return;

	/* The list type */
	w = signals_connect_type_option_menu ("list_type");
	if (w == NULL)
		return;

	/* The pair car type */
	w = signals_connect_type_option_menu ("paircar_type");
	if (w == NULL)
		return;

	/* The pair cdr type */
	w = signals_connect_type_option_menu ("paircdr_type");
	if (w == NULL)
		return;
}

void
elements_clist_select_row (GtkCList *clist, int row, int column,
			   GdkEvent *event)
{
	GtkWidget *w;
	XMLTag *tag = gtk_clist_get_row_data (clist, row);

	selected_element = NULL;

	w = glade_helper_get (app_xml, "key_property_notebook",
			      GTK_TYPE_NOTEBOOK);
	gtk_widget_set_sensitive (w, TRUE);

	setup_element_edit_area (tag);
	selected_element = tag;
}

void
elements_clist_unselect_row (GtkCList *clist, int row, int column,
			     GdkEvent *event)
{
	GtkWidget *w;

	selected_element = NULL;
	clear_element_edit_area ();

	w = glade_helper_get (app_xml, "key_property_notebook",
			      GTK_TYPE_NOTEBOOK);
	gtk_widget_set_sensitive (w, FALSE);
}

void
default_owner_changed (const char *owner)
{
	GtkWidget *entry;

	if (selected_element == NULL ||
	    owner_set)
		return;

	ignore_owner_change = TRUE;
	entry = glade_helper_get (app_xml, "owner_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (entry), pong_sure_string (owner));
	ignore_owner_change = FALSE;
}

void
focus_out_widget (GtkWidget *entry, GdkEvent *event)
{
	char *text;
	char *entry_text;

	if (selected_element == NULL)
		return;

	entry_text = gtk_entry_get_text (GTK_ENTRY (entry));

	if ( ! pong_string_empty (entry_text))
		return;

	text = tag_get_text (selected_element, PONG_S_ConfPath, NULL);

	ignore_widget_change = TRUE;
	gtk_entry_set_text (GTK_ENTRY (entry), pong_sure_string (text));
	ignore_widget_change = FALSE;

	g_free (text);
}

void
focus_out_owner (GtkWidget *entry, GdkEvent *event)
{
	XMLTag *root;
	char *text;
	char *entry_text;

	if (selected_element == NULL)
		return;

	entry_text = gtk_entry_get_text (GTK_ENTRY (entry));

	if ( ! pong_string_empty (entry_text))
		return;

	root = tree_root (xml_tree);
	text = tag_get_text (root, PONG_S_SchemaOwner, NULL);

	ignore_owner_change = TRUE;
	gtk_entry_set_text (GTK_ENTRY (entry), pong_sure_string (text));
	ignore_owner_change = FALSE;

	tag_unref (root);
	g_free (text);
}
