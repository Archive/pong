/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef UTIL_H
#define UTIL_H

void	pong_edit_set_red		(GtkWidget *w,
					 gboolean state);
int	pong_edit_find_tag_row		(GtkCList *clist,
					 XMLTag *the_tag);

void	connection_add			(GList **list,
					 XMLTag *tag,
					 int connection);
void	connection_disconnect_all	(GList **list);

GtkWidget * pong_gtk_option_menu_get_item (GtkOptionMenu *option_menu,
					   int index);
int	pong_gtk_option_menu_get_history (GtkOptionMenu *option_menu);

gboolean is_string_in_list		(const GList *list,
					 const char *string);
gboolean is_string_in_list_case_no_locale (const GList *list,
					   const char *string);

GtkCTreeNode * find_next_in_ctree	(GtkCTree *ctree,
					 XMLTag *tag);
int	find_next_in_clist		(GtkCList *clist,
					 XMLTag *tag);

void	ctree_select_thing		(GtkCTree *ctree,
					 XMLTag *tag);
void	clist_select_thing		(GtkCList *clist,
					 XMLTag *tag);

#endif /* UTIL_H */
