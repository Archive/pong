/* Pong
 *   Pong XML tree traversal
 * Author: George Lebl
 * (c) 2000,2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <string.h>
#include <gnome.h>
#include <stdlib.h>
#include <gconf/gconf.h>
#include <gconf/gconf-value.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/xmlmemory.h>

#include "../pong/pongutil.h"
#include "../pong/pongelement.h"
#include "../pong/pong-type.h"
#include "../pong/pong-strings.h"

#include "tree.h"
#include "util.h"

struct _XMLTree {
	int ref_count;

	GHashTable *tags;

	xmlDoc *doc;

	gboolean *changed;
};

struct _XMLTag {
	int ref_count;

	XMLTree *tree;

	xmlNode *node;

	GList *signals;
};

/*EEEEEEK FIXME: these functions are from gconf-internals, but I really need
 * to either make havoc export them or I have to make the pong ones accept
 * gconf like strings (which would be a good idea anyway) */
GConfValue* gconf_value_new_from_string (GConfValueType type,
					 const gchar* value_str,
					 GError** err);
GConfValue* gconf_value_new_pair_from_string (GConfValueType car_type,
					      GConfValueType cdr_type,
					      const gchar* str,
					      GError** err);
GConfValue* gconf_value_new_list_from_string (GConfValueType list_type,
					      const gchar* str,
					      GError** err);

/* Utilities */
static GList * find_value (GList *list, const char *value);
static char * unique_name (XMLTree *tree);
static char * unique_level_name (XMLTree *tree);
static char * unique_conf_path_name (XMLTree *tree);
static char * unique_argument_name (XMLTag *widget);

/*
 * Signals
 */

enum {
	SIGNAL_KILL,
	SIGNAL_TEXT,
	SIGNAL_MOVE_UP,
	SIGNAL_MOVE_DOWN,
	SIGNAL_ADD,
	SIGNAL_PREPEND,
	SIGNAL_APPEND,
	SIGNAL_KILL_CHILD
};

typedef struct {
	int id;
	int signum;
	union {
		XMLTagNotify notify;
		XMLTagRefNotify ref_notify;
		XMLTagTextNotify text_notify;
	} sfunc;
	gpointer data;
	XMLDestroyNotify destroy_notify;

	char *key;
} XMLSignal;

static int signal_id = 1;

static XMLSignal * signal_new (XMLTag *tag, int signum,
			       gpointer data, XMLDestroyNotify destroy_notify);
static void signal_free (XMLSignal *signal);
static void signal_emit (XMLTag *tag, int signum);
static void signal_emit_ref (XMLTag *tag, int signum, XMLTag *reference);
static void signal_emit_text (XMLTag *tag, int signum,
			      const char *key, const char *text);

/* Other funcs */
static XMLTag *tag_from_node (XMLTree *tree, xmlNode *node);
static XMLTag *tag_from_node_no_new (XMLTree *tree, xmlNode *node);

enum {
	MOVE_UP,
	MOVE_DOWN
};

static void move_until_list (XMLTag *tag, GList *strings, int direction);
static void move_until (XMLTag *tag, const char *string, int direction);

static XMLTag * tree_find_by_name_from_group (XMLTree *tree, XMLTag *group, const char *name);
static XMLTag * tree_find_by_name_from_pane (XMLTree *tree, XMLTag *pane, const char *name);

XMLTree *
tree_load (const char *file, const char *toplevel)
{
	xmlDoc *doc;
	XMLTree *tree;

	g_return_val_if_fail (file != NULL, NULL);

	doc = xmlParseFile (file);

	if (doc == NULL)
		return NULL;

	if (doc->root == NULL ||
	    doc->root->name == NULL ||
	    pong_strcasecmp_no_locale (doc->root->name,
				       toplevel) != 0) {
		xmlFreeDoc (doc);
		return NULL;
	}

	tree = g_new0 (XMLTree, 1);
	tree->ref_count = 1;
	tree->doc = doc;
	tree->tags = g_hash_table_new (NULL, NULL);

	return tree;
}

XMLTree *
tree_new (const char *toplevel)
{
	xmlDoc *doc;
	XMLTree *tree;

	doc = xmlNewDoc ("1.0");

	doc->root = xmlNewDocNode (doc, NULL, toplevel, NULL);

	tree = g_new0 (XMLTree, 1);
	tree->ref_count = 1;
	tree->doc = doc;
	tree->tags = g_hash_table_new (NULL, NULL);

	return tree;
}

void
tree_set_changed_var (XMLTree *tree, gboolean *changed)
{
	g_return_if_fail (tree != NULL);

	tree->changed = changed;
}

/* handles null */
static void
tree_changed (XMLTree *tree)
{
	if (tree != NULL &&
	    tree->changed != NULL)
		*(tree->changed) = TRUE;
}

static void
disconnect_signals (gpointer key, gpointer data, gpointer user_data)
{
	XMLTag *tag = data;
	GList *list = tag->signals;

	tag->signals = NULL;

	g_list_foreach (list, (GFunc)signal_free, NULL);
	g_list_free (list);
}

/* when silent is on, then all signals are disconnected first and
 * changed var is set to NULL */
void
tree_kill (XMLTree *tree, gboolean silent)
{
	XMLTag *root;

	g_return_if_fail (tree != NULL);

	root = tree_root (tree);

	if (silent) {
		tree_set_changed_var (tree, NULL);
		/* kill all signals first */
		g_hash_table_foreach (tree->tags, disconnect_signals, NULL);
	}

	tag_kill (root);

	tag_unref (root);
}

XMLTree *
tree_ref (XMLTree *tree)
{
	g_return_val_if_fail (tree != NULL, NULL);

	tree->ref_count++;

	return tree;
}

static void
free_tag_hash (gpointer key, gpointer value, gpointer user_data)
{
	XMLTag *tag = value;

	tag_unref (tag);
}

void
tree_unref (XMLTree *tree)
{
	g_return_if_fail (tree != NULL);

	tree->ref_count--;

	if (tree->ref_count == 0) {
		g_hash_table_foreach (tree->tags, free_tag_hash, NULL);
		g_hash_table_destroy (tree->tags);	
		tree->tags = NULL;	

		xmlFreeDoc (tree->doc);
		tree->doc = NULL;

		g_free (tree);
	}
}

void
tree_get_xml_memory (XMLTree *tree, char **memory, int *size)
{
	g_return_if_fail (tree != NULL);
	g_return_if_fail (memory != NULL);
	g_return_if_fail (size != NULL);

	/* sanity */
	*memory = NULL;
	*size = 0;

	xmlDocDumpMemory (tree->doc, (xmlChar **)memory, size);
}

void
tree_free_xml_memory (char *memory)
{
	if (memory != NULL)
		xmlFree (memory);
}

gboolean
tree_write (XMLTree *tree, const char *file)
{
	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (file != NULL, FALSE);

	return xmlSaveFile (file, tree->doc) >= 0;
}

static XMLTag *
tag_from_node (XMLTree *tree, xmlNode *node)
{
	XMLTag *tag = NULL;

	g_return_val_if_fail (tree != NULL, NULL);

	if (node != NULL) {
		tag = g_hash_table_lookup (tree->tags, node);

		if (tag == NULL) {
			tag = g_new0 (XMLTag, 1);
			tag->ref_count = 1;
			tag->node = node;
			tag->signals = NULL;
			tag->tree = tree;

			g_hash_table_insert (tree->tags, node, tag);
		}

		tag_ref (tag);
	}

	return tag;
}

static XMLTag *
tag_from_node_no_new (XMLTree *tree, xmlNode *node)
{
	XMLTag *tag = NULL;

	g_return_val_if_fail (tree != NULL, NULL);

	if (node != NULL) {
		tag = g_hash_table_lookup (tree->tags, node);

		tag_ref (tag);
	}

	return tag;
}

XMLTag *
tree_root (XMLTree *tree)
{
	g_return_val_if_fail (tree != NULL, NULL);

	return tag_from_node (tree, xmlDocGetRootElement (tree->doc));
}

XMLTag *
tag_ref (XMLTag *tag)
{
	if (tag != NULL)
		tag->ref_count++;

	return tag;
}

void
tag_unref (XMLTag *tag)
{
	if (tag != NULL) {
		tag->ref_count--;

		if (tag->ref_count == 0) {
			GList *list = tag->signals;

			tag->node = NULL;
			tag->signals = NULL;

			g_list_foreach (list, (GFunc)signal_free, NULL);
			g_list_free (list);

			tag->tree = NULL;

			g_free (tag);
		}
	}
}

const char *
tag_peek_type (XMLTag *tag)
{
	if (tag != NULL)
		return tag->node->name;
	else
		return NULL;
}

GList *
tag_get_tags (XMLTag *parent, const char *type)
{
	xmlNode *iter;
	GList *list = NULL;

	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (type != NULL, NULL);

	if (parent->node == NULL ||
	    parent->node->type != XML_ELEMENT_NODE)
		return NULL;

	for (iter = parent->node->childs;
	     iter != NULL;
	     iter = iter->next) {
		if (iter->name != NULL &&
		    pong_tags_equal (type, iter->name))
			list = g_list_prepend
				(list, tag_from_node (parent->tree, iter));
	}

	return g_list_reverse (list);
}

GList *
tag_get_tags_multiple (XMLTag *parent, const GList *types)
{
	xmlNode *iter;
	GList *list = NULL;

	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (types != NULL, NULL);

	if (parent->node == NULL ||
	    parent->node->type != XML_ELEMENT_NODE)
		return NULL;

	for (iter = parent->node->childs;
	     iter != NULL;
	     iter = iter->next) {
		if (iter->name != NULL &&
		    is_string_in_list_case_no_locale (types, iter->name))
			list = g_list_prepend
				(list, tag_from_node (parent->tree, iter));
	}

	return g_list_reverse (list);
}

static void
recursive_kill_children (XMLTree *tree, xmlNode *node)
{
	/* keep killing children until they're all gone */
	while (node->childs != NULL) {
		xmlNode *child = node->childs;
		XMLTag *tag = tag_from_node_no_new (tree, child);

		if (tag != NULL) {
			/* this recursively kills those children */
			tag_kill (tag);

			tag_unref (tag);
		} else {
			recursive_kill_children (tree, child);

			/* XXX: work around an apparent libxml bug,
			 * sent a patch to Daniel */
			if (child->parent == NULL)
				child->parent = node;

			xmlUnlinkNode (child);
			xmlFreeNode (child);

			/* XXX: work around an apparent libxml bug,
			 * sent a patch to Daniel */
			if (child == tree->doc->root)
				tree->doc->root = NULL;
		}
	}
}

/* Doesn't unref, just kills it from the document,
 * though that may end up unreffing */
void
tag_kill (XMLTag *tag)
{
	GList *list;
	XMLTree *tree;
	xmlNode *node;
	XMLTag *parent;

	g_return_if_fail (tag != NULL);

	if (tag->node == NULL ||
	    tag->tree == NULL)
		return;

	tag_ref (tag);

	tree = tag->tree;
	node = tag->node;
	if (node->parent)
		parent = tag_from_node_no_new (tree, node->parent);
	else
		parent = NULL;

	tag->node = NULL;
	tag->tree = NULL;

	tree_changed (tree);

	if (g_hash_table_lookup (tree->tags, node) == tag) {
		g_hash_table_remove (tree->tags, node);
		tag_unref (tag);
	}

	recursive_kill_children (tree, node);

	xmlUnlinkNode (node);
	xmlFreeNode (node);

	/* XXX: work around an apparent libxml bug,
	 * sent a patch to Daniel */
	if (node == tree->doc->root)
		tree->doc->root = NULL;

	signal_emit (tag, SIGNAL_KILL);

	if (parent != NULL)
		signal_emit_ref (parent, SIGNAL_KILL_CHILD, tag);

	list = tag->signals;

	tag->signals = NULL;

	g_list_foreach (list, (GFunc)signal_free, NULL);
	g_list_free (list);

	tag_unref (tag);
}

XMLTag *
tag_prev (XMLTag *tag)
{
	g_return_val_if_fail (tag != NULL, NULL);

	if (tag->tree != NULL)
		return tag_from_node (tag->tree, tag->node->prev);
	else
		return NULL;
}

XMLTag *
tag_prev_by_ids (XMLTag *tag, const GList *list)
{
	const char *type;
	XMLTag *prev;

	g_return_val_if_fail (tag != NULL, NULL);
	g_return_val_if_fail (list != NULL, NULL);

	prev = tag_ref (tag);

	do {
		XMLTag *new_prev = tag_prev (prev);

		tag_unref (prev);

		prev = new_prev;

		type = tag_peek_type (prev);
	} while (prev != NULL &&
		 (type == NULL ||
		  ! is_string_in_list_case_no_locale (list, type)));

	return prev;
}

XMLTag *
tag_prev_by_id (XMLTag *tag, const char *id)
{
	XMLTag *prev;

	GList *list = NULL;

	list = g_list_append (list, (char *)id);

	prev = tag_prev_by_ids (tag, list);

	g_list_free (list);

	return prev;
}

XMLTag *
tag_next (XMLTag *tag)
{
	g_return_val_if_fail (tag != NULL, NULL);

	if (tag->tree != NULL)
		return tag_from_node (tag->tree, tag->node->next);
	else
		return NULL;
}

XMLTag *
tag_next_by_ids (XMLTag *tag, const GList *list)
{
	const char *type;
	XMLTag *next;

	g_return_val_if_fail (tag != NULL, NULL);
	g_return_val_if_fail (list != NULL, NULL);

	next = tag_ref (tag);

	do {
		XMLTag *new_next = tag_next (next);

		tag_unref (next);

		next = new_next;

		type = tag_peek_type (next);
	} while (next != NULL &&
		 (type == NULL ||
		  ! is_string_in_list_case_no_locale (list, type)));

	return next;
}

XMLTag *
tag_next_by_id (XMLTag *tag, const char *id)
{
	XMLTag *next;

	GList *list = NULL;

	list = g_list_append (list, (char *)id);

	next = tag_next_by_ids (tag, list);

	g_list_free (list);

	return next;
}

XMLTag *
tag_parent (XMLTag *tag)
{
	g_return_val_if_fail (tag != NULL, NULL);

	if (tag->tree != NULL)
		return tag_from_node (tag->tree, tag->node->parent);
	else
		return NULL;
}

XMLTag *
tag_parent_parent (XMLTag *tag)
{
	g_return_val_if_fail (tag != NULL, NULL);

	if (tag->node != NULL &&
	    tag->node->parent != NULL)
		return tag_from_node (tag->tree, tag->node->parent->parent);
	else
		return NULL;
}

void
tag_move_up (XMLTag *tag)
{
	XMLTag *prev;

	prev = tag_prev (tag);

	if (prev == NULL)
		return;

	tree_changed (tag->tree);

	/* work around bugs and fixes */
	xmlAddPrevSibling (prev->node, tag->node);
	if (prev->node->next == tag->node)
		xmlAddNextSibling (prev->node, tag->node);


	tag_unref (prev);

	signal_emit (tag, SIGNAL_MOVE_UP);
}

void
tag_move_down (XMLTag *tag)
{
	XMLTag *next;

	next = tag_next (tag);

	if (next == NULL)
		return;

	tree_changed (tag->tree);

	/* work around bugs and fixes */
	xmlAddNextSibling (next->node, tag->node);
	if (next->node->prev == tag->node)
		xmlAddPrevSibling (next->node, tag->node);

	tag_unref (next);

	signal_emit (tag, SIGNAL_MOVE_DOWN);
}

XMLTag *
tag_add (XMLTag *parent, const char *name)
{
	XMLTag *tag;
	xmlNode *node;

	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (parent->tree == NULL ||
	    parent->node == NULL)
		return NULL;

	tree_changed (parent->tree);

	node = xmlNewChild (parent->node, parent->node->ns, name, NULL);

	tag = tag_from_node (parent->tree, node);

	signal_emit_ref (parent, SIGNAL_ADD, tag);

	return tag;
}

XMLTag *
tag_append (XMLTag *previous, const char *name)
{
	XMLTag *tag;
	XMLTag *parent;
	xmlNode *node;

	g_return_val_if_fail (previous != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (previous->tree == NULL ||
	    previous->node == NULL)
		return NULL;

	tree_changed (previous->tree);

	node = xmlNewNode (previous->node->ns, name);
	xmlAddNextSibling (previous->node, node);
	if (previous->node->prev == node)
		xmlAddPrevSibling (previous->node, node);

	tag = tag_from_node (previous->tree, node);

	signal_emit_ref (previous, SIGNAL_APPEND, tag);

	parent = tag_parent (previous);

	/* paranoia */
	if (parent != NULL) {
		signal_emit_ref (parent, SIGNAL_ADD, tag);

		tag_unref (parent);
	}

	return tag;
}

XMLTag *
tag_prepend (XMLTag *next, const char *name)
{
	XMLTag *tag;
	XMLTag *parent;
	xmlNode *node;

	g_return_val_if_fail (next != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (next->tree == NULL ||
	    next->node == NULL)
		return NULL;

	tree_changed (next->tree);

	node = xmlNewNode (next->node->ns, name);
	/* work around bugs and fixes */
	xmlAddPrevSibling (next->node, node);
	if (next->node->next == node)
		xmlAddNextSibling (next->node, node);

	tag = tag_from_node (next->tree, node);

	signal_emit_ref (next, SIGNAL_PREPEND, tag);

	parent = tag_parent (next);

	/* paranoia */
	if (parent != NULL) {
		signal_emit_ref (parent, SIGNAL_ADD, tag);

		tag_unref (parent);
	}

	return tag;
}

static gboolean
is_locale (xmlNode *node, const char *locale)
{
	gboolean ret;
	char *lang;
	char *xmllang = xmlGetProp (node, "xml:lang");

	lang = xmllang;

	if (pong_string_empty (lang))
		lang = "C";
	if (locale == NULL)
		locale = "C";

	if (strcmp (lang, locale) == 0)
		ret = TRUE;
	else
		ret = FALSE;

	if (xmllang != NULL)
		xmlFree (xmllang);

	return ret;
}

static XMLTag *
find_child (XMLTag *tag, const char *name,
	    gboolean check_locale, const char *locale)
{
	xmlNode *iter;

	g_return_val_if_fail (tag != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	if (tag->node == NULL ||
	    tag->tree == NULL ||
	    tag->node->type != XML_ELEMENT_NODE)
		return NULL;

	for (iter = tag->node->childs;
	     iter != NULL;
	     iter = iter->next) {
		if (iter->name != NULL &&
		    pong_tags_equal (name, iter->name) &&
		    ( ! check_locale ||
		     is_locale (iter, locale))) {
			return tag_from_node (tag->tree, iter);
		}
	}

	return NULL;
}

char *
tag_get_text (XMLTag *tag, const char *path, const char *locale)
{
	XMLTag *child;
	char *ret;
	xmlChar *xml_char;

	g_return_val_if_fail (tag != NULL, NULL);

	child = tag_ref (tag);

	if (path != NULL) {
		char **pathv;
		int i;

		pathv = g_strsplit (path, "/", 0);

		for (i = 0; child != NULL && pathv[i] != NULL; i++) {
			XMLTag *old_child = child;
			child = find_child (old_child, pathv[i],
					    pathv[i+1] == NULL, /* check locale
								   on last
								   element only
								 */
					    locale);
			tag_unref (old_child);
		}

		g_strfreev (pathv);

		if (child == NULL)
			return NULL;
	}

	if (child->node != NULL)
		xml_char = xmlNodeGetContent (child->node);
	else
		xml_char = NULL;

	ret = g_strdup (xml_char);

	if (xml_char != NULL)
		xmlFree (xml_char);

	tag_unref (child);

	return ret;
}

static void
do_text_signal_recursive (XMLTree *tree, xmlNode *node,
			  const char *key, const char *text)
{
	char *newkey;
	XMLTag *tag;

	if (node == NULL)
		return;

	tag = tag_from_node_no_new (tree, node);

	if (tag != NULL) {
		signal_emit_text (tag, SIGNAL_TEXT, key, text);
		tag_unref (tag);
	}

	/* can this ever happen, oh well, just in case */
	if (node->name == NULL)
		return;

	/* probably too anal, but just in case */
	if (node->parent == NULL ||
	    node->parent == node)
		return;

	if (key == NULL)
		newkey = g_strdup (node->name);
	else
		newkey = g_strconcat (node->name, "/", key, NULL);

	do_text_signal_recursive (tree, node->parent, newkey, text);

	g_free (newkey);
}

void
tag_set_text (XMLTag *tag, const char *path, const char *text,
	      const char *locale)
{
	XMLTag *child;
	XMLTree *tree;
	xmlNode *child_node;
	xmlAttr *attr;

	g_return_if_fail (tag != NULL);

	if (locale != NULL &&
	    strcmp (locale, "C") == 0)
		locale = NULL;

	if (tag->tree == NULL ||
	    tag->node == NULL)
		return;

	tree = tree_ref (tag->tree);

	child = tag_ref (tag);

	if (path != NULL) {
		char **pathv;
		int i;

		pathv = g_strsplit (path, "/", 0);

		for (i = 0; pathv[i] != NULL; i++) {
			XMLTag *old_child = child;
			child = find_child (old_child, pathv[i],
					    pathv[i+1] == NULL, /* check locale
								   on last
								   element only
								 */
					    locale);

			if (child == NULL) {
				/* we're killing the node anyway,
				 * so don't create the path if it don't
				 * exist */
				if (text == NULL) {
					tag_unref (old_child);
					g_strfreev (pathv);
					return;
				}
				/* if this is the last element of the path,
				 * first try adding the child after the first
				 * such element in case this was due to
				 * missing locale */
				if (pathv[i+1] == NULL) {
					XMLTag *any_child;
					any_child = find_child (old_child, pathv[i],
								FALSE, NULL);

					if (any_child != NULL) {
						child = tag_append (any_child, pathv[i]);
						tag_unref (any_child);
					}
				}
				if (child == NULL)
					child = tag_add (old_child, pathv[i]);

			}

			tag_unref (old_child);

			if (child == NULL) {
				g_warning ("XML tree on crack");
				g_strfreev (pathv);
				return;
			}
		}

		g_strfreev (pathv);
	}

	g_assert (child->node != NULL);

	child_node = child->node;

	attr = xmlSetProp (child_node, "xml:lang", locale);
	if (attr != NULL &&
	    locale == NULL) {
		xmlRemoveProp (attr);
	}

	if (text != NULL) {
		char *xml_char;

		xml_char = xmlNodeGetContent (child_node);

		if (xml_char == NULL ||
		    strcmp (xml_char, text) != 0) {
			tree_changed (tree);
			xmlNodeSetContent (child_node, text);

			do_text_signal_recursive (tree, child_node, NULL, text);
		}

		if (xml_char != NULL)
			xmlFree (xml_char);
	} else {
		char *node_name = g_strdup (child_node->name);
		xmlNode *parent = child_node->parent;

		tag_kill (child);

		do_text_signal_recursive (tree, parent, node_name, NULL);

		g_free (node_name);
	}

	tag_unref (child);
	tree_unref (tree);
}

gboolean
tag_get_bool (XMLTag *tag, const char *path, gboolean def)
{
	char *text;
	gboolean ret;

	g_return_val_if_fail (tag != NULL, FALSE);

	text = tag_get_text (tag, path, NULL);

	if (pong_string_empty (text))
		return def;

	ret = pong_bool_from_string (text);

	g_free (text);

	return ret;
}

void
tag_set_bool (XMLTag *tag, const char *path, gboolean value)
{
	g_return_if_fail (tag != NULL);

	tag_set_text (tag, path, value ? "True" : "False", NULL);
}

int
tag_get_int (XMLTag *tag, const char *path, int def)
{
	char *text;
	int ret;

	g_return_val_if_fail (tag != NULL, FALSE);

	text = tag_get_text (tag, path, NULL);

	if (pong_string_empty (text))
		return def;

	ret = atoi (text);

	g_free (text);

	return ret;
}

void
tag_set_int (XMLTag *tag, const char *path, int value)
{
	char *text;

	g_return_if_fail (tag != NULL);

	text = g_strdup_printf ("%d", value);

	tag_set_text (tag, path, text, NULL);

	g_free (text);
}

GList *
tag_get_text_list (XMLTag *the_tag, const char *id)
{
	GList *list, *li;

	g_return_val_if_fail (the_tag != NULL, NULL);
	g_return_val_if_fail (id != NULL, NULL);

	list = tag_get_tags (the_tag, id);

	/* replace list of tags with list of strings */
	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *text = tag_get_text (tag, NULL, NULL);

		if (text != NULL)
			li->data = text;
		else
			li->data = g_strdup ("");

		tag_unref (tag);
	}

	return list;
}

void
tag_set_text_list (XMLTag *the_tag, const char *id, GList *values)
{
	GList *list, *li;

	g_return_if_fail (the_tag != NULL);

	list = tag_get_tags (the_tag, id);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *text = tag_get_text (tag, NULL, NULL);

		if (text == NULL) {
			li->data = NULL;
			tag_kill (tag);
			tag_unref (tag);
			continue;
		}

		if ( ! is_string_in_list (values, text)) {
			li->data = NULL;
			tag_kill (tag);
			tag_unref (tag);
		}

		g_free (text);
	}

	for (li = values; li != NULL; li = li->next) {
		const char *string = li->data;
		GList *val;

		val = find_value (list, string);

		if (val == NULL) {
			XMLTag *tag;
			tag = tag_add (the_tag, id);
			tag_set_text (tag, NULL, string, NULL);
			tag_unref (tag);
		}
	}

	tree_free_tag_list (list);
}

void
tag_text_list_add (XMLTag *the_tag, const char *id, const char *value)
{
	XMLTag *tag;
	GList *list, *li;

	g_return_if_fail (the_tag != NULL);

	list = tag_get_tags (the_tag, id);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *text = tag_get_text (tag, NULL, NULL);

		if ((pong_string_empty (text) &&
		     pong_string_empty (value)) ||
		    (text != NULL &&
		     value != NULL &&
		     strcmp (text, value) == 0)) {
			g_free (text);
			tree_free_tag_list (list);
			return;
		}
		g_free (text);
	}

	tree_free_tag_list (list);

	tag = tag_add (the_tag, id);
	tag_set_text (tag, NULL, value, NULL);
	tag_unref (tag);
}

void
tag_text_list_remove (XMLTag *the_tag, const char *id, const char *value)
{
	GList *list, *li;

	g_return_if_fail (the_tag != NULL);

	list = tag_get_tags (the_tag, id);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *text = tag_get_text (tag, NULL, NULL);

		if ((pong_string_empty (text) &&
		     pong_string_empty (value)) ||
		    (text != NULL &&
		     value != NULL &&
		     strcmp (text, value) == 0)) {
			li->data = NULL;
			tag_kill (tag);
			tag_unref (tag);
			g_free (text);
			tree_free_tag_list (list);
			return;
		}
		g_free (text);
	}

	tree_free_tag_list (list);
}

void
tag_set_translatable (XMLTag *tag, gboolean translatable)
{
	const char *name;

	g_return_if_fail (tag != NULL);
	g_return_if_fail (tag->node != NULL);

	name = tag->node->name;

	if (name == NULL)
		return;

	if (name[0] == '_' &&
	    ! translatable) {
		char *new_name = g_strdup (&name[1]);
		xmlNodeSetName (tag->node, new_name);
		g_free (new_name);

		tree_changed (tag->tree);
	} else if (name[0] != '_' &&
		 translatable) {
		char *new_name = g_strconcat ("_", name, NULL);
		xmlNodeSetName (tag->node, new_name);
		g_free (new_name);

		tree_changed (tag->tree);
	}
}

/* Filter a list of tags to return a new list of only
 * those tags where path text is equal to text */
GList *
tag_find (GList *tags, const char *path, const char *text)
{
	GList *new_list = NULL;

	g_return_val_if_fail (tags != NULL, NULL);
	g_return_val_if_fail (path != NULL, NULL);
	g_return_val_if_fail (text != NULL, NULL);

	while (tags != NULL) {
		XMLTag *tag = tags->data;
		char *s;

		s = tag_get_text (tag, path, NULL);

		if (strcmp (s, text) == 0) {
			new_list = g_list_prepend (new_list, tag_ref (tag));
		}

		g_free (s);

		tags = tags->next;
	}

	return g_list_reverse (new_list);
}

char *
tag_get_prop (XMLTag *tag, const char *prop_name)
{
	char *ret, *gret;

	g_return_val_if_fail (tag != NULL, NULL);
	g_return_val_if_fail (prop_name != NULL, NULL);

	ret = xmlGetProp (tag->node, prop_name);

	gret = g_strdup (ret);

	if (ret != NULL)
		xmlFree (ret);

	return gret;
}

/* Specific API */

static void
i18n_set_translatable (XMLTag *tag,
		       const char *subtag,
		       gboolean translatable,
		       gboolean *xml_lang_return)
{
	GList *list, *li;

	list = tag_get_tags (tag, subtag);
	for (li = list; li != NULL; li = li->next) {
		XMLTag *it = li->data;
		char *xml_lang = tag_get_prop (it, "xml:lang");
		tag_set_translatable (it, translatable);

		if ( ! pong_string_empty (xml_lang)) {
			*xml_lang_return = FALSE;
		}
		g_free (xml_lang);
	}
	tree_free_tag_list (list);
}

static void
i18n_sanitize_widget (XMLTag *widget,
		      gboolean *xml_lang_return)
{
	GList *list, *li;

	i18n_set_translatable (widget, PONG_S_Tooltip, TRUE,
			       xml_lang_return);
	i18n_set_translatable (widget, PONG_S_Label, TRUE,
			       xml_lang_return);

	list = tag_get_tags (widget, PONG_S_Argument);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *arg = li->data;
		gboolean translatable =
			tag_get_bool (arg, PONG_S_Translatable, FALSE);

		i18n_set_translatable (arg, PONG_S_Value, 
				       translatable,
				       xml_lang_return);
	}

	tree_free_tag_list (list);

	list = tag_get_tags (widget, PONG_S_Option);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *opt = li->data;

		i18n_set_translatable (opt, PONG_S_Label, TRUE,
				       xml_lang_return);
	}

	tree_free_tag_list (list);
}

static void
i18n_sanitize_group (XMLTag *group,
		     gboolean *xml_lang_return)
{
	GList *list, *li;

	i18n_set_translatable (group, PONG_S_Label, TRUE,
			       xml_lang_return);

	list = tree_get_widgets (group->tree, group);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		i18n_sanitize_widget (tag, xml_lang_return);
	}

	tree_free_tag_list (list);

}

static void
i18n_sanitize_pane (XMLTag *pane,
		    gboolean *xml_lang_return)
{
	GList *list, *li;

	i18n_set_translatable (pane, PONG_S_Label, TRUE,
			       xml_lang_return);

	list = tree_get_groups (pane->tree, pane);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		const char *type = tag_peek_type (tag);

		if (type != NULL &&
		    pong_strcasecmp_no_locale (type, PONG_S_Group) == 0) {
			i18n_sanitize_group (tag, xml_lang_return);
		} else if (type != NULL &&
			   (pong_strcasecmp_no_locale (type, PONG_S_Plug) == 0 ||
			    pong_strcasecmp_no_locale (type, PONG_S_Bonobo) == 0 ||
			    pong_strcasecmp_no_locale (type, PONG_S_Glade) == 0)) {
			i18n_sanitize_widget (tag, xml_lang_return);
		}
	}

	tree_free_tag_list (list);
}

/* make things translatable that are supposed to be translatable
 * and such things
 * Returns FALSE if xml:lang things exist (thus unsane) */
gboolean
tree_i18n_sanitize (XMLTree *tree)
{
	GList *list, *li;
	XMLTag *root;
	gboolean ret = TRUE;

	g_return_val_if_fail (tree != NULL, TRUE);

	root = tree_root (tree);

	i18n_set_translatable (root, PONG_S_DialogTitle, TRUE,
			       &ret);

	tag_unref (root);

	list = tree_get_panes (tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		i18n_sanitize_pane (tag, &ret);
	}

	tree_free_tag_list (list);

	list = tree_get_elements (tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		i18n_set_translatable (tag, PONG_S_SchemaShortDescription,
				       TRUE, &ret);
		i18n_set_translatable (tag, PONG_S_SchemaLongDescription,
				       TRUE, &ret);
	}

	tree_free_tag_list (list);

	return ret;
}

static void
move_until_list (XMLTag *tag, GList *strings, int direction)
{
	XMLTag *prev = NULL;
	const char *prev_type = NULL;

	g_return_if_fail (tag != NULL);
	g_return_if_fail (strings != NULL);

	do {
		tag_unref (prev);
		if (direction == MOVE_UP) {
			prev = tag_prev (tag);
			tag_move_up (tag);
		} else {
			prev = tag_next (tag);
			tag_move_down (tag);
		}

		if (prev == NULL)
			return;

		prev_type = tag_peek_type (prev);
	} while (prev_type != NULL &&
		 ! is_string_in_list_case_no_locale (strings, prev_type));
	tag_unref (prev);
}

static void
move_until (XMLTag *tag, const char *string, int direction)
{
	GList *list;

	g_return_if_fail (tag != NULL);
	g_return_if_fail (string != NULL);

	list = g_list_append (NULL, (gpointer)string);
	move_until_list (tag, list, direction);
	g_list_free (list);
}

/* returns a list that's the list of reffed tags */
GList *
tree_get_levels (XMLTree *tree)
{
	XMLTag *root;
	GList *list;

	g_return_val_if_fail (tree != NULL, NULL);

	root = tree_root (tree);
	list = tag_get_tags (root, PONG_S_Level);
	tag_unref (root);

	return list;
}

XMLTag *
tree_get_level (XMLTree *tree, const char *level)
{
	GList *list, *li;

	list = tree_get_levels (tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *name = tag_get_text (tag, PONG_S_Name, NULL);

		if (name != NULL &&
		    strcmp (name, level) == 0) {
			tag_ref (tag);

			g_free (name);
			tree_free_tag_list (list);

			return tag;
		}

		g_free (name);
	}

	tree_free_tag_list (list);

	return NULL;
}

XMLTag *
tree_add_level (XMLTree *tree)
{
	XMLTag *tag, *root;
	char *name;

	name = unique_level_name (tree);

	root = tree_root (tree);
	tag = tag_add (root, PONG_S_Level);
	tag_unref (root);
	tag_set_text (tag, PONG_S_Name, name, NULL);

	g_free (name);

	return tag;
}

XMLTag *
tree_prepend_level (XMLTag *sibling)
{
	XMLTag *tag;
	char *name;

	if (sibling->tree == NULL)
		return NULL;

	name = unique_level_name (sibling->tree);

	tag = tag_prepend (sibling, PONG_S_Level);
	tag_set_text (tag, PONG_S_Name, name, NULL);

	g_free (name);

	return tag;
}


void
tree_move_level_up (XMLTag *level)
{
	g_return_if_fail (level != NULL);

	move_until (level, PONG_S_Level, MOVE_UP);
}

void
tree_move_level_down (XMLTag *level)
{
	g_return_if_fail (level != NULL);

	move_until (level, PONG_S_Level, MOVE_DOWN);
}

/* error checking */
gboolean
tree_set_level_name (XMLTree *tree, XMLTag *level, const char *name)
{
	XMLTag *found;

	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (level != NULL, FALSE);
	g_return_val_if_fail (name != NULL, FALSE);

	if (name[0] == '\0')
		return FALSE;

	found = tree_get_level (tree, name);

	if (found != NULL) {
		if (found != level) {
			tag_unref (found);
			return FALSE;
		} else {
			tag_unref (found);
			return TRUE;
		}
	}

	tag_set_text (level, PONG_S_Name, name, NULL);

	return TRUE;
}

void
tree_remove_level (XMLTree *tree, const char *level)
{
	XMLTag *tag;

	tag = tree_get_level (tree, level);

	if (tag != NULL) {
		tag_kill (tag);
		tag_unref (tag);
	}
}


/* Pane stuff */

static XMLTag *
tree_find_by_name_from_group (XMLTree *tree, XMLTag *group, const char *name)
{
	GList *list, *li;

	list = tree_get_widgets (tree, group);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *the_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (the_name != NULL &&
		    strcmp (the_name, name) == 0) {
			tag_ref (tag);

			g_free (the_name);
			tree_free_tag_list (list);

			return tag;
		}

		g_free (the_name);
	}

	tree_free_tag_list (list);

	return NULL;
}

static XMLTag *
tree_find_by_name_from_pane (XMLTree *tree, XMLTag *pane, const char *name)
{
	GList *list, *li;

	list = tree_get_groups (tree, pane);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		XMLTag *found;
		char *the_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (the_name != NULL &&
		    strcmp (the_name, name) == 0) {
			tag_ref (tag);

			g_free (the_name);
			tree_free_tag_list (list);

			return tag;
		}

		found = tree_find_by_name_from_group (tree, tag, name);

		if (found != NULL) {
			g_free (the_name);
			tree_free_tag_list (list);

			return found;
		}

		g_free (the_name);
	}

	tree_free_tag_list (list);

	return NULL;
}

XMLTag *
tree_find_by_name (XMLTree *tree, const char *name)
{
	GList *list, *li;

	list = tree_get_panes (tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		XMLTag *found;
		char *the_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (the_name != NULL &&
		    strcmp (the_name, name) == 0) {
			tag_ref (tag);

			g_free (the_name);
			tree_free_tag_list (list);

			return tag;
		}

		found = tree_find_by_name_from_pane (tree, tag, name);

		if (found != NULL) {
			g_free (the_name);
			tree_free_tag_list (list);

			return found;
		}

		g_free (the_name);
	}

	tree_free_tag_list (list);

	return NULL;
}

static void
tree_get_names_from_group (XMLTree *tree, XMLTag *group, GList **retlist)
{
	GList *list, *li;

	list = tree_get_widgets (tree, group);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *the_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (the_name != NULL)
			*retlist = g_list_prepend (*retlist, the_name);
	}

	tree_free_tag_list (list);
}

static void
tree_get_names_from_pane (XMLTree *tree, XMLTag *pane, GList **retlist)
{
	GList *list, *li;

	list = tree_get_groups (tree, pane);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *the_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (the_name != NULL)
			*retlist = g_list_prepend (*retlist, the_name);

		tree_get_names_from_group (tree, tag, retlist);
	}

	tree_free_tag_list (list);
}

/* list of strings */
GList *
tree_get_names (XMLTree *tree)
{
	GList *retlist = NULL;
	GList *list, *li;

	list = tree_get_panes (tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *the_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (the_name != NULL)
			retlist = g_list_prepend (retlist, the_name);

		tree_get_names_from_pane (tree, tag, &retlist);
	}

	tree_free_tag_list (list);

	return g_list_reverse (retlist);
}

/* returns a list that's the list of reffed tags */
GList *
tree_get_panes (XMLTree *tree)
{
	XMLTag *root;
	GList *list;

	g_return_val_if_fail (tree != NULL, NULL);

	root = tree_root (tree);
	list = tag_get_tags (root, PONG_S_Pane);
	tag_unref (root);

	return list;
}


/* error checking */
gboolean
tree_set_pane_name (XMLTree *tree, XMLTag *pane, const char *name)
{
	XMLTag *found;

	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (pane != NULL, FALSE);
	g_return_val_if_fail (name != NULL, FALSE);

	found = tree_find_by_name (tree, name);

	if (found != NULL) {
		if (found != pane) {
			tag_unref (found);
			return FALSE;
		} else {
			tag_unref (found);
			return TRUE;
		}
	}

	tag_set_text (pane, PONG_S_Name, name, NULL);

	return TRUE;
}


/* add onto the end */
XMLTag *
tree_add_pane (XMLTree *tree)
{
	XMLTag *tag, *root;

	g_return_val_if_fail (tree != NULL, NULL);

	root = tree_root (tree);
	tag = tag_add (root, PONG_S_Pane);
	tag_unref (root);

	return tag;
}

XMLTag *
tree_prepend_pane (XMLTag *sibling)
{
	XMLTag *tag;

	g_return_val_if_fail (sibling != NULL, NULL);

	tag = tag_prepend (sibling, PONG_S_Pane);

	return tag;
}

void
tree_move_pane_up (XMLTag *pane)
{
	g_return_if_fail (pane != NULL);

	move_until (pane, PONG_S_Pane, MOVE_UP);
}

void
tree_move_pane_down (XMLTag *pane)
{
	g_return_if_fail (pane != NULL);

	move_until (pane, PONG_S_Pane, MOVE_DOWN);
}


/* Get groups, though this includes Plug/Glade/Bonobo thingies
 * as well, this is ordered so be careful */
GList *
tree_get_groups (XMLTree *tree, XMLTag *pane)
{
	GList *ret;
	GList *list = NULL;

	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (pane != NULL, NULL);

	list = g_list_prepend (list, PONG_S_Group);
	list = g_list_prepend (list, PONG_S_Plug);
	list = g_list_prepend (list, PONG_S_Glade);
	list = g_list_prepend (list, PONG_S_Bonobo);

	ret = tag_get_tags_multiple (pane, list);

	g_list_free (list);

	return ret;
}


/* error checking */
gboolean
tree_set_group_name (XMLTree *tree, XMLTag *group, const char *name)
{
	XMLTag *found;

	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (group != NULL, FALSE);
	g_return_val_if_fail (name != NULL, FALSE);

	found = tree_find_by_name (tree, name);

	if (found != NULL) {
		if (found != group) {
			tag_unref (found);
			return FALSE;
		} else {
			tag_unref (found);
			return TRUE;
		}
	}

	tag_set_text (group, PONG_S_Name, name, NULL);

	return TRUE;
}


/* add onto the end */
XMLTag *
tree_add_group (XMLTag *pane)
{
	XMLTag *tag;

	g_return_val_if_fail (pane != NULL, NULL);

	tag = tag_add (pane, PONG_S_Group);

	return tag;
}

XMLTag *
tree_prepend_group (XMLTag *sibling)
{
	XMLTag *tag;

	g_return_val_if_fail (sibling != NULL, NULL);

	tag = tag_prepend (sibling, PONG_S_Group);

	return tag;
}

XMLTag *
tree_add_plug (XMLTag *pane_or_group)
{
	XMLTag *tag;

	g_return_val_if_fail (pane_or_group != NULL, NULL);

	tag = tag_add (pane_or_group, PONG_S_Plug);

	return tag;
}

XMLTag *
tree_prepend_plug (XMLTag *sibling)
{
	XMLTag *tag;

	g_return_val_if_fail (sibling != NULL, NULL);

	tag = tag_prepend (sibling, PONG_S_Plug);

	return tag;
}

XMLTag *
tree_add_glade (XMLTag *pane_or_group)
{
	XMLTag *tag;

	g_return_val_if_fail (pane_or_group != NULL, NULL);

	tag = tag_add (pane_or_group, PONG_S_Glade);

	return tag;
}

XMLTag *
tree_prepend_glade (XMLTag *sibling)
{
	XMLTag *tag;

	g_return_val_if_fail (sibling != NULL, NULL);

	tag = tag_prepend (sibling, PONG_S_Glade);

	return tag;
}

XMLTag *
tree_add_bonobo (XMLTag *pane_or_group)
{
	XMLTag *tag;

	g_return_val_if_fail (pane_or_group != NULL, NULL);

	tag = tag_add (pane_or_group, PONG_S_Bonobo);

	return tag;
}

XMLTag *
tree_prepend_bonobo (XMLTag *sibling)
{
	XMLTag *tag;

	g_return_val_if_fail (sibling != NULL, NULL);

	tag = tag_prepend (sibling, PONG_S_Bonobo);

	return tag;
}

void
tree_move_group_up (XMLTag *group)
{
	GList *list;

	list = NULL;
	list = g_list_prepend (list, PONG_S_Group);
	list = g_list_prepend (list, PONG_S_Plug);
	list = g_list_prepend (list, PONG_S_Glade);
	list = g_list_prepend (list, PONG_S_Bonobo);

	move_until_list (group, list, MOVE_UP);

	g_list_free (list);
}

void
tree_move_group_down (XMLTag *group)
{
	GList *list;

	list = NULL;
	list = g_list_prepend (list, PONG_S_Group);
	list = g_list_prepend (list, PONG_S_Plug);
	list = g_list_prepend (list, PONG_S_Glade);
	list = g_list_prepend (list, PONG_S_Bonobo);

	move_until_list (group, list, MOVE_DOWN);

	g_list_free (list);
}


/* Get widgets, though this includes Plug/Glade/Bonobo thingies
 * as well, this is ordered so be careful */
GList *
tree_get_widgets (XMLTree *tree, XMLTag *group)
{
	GList *ret;
	GList *list = NULL;

	list = g_list_prepend (list, PONG_S_Widget);
	list = g_list_prepend (list, PONG_S_Plug);
	list = g_list_prepend (list, PONG_S_Glade);
	list = g_list_prepend (list, PONG_S_Bonobo);

	ret = tag_get_tags_multiple (group, list);

	g_list_free (list);

	return ret;
}


/* error checking */
gboolean
tree_set_widget_name (XMLTree *tree, XMLTag *widget, const char *name)
{
	XMLTag *found;

	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (name != NULL, FALSE);

	found = tree_find_by_name (tree, name);

	if (found != NULL) {
		if (found != widget) {
			tag_unref (found);
			return FALSE;
		} else {
			tag_unref (found);
			return TRUE;
		}
	}

	tag_set_text (widget, PONG_S_Name, name, NULL);

	return TRUE;
}


/* add onto the end */
XMLTag *
tree_add_widget (XMLTag *group)
{
	char *name;
	XMLTag *tag;

	g_return_val_if_fail (group != NULL, NULL);

	if (group->tree == NULL)
		return NULL;

	name = unique_name (group->tree);

	tag = tag_add (group, PONG_S_Widget);

	tag_set_text (tag, PONG_S_Name, name, NULL);

	return tag;
}

XMLTag *
tree_prepend_widget (XMLTag *sibling)
{
	char *name;
	XMLTag *tag;

	g_return_val_if_fail (sibling != NULL, NULL);

	if (sibling->tree == NULL)
		return NULL;

	name = unique_name (sibling->tree);

	tag = tag_prepend (sibling, PONG_S_Widget);

	tag_set_text (tag, PONG_S_Name, name, NULL);

	return tag;
}

void
tree_move_widget_up (XMLTag *widget)
{
	GList *list;

	list = NULL;
	list = g_list_prepend (list, PONG_S_Widget);
	list = g_list_prepend (list, PONG_S_Plug);
	list = g_list_prepend (list, PONG_S_Glade);
	list = g_list_prepend (list, PONG_S_Bonobo);

	move_until_list (widget, list, MOVE_UP);

	g_list_free (list);
}

void
tree_move_widget_down (XMLTag *widget)
{
	GList *list;

	list = NULL;
	list = g_list_prepend (list, PONG_S_Widget);
	list = g_list_prepend (list, PONG_S_Plug);
	list = g_list_prepend (list, PONG_S_Glade);
	list = g_list_prepend (list, PONG_S_Bonobo);

	move_until_list (widget, list, MOVE_DOWN);

	g_list_free (list);
}

XMLTag *
tree_widget_get_argument (XMLTag *widget, const char *name)
{
	GList *list, *li;

	list = tag_get_tags (widget, PONG_S_Argument);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *arg_name;

		arg_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (arg_name != NULL &&
		    strcmp (arg_name, name) == 0) {
			tree_free_tag_list (list);
			g_free (arg_name);
			return tag;
		}
		g_free (arg_name);
	}
	tree_free_tag_list (list);

	return NULL;
}

XMLTag *
tree_widget_add_argument (XMLTag *widget)
{
	char *name;
	XMLTag *tag;

	g_return_val_if_fail (widget != NULL, NULL);

	name = unique_argument_name (widget);

	tag = tag_add (widget, PONG_S_Argument);
	tag_set_text (tag, PONG_S_Name, name, NULL);

	return tag;
}

/* error checking */
gboolean
tree_set_argument_name (XMLTag *widget, XMLTag *argument, const char *name)
{
	XMLTag *found;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (argument != NULL, FALSE);
	g_return_val_if_fail (name != NULL, FALSE);

	if (name[0] == '\0')
		return FALSE;

	found = tree_widget_get_argument (widget, name);

	if (found != NULL) {
		if (found != argument) {
			tag_unref (found);
			return FALSE;
		} else {
			tag_unref (found);
			return TRUE;
		}
	}

	tag_set_text (argument, PONG_S_Name, name, NULL);

	return TRUE;
}

void
tree_widget_remove_argument (XMLTag *widget, const char *name)
{
	GList *list, *li;

	list = tag_get_tags (widget, PONG_S_Argument);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *arg_name = tag_get_text (tag, PONG_S_Name, NULL);

		if (arg_name != NULL &&
		    strcmp (arg_name, name) == 0) {
			tag_ref (tag);

			tree_free_tag_list (list);
			g_free (arg_name);

			tag_kill (tag);

			tag_unref (tag);
			return;
		}
		g_free (arg_name);
	}
	tree_free_tag_list (list);
}

static int option_number = 1;

XMLTag *
tree_widget_add_option (XMLTag *widget)
{
	char *label;
	XMLTag *tag;

	g_return_val_if_fail (widget != NULL, NULL);

	tag = tag_add (widget, PONG_S_Option);
	label = g_strdup_printf ("Option %d", option_number++);
	tag_set_text (tag, "_" PONG_S_Label, label, NULL);
	g_free (label);

	return tag;
}

XMLTag *
tree_widget_prepend_option (XMLTag *sibling)
{
	char *label;
	XMLTag *tag;

	g_return_val_if_fail (sibling != NULL, NULL);

	tag = tag_prepend (sibling, PONG_S_Option);
	label = g_strdup_printf ("Option %d", option_number++);
	tag_set_text (tag, "_" PONG_S_Label, label, NULL);
	g_free (label);

	return tag;
}

void
tree_widget_move_option_up (XMLTag *option)
{
	g_return_if_fail (option != NULL);

	move_until (option, PONG_S_Option, MOVE_UP);
}

void
tree_widget_move_option_down (XMLTag *option)
{
	g_return_if_fail (option != NULL);

	move_until (option, PONG_S_Option, MOVE_DOWN);
}


/* Get elements */
GList *
tree_get_elements (XMLTree *tree)
{
	XMLTag *root;
	GList *list;

	g_return_val_if_fail (tree != NULL, NULL);

	root = tree_root (tree);
	list = tag_get_tags (root, PONG_S_Element);
	tag_unref (root);

	return list;
}

XMLTag *
tree_add_element (XMLTree *tree)
{
	XMLTag *tag, *root;
	char *conf_path;

	conf_path = unique_conf_path_name (tree);

	root = tree_root (tree);
	tag = tag_add (root, PONG_S_Element);
	tag_unref (root);
	tag_set_text (tag, PONG_S_Type, PONG_S_Int, NULL);
	tag_set_text (tag, PONG_S_ConfPath, conf_path, NULL);

	g_free (conf_path);

	return tag;
}

XMLTag *
tree_get_element (XMLTree *tree, const char *conf_path)
{
	XMLTag *ret;

	GList *tags;
	GList *matching;

	g_return_val_if_fail (tree != NULL, NULL);

	tags = tree_get_elements (tree);
	
	if (tags == NULL)
		return NULL;

	matching = tag_find (tags, PONG_S_ConfPath, conf_path);

	tree_free_tag_list (tags);
	tags = NULL;

	if (matching != NULL)
		ret = tag_ref (matching->data);
	else
		ret = NULL;

	tree_free_tag_list (matching);
	matching = NULL;

	return ret;
}


gboolean
tree_element_set_conf_path (XMLTree *tree, XMLTag *element,
			    const char *conf_path)
{
	XMLTag *found;

	g_return_val_if_fail (element != NULL, FALSE);
	g_return_val_if_fail (conf_path != NULL, FALSE);

	if (conf_path[0] == '\0')
		return FALSE;

	found = tree_get_element (tree, conf_path);

	if (found != NULL) {
		if (found != element) {
			tag_unref (found);
			return FALSE;
		} else {
			tag_unref (found);
			return TRUE;
		}
	}

	tag_set_text (element, PONG_S_ConfPath, conf_path, NULL);

	return TRUE;
}

PongType
tree_element_get_pong_type (XMLTag *element)
{
	char *text;
	PongType type;

	g_return_val_if_fail (element != NULL, PONG_TYPE_INVALID);

	text = tag_get_text (element, PONG_S_Type, NULL);
	if (text == NULL)
		type = PONG_TYPE_INVALID;
	else
		type = pong_type_from_string (text);
	g_free (text);

	return type;
}

void
tree_element_set_pong_type (XMLTag *element, PongType type)
{
	const char *text;

	g_return_if_fail (element != NULL);

	text = pong_string_from_type (type);

	if (text != NULL)
		tag_set_text (element, PONG_S_Type, text, NULL);
}

XMLTag *
tree_element_add_sensitivity (XMLTag *element, const char *value)
{
	XMLTag *tag;

	g_return_val_if_fail (element != NULL, NULL);

	tag = tag_add (element, PONG_S_Sensitivity);
	tag_set_text (tag, PONG_S_Value, value, NULL);

	return tag;
}

void
tree_sensitivity_add_sensitive (XMLTag *sensitivity, const char *name,
				gboolean sensitive)
{
	GList *list;
	XMLTag *tag;

	g_return_if_fail (sensitivity != NULL);
	g_return_if_fail (name != NULL);

	list = tag_get_text_list (sensitivity,
				  sensitive ?
				    PONG_S_Sensitive :
				    PONG_S_Insensitive);

	if (is_string_in_list (list, name)) {
		tree_free_string_list (list);
		return;
	}
	tree_free_string_list (list);

	if (sensitive)
		tag = tag_add (sensitivity, PONG_S_Sensitive);
	else
		tag = tag_add (sensitivity, PONG_S_Insensitive);
	tag_set_text (tag, NULL, name, NULL);
	tag_unref (tag);
}

void
tree_sensitivity_remove_sensitive (XMLTag *sensitivity, const char *name,
				   gboolean sensitive)
{
	GList *list, *li;

	g_return_if_fail (sensitivity != NULL);
	g_return_if_fail (name != NULL);

	if (sensitive)
		list = tag_get_tags (sensitivity, PONG_S_Sensitive);
	else
		list = tag_get_tags (sensitivity, PONG_S_Insensitive);

	/* replace elements of the list with strings */
	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *widget = tag_get_text (tag, NULL, NULL);

		if (widget != NULL &&
		    strcmp (widget, name) == 0) {
			tag_ref (tag);

			g_free (widget);
			tree_free_tag_list (list);

			tag_kill (tag);
			tag_unref (tag);
			return;
		}

		g_free (widget);
	}

	tree_free_tag_list (list);
}


/* Free list of newly allocated strings */
void
tree_free_string_list (GList *list)
{
	GList *li;

	if (list != NULL) {
		for (li = list; li != NULL; li = li->next) {
			char *string = li->data;

			li->data = NULL;

			g_free (string);
		}

		g_list_free (list);
	}
}

/* Free list of reffed tags */
void
tree_free_tag_list (GList *list)
{
	GList *li;

	if (list != NULL) {
		for (li = list; li != NULL; li = li->next) {
			XMLTag *tag = li->data;

			li->data = NULL;

			tag_unref (tag);
		}

		g_list_free (list);
	}
}

/*
 * Utilities
 */
static GList *
find_value (GList *list, const char *value)
{
	GList *li;

	if (value == NULL)
		return NULL;

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *text;

		if (tag == NULL)
			continue;

		text = tag_get_text (tag, NULL, NULL);

		if (text != NULL && strcmp (value, text) == 0) {
			g_free (text);
			return li;
		}

		g_free (text);
	}

	return NULL;
}


static char *
unique_name (XMLTree *tree)
{
	char *foo = NULL;
	XMLTag *tag = NULL;
	static int num = 1;

	do {
		tag_unref (tag);
		g_free (foo);

		foo = g_strdup_printf ("unknown_%d", num++);
		tag = tree_find_by_name (tree, foo);
	} while (tag != NULL);

	return foo;
}

static char *
unique_conf_path_name (XMLTree *tree)
{
	char *foo = NULL;
	XMLTag *tag = NULL;
	static int num = 1;

	do {
		tag_unref (tag);
		g_free (foo);

		foo = g_strdup_printf ("unknown_%d", num++);
		tag = tree_get_element (tree, foo);
	} while (tag != NULL);

	return foo;
}

static char *
unique_level_name (XMLTree *tree)
{
	char *foo = NULL;
	XMLTag *tag = NULL;
	static int num = 1;

	do {
		tag_unref (tag);
		g_free (foo);

		foo = g_strdup_printf ("Unnamed%d", num++);
		tag = tree_get_level (tree, foo);
	} while (tag != NULL);

	return foo;
}

static char *
unique_argument_name (XMLTag *widget)
{
	char *foo = NULL;
	XMLTag *tag = NULL;
	static int num = 1;

	do {
		tag_unref (tag);
		g_free (foo);

		foo = g_strdup_printf ("unknown_%d", num++);
		tag = tree_widget_get_argument (widget, foo);
	} while (tag != NULL);

	return foo;
}

/*
 * Signals
 */

static XMLSignal *
signal_new (XMLTag *tag, int signum,
	    gpointer data, XMLDestroyNotify destroy_notify)
{
	XMLSignal *signal = g_new0 (XMLSignal, 1);

	signal->id = signal_id ++;
	signal->signum = signum;
	signal->data = data;
	signal->destroy_notify = destroy_notify;

	tag->signals = g_list_prepend (tag->signals, signal);

	return signal;
}

int
tag_connect_kill (XMLTag *tag,
		  XMLTagNotify notify,
		  gpointer data,
		  XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_KILL, data, destroy_notify);
	sig->sfunc.notify = notify;

	return sig->id;
}

int
tag_connect_text (XMLTag *tag,
		  const char *key,
		  XMLTagTextNotify notify,
		  gpointer data,
		  XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_TEXT, data, destroy_notify);
	/* note: handles NULL well */
	sig->key = g_strdup (key);
	sig->sfunc.text_notify = notify;

	return sig->id;
}

int
tag_connect_move_up (XMLTag *tag,
		     XMLTagNotify notify,
		     gpointer data,
		     XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_MOVE_UP, data, destroy_notify);
	sig->sfunc.notify = notify;

	return sig->id;
}

int
tag_connect_move_down (XMLTag *tag,
		       XMLTagNotify notify,
		       gpointer data,
		       XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_MOVE_DOWN, data, destroy_notify);
	sig->sfunc.notify = notify;

	return sig->id;
}

int
tag_connect_add (XMLTag *tag,
		 XMLTagRefNotify notify,
		 gpointer data,
		 XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_ADD, data, destroy_notify);
	sig->sfunc.ref_notify = notify;

	return sig->id;
}

int
tag_connect_prepend (XMLTag *tag,
		     XMLTagRefNotify notify,
		     gpointer data,
		     XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_PREPEND, data, destroy_notify);
	sig->sfunc.ref_notify = notify;

	return sig->id;
}

int
tag_connect_append (XMLTag *tag,
		    XMLTagRefNotify notify,
		    gpointer data,
		    XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_APPEND, data, destroy_notify);
	sig->sfunc.ref_notify = notify;

	return sig->id;
}

int
tag_connect_kill_child (XMLTag *tag,
			XMLTagRefNotify notify,
			gpointer data,
			XMLDestroyNotify destroy_notify)
{
	XMLSignal *sig;

	sig = signal_new (tag, SIGNAL_KILL_CHILD, data, destroy_notify);
	sig->sfunc.ref_notify = notify;

	return sig->id;
}

static void
signal_free (XMLSignal *signal)
{
	if (signal != NULL) {
		if (signal->destroy_notify != NULL)
			signal->destroy_notify (signal->data);
		signal->destroy_notify = NULL;
		signal->data = NULL;

		g_free (signal->key);
		signal->key = NULL;
	}
}

static void
signal_emit (XMLTag *tag, int signum)
{
	GList *li;

	for (li = tag->signals; li != NULL; li = li->next) {
		XMLSignal *sig = li->data;

		if (sig->signum == signum) {
			sig->sfunc.notify (tag, sig->data);
		}
	}
}

static void
signal_emit_ref (XMLTag *tag, int signum, XMLTag *reference)
{
	GList *li;

	tag_ref (reference);

	for (li = tag->signals; li != NULL; li = li->next) {
		XMLSignal *sig = li->data;

		if (sig->signum == signum) {
			sig->sfunc.ref_notify (tag, reference, sig->data);
		}
	}

	tag_unref (reference);
}

static void
signal_emit_text (XMLTag *tag, int signum,
		  const char *key, const char *text)
{
	GList *li;

	for (li = tag->signals; li != NULL; li = li->next) {
		XMLSignal *sig = li->data;

		if (sig->signum == signum &&
		    pong_tags_equal (key, sig->key)) {
			sig->sfunc.text_notify (tag, key, text, sig->data);
		}
	}
}

void
tag_disconnect (XMLTag *tag, int id)
{
	GList *li;

	for (li = tag->signals; li != NULL; li = li->next) {
		XMLSignal *sig = li->data;

		if (sig->id == id) {
			tag->signals =
				g_list_remove_link (tag->signals, li);
			g_list_free_1 (li);
			signal_free (sig);
			return;
		}
	}
}

void
tag_disconnect_by_data (XMLTag *tag, gpointer data)
{
	GList *li, *list;

	list = g_list_copy (tag->signals);

	for (li = list; li != NULL; li = li->next) {
		XMLSignal *sig = li->data;

		if (sig->data == data) {
			tag->signals =
				g_list_remove (tag->signals, sig);
			signal_free (sig);
		}
	}

	g_list_free (list);
}

/* stolen from gconf itself */
static const gchar* 
value_type_to_string (GConfValueType type)
{
	switch (type) {
	case GCONF_VALUE_INT: return PONG_S_int;
	case GCONF_VALUE_STRING: return PONG_S_string;
	case GCONF_VALUE_FLOAT: return PONG_S_float;
	case GCONF_VALUE_BOOL: return PONG_S_bool;
	case GCONF_VALUE_SCHEMA: return PONG_S_schema;
	case GCONF_VALUE_LIST: return PONG_S_list;
	case GCONF_VALUE_PAIR: return PONG_S_pair;
	case GCONF_VALUE_INVALID: return "*invalid*";
	default:
		g_assert_not_reached ();
		return NULL; /* for warnings */
	}
}

/* ignores the "schema" type since that would mess us up anyway */
static GConfValueType
string_to_value_type (const char *string)
{
	if (string == NULL)
		return GCONF_VALUE_INVALID;
	else if (pong_strcasecmp_no_locale (string, PONG_S_int) == 0)
		return GCONF_VALUE_INT;
	else if (pong_strcasecmp_no_locale (string, PONG_S_string) == 0)
		return GCONF_VALUE_STRING;
	else if (pong_strcasecmp_no_locale (string, PONG_S_float) == 0)
		return GCONF_VALUE_FLOAT;
	else if (pong_strcasecmp_no_locale (string, PONG_S_bool) == 0)
		return GCONF_VALUE_BOOL;
	else if (pong_strcasecmp_no_locale (string, PONG_S_list) == 0)
		return GCONF_VALUE_LIST;
	else if (pong_strcasecmp_no_locale (string, PONG_S_pair) == 0)
		return GCONF_VALUE_PAIR;
	else
		return GCONF_VALUE_INVALID;
}

static void
make_new_schema_locale (XMLTag *tag,
			xmlNode *parent,
			const char *locale_name,
			const char *addendum)
{
	xmlNode *locale;
	char *text;

	locale = xmlNewChild (parent, NULL, PONG_S_locale, NULL);
	xmlNewProp (locale, PONG_S_name, locale_name);

	text = tag_get_text (tag, PONG_S_SchemaShortDescription, locale_name);
	if (text != NULL) {
		char *desc;
		desc = g_strconcat (text, addendum, NULL);
		xmlNewChild (locale, NULL, PONG_S_short, desc);
		g_free (desc);
	} else {
		xmlNewChild (locale, NULL, PONG_S_short, addendum);
	}
	g_free (text);

	text = tag_get_text (tag, PONG_S_SchemaLongDescription, locale_name);
	if (text != NULL) {
		char *desc;
		desc = g_strconcat (text, addendum, NULL);
		xmlNewChild (locale, NULL, PONG_S_long, desc);
		g_free (desc);
	} else {
		xmlNewChild (locale, NULL, PONG_S_long, addendum);
	}
	g_free (text);
}

static GList *
get_schema_locales (XMLTag *element)
{
	GList *locales = NULL;
	GList *list, *list2, *li;
       
	list = tag_get_tags (element, PONG_S_SchemaShortDescription);
	list2 = tag_get_tags (element, PONG_S_SchemaLongDescription);
	list = g_list_concat (list, list2);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *locale = tag_get_prop (tag, "xml:lang");

		if ( ! pong_string_empty (locale) &&
		    strcmp (locale, "C") != 0 &&
		    ! is_string_in_list (locales, locale)) {
			locales = g_list_prepend (locales, locale);
		} else {
			g_free (locale);
		}
	}
	tree_free_tag_list (list);

	return locales;
}

static void
write_schemas (XMLTree *tree, xmlNode *schemalist, const char *base_conf_path,
	       const char *addendum, const char *global_owner)
{
	GList *list, *li;

	list = tree_get_elements (tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *element = li->data;
		xmlNode *node;
		char *text, *key;
		PongType type;
		GConfValueType gconf_type;
		GList *locales;
		GList *li;

		text = tag_get_text (element, PONG_S_ConfPath, NULL);
		if (pong_string_empty (text)) {
			g_free (text);
			continue;
		}

		node = xmlNewChild (schemalist, NULL, PONG_S_schema, NULL);

		if (text[0] == '/') {
			key = text;
		} else if (base_conf_path != NULL) {
			key = g_concat_dir_and_file (base_conf_path, text);
			g_free (text);
		} else {
			key = g_strconcat ("/", text, NULL);
			g_free (text);
		}

		xmlNewChild (node, NULL, PONG_S_applyto, key);
		text = g_strconcat ("/schemas", key, NULL);
		xmlNewChild (node, NULL, PONG_S_key, text);
		g_free (text);
		g_free (key);

		text = tag_get_text (element, PONG_S_SchemaOwner, NULL);
		if (pong_string_empty (text)) {
			xmlNewChild (node, NULL, PONG_S_owner,
				     pong_sure_string (global_owner));
		} else {
			xmlNewChild (node, NULL, PONG_S_owner, text);
		}
		g_free (text);

		text = tag_get_text (element, PONG_S_Type, NULL);
		type = pong_type_from_string (text);
		g_free (text);

		gconf_type = pong_gconf_value_type_from_pong_type (type);
		xmlNewChild (node, NULL, PONG_S_type,
			     value_type_to_string (gconf_type));

		if (gconf_type == GCONF_VALUE_LIST) {
			gconf_type =
			     pong_list_gconf_value_type_from_pong_type (type);
			xmlNewChild (node, NULL, PONG_S_list_type,
				     value_type_to_string (gconf_type));
		} else if (gconf_type == GCONF_VALUE_PAIR) {
			gconf_type =
			     pong_car_gconf_value_type_from_pong_type (type);
			xmlNewChild (node, NULL, PONG_S_car_type,
				     value_type_to_string (gconf_type));
			gconf_type =
			     pong_cdr_gconf_value_type_from_pong_type (type);
			xmlNewChild (node, NULL, PONG_S_cdr_type,
				     value_type_to_string (gconf_type));
		}

		text = tag_get_text (element, PONG_S_Default, NULL);
		if ( ! pong_string_empty (text)) {
			char *value;
			GConfValue *def;

			def = pong_gconf_value_from_string (text, type);

			value = gconf_value_to_string (def);

			gconf_value_free (def);

			if ( ! pong_string_empty (value)) {
				xmlNewChild (node, NULL, PONG_S_default,
					     value);
			}

			g_free (value);
		}

		g_free (text);

		make_new_schema_locale (element, node, "C", addendum); 

		locales = get_schema_locales (element);
		for (li = locales; li != NULL; li = li->next) {
			char *locale = li->data;
			make_new_schema_locale (element, node, locale, addendum); 
		}
		tree_free_string_list (locales);
	}

	tree_free_tag_list (list);
}


/* Write schemas from the elements */
gboolean
tree_write_schemas (XMLTree *tree, const char *file)
{
	GList *list, *li;
	xmlDoc *doc;
	xmlNode *schemalist;
	char *base_conf_path, *schema_owner;
	XMLTag *root;
	gboolean success;

	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (file != NULL, FALSE);

	root = tree_root (tree);

	doc = xmlNewDoc ("1.0");

	doc->root = xmlNewDocNode (doc, NULL, PONG_S_gconfschemafile, NULL);
	schemalist = xmlNewChild (doc->root, NULL, PONG_S_schemalist, NULL);

	schema_owner = tag_get_text (root, PONG_S_SchemaOwner, NULL);

	/* default/no-user-level */
	base_conf_path = tag_get_text (root, PONG_S_BaseConfPath, NULL);

	write_schemas (tree, schemalist, base_conf_path, "", schema_owner);

	g_free (base_conf_path);

	list = tree_get_levels (tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *level = li->data;
		char *path = tag_get_text (level, PONG_S_BaseConfPath, NULL);
		char *name = tag_get_text (level, PONG_S_Name, NULL);
		char *addendum;

		/* do not translate, this is to be in "C" locale,
		 * yes we do need to get this somehow translated, BUT,
		 * that doesn't seem to be a 1.0 task */
		addendum = g_strdup_printf (" [userlevel: %s]", 
					    pong_sure_string (name));
		g_free (name);

		if ( ! pong_string_empty (path)) {
			write_schemas (tree, schemalist, path, addendum,
				       schema_owner);
		}

		g_free (path);
		g_free (addendum);
	}

	tree_free_tag_list (list);

	g_free (schema_owner);

	if (xmlSaveFile (file, doc) >= 0)
		success = TRUE;
	else
		success = FALSE;

	xmlFreeDoc (doc);

	tag_unref (root);

	return success;
}

/* subtracts the 'prefix' from 'full' and returns the remaining part
 * of full as a pointer to inside of 'full' or NULL if full doesn't
 * start with 'prefix'.  For example '/foo/bar' '/foo/' will result
 * in 'bar' and '/foo/bar' '/bar/' will result in NULL */
static const char *
subtract_string (const char *full, const char *prefix)
{
	const char *pf, *pp;

	if (full == NULL ||
	    prefix == NULL)
		return NULL;

	/* perhaps not the nicest of code really */
	for (pf = full, pp = prefix;
	     *pf != '\0' && *pp != '\0';
	     pp++, pf++) {
		if (*pf != *pp)
			return NULL;
	}

	/* if prefix ran out first, then we've succeeded */
	if (*pf != '\0')
		return pf;
	else
		return NULL;
}

static PongType
suck_type_and_default_from_schema (XMLTag *schema, GConfValue **def)
{
	char *type_s, *list_type_s, *car_type_s, *cdr_type_s, *def_s;
	GConfValueType type, list_type, car_type, cdr_type;

	*def = NULL;

	type_s = tag_get_text (schema, PONG_S_type, NULL);
	list_type_s = tag_get_text (schema, PONG_S_list_type, NULL);
	car_type_s = tag_get_text (schema, PONG_S_car_type, NULL);
	cdr_type_s = tag_get_text (schema, PONG_S_cdr_type, NULL);
	def_s = tag_get_text (schema, PONG_S_default, NULL);

	type = string_to_value_type (type_s);
	list_type = string_to_value_type (list_type_s);
	car_type = string_to_value_type (car_type_s);
	cdr_type = string_to_value_type (cdr_type_s);

	switch (type) {
	case GCONF_VALUE_LIST:
		if (list_type != GCONF_VALUE_LIST &&
		    list_type != GCONF_VALUE_PAIR &&
		    list_type != GCONF_VALUE_INVALID &&
		    def_s != NULL)
			*def = gconf_value_new_list_from_string (list_type,
								 def_s,
								 NULL);
		break;
	case GCONF_VALUE_PAIR:
		if (car_type != GCONF_VALUE_LIST &&
		    car_type != GCONF_VALUE_PAIR &&
		    car_type != GCONF_VALUE_INVALID &&
		    cdr_type != GCONF_VALUE_LIST &&
		    cdr_type != GCONF_VALUE_PAIR &&
		    cdr_type != GCONF_VALUE_INVALID &&
		    def_s != NULL)
			*def = gconf_value_new_pair_from_string (car_type,
								 cdr_type,
								 def_s,
								 NULL);
		break;
	default:
		if (type != GCONF_VALUE_INVALID &&
		    def_s != NULL)
			*def = gconf_value_new_from_string (type,
							    def_s,
							    NULL);
		break;
	}

	g_free (type_s);
	g_free (list_type_s);
	g_free (car_type_s);
	g_free (cdr_type_s);
	g_free (def_s);

	return pong_type_from_gconf_types (type, list_type,
					   car_type, cdr_type);
}

static void
import_a_schema_locale (gpointer data, gpointer user_data)
{
	XMLTag *locale = data;
	XMLTag *element = user_data;
	char *name;
	char *short_s, *long_s;

	name = tag_get_prop (locale, PONG_S_name);

	/* XXX: only imports the "C" locale on purpose */
	if (name == NULL ||
	    strcmp (name, "C") == 0) {
		short_s = tag_get_text (locale, PONG_S_short, NULL);
		long_s = tag_get_text (locale, PONG_S_long, NULL);

		tag_set_text (element, "_" PONG_S_SchemaShortDescription, short_s, NULL);
		tag_set_text (element, "_" PONG_S_SchemaLongDescription, long_s, NULL);
		g_free (short_s);
		g_free (long_s);
	}

	g_free (name);
}

static void
import_a_schema (gpointer data, gpointer user_data)
{
	XMLTag *schema = data;
	XMLTree *tree = user_data;
	XMLTag *root;
	char *applyto;
	char *owner;
	PongType type;
	XMLTag *element;
	GList *locales;
	GConfValue *defval;

	root = tree_root (tree);

	/*FIXME: check applyto against key, if they really match */

	applyto = tag_get_text (schema, PONG_S_applyto, NULL);

	if (applyto == NULL) {
		tag_unref (root);
		return;
	}

	element = tree_get_element (tree, applyto);
	if (element == NULL) {
		const char *tempkey;
		char *baseconfpath;

		baseconfpath = tag_get_text (root, PONG_S_BaseConfPath, NULL);

		tempkey = subtract_string (applyto, baseconfpath);

		if (tempkey != NULL)
			element = tree_get_element (tree, tempkey);

		g_free (baseconfpath);

		if (element == NULL) {
			element = tree_add_element (tree);
			tag_set_text (element, PONG_S_ConfPath, applyto, NULL);
		}
	}

	g_free (applyto);

	/* owner */
	owner = tag_get_text (schema, PONG_S_owner, NULL);
	if (owner != NULL) {
		tag_set_text (element, PONG_S_SchemaOwner, owner, NULL);
		g_free (owner);
	}

	/* type and default */
	defval = NULL;
	type = suck_type_and_default_from_schema (schema, &defval);

	/* default to string */
	if (type == PONG_TYPE_INVALID)
		type = PONG_TYPE_STRING;
	tree_element_set_pong_type (element, type);

	if (defval != NULL) {
		char *defstring = pong_string_from_gconf_value (defval);
		gconf_value_free (defval);
		tag_set_text (element, PONG_S_Default, defstring, NULL);
		g_free (defstring);
	}

	/* locale foo stuff */
	locales = tag_get_tags (schema, PONG_S_locale);

	/* XXX: only imports the "C" locale on purpose */
	g_list_foreach (locales, import_a_schema_locale, element);

	tree_free_tag_list (locales);

	tag_unref (root);
}

gboolean
tree_import_schemas (XMLTree *tree,
		     XMLTree *schemas)
{
	XMLTag *sch_root;
	GList *list, *li;
	GList *schemalist;

	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (schemas != NULL, FALSE);

	sch_root = tree_root (schemas);

	if (sch_root == NULL)
		return FALSE;

	list = tag_get_tags (sch_root, PONG_S_schemalist);

	tag_unref (sch_root);

	if (list == NULL)
		return FALSE;

	schemalist = NULL;
	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		schemalist = g_list_concat (schemalist,
					    tag_get_tags (tag,
							  PONG_S_schema));
	}

	tree_free_tag_list (list);

	g_list_foreach (schemalist, import_a_schema, tree);

	tree_free_tag_list (schemalist);

	return TRUE;
}
