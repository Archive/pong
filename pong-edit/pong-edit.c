/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000,2001 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include "../bonobo/pong-bonobo.h"
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <liboaf/liboaf.h>

#include "tree.h"
#include "levels.h"
#include "elements.h"
#include "panes.h"
#include "glade-helper.h"

#include "pong-edit.h"

GladeXML *app_xml = NULL;
GnomeApp *app = NULL;
GtkWidget *elements_clist = NULL;
GtkWidget *levels_clist = NULL;
GtkWidget *panes_ctree = NULL;
GtkWidget *pane_options_clist = NULL;
GtkWidget *pane_arguments_clist = NULL;
GtkWidget *element_sensitivities_clist = NULL;

static char *the_filename = NULL;
gboolean file_changed = FALSE;

static GtkWidget *base_conf_path;
static GtkWidget *help_name;
static GtkWidget *help_path;
static GtkWidget *dialog_title;
static GtkWidget *schema_owner;

/* widgets to destroy when the file is cleared */
static GList *to_kill_windows = NULL;

XMLTree *xml_tree = NULL;

void file_new			(GtkWidget *menuitem);
void file_open			(GtkWidget *menuitem);
void file_import_schemas	(GtkWidget *menuitem);
void file_save			(GtkWidget *menuitem);
void file_save_as		(GtkWidget *menuitem);
void file_exit			(GtkWidget *menuitem);

gboolean delete_event		(GtkWidget *w, GdkEvent *event);

void test_with_gconf		(GtkWidget *widget);
void test_without_gconf		(GtkWidget *widget);

void export_schema		(GtkWidget *menuitem);

void show_help_manual		(GtkWidget *menuitem);
void run_about_dialog		(GtkWidget *menuitem);


/* This function is from pong, but isn't in the headers
 * (for an obvious reason if you read it) */
gboolean pong_dlg_keypress (GtkWidget *widget, GdkEventKey *event);

void
setup_dlg (GtkWidget *dlg)
{
	gtk_signal_connect (GTK_OBJECT (dlg),
			    "key_press_event",
			    GTK_SIGNAL_FUNC (pong_dlg_keypress),
			    NULL);
}

static void
setup_title (void)
{
	char *s;
	if (app == NULL)
		return;

	if (the_filename != NULL)
		s = g_strdup_printf (_("PonG-Edit: %s"), the_filename);
	else
		s = g_strdup (_("PonG-Edit"));

	gtk_window_set_title (GTK_WINDOW (app), s);

	g_free (s);
}

void
run_about_dialog (GtkWidget *menuitem)
{
	static GtkWidget *dialog = NULL;

	if (dialog != NULL) {
		gtk_widget_show_now (dialog);
		gdk_window_raise (dialog->window);
		return;
	}

	dialog = glade_helper_load_widget ("pong-edit.glade", "about",
					   gnome_about_get_type ());


	gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (app));

	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &dialog);
}

void
show_help_manual (GtkWidget *menuitem)
{
	GnomeHelpMenuEntry help_entry = { "pong", "index.html" };

	gnome_help_display (NULL, &help_entry);
}

static gboolean
kill_save (void)
{
	if (xml_tree == NULL)
		return TRUE;

	if (the_filename == NULL) {
		GtkWidget *w = gnome_error_dialog (_("No filename specified, "
						     "please choose "
						     "\"File/Save As\""));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return FALSE;
	}

	if ( ! tree_write (xml_tree, the_filename)) {
		char *s = g_strdup_printf (_("File '%s' cannot be saved"),
					   the_filename);
		GtkWidget *w = gnome_error_dialog (s);
		g_free (s);
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return FALSE;
	} else {
		return TRUE;
	}
}

static gboolean
kill_file (GtkWidget *parent)
{
	GtkWidget *w;
	int result;

	if (xml_tree == NULL ||
	     ! file_changed)
		return TRUE;

	w = gnome_message_box_new (_("File not saved, continue?\n"
				     "(if you continue, changes will be lost)"),
				   GNOME_MESSAGE_BOX_QUESTION,
				   _("Continue"),
				   _("Save"),
				   GNOME_STOCK_BUTTON_CANCEL,
				   NULL, NULL);
	if (parent != NULL)
		gnome_dialog_set_parent (GNOME_DIALOG (w),
					 GTK_WINDOW (parent));
	setup_dlg (w);

	result = gnome_dialog_run (GNOME_DIALOG (w));
	switch (result) {
	case 1 /*save*/:
		return kill_save ();
	case 0 /*continue*/:
		return TRUE;
	default /*cancel*/:
		return FALSE;
	}
}

static void
clear_window (void)
{
	GList *list;

	list = to_kill_windows;
	to_kill_windows = NULL;

	g_list_foreach (list, (GFunc)gtk_widget_destroy, NULL);
	g_list_free (list);

	clear_levels_list ();
	clear_elements_list ();
	clear_panes_list ();

	gtk_entry_set_text (GTK_ENTRY (base_conf_path), "");
	gtk_entry_set_text (GTK_ENTRY (help_name), "");
	gtk_entry_set_text (GTK_ENTRY (help_path), "");
	gtk_entry_set_text (GTK_ENTRY (dialog_title), "");
	gtk_entry_set_text (GTK_ENTRY (schema_owner), "");
}

static void
setup_window (void)
{
	char *text;
	XMLTag *root;

	if (xml_tree == NULL)
		return;

	root = tree_root (xml_tree);

	setup_elements_list (root);
	setup_levels_list (root);
	setup_panes_list (root);

	text = tag_get_text (root, PONG_S_BaseConfPath, NULL);
	gtk_entry_set_text (GTK_ENTRY (base_conf_path),
			    pong_sure_string (text));
	g_free (text);

	text = tag_get_text (root, PONG_S_Help "/" PONG_S_Name, NULL);
	gtk_entry_set_text (GTK_ENTRY (help_name),
			    pong_sure_string (text));
	g_free (text);

	text = tag_get_text (root, PONG_S_Help "/" PONG_S_Path, NULL);
	gtk_entry_set_text (GTK_ENTRY (help_path),
			    pong_sure_string (text));
	g_free (text);

	text = tag_get_text (root, PONG_S_DialogTitle, NULL);
	gtk_entry_set_text (GTK_ENTRY (dialog_title),
			    pong_sure_string (text));
	g_free (text);

	text = tag_get_text (root, PONG_S_SchemaOwner, NULL);
	gtk_entry_set_text (GTK_ENTRY (schema_owner),
			    pong_sure_string (text));
	g_free (text);

	tag_unref (root);
}

void
file_new (GtkWidget *menuitem)
{
	if ( ! kill_file (GTK_WIDGET (app)))
		return;

	if (xml_tree != NULL) {
		tree_kill (xml_tree, TRUE);
		tree_unref (xml_tree);
		xml_tree = NULL;
	}

	clear_window ();

	xml_tree = tree_new (PONG_S_PongElements);
	g_free (the_filename);
	the_filename = NULL;
	setup_title ();

	/* Do before checking for changes, we fill in
	 * with some defaults */
	{
		XMLTag *root;
		
		root = tree_root (xml_tree);

		tag_set_text (root, PONG_S_BaseConfPath, "/apps/gnome-foo/", NULL);
		/* Do not translate !
		 * This should be in C locale */
		tag_set_text (root, "_" PONG_S_DialogTitle, "GNOME Foo Preferences", NULL);
		tag_set_text (root, PONG_S_SchemaOwner, "gnome-foo", NULL);

		tag_unref (root);
	}

	setup_window ();

	tree_set_changed_var (xml_tree, &file_changed);
	file_changed = FALSE;
}

static void
file_open_really (GtkWidget *w, gpointer data)
{
	GtkFileSelection *fsel = data;
	XMLTree *tree;
	char *filename;

	filename = gtk_file_selection_get_filename (fsel);

	if ( ! pong_file_exists (filename)) {
		GtkWidget *w = gnome_error_dialog (_("File does not exist"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (fsel));
		return;
	}

	tree = tree_load (filename, PONG_S_PongElements);

	if (tree == NULL) {
		GtkWidget *w = gnome_error_dialog (_("File cannot be loaded"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (fsel));
		return;
	}

	if (xml_tree != NULL) {
		tree_kill (xml_tree, TRUE);
		tree_unref (xml_tree);
		xml_tree = NULL;
	}

	clear_window ();

	xml_tree = tree;
	g_free (the_filename);
	the_filename = g_strdup (filename);
	setup_title ();

	setup_window ();

	tree_set_changed_var (xml_tree, &file_changed);
	file_changed = FALSE;

	gtk_widget_destroy (GTK_WIDGET (fsel));

	if ( ! tree_i18n_sanitize (tree)) {
		GtkWidget *w = gnome_warning_dialog
			(_("The file that was loaded contains xml:lang\n"
			   "properties.  This would mean that it is a file\n"
			   "generated by xml-i18n-tools.  You probably want\n"
			   "to edit the .pong.in file and not the .pong file.\n"
			   "PonG-Edit cannot directly edit translated files\n"
			   "properly."));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
	}
}

void
file_open (GtkWidget *menuitem)
{
	static GtkWidget *dialog = NULL;
	GtkFileSelection *fsel;

	if ( ! kill_file (GTK_WIDGET (app)))
		return;

	if (dialog != NULL) {
		gtk_widget_show_now (dialog);
		gdk_window_raise (dialog->window);
		return;
	}

	dialog = gtk_file_selection_new (_("Open"));
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (app));
	fsel = GTK_FILE_SELECTION (dialog);
	setup_dlg (dialog);

	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &dialog);

	gtk_signal_connect (GTK_OBJECT (fsel->ok_button), "clicked",
			    GTK_SIGNAL_FUNC (file_open_really),
			    fsel);

	gtk_signal_connect_object (GTK_OBJECT (fsel->cancel_button), "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (fsel));

	gtk_widget_show (dialog);
}

static void
file_import_schemas_really (GtkWidget *w, gpointer data)
{
	GtkFileSelection *fsel = data;
	XMLTree *tree;
	char *filename;
	gboolean success;

	filename = gtk_file_selection_get_filename (fsel);

	if ( ! pong_file_exists (filename)) {
		GtkWidget *w = gnome_error_dialog (_("File does not exist"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (fsel));
		return;
	}

	tree = tree_load (filename, PONG_S_gconfschemafile);

	if (tree == NULL) {
		GtkWidget *w = gnome_error_dialog (_("File cannot be loaded"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (fsel));
		return;
	}

	if (xml_tree == NULL) {
		/* basically ensure that we have an XMLTree of the elements */
		file_new (NULL);
	}

	success = tree_import_schemas (xml_tree, tree);
	if ( ! success) {
		GtkWidget *w = gnome_error_dialog (_("Cannot import schemas"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (fsel));
	}

	tree_kill (tree, TRUE);
	tree_unref (tree);

	if (success)
		gtk_widget_destroy (GTK_WIDGET (fsel));
}

void
file_import_schemas (GtkWidget *menuitem)
{
	static GtkWidget *dialog = NULL;
	GtkWidget *w;
	GtkFileSelection *fsel;

	if ( ! kill_file (GTK_WIDGET (app)))
		return;

	if (dialog != NULL) {
		gtk_widget_show_now (dialog);
		gdk_window_raise (dialog->window);
		return;
	}

	w = gnome_ok_dialog (_("Note that importing gconf schema files "
			       "is not,\n"
			       "and will not ever be perfect.  It is only "
			       "meant\n"
			       "as a timesaver when converting to pong,\n"
			       "or using pong to edit .schema files."));
	setup_dlg (w);
	gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
	gnome_dialog_run (GNOME_DIALOG (w));

	dialog = gtk_file_selection_new (_("Import schemas"));
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (app));
	fsel = GTK_FILE_SELECTION (dialog);
	setup_dlg (dialog);

	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &dialog);

	gtk_signal_connect (GTK_OBJECT (fsel->ok_button), "clicked",
			    GTK_SIGNAL_FUNC (file_import_schemas_really),
			    fsel);

	gtk_signal_connect_object (GTK_OBJECT (fsel->cancel_button), "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (fsel));

	gtk_widget_show (dialog);
}


static void
call_level_func (GtkWidget *w, gpointer data)
{
	/* we must strdup this as we kill the dialog
	 * in this function */
	char *level = g_strdup (data);
	void (* func) (const char *level) =
		gtk_object_get_data (GTK_OBJECT (w), "func");
	GtkWidget *dlg = 
		gtk_object_get_data (GTK_OBJECT (w), "dlg");

	g_assert (func != NULL);

	gnome_dialog_close (GNOME_DIALOG (dlg));

	func (level);

	g_free (level);
}

static void
run_levels_dialog (void (* func) (const char *level))
{
	GList *list, *li;

	list = tree_get_levels (xml_tree);

	if (list == NULL) {
		/* No levels */
		func (NULL);
	} else {
		GtkWidget *dlg, *w;
		GtkBox *box;

		dlg = gnome_dialog_new (_("Select a level"),
					GNOME_STOCK_BUTTON_CANCEL,
					NULL);
		gnome_dialog_set_close (GNOME_DIALOG (dlg),
					TRUE /*auto_close*/);
		gnome_dialog_set_parent (GNOME_DIALOG (dlg),
					 GTK_WINDOW (app));
		setup_dlg (dlg);

		box = GTK_BOX (GNOME_DIALOG (dlg)->vbox);

		to_kill_windows = g_list_prepend (to_kill_windows, dlg);

		w = gtk_label_new (_("Select a level:"));
		gtk_box_pack_start (box, w, FALSE, FALSE, 0);

		w = gtk_button_new_with_label (_("Default"));
		gtk_box_pack_start (box, w, FALSE, FALSE, 0);
		gtk_object_set_data (GTK_OBJECT (w), "func", func);
		gtk_object_set_data (GTK_OBJECT (w), "dlg", dlg);
		gtk_signal_connect (GTK_OBJECT (w), "clicked",
				    GTK_SIGNAL_FUNC (call_level_func),
				    NULL);

		for (li = list; li != NULL; li = li->next) {
			XMLTag *tag = li->data;
			char *name = tag_get_text (tag, PONG_S_Name, NULL);

			if (name == NULL)
				continue;

			w = gtk_button_new_with_label (name);
			gtk_box_pack_start (box, w, FALSE, FALSE, 0);
			gtk_object_set_data (GTK_OBJECT (w), "func", func);
			gtk_object_set_data (GTK_OBJECT (w), "dlg", dlg);
			gtk_signal_connect_full (GTK_OBJECT (w), "clicked",
						 GTK_SIGNAL_FUNC (call_level_func),
						 NULL /* marshal */,
						 name,
						 (GtkDestroyNotify) g_free,
						 FALSE /* object_signal */,
						 FALSE /* after */);
		}

		tree_free_tag_list (list);

		gtk_widget_show_all (dlg);
	}
}

static void
run_level_with_gconf (const char *level)
{
	GtkWidget *w;
	char *memory = NULL;
	int size = 0;
	GtkWidget *dlg;
	PongXML *xml;

	if (xml_tree == NULL)
		return;

	tree_get_xml_memory (xml_tree, &memory, &size);

	if (memory == NULL) {
		w = gnome_error_dialog (_("Cannot create XML memory"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return;
	}

	xml = pong_xml_new_from_memory (memory, size);
	tree_free_xml_memory (memory);
	if (xml == NULL) {
		w = gnome_error_dialog (_("Cannot load config dialog"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return;
	}

	if (level != NULL)
		pong_xml_set_level (xml, level);

	pong_xml_show_dialog (xml);
	dlg = pong_xml_get_dialog_widget (xml);

	gtk_object_unref (GTK_OBJECT (xml));

	if (dlg == NULL) {
		w = gnome_error_dialog (_("Cannot run config dialog"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
	}
}


void
test_with_gconf (GtkWidget *widget)
{
	if (xml_tree == NULL)
		return;

	run_levels_dialog (run_level_with_gconf);
}

static void
run_level_no_gconf (const char *level)
{
	GtkWidget *w;
	PongFile *file;
	PongUI *ui;
	char *memory = NULL;
	int size = 0;

	if (xml_tree == NULL)
		return;

	tree_get_xml_memory (xml_tree, &memory, &size);

	if (memory == NULL) {
		w = gnome_error_dialog (_("Cannot create XML memory"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return;
	}

	file = pong_file_load_xml_from_memory (memory, size);

	tree_free_xml_memory (memory);

	if (file == NULL) {
		w = gnome_error_dialog (_("Cannot parse XML "
					  "buffer, strange!"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return;
	}

	ui = pong_ui_internal_new ();
	pong_ui_internal_add_default_plugs (PONG_UI_INTERNAL (ui));
	pong_ui_load_dialog (ui, file, level);

	gtk_object_unref (GTK_OBJECT (ui));

	pong_file_unref (file);
}

void
test_without_gconf (GtkWidget *widget)
{
	GtkWidget *w;

	if (xml_tree == NULL)
		return;

	w = gnome_ok_dialog (_("Note that sensitivities do not work when\n"
			       "testing without gconf, it is meant only\n"
			       "to test the dialog layout"));
	setup_dlg (w);
	gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
	gnome_dialog_run (GNOME_DIALOG (w));

	run_levels_dialog (run_level_no_gconf);
}

static const char *
saver_routine (GtkFileSelection *fsel, gboolean (* saver_func) (XMLTree *tree, const char *fname))
{
	char *filename;

	if (xml_tree == NULL)
		return NULL;

	filename = gtk_file_selection_get_filename (fsel);

	if (pong_file_exists (filename)) {
		GtkWidget *w;
		char *s;

		s = g_strdup_printf (_("File %s exists, overwrite?"),
				     filename);
		w = gnome_question_dialog_parented (s, NULL, NULL,
						    GTK_WINDOW (fsel));
		setup_dlg (w);
		g_free (s);

		if (gnome_dialog_run (GNOME_DIALOG (w)) != 0)
			return NULL;
	}

	if ( ! saver_func (xml_tree, filename)) {
		GtkWidget *w = gnome_error_dialog (_("File cannot be written"));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (fsel));
		return NULL;
	}

	return filename;
}

static void
export_schema_really (GtkWidget *w, gpointer data)
{
	GtkFileSelection *fsel = data;
	const char *file;

	file = saver_routine (fsel, tree_write_schemas);

	if (file != NULL) {
		/* saved all ok */
		gtk_widget_destroy (GTK_WIDGET (fsel));
	}
}

void
export_schema (GtkWidget *menuitem)
{
	static GtkWidget *dialog = NULL;
	GtkFileSelection *fsel;

	if (xml_tree == NULL)
		return;

	if (dialog != NULL) {
		gtk_widget_show_now (dialog);
		gdk_window_raise (dialog->window);
		return;
	}

	dialog = gtk_file_selection_new (_("Export Schemas"));
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (app));
	fsel = GTK_FILE_SELECTION (dialog);
	setup_dlg (dialog);

	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &dialog);

	gtk_signal_connect (GTK_OBJECT (fsel->ok_button), "clicked",
			    GTK_SIGNAL_FUNC (export_schema_really),
			    fsel);

	gtk_signal_connect_object (GTK_OBJECT (fsel->cancel_button), "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (fsel));

	gtk_widget_show (dialog);
}

void
file_save (GtkWidget *menuitem)
{
	if (xml_tree == NULL)
		return;

	if (the_filename == NULL) {
		file_save_as (menuitem);
		return;
	}

	if ( ! tree_write (xml_tree, the_filename)) {
		char *s = g_strdup_printf (_("File '%s' cannot be saved"),
					   the_filename);
		GtkWidget *w = gnome_error_dialog (s);
		g_free (s);
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
	} else {
		file_changed = FALSE;
	}
}

static void
file_save_as_really (GtkWidget *w, gpointer data)
{
	GtkFileSelection *fsel = data;
	const char *file;

	file = saver_routine (fsel, tree_write);

	if (file != NULL) {
		/* saved all ok */
		file_changed = FALSE;

		g_free (the_filename);
		the_filename = g_strdup (file);
		setup_title ();

		gtk_widget_destroy (GTK_WIDGET (fsel));
	}
}

void
file_save_as (GtkWidget *menuitem)
{
	static GtkWidget *dialog = NULL;
	GtkFileSelection *fsel;

	if (xml_tree == NULL)
		return;

	if (dialog != NULL) {
		gtk_widget_show_now (dialog);
		gdk_window_raise (dialog->window);
		return;
	}

	dialog = gtk_file_selection_new (_("Save As"));
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (app));
	fsel = GTK_FILE_SELECTION (dialog);
	setup_dlg (dialog);

	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &dialog);

	gtk_signal_connect (GTK_OBJECT (fsel->ok_button), "clicked",
			    GTK_SIGNAL_FUNC (file_save_as_really),
			    fsel);

	gtk_signal_connect_object (GTK_OBJECT (fsel->cancel_button), "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (fsel));

	gtk_widget_show (dialog);
}

void
file_exit (GtkWidget *menuitem)
{
	if (kill_file (GTK_WIDGET (app)))
		gtk_main_quit ();
}

gboolean
delete_event (GtkWidget *w, GdkEvent *event)
{
	if (kill_file (GTK_WIDGET (app)))
		return FALSE;
	else
		return TRUE;
}

static const struct poptOption options[] = {
	{ NULL } 
};

static gboolean
initial_load (const char *filename)
{
	XMLTree *tree;

	if (pong_string_empty (filename)) {
		return FALSE;
	}

	if ( ! pong_file_exists (filename)) {
		char *s = g_strdup_printf (_("File '%s' does not exist"),
					   filename);
		GtkWidget *w = gnome_error_dialog (s);
		g_free (s);
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return FALSE;
	}

	tree = tree_load (filename, PONG_S_PongElements);

	if (tree == NULL) {
		char *s = g_strdup_printf (_("File '%s' cannot be loaded"),
					   filename);
		GtkWidget *w = gnome_error_dialog (s);
		g_free (s);
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
		gnome_dialog_run (GNOME_DIALOG (w));
		return FALSE;
	}

	if (xml_tree != NULL) {
		tree_kill (xml_tree, TRUE);
		tree_unref (xml_tree);
		xml_tree = NULL;
	}

	clear_window ();

	xml_tree = tree;

	g_free (the_filename);
	the_filename = g_strdup (filename);
	setup_title ();

	setup_window ();

	tree_set_changed_var (xml_tree, &file_changed);
	file_changed = FALSE;

	if ( ! tree_i18n_sanitize (tree)) {
		GtkWidget *w = gnome_warning_dialog
			(_("The file that was loaded contains xml:lang\n"
			   "properties.  This would mean that it is a file\n"
			   "generated by xml-i18n-tools.  You probably want\n"
			   "to edit the .pong.in file and not the .pong file.\n"
			   "PonG-Edit cannot directly edit translated files\n"
			   "properly."));
		setup_dlg (w);
		gnome_dialog_set_parent (GNOME_DIALOG (w), GTK_WINDOW (app));
	}

	return TRUE;
}

static void
root_entry_changed (GtkWidget *entry, gpointer data)
{
	const char *path = data;
	char *text;
	XMLTag *root;

	if (xml_tree == NULL)
		return;

	root = tree_root (xml_tree);

	text = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

	if (text != NULL)
		g_strstrip (text);

	if (pong_string_empty (text))
		tag_set_text (root, path, NULL, NULL);
	else
		tag_set_text (root, path, text, NULL);

	if (strcmp (path, PONG_S_SchemaOwner) == 0) {
		default_owner_changed (text);
	}

	g_free (text);

	tag_unref (root);
}

static void
setup_auto_resize (GtkWidget *widget)
{
	int i;
	GtkCList *clist = GTK_CLIST (widget);

	for (i = 0; i < clist->columns; i++) {
		gtk_clist_set_column_auto_resize (clist, i, TRUE);
	}
}

static void
setup_bonobo_combo (void)
{
	CORBA_Environment ev;
	const char *query;
        OAF_ServerInfoList *oaf_result;
	guint i;
	GList *list;
	GtkWidget *combo;

	CORBA_exception_init (&ev);

	query = "repo_ids.has ('IDL:Bonobo/Control:1.0') AND "
		"repo_ids.has ('IDL:GNOME/PonG/ControlInterface:1.0')";
	oaf_result = oaf_query (query, NULL, &ev);
		
	list = NULL;

        if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL) {
		for (i = 0; i < oaf_result->_length; i++) {
			OAF_ServerInfo *info = &oaf_result->_buffer[i];
			char *iid_query;

			iid_query = g_strdup (info->iid);

			list = g_list_prepend (list, iid_query);
		}
		list = g_list_reverse (list);
	} 

	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}
	
	CORBA_exception_free (&ev);

	/* just sanity */
	list = g_list_prepend (list, g_strdup (""));

	combo = glade_helper_get (app_xml, "bonobo_query_combo",
				  gtk_combo_get_type ());
	gtk_combo_set_popdown_strings (GTK_COMBO (combo), list);

	/* hmmm ... should go to utils, and not be in tree I suppose */
	tree_free_string_list (list);
}


int
main (int argc, char *argv[])
{
	CORBA_ORB orb;
	poptContext ctx;
	const char **args;
	const char *file;
	GError *error = NULL;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	gnome_init_with_popt_table ("pong-edit", VERSION,
				    argc, argv, options, 0, &ctx);

	/* Initialize components and libraries we use */
	orb = oaf_init (argc, argv);
	bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);

	if ( ! gconf_init (argc, argv, &error)) {
		g_assert (error != NULL);
		g_warning (_("GConf init failed:\n  %s"), error->message);
		g_error_free (error);
		error = NULL;
		return 1;
	}

	/* Hackish workaround a GConf bug.  When the client would be
	 * unreffed, next time it would be created we would die, sending
	 * a patch to havoc, but for now we must do this to work with the
	 * version that came with 1.4 */
	(void)gconf_client_get_default ();

	gdk_rgb_init ();
	glade_gnome_init ();
	pong_glade_init ();
	pong_bonobo_init ();

	args = poptGetArgs (ctx);

	if (args != NULL &&
	    args[0] != NULL &&
	    args[1] != NULL) {
		g_print (_("You can only supply a single argument with the "
			   ".pong file to use\n"));
		return 1;
	}

	if (args != NULL)
		file = args[0];
	else
		file = NULL;

	pong_add_glade_directory (GLADE_DIR);

	app_xml = glade_helper_load ("pong-edit.glade", "app", 
				     gnome_app_get_type (), FALSE);

	setup_bonobo_combo ();

	app = (GnomeApp *)glade_helper_get (app_xml, "app",
					    gnome_app_get_type ());
	elements_clist = glade_helper_get_clist (app_xml, "keys_clist",
						 GTK_TYPE_CLIST, 3);
	setup_auto_resize (elements_clist);
	levels_clist = glade_helper_get_clist (app_xml, "levels_clist",
					       GTK_TYPE_CLIST, 2);
	setup_auto_resize (levels_clist);
	panes_ctree = glade_helper_get_clist (app_xml, "panes_ctree",
					      GTK_TYPE_CTREE, 1);
	pane_arguments_clist =
		glade_helper_get_clist (app_xml, "pane_arguments_clist",
					GTK_TYPE_CLIST, 3);
	setup_auto_resize (pane_arguments_clist);
	pane_options_clist =
		glade_helper_get_clist (app_xml, "pane_options_clist",
					GTK_TYPE_CLIST, 2);
	setup_auto_resize (pane_options_clist);
	element_sensitivities_clist =
		glade_helper_get_clist (app_xml, "element_sensitivities_clist",
					GTK_TYPE_CLIST, 3);
	setup_auto_resize (element_sensitivities_clist);
	base_conf_path = glade_helper_get (app_xml, "base_conf_path",
					   GTK_TYPE_ENTRY);
	gtk_signal_connect (GTK_OBJECT (base_conf_path), "changed",
			    GTK_SIGNAL_FUNC (root_entry_changed),
			    PONG_S_BaseConfPath);
	help_name = glade_helper_get (app_xml, "help_name",
				      GTK_TYPE_ENTRY);
	gtk_signal_connect (GTK_OBJECT (help_name), "changed",
			    GTK_SIGNAL_FUNC (root_entry_changed),
			    PONG_S_Help "/" PONG_S_Name);
	help_path = glade_helper_get (app_xml, "help_path",
				      GTK_TYPE_ENTRY);
	gtk_signal_connect (GTK_OBJECT (help_path), "changed",
			    GTK_SIGNAL_FUNC (root_entry_changed),
			    PONG_S_Help "/" PONG_S_Path);
	dialog_title = glade_helper_get (app_xml, "dialog_title",
					 GTK_TYPE_ENTRY);
	gtk_signal_connect (GTK_OBJECT (dialog_title), "changed",
			    GTK_SIGNAL_FUNC (root_entry_changed),
			    "_" PONG_S_DialogTitle);
	schema_owner = glade_helper_get (app_xml, "schema_owner",
					 GTK_TYPE_ENTRY);
	gtk_signal_connect (GTK_OBJECT (schema_owner), "changed",
			    GTK_SIGNAL_FUNC (root_entry_changed),
			    PONG_S_SchemaOwner);

	signals_connect_element_edit_area ();

	glade_xml_signal_autoconnect (app_xml);

	if ( ! initial_load (file)) {
		/* a new file if we can't load anything */
		file_new (NULL);
	}

	gtk_main ();

	gtk_object_unref (GTK_OBJECT (app_xml));

	return 0;
}
