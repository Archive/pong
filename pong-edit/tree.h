/* Pong
 *   Pong XML tree API
 * Author: George Lebl
 * (c) 2000,2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef TREE_H
#define TREE_H

#include "../pong/pong-type.h"

/* Very high level interface, that's probably incredibly inefficent
 * underneath, but it doesn't need to be fast */

/* We also need to do some sanitization of the tree somewhere I guess */

typedef struct _XMLTree XMLTree;
typedef struct _XMLTag XMLTag;

/* This should do verification as well */
XMLTree *	tree_load		(const char *file,
					 const char *toplevel);
XMLTree *	tree_new		(const char *toplevel);
void		tree_kill		(XMLTree *tree,
					 gboolean silent);

void		tree_set_changed_var	(XMLTree *tree,
					 gboolean *changed);

XMLTree *	tree_ref		(XMLTree *tree);
void		tree_unref		(XMLTree *tree);

void		tree_get_xml_memory	(XMLTree *tree,
					 char **memory,
					 int *size);
void		tree_free_xml_memory	(char *memory);

gboolean	tree_write		(XMLTree *tree,
					 const char *file);

/* XXX: Write schemas from the elements */
gboolean	tree_write_schemas	(XMLTree *tree,
					 const char *file);

gboolean	tree_import_schemas	(XMLTree *tree,
					 XMLTree *schemas);

XMLTag *	tree_root		(XMLTree *tree);

XMLTag *	tag_ref			(XMLTag *tag);
void		tag_unref		(XMLTag *tag);

const char *	tag_peek_type		(XMLTag *tag);

GList *		tag_get_tags		(XMLTag *parent,
					 const char *type);
GList *		tag_get_tags_multiple	(XMLTag *parent,
					 const GList *types);

typedef void (* XMLDestroyNotify) (gpointer data);
typedef void (* XMLTagNotify) (XMLTag *tag, gpointer data);
typedef void (* XMLTagRefNotify) (XMLTag *tag,
				  XMLTag *reference,
				  gpointer data);
typedef void (* XMLTagTextNotify) (XMLTag *tag,
				   const char *key,
				   const char *text,
				   gpointer data);

int		tag_connect_kill	(XMLTag *tag,
					 XMLTagNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);
int		tag_connect_text	(XMLTag *tag,
					 const char *key,
					 XMLTagTextNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);
int		tag_connect_move_up	(XMLTag *tag,
					 XMLTagNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);
int		tag_connect_move_down	(XMLTag *tag,
					 XMLTagNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);
int		tag_connect_kill_child	(XMLTag *tag,
					 XMLTagRefNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);
int		tag_connect_add		(XMLTag *tag,
					 XMLTagRefNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);
int		tag_connect_prepend	(XMLTag *tag,
					 XMLTagRefNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);
int		tag_connect_append	(XMLTag *tag,
					 XMLTagRefNotify notify,
					 gpointer data,
					 XMLDestroyNotify destroy_notify);

void		tag_disconnect		(XMLTag *tag,
					 int id);
void		tag_disconnect_by_data	(XMLTag *tag,
					 gpointer data);



/* Doesn't unref, just kills it from the document,
 * though that may end up unreffing */
void		tag_kill		(XMLTag *tag);

XMLTag *	tag_prev		(XMLTag *tag);
XMLTag *	tag_prev_by_id		(XMLTag *tag,
					 const char *id);
XMLTag *	tag_prev_by_ids		(XMLTag *tag,
					 const GList *list);

XMLTag *	tag_next		(XMLTag *tag);
XMLTag *	tag_next_by_id		(XMLTag *tag,
					 const char *id);
XMLTag *	tag_next_by_ids		(XMLTag *tag,
					 const GList *list);

XMLTag *	tag_parent		(XMLTag *tag);
XMLTag *	tag_parent_parent	(XMLTag *tag);

void		tag_move_up		(XMLTag *tag);

void		tag_move_down		(XMLTag *tag);


XMLTag *	tag_add			(XMLTag *parent,
					 const char *name);

XMLTag *	tag_append		(XMLTag *previous,
					 const char *name);
XMLTag *	tag_prepend		(XMLTag *next,
					 const char *name);

char *		tag_get_text		(XMLTag *tag,
					 const char *path,
					 const char *locale);
void		tag_set_text		(XMLTag *tag,
					 const char *path,
					 const char *text,
					 const char *locale);
gboolean	tag_get_bool		(XMLTag *tag,
					 const char *path,
					 gboolean def);
void		tag_set_bool		(XMLTag *tag,
					 const char *path,
					 gboolean value);
int		tag_get_int		(XMLTag *tag,
					 const char *path,
					 int def);
void		tag_set_int		(XMLTag *tag,
					 const char *path,
					 int value);
GList *		tag_get_text_list	(XMLTag *the_tag,
					 const char *id);

void		tag_set_text_list	(XMLTag *the_tag,
					 const char *id,
					 GList *values);

void		tag_text_list_add	(XMLTag *the_tag,
					 const char *id,
					 const char *value);
void		tag_text_list_remove	(XMLTag *the_tag,
					 const char *id,
					 const char *value);

void		tag_set_translatable	(XMLTag *tag,
					 gboolean translatable);


/* Filter a list of tags to return a new list of only
 * those tags where path text is equal to text */
GList *		tag_find		(GList *tags,
					 const char *path,
					 const char *text);

/* have not yet needed to set these, and I don't generally like
 * properties, but i need to read them for schema importing. */
char *		tag_get_prop		(XMLTag *tag,
					 const char *prop_name);

/* Specific API */

/* make things translatable that are supposed to be translatable
 * and such things
 * Returns FALSE if xml:lang things exist (thus unsane) */
gboolean	tree_i18n_sanitize	(XMLTree *tree);

/* returns a list that's the list of reffed tags */
GList *		tree_get_levels		(XMLTree *tree);
XMLTag *	tree_get_level 		(XMLTree *tree,
					 const char *level);
/* add level with a unique name */
XMLTag *	tree_add_level		(XMLTree *tree);
XMLTag *	tree_prepend_level	(XMLTag *sibling);
void		tree_remove_level	(XMLTree *tree,
					 const char *level);
gboolean	tree_set_level_name	(XMLTree *tree,
					 XMLTag *level,
					 const char *name);
void		tree_move_level_up	(XMLTag *level);
void		tree_move_level_down	(XMLTag *level);

/* Pane stuff */

XMLTag *	tree_find_by_name	(XMLTree *tree,
					 const char *name);

/* list of strings */
GList *		tree_get_names		(XMLTree *tree);

/* returns a list that's the list of reffed tags */
GList *		tree_get_panes		(XMLTree *tree);

/* error checking */
gboolean	tree_set_pane_name	(XMLTree *tree,
					 XMLTag *pane,
					 const char *name);

/* add onto the end */
XMLTag *	tree_add_pane		(XMLTree *tree);
XMLTag *	tree_prepend_pane	(XMLTag *sibling);
void		tree_move_pane_up	(XMLTag *pane);
void		tree_move_pane_down	(XMLTag *pane);

/* Get groups, though this includes Plug/Glade/Bonobo thingies
 * as well, this is ordered so be careful */
GList *		tree_get_groups		(XMLTree *tree,
					 XMLTag *pane);

/* error checking */
gboolean	tree_set_group_name	(XMLTree *tree,
					 XMLTag *group,
					 const char *name);

/* add onto the end */
XMLTag *	tree_add_group		(XMLTag *pane);
XMLTag *	tree_prepend_group	(XMLTag *sibling);

XMLTag *	tree_add_plug		(XMLTag *pane_or_group);
XMLTag *	tree_prepend_plug	(XMLTag *sibling);
XMLTag *	tree_add_glade		(XMLTag *pane_or_group);
XMLTag *	tree_prepend_glade	(XMLTag *sibling);
XMLTag *	tree_add_bonobo		(XMLTag *pane_or_group);
XMLTag *	tree_prepend_bonobo	(XMLTag *sibling);

void		tree_move_group_up	(XMLTag *group);
void		tree_move_group_down	(XMLTag *group);

/* Get widgets, though this includes Plug/Glade/Bonobo thingies
 * as well, this is ordered so be careful */
GList *		tree_get_widgets	(XMLTree *tree,
					 XMLTag *group);

/* error checking */
gboolean	tree_set_widget_name	(XMLTree *tree,
					 XMLTag *widget,
					 const char *name);

/* add onto the end */
XMLTag *	tree_add_widget		(XMLTag *group);
XMLTag *	tree_prepend_widget	(XMLTag *sibling);

void		tree_move_widget_up	(XMLTag *widget);
void		tree_move_widget_down	(XMLTag *widget);

XMLTag *	tree_widget_get_argument (XMLTag *widget,
					  const char *name);
XMLTag *	tree_widget_add_argument (XMLTag *widget);
void		tree_widget_remove_argument (XMLTag *widget,
					     const char *name);
gboolean	tree_set_argument_name	(XMLTag *widget,
					 XMLTag *argument,
					 const char *name);

XMLTag *	tree_widget_add_option	(XMLTag *widget);
XMLTag *	tree_widget_prepend_option (XMLTag *sibling);
void		tree_widget_move_option_up (XMLTag *option);
void		tree_widget_move_option_down (XMLTag *option);

/* Get elements */
GList *		tree_get_elements	(XMLTree *tree);
XMLTag *	tree_add_element	(XMLTree *tree);

XMLTag *	tree_get_element	(XMLTree *tree,
					 const char *conf_path);

gboolean	tree_element_set_conf_path (XMLTree *tree,
					    XMLTag *element,
					    const char *conf_path);

PongType	tree_element_get_pong_type (XMLTag *element);
void		tree_element_set_pong_type (XMLTag *element,
					    PongType type);

XMLTag *	tree_element_add_sensitivity	(XMLTag *element,
						 const char *value);

void		tree_sensitivity_add_sensitive (XMLTag *sensitivity,
						const char *name,
						gboolean sensitive);
void		tree_sensitivity_remove_sensitive (XMLTag *sensitivity,
						   const char *name,
						   gboolean sensitive);

/* Free list of newly allocated strings */
void		tree_free_string_list	(GList *list);
/* Free list of reffed tags */
void		tree_free_tag_list	(GList *list);

#endif /* TREE_H */
