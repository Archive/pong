/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PONG_EDIT_H
#define PONG_EDIT_H

extern GladeXML *app_xml;
extern GnomeApp *app;
extern GtkWidget *elements_clist;
extern GtkWidget *levels_clist;
extern GtkWidget *panes_ctree;
extern GtkWidget *pane_options_clist;
extern GtkWidget *pane_arguments_clist;
extern GtkWidget *element_sensitivities_clist;

extern XMLTree *xml_tree;

extern gboolean file_changed;

void	setup_dlg		(GtkWidget *dlg);

#endif /* PONG_EDIT_H */
