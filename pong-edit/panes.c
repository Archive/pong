/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "tree.h"
#include "util.h"
#include "pong-edit.h"
#include "glade-helper.h"
#include "pane-arguments.h"
#include "pane-options.h"

#include "panes.h"

static XMLTag *selected_pane = NULL;

void change_pane_name		(GtkWidget *entry);
void change_pane_label		(GtkWidget *entry);
void change_group_columns	(GtkWidget *sb);
void change_widget_tooltip	(GtkWidget *entry);
void change_widget_type		(GtkWidget *entry);
void change_plug_type		(GtkWidget *entry);
void change_plug_path		(GtkWidget *entry);
void change_plug_specifier	(GtkWidget *entry);
void change_bonobo_query	(GtkWidget *entry);
void change_glade_file		(GtkWidget *entry);
void change_glade_widget	(GtkWidget *entry);

void change_expand		(GtkWidget *widget);
void change_align		(GtkWidget *widget);

void change_all_levels		(GtkWidget *cb);

void panes_ctree_select_row	(GtkCTree *ctree,
				 GtkCTreeNode *node,
				 int column);
void panes_ctree_unselect_row	(GtkCTree *ctree,
				 GtkCTreeNode *node,
				 int column);

void pane_remove		(GtkWidget *w);
void pane_move_up		(GtkWidget *w);
void pane_move_down		(GtkWidget *w);

void add_pane			(GtkWidget *w);
void add_group			(GtkWidget *w);
void add_widget			(GtkWidget *w);
void add_bonobo			(GtkWidget *w);
void add_glade			(GtkWidget *w);
void add_plug			(GtkWidget *w);

static GtkCTreeNode *
add_anything_to_ctree (XMLTag *pane, const char *type,
		       gboolean is_leaf, GtkCTreeNode *parent,
		       GtkCTreeNode *sibling);

static GList *per_list_connections = NULL;

static char *
make_thing_text (XMLTag *tag)
{
	char *text;
	const char *type = tag_peek_type (tag);
	char *label, *name;
	GString *str = g_string_new (NULL);

	name = tag_get_text (tag, PONG_S_Name, NULL);
	label = tag_get_text (tag, PONG_S_Label, NULL);

	if (pong_strcasecmp_no_locale (type, PONG_S_Widget) == 0) {
		char *wtype;
		wtype = tag_get_text (tag, PONG_S_Type, NULL);
		if ( ! pong_string_empty (wtype))
			g_string_append (str, wtype);
		else
			g_string_append (str, _("Widget"));
		g_free (wtype);
	} else {
		g_string_append (str, type);
	}

	if ( ! pong_string_empty (name))
		g_string_sprintfa (str, " [%s]", name);

	if ( ! pong_string_empty (label))
		g_string_sprintfa (str, " \"%s\"", label);

	g_free (name);
	g_free (label);

	text = str->str;
	g_string_free (str, FALSE);
	return text;
}

static void
pane_updated (XMLTag *tag,
	      const char *key,
	      const char *text,
	      gpointer data)
{
	GtkCTreeNode *node;

	node = gtk_ctree_find_by_row_data (GTK_CTREE (panes_ctree),
					   NULL,
					   tag);

	if (node != NULL) {
		char *new_text = make_thing_text (tag);
		gtk_ctree_node_set_text (GTK_CTREE (panes_ctree),
					 node, 0, new_text);
		g_free (new_text);
	}
}

static void
pane_killed (XMLTag *tag, gpointer data)
{
	GtkCTreeNode *node;

	node = gtk_ctree_find_by_row_data (GTK_CTREE (panes_ctree),
					   NULL,
					   tag);

	if (node != NULL) {
		gtk_ctree_remove_node (GTK_CTREE (panes_ctree), node);
	}
}

static void
thing_added (XMLTag *pane, XMLTag *reference, gpointer data)
{
	const char *type;
	GtkCTreeNode *node;
	GtkCTreeNode *parent;

	type = tag_peek_type (reference);

	if (type == NULL ||
	    (pong_strcasecmp_no_locale (type, PONG_S_Widget) != 0 &&
	     pong_strcasecmp_no_locale (type, PONG_S_Plug) != 0 &&
	     pong_strcasecmp_no_locale (type, PONG_S_Bonobo) != 0 &&
	     pong_strcasecmp_no_locale (type, PONG_S_Glade) != 0))
		return;

	parent = gtk_ctree_find_by_row_data (GTK_CTREE (panes_ctree),
					     NULL,
					     pane);

	if (parent == NULL)
		return;

	node = find_next_in_ctree (GTK_CTREE (panes_ctree), reference);

	add_anything_to_ctree (reference,
			       type, TRUE,
			       parent,
			       node);
}

static void
group_added (XMLTag *pane, XMLTag *reference, gpointer data)
{
	const char *type;
	GtkCTreeNode *node;
	GtkCTreeNode *parent;
	gboolean is_leaf;

	type = tag_peek_type (reference);

	if (type == NULL ||
	    (pong_strcasecmp_no_locale (type, PONG_S_Group) != 0 &&
	     pong_strcasecmp_no_locale (type, PONG_S_Plug) != 0 &&
	     pong_strcasecmp_no_locale (type, PONG_S_Bonobo) != 0 &&
	     pong_strcasecmp_no_locale (type, PONG_S_Glade) != 0))
		return;

	parent = gtk_ctree_find_by_row_data (GTK_CTREE (panes_ctree),
					     NULL,
					     pane);

	if (parent == NULL)
		return;

	node = find_next_in_ctree (GTK_CTREE (panes_ctree), reference);

	if (pong_strcasecmp_no_locale (type, PONG_S_Group) == 0)
		is_leaf = FALSE;
	else
		is_leaf = TRUE;

	add_anything_to_ctree (reference,
			       type, is_leaf,
			       parent,
			       node);
}

static GtkCTreeNode *
get_prev_child (GtkCTreeNode *node)
{
	GtkCTreeNode *prev = node;

	for (;;) {
		prev = GTK_CTREE_NODE_PREV (prev);

		if (prev == NULL ||
		    prev == GTK_CTREE_ROW (node)->parent)
			return NULL;

		if (GTK_CTREE_ROW (prev)->parent ==
		    GTK_CTREE_ROW (node)->parent)
			return prev;
	}
}


static void
pane_moved_up (XMLTag *tag, gpointer data)
{
	XMLTag *prev_data, *next_tag;
	GtkCTreeNode *node, *prev;

	node = gtk_ctree_find_by_row_data (GTK_CTREE (panes_ctree),
					   NULL,
					   tag);
	if (node == NULL)
		return;

	prev = get_prev_child (node);

	if (prev == NULL)
		return;

	prev_data = gtk_ctree_node_get_row_data (GTK_CTREE (panes_ctree), prev);

	next_tag = tag_next (tag);

	/* when we just skipped one tag displayed */
	if (next_tag == prev_data &&
	    /* if we didn't go over the edge, general paranoia */
	    GTK_CTREE_ROW (node)->parent == GTK_CTREE_ROW (prev)->parent) {
		gtk_ctree_move (GTK_CTREE (panes_ctree),
				node,
				GTK_CTREE_ROW (node)->parent,
				prev);
	}

	tag_unref (next_tag);
}

static void
pane_moved_down (XMLTag *tag, gpointer data)
{
	XMLTag *next_data, *prev_tag;
	GtkCTreeNode *node, *next;

	node = gtk_ctree_find_by_row_data (GTK_CTREE (panes_ctree),
					   NULL,
					   tag);
	if (node == NULL)
		return;

	next = GTK_CTREE_ROW (node)->sibling;

	if (next == NULL)
		return;

	next_data = gtk_ctree_node_get_row_data (GTK_CTREE (panes_ctree), next);

	prev_tag = tag_prev (tag);

	/* when we just skipped one tag displayed */
	if (prev_tag == next_data) {
		/* skip another one since we are inserting */
		next = GTK_CTREE_NODE_NEXT (next);
		/* if we went over the edge */
		if (next != NULL &&
		    GTK_CTREE_ROW (node)->parent != 
		    GTK_CTREE_ROW (next)->parent)
			next = NULL;
		gtk_ctree_move (GTK_CTREE (panes_ctree),
				node,
				GTK_CTREE_ROW (node)->parent,
				next);
	}

	tag_unref (prev_tag);
}

static GtkCTreeNode *
add_anything_to_ctree (XMLTag *pane, const char *type,
		       gboolean is_leaf, GtkCTreeNode *parent,
		       GtkCTreeNode *sibling)
{
	GtkCTreeNode *node;
	char *text[1];
	int con_id;

	text[0] = make_thing_text (pane);

	node = gtk_ctree_insert_node (GTK_CTREE (panes_ctree), 
				      parent, sibling, text,
				      0 /*spacing*/,
				      NULL /*pixmap_closed*/,
				      NULL /*mask_closed*/,
				      NULL /*pixmap_opened*/,
				      NULL /*mask_opened*/,
				      is_leaf,
				      TRUE);

	g_free (text[0]);

	gtk_ctree_node_set_row_data_full (GTK_CTREE (panes_ctree), node,
					  tag_ref (pane),
					  (GtkDestroyNotify)tag_unref);

	con_id = tag_connect_text (pane, PONG_S_Name,
				   pane_updated, NULL, NULL);
	connection_add (&per_list_connections, pane, con_id);
	con_id = tag_connect_text (pane, PONG_S_Label,
				   pane_updated, NULL, NULL);
	connection_add (&per_list_connections, pane, con_id);
	con_id = tag_connect_kill (pane, pane_killed, NULL, NULL);
	connection_add (&per_list_connections, pane, con_id);

	if (pong_strcasecmp_no_locale (type, PONG_S_Pane) == 0) {
		con_id = tag_connect_add (pane, group_added, NULL, NULL);
		connection_add (&per_list_connections, pane, con_id);
	} else if (pong_strcasecmp_no_locale (type, PONG_S_Group) == 0) {
		con_id = tag_connect_add (pane, thing_added, NULL, NULL);
		connection_add (&per_list_connections, pane, con_id);
	}

	con_id = tag_connect_move_up (pane, pane_moved_up, NULL, NULL);
	connection_add (&per_list_connections, pane, con_id);
	con_id = tag_connect_move_down (pane, pane_moved_down, NULL, NULL);
	connection_add (&per_list_connections, pane, con_id);

	return node;
}

static void
pane_added (XMLTag *root, XMLTag *reference, gpointer data)
{
	const char *type;
	GtkCTreeNode *node;

	type = tag_peek_type (reference);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, PONG_S_Pane) != 0)
		return;

	node = find_next_in_ctree (GTK_CTREE (panes_ctree), reference);

	add_anything_to_ctree (reference,
			       type, FALSE,
			       NULL /*parent*/,
			       node /*sibling*/);
}

static void
add_widgets (XMLTag *group, GtkCTreeNode *group_node)
{
	GList *list, *li;

	list = tree_get_widgets (xml_tree, group);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		const char *type = tag_peek_type (tag);

		add_anything_to_ctree (tag, type, TRUE,
				       group_node, NULL);
	}

	tree_free_tag_list (list);
}

static void
add_groups (XMLTag *pane, GtkCTreeNode *pane_node)
{
	GList *list, *li;

	list = tree_get_groups (xml_tree, pane);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		const char *type = tag_peek_type (tag);

		if (pong_strcasecmp_no_locale (type, PONG_S_Group) == 0) {
			GtkCTreeNode *node;
			node = add_anything_to_ctree (tag, type, FALSE,
						      pane_node, NULL);
			add_widgets (tag, node);
		} else {
			add_anything_to_ctree (tag, type, TRUE,
					       pane_node, NULL);
		}
	}

	tree_free_tag_list (list);
}

void
setup_panes_list (XMLTag *root)
{
	GList *list, *li;
	int con_id;

	gtk_clist_freeze (GTK_CLIST (panes_ctree));

	list = tree_get_panes (xml_tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		const char *type = tag_peek_type (tag);

		if (pong_strcasecmp_no_locale (type, PONG_S_Pane) == 0) {
			GtkCTreeNode *node;
			node = add_anything_to_ctree (tag, type, FALSE,
						      NULL, NULL);
			add_groups (tag, node);
		} else {
			add_anything_to_ctree (tag, type, TRUE,
					       NULL, NULL);
		}
	}

	tree_free_tag_list (list);

	gtk_clist_thaw (GTK_CLIST (panes_ctree));

	con_id = tag_connect_add (root, pane_added, NULL, NULL);
	connection_add (&per_list_connections, root, con_id);
}

void
clear_panes_list (void)
{
	connection_disconnect_all (&per_list_connections);

	gtk_clist_unselect_all (GTK_CLIST (panes_ctree));
	gtk_clist_clear (GTK_CLIST (panes_ctree));
}


void
change_group_columns (GtkWidget *sb)
{
	int value;

	if (selected_pane == NULL)
		return;

	value = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (sb));
	tag_set_int (selected_pane, PONG_S_Columns, value);
}

static void
set_stripped_pane_text (GtkWidget *entry, const char *key)
{
	char *text;

	if (selected_pane == NULL)
		return;

	text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);

	if (text != NULL)
		g_strstrip (text);

	if (pong_string_empty (text))
		tag_set_text (selected_pane, key, NULL, NULL);
	else
		tag_set_text (selected_pane, key, text, NULL);

	g_free (text);
}

void
change_pane_label (GtkWidget *entry)
{
	set_stripped_pane_text (entry, "_" PONG_S_Label);
}

void
change_widget_tooltip (GtkWidget *entry)
{
	set_stripped_pane_text (entry, "_" PONG_S_Tooltip);
}

void
change_widget_type (GtkWidget *entry)
{
	set_stripped_pane_text (entry, PONG_S_Type);
}

void
change_plug_type (GtkWidget *entry)
{
	set_stripped_pane_text (entry, PONG_S_Type);
}

void
change_plug_path (GtkWidget *entry)
{
	set_stripped_pane_text (entry, PONG_S_Path);
}

void
change_plug_specifier (GtkWidget *entry)
{
	set_stripped_pane_text (entry, PONG_S_Specifier);
}

void
change_bonobo_query (GtkWidget *entry)
{
	set_stripped_pane_text (entry, PONG_S_Query);
}

void
change_glade_file (GtkWidget *entry)
{
	set_stripped_pane_text (entry, PONG_S_File);
}

void
change_glade_widget (GtkWidget *entry)
{
	set_stripped_pane_text (entry, PONG_S_Widget);
}

void
change_pane_name (GtkWidget *entry)
{
	gboolean success;

	if (selected_pane == NULL)
		return;

	success = tree_set_pane_name (xml_tree, selected_pane,
				      gtk_entry_get_text (GTK_ENTRY (entry)));

	pong_edit_set_red (entry, success ? FALSE : TRUE);
}

void
change_expand (GtkWidget *widget)
{
	gboolean value;

	if (selected_pane == NULL)
		return;

	value = GTK_TOGGLE_BUTTON (widget)->active;

	tag_set_bool (selected_pane, PONG_S_Expand, value);
}

void
change_align (GtkWidget *widget)
{
	gboolean value;

	if (selected_pane == NULL)
		return;

	value = GTK_TOGGLE_BUTTON (widget)->active;

	tag_set_bool (selected_pane, PONG_S_AlignLabel, value);
}

static void
setup_group_page (XMLTag *tag)
{
	GtkWidget *w;
	gboolean value;
	int ivalue;

	/* The expand */
	w = glade_helper_get (app_xml, "group_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_Expand, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The columns */
	w = glade_helper_get (app_xml, "group_columns_sb",
			      GTK_TYPE_SPIN_BUTTON);
	ivalue = tag_get_int (tag, PONG_S_Columns, 1);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (w), ivalue);
}

static void
setup_widget_page (XMLTag *tag)
{
	GtkWidget *w;
	gboolean value;
	char *text;

	/* The expand */
	w = glade_helper_get (app_xml, "widget_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_Expand, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The align */
	w = glade_helper_get (app_xml, "widget_align_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_AlignLabel, TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The widget tooltip */
	w = glade_helper_get (app_xml, "widget_tooltip_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_Tooltip, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The widget type */
	w = glade_helper_get (app_xml, "widget_type_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_Type, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);
}

static void
setup_plug_page (XMLTag *tag)
{
	GtkWidget *w;
	gboolean value;
	char *text;

	/* The expand */
	w = glade_helper_get (app_xml, "plug_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_Expand, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The align */
	w = glade_helper_get (app_xml, "plug_align_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_AlignLabel, TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The plug type */
	w = glade_helper_get (app_xml, "plug_type_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_Type, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The plug path */
	w = glade_helper_get (app_xml, "plug_path_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_Path, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The plug specifier */
	w = glade_helper_get (app_xml, "plug_specifier_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_Specifier, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);
}

static void
setup_bonobo_page (XMLTag *tag)
{
	GtkWidget *w;
	gboolean value;
	char *text;

	/* The expand */
	w = glade_helper_get (app_xml, "bonobo_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_Expand, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The align */
	w = glade_helper_get (app_xml, "bonobo_align_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_AlignLabel, TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The bonobo query */
	w = glade_helper_get (app_xml, "bonobo_query_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_Query, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);
}

static void
setup_glade_page (XMLTag *tag)
{
	GtkWidget *w;
	gboolean value;
	char *text;

	/* The expand */
	w = glade_helper_get (app_xml, "glade_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_Expand, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The align */
	w = glade_helper_get (app_xml, "glade_align_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (tag, PONG_S_AlignLabel, TRUE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);

	/* The glade file */
	w = glade_helper_get (app_xml, "glade_file_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_File, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The glade widget */
	w = glade_helper_get (app_xml, "glade_widget_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (tag, PONG_S_Widget, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);
}

static void
enable_options_arguments (gboolean value)
{
	GtkWidget *w;

	w = glade_helper_get (app_xml, "pane_options_label", GTK_TYPE_LABEL);
	gtk_widget_set_sensitive (w, value);
	w = glade_helper_get (app_xml, "pane_options_vbox", GTK_TYPE_VBOX);
	gtk_widget_set_sensitive (w, value);
	w = glade_helper_get (app_xml, "pane_arguments_label", GTK_TYPE_LABEL);
	gtk_widget_set_sensitive (w, value);
	w = glade_helper_get (app_xml, "pane_arguments_vbox", GTK_TYPE_VBOX);
	gtk_widget_set_sensitive (w, value);
}

void
change_all_levels (GtkWidget *cb)
{
	GtkWidget *vbox, *bin;

	if (selected_pane == NULL)
		return;

	bin = glade_helper_get (app_xml, "pane_levels_bin",
				GTK_TYPE_EVENT_BOX);

	vbox = GTK_BIN (bin)->child;

	if (GTK_TOGGLE_BUTTON (cb)->active) {
		/* do the deed */
		tag_set_text_list (selected_pane, PONG_S_Level, NULL);

		if (vbox != NULL) {
			gtk_widget_set_sensitive (vbox, FALSE);
		}
	/* we will only do something if there are actual children */
	} else if (vbox != NULL) {
		GList *newlist;
		GList *children, *li;

		gtk_widget_set_sensitive (vbox, TRUE);

		children = gtk_container_children (GTK_CONTAINER (vbox));

		newlist = NULL;
		for (li = children; li != NULL; li = li->next) {
			GtkWidget *cb = li->data;
			char *name;

			g_assert (GTK_IS_CHECK_BUTTON (cb));

			name = gtk_object_get_data (GTK_OBJECT (cb),
						    "Level-Name");

			g_assert (name != NULL);

			if (GTK_TOGGLE_BUTTON (cb)->active) {
				newlist = g_list_prepend (newlist,
							  g_strdup (name));
			}
		}

		tag_set_text_list (selected_pane, PONG_S_Level, newlist);

		g_list_free (children);
		tree_free_string_list (newlist);
	}
}

static void
level_cb_toggled (GtkWidget *w, gpointer data)
{
	char *name = gtk_object_get_data (GTK_OBJECT (w), "Level-Name");

	g_assert (name != NULL);

	if (selected_pane == NULL)
		return;

	if (GTK_TOGGLE_BUTTON (w)->active)
		tag_text_list_add (selected_pane, PONG_S_Level, name);
	else
		tag_text_list_remove (selected_pane, PONG_S_Level, name);
}

static void
setup_levels (XMLTag *pane)
{
	GtkWidget *bin, *box, *w, *all_levels;
	GList *level_list, *list, *li;

	bin = glade_helper_get (app_xml, "pane_levels_bin",
				GTK_TYPE_EVENT_BOX);
	if (GTK_BIN (bin)->child != NULL)
		gtk_widget_destroy (GTK_BIN (bin)->child);

	if (pane == NULL)
		return;

	box = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (bin), box);

	all_levels = glade_helper_get (app_xml, "pane_all_levels_cb",
				       GTK_TYPE_CHECK_BUTTON);

	level_list = tag_get_text_list (pane, PONG_S_Level);

	if (level_list == NULL) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (all_levels),
					      TRUE);
		gtk_widget_set_sensitive (box, FALSE);
	} else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (all_levels),
					      FALSE);
		gtk_widget_set_sensitive (box, TRUE);
	}

	list = tree_get_levels (xml_tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *level = li->data;
		char *name;

		if (level == NULL)
			continue;

		name = tag_get_text (level, PONG_S_Name, NULL);

		if (pong_string_empty (name)) {
			g_free (name);
			continue;
		}

		w = gtk_check_button_new_with_label (name);

		gtk_object_set_data_full (GTK_OBJECT (w),
					  "Level-Name",
					  g_strdup (name),
					  (GtkDestroyNotify) g_free);

		gtk_box_pack_start (GTK_BOX (box), w,
				    FALSE, FALSE, 0);

		if (is_string_in_list (level_list, name))
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w),
						      TRUE);

		gtk_signal_connect (GTK_OBJECT (w), "toggled",
				    GTK_SIGNAL_FUNC (level_cb_toggled),
				    NULL);
	}

	tree_free_tag_list (list);
	tree_free_string_list (level_list);

	gtk_widget_show_all (box);
}

/* an utter hack, we will call this from outside when the level list
 * changes, ugly ugly ugly, but simple
 */
void
panes_change_level_list (void)
{
	setup_levels (selected_pane);
}


static void
setup_pane_edit_area (XMLTag *pane)
{
	GtkWidget *w, *label;
	char *text;
	const char *name;
	int page;

	/* The pane name */
	w = glade_helper_get (app_xml, "pane_name", GTK_TYPE_ENTRY);
	text = tag_get_text (pane, PONG_S_Name, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The pane label */
	w = glade_helper_get (app_xml, "pane_label", GTK_TYPE_ENTRY);
	text = tag_get_text (pane, PONG_S_Label, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* the type settings notebook */
	label = glade_helper_get (app_xml, "pane_type_label", GTK_TYPE_LABEL);
	name = tag_peek_type (pane);
	if (pong_strcasecmp_no_locale (name, PONG_S_Pane) == 0) {
		gtk_label_set (GTK_LABEL (label), _("Pane"));
		enable_options_arguments (FALSE);
		page = 1;
		/* no setup for pane */
	} else if (pong_strcasecmp_no_locale (name, PONG_S_Group) == 0) {
		gtk_label_set (GTK_LABEL (label), _("Group"));
		enable_options_arguments (FALSE);
		page = 2;
		setup_group_page (pane);
	} else if (pong_strcasecmp_no_locale (name, PONG_S_Widget) == 0) {
		gtk_label_set (GTK_LABEL (label), _("Widget"));
		enable_options_arguments (TRUE);
		page = 3;
		setup_widget_page (pane);
		setup_pane_arguments_list (pane);
		setup_pane_options_list (pane);
	} else if (pong_strcasecmp_no_locale (name, PONG_S_Plug) == 0) {
		gtk_label_set (GTK_LABEL (label), _("Plug"));
		enable_options_arguments (TRUE);
		page = 4;
		setup_plug_page (pane);
		setup_pane_arguments_list (pane);
		setup_pane_options_list (pane);
	} else if (pong_strcasecmp_no_locale (name, PONG_S_Bonobo) == 0) {
		gtk_label_set (GTK_LABEL (label), _("Bonobo"));
		enable_options_arguments (TRUE);
		page = 5;
		setup_bonobo_page (pane);
		setup_pane_arguments_list (pane);
		setup_pane_options_list (pane);
	} else if (pong_strcasecmp_no_locale (name, PONG_S_Glade) == 0) {
		gtk_label_set (GTK_LABEL (label), _("Glade"));
		enable_options_arguments (FALSE);
		page = 6;
		setup_glade_page (pane);
	} else {
		gtk_label_set (GTK_LABEL (label), _("???"));
		enable_options_arguments (FALSE);
		page = 0;
	}

	w = glade_helper_get (app_xml, "pane_type_notebook", GTK_TYPE_NOTEBOOK);
	gtk_notebook_set_page (GTK_NOTEBOOK (w), page);

	setup_levels (pane);
}

static void
clear_group_page (void)
{
	GtkWidget *w;

	/* The expand */
	w = glade_helper_get (app_xml, "group_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), FALSE);
}

static void
clear_widget_page (void)
{
	GtkWidget *w;

	/* The expand */
	w = glade_helper_get (app_xml, "widget_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), FALSE);

	/* The widget tooltip */
	w = glade_helper_get (app_xml, "widget_tooltip_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");
}

static void
clear_plug_page (void)
{
	GtkWidget *w;

	/* The expand */
	w = glade_helper_get (app_xml, "plug_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), FALSE);

	/* The plug type */
	w = glade_helper_get (app_xml, "plug_type_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The plug path */
	w = glade_helper_get (app_xml, "plug_path_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The plug specifier */
	w = glade_helper_get (app_xml, "plug_specifier_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");
}

static void
clear_bonobo_page (void)
{
	GtkWidget *w;

	/* The expand */
	w = glade_helper_get (app_xml, "bonobo_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), FALSE);

	/* The bonobo query */
	w = glade_helper_get (app_xml, "bonobo_query_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");
}

static void
clear_glade_page (void)
{
	GtkWidget *w;

	/* The expand */
	w = glade_helper_get (app_xml, "glade_expand_cb",
			      GTK_TYPE_CHECK_BUTTON);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), FALSE);

	/* The glade file */
	w = glade_helper_get (app_xml, "glade_file_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The glade widget */
	w = glade_helper_get (app_xml, "glade_widget_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");
}

static void
clear_pane_edit_area (void)
{
	GtkWidget *w;

	/* The pane name */
	w = glade_helper_get (app_xml, "pane_name", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The pane label */
	w = glade_helper_get (app_xml, "pane_label", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* the type label */
	w = glade_helper_get (app_xml, "pane_type_label", GTK_TYPE_LABEL);
	gtk_label_set (GTK_LABEL (w), _("<type>"));

	/* the inner notebook */
	w = glade_helper_get (app_xml, "pane_type_notebook", GTK_TYPE_NOTEBOOK);
	gtk_notebook_set_page (GTK_NOTEBOOK (w), 0);

	clear_group_page ();
	clear_widget_page ();
	clear_plug_page ();
	clear_bonobo_page ();
	clear_glade_page ();

	clear_pane_arguments_list ();
	clear_pane_options_list ();

	w = glade_helper_get (app_xml, "pane_levels_bin",
			      GTK_TYPE_EVENT_BOX);
	if (GTK_BIN (w)->child != NULL)
		gtk_widget_destroy (GTK_BIN (w)->child);
}

static gboolean
is_foo (XMLTag *tag, const char *foo)
{
	const char *type;

	if (tag == NULL)
		return FALSE;

	type = tag_peek_type (tag);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, foo) != 0)
		return FALSE;
	else
		return TRUE;
}

static gboolean
is_parent_foo (XMLTag *tag, const char *foo)
{
	XMLTag *parent;
	const char *type;

	if (tag == NULL)
		return FALSE;

	parent = tag_parent (tag);
	
	if (parent == NULL)
		return FALSE;

	type = tag_peek_type (parent);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, foo) != 0) {
		tag_unref (parent);
		return FALSE;
	} else {
		tag_unref (parent);
		return TRUE;
	}
}

void
pane_remove (GtkWidget *w)
{
	if (selected_pane != NULL)
		tag_kill (selected_pane);
}

void
pane_move_up (GtkWidget *w)
{
	if (selected_pane == NULL)
		return;

	if (is_foo (selected_pane, PONG_S_Pane))
		tree_move_pane_up (selected_pane);
	else if (is_foo (selected_pane, PONG_S_Group))
		tree_move_group_up (selected_pane);
	else
		tree_move_widget_up (selected_pane);
}

void
pane_move_down (GtkWidget *w)
{
	if (selected_pane == NULL)
		return;

	if (is_foo (selected_pane, PONG_S_Pane))
		tree_move_pane_down (selected_pane);
	else if (is_foo (selected_pane, PONG_S_Group))
		tree_move_group_down (selected_pane);
	else
		tree_move_widget_down (selected_pane);
}

#define SEL(__x) { \
	XMLTag *__tag = (__x); \
	ctree_select_thing (GTK_CTREE (panes_ctree), __tag); \
	tag_unref (__tag); }

void
add_pane (GtkWidget *w)
{
	if (xml_tree == NULL)
		return;

	if (selected_pane == NULL) {
		SEL (tree_add_pane (xml_tree));
		return;
	}

	if (is_foo (selected_pane, PONG_S_Pane)) {
		SEL (tree_prepend_pane (selected_pane));
	} else if (is_parent_foo (selected_pane, PONG_S_Pane)) {
		XMLTag *tag;

		tag = tag_parent (selected_pane);

		SEL (tree_prepend_pane (tag));

		tag_unref (tag);
	} else if (is_parent_foo (selected_pane, PONG_S_Group)) {
		XMLTag *tag;

		tag = tag_parent_parent (selected_pane);

		/* paranoia */
		if (is_foo (tag, PONG_S_Pane)) {
			SEL (tree_prepend_pane (tag));
		} else {
			SEL (tree_add_pane (xml_tree));
		}

		tag_unref (tag);
	}
}

void
add_group (GtkWidget *w)
{
	if (selected_pane == NULL)
		return;

	if (is_foo (selected_pane, PONG_S_Pane)) {
		SEL (tree_add_group (selected_pane));
	} else if (is_parent_foo (selected_pane, PONG_S_Pane)) {
		SEL (tree_prepend_group (selected_pane));
	} else if (is_parent_foo (selected_pane, PONG_S_Group)) {
		XMLTag *tag;

		tag = tag_parent (selected_pane);

		SEL (tree_prepend_group (tag));

		tag_unref (tag);
	}
}

void
add_widget (GtkWidget *w)
{
	if (selected_pane == NULL)
		return;

	if (is_foo (selected_pane, PONG_S_Group)) {
		SEL (tree_add_widget (selected_pane));
	} else if (is_foo (selected_pane, PONG_S_Widget)) {
		SEL (tree_prepend_widget (selected_pane));
	} else if ((is_foo (selected_pane, PONG_S_Plug) ||
		    is_foo (selected_pane, PONG_S_Bonobo) ||
		    is_foo (selected_pane, PONG_S_Glade)) &&
		   (is_parent_foo (selected_pane, PONG_S_Group))) {
		SEL (tree_prepend_widget (selected_pane));
	}
}

void
add_bonobo (GtkWidget *w)
{
	if (selected_pane == NULL)
		return;

	if (is_foo (selected_pane, PONG_S_Pane) ||
	    is_foo (selected_pane, PONG_S_Group)) {
		SEL (tree_add_bonobo (selected_pane));
	} else if (is_foo (selected_pane, PONG_S_Plug) ||
		   is_foo (selected_pane, PONG_S_Bonobo) ||
		   is_foo (selected_pane, PONG_S_Glade) ||
		   is_foo (selected_pane, PONG_S_Widget)) {
		SEL (tree_prepend_bonobo (selected_pane));
	}
}

void
add_glade (GtkWidget *w)
{
	if (selected_pane == NULL)
		return;

	if (is_foo (selected_pane, PONG_S_Pane) ||
	    is_foo (selected_pane, PONG_S_Group)) {
		SEL (tree_add_glade (selected_pane));
	} else if (is_foo (selected_pane, PONG_S_Plug) ||
		   is_foo (selected_pane, PONG_S_Bonobo) ||
		   is_foo (selected_pane, PONG_S_Glade) ||
		   is_foo (selected_pane, PONG_S_Widget)) {
		SEL (tree_prepend_glade (selected_pane));
	}
}

void
add_plug (GtkWidget *w)
{
	if (selected_pane == NULL)
		return;

	if (is_foo (selected_pane, PONG_S_Pane) ||
	    is_foo (selected_pane, PONG_S_Group)) {
		SEL (tree_add_plug (selected_pane));
	} else if (is_foo (selected_pane, PONG_S_Plug) ||
		   is_foo (selected_pane, PONG_S_Bonobo) ||
		   is_foo (selected_pane, PONG_S_Glade) ||
		   is_foo (selected_pane, PONG_S_Widget)) {
		SEL (tree_prepend_plug (selected_pane));
	}
}

static void
setup_sensitivity_nothing (void)
{
	GtkWidget *w;

	w = glade_helper_get (app_xml, "add_group_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, FALSE);
	w = glade_helper_get (app_xml, "add_widget_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, FALSE);
	w = glade_helper_get (app_xml, "add_glade_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, FALSE);
	w = glade_helper_get (app_xml, "add_bonobo_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, FALSE);
	w = glade_helper_get (app_xml, "add_plug_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, FALSE);
}

static void
setup_sensitivity_pane (void)
{
	GtkWidget *w;

	w = glade_helper_get (app_xml, "add_group_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
	w = glade_helper_get (app_xml, "add_widget_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, FALSE);
	w = glade_helper_get (app_xml, "add_glade_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
	w = glade_helper_get (app_xml, "add_bonobo_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
	w = glade_helper_get (app_xml, "add_plug_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
}

static void
setup_sensitivity_everything (void)
{
	GtkWidget *w;

	w = glade_helper_get (app_xml, "add_group_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
	w = glade_helper_get (app_xml, "add_widget_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
	w = glade_helper_get (app_xml, "add_glade_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
	w = glade_helper_get (app_xml, "add_bonobo_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
	w = glade_helper_get (app_xml, "add_plug_button", GTK_TYPE_BUTTON);
	gtk_widget_set_sensitive (w, TRUE);
}

void
panes_ctree_select_row (GtkCTree *ctree,
			GtkCTreeNode *node,
			int column)
{
	const char *type;
	GtkWidget *w;
	XMLTag *tag = gtk_ctree_node_get_row_data (ctree, node);

	selected_pane = NULL;

	w = glade_helper_get (app_xml, "pane_property_notebook",
			      GTK_TYPE_NOTEBOOK);
	gtk_widget_set_sensitive (w, TRUE);

	setup_pane_edit_area (tag);
	selected_pane = tag;

	type = tag_peek_type (tag);

	if (type == NULL) {
		setup_sensitivity_nothing ();
	} else if (pong_strcasecmp_no_locale (type, PONG_S_Pane) == 0) {
		setup_sensitivity_pane ();
	} else if (pong_strcasecmp_no_locale (type, PONG_S_Widget) == 0 ||
		   pong_strcasecmp_no_locale (type, PONG_S_Group) == 0) {
		setup_sensitivity_everything ();
	} else if (pong_strcasecmp_no_locale (type, PONG_S_Bonobo) == 0 ||
		   pong_strcasecmp_no_locale (type, PONG_S_Glade) == 0 ||
		   pong_strcasecmp_no_locale (type, PONG_S_Plug) == 0) {
		if (is_parent_foo (tag, PONG_S_Group))
			setup_sensitivity_everything ();
		else
			setup_sensitivity_pane ();
	} else {
		setup_sensitivity_nothing ();
	}
}

void
panes_ctree_unselect_row (GtkCTree *ctree,
			  GtkCTreeNode *node,
			  int column)
{
	GtkWidget *w;

	selected_pane = NULL;

	clear_pane_edit_area ();

	w = glade_helper_get (app_xml, "pane_property_notebook",
			      GTK_TYPE_NOTEBOOK);
	gtk_widget_set_sensitive (w, FALSE);

	setup_sensitivity_nothing ();
}

void
setup_pane_name_combo (void)
{
	GList *popdown = NULL;
	GList *list, *li;
	GtkWidget *combo;
	GtkWidget *entry;
	XMLTag *old_selected_pane;
	char *old_text;

	list = tree_get_elements (xml_tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		char *widget, *conf_path;

		widget = tag_get_text (tag, PONG_S_Widget, NULL);
		if ( ! pong_string_empty (widget)) {
			popdown = g_list_prepend (popdown, widget);
			continue;
		}
		g_free (widget);

		conf_path = tag_get_text (tag, PONG_S_ConfPath, NULL);
		if ( ! pong_string_empty (conf_path)) {
			popdown = g_list_prepend (popdown, conf_path);
			continue;
		}
		g_free (conf_path);
	}

	tree_free_tag_list (list);

	popdown = g_list_reverse (popdown);

	combo = glade_helper_get (app_xml, "pane_name_combo",
				  gtk_combo_get_type ());
	entry = glade_helper_get (app_xml, "pane_name",
				  gtk_entry_get_type ());

	/* temporairly remove selection pointer so that we can update
	 * the entry withough doing anything bad to the tree */
	old_selected_pane = selected_pane;
	selected_pane = NULL;

	old_text = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

	if ( ! is_string_in_list (popdown, pong_sure_string (old_text))) {
		popdown = g_list_prepend
			(popdown, g_strdup (pong_sure_string (old_text)));
	}

	g_assert (popdown != NULL);

	gtk_combo_set_popdown_strings (GTK_COMBO (combo), popdown);

	gtk_entry_set_text (GTK_ENTRY (entry), old_text);
	g_free (old_text);

	selected_pane = old_selected_pane;

	/* hmmm ... should go to utils, and not be in tree I suppose */
	tree_free_string_list (popdown);
}
