/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong-strings.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "../pong/pongutil.h"

#include "tree.h"

#include "util.h"

void
pong_edit_set_red (GtkWidget *w, gboolean state)
{
	if (state) {
		GtkStyle *ns;
		GdkColor red = { 0, 65535, 0, 0 };

		ns = gtk_style_copy (w->style);
		gtk_style_ref (ns);

		ns->fg[GTK_STATE_NORMAL] = red;
		ns->text[GTK_STATE_NORMAL] = red;

		gtk_widget_set_style (w, ns);
		gtk_style_unref (ns);

		gtk_widget_queue_draw (w);
	} else {
		gtk_widget_set_rc_style (w);
	}
}

int
pong_edit_find_tag_row (GtkCList *clist, XMLTag *the_tag)
{
	int i;

	g_return_val_if_fail (clist != NULL, -1);
	g_return_val_if_fail (GTK_IS_CLIST (clist), -1);

	for (i = 0; i < clist->rows; i++) {
		XMLTag *tag;

		tag = gtk_clist_get_row_data (clist, i);

		if (tag == the_tag)
			return i;
	}

	return -1;
}

typedef struct {
	XMLTag *tag;
	int id;
} Connection;

void
connection_add (GList **list, XMLTag *tag, int connection)
{
	Connection *con;

	g_return_if_fail (list != NULL);
	g_return_if_fail (tag != NULL);
	g_return_if_fail (connection > 0);

	con = g_new0 (Connection, 1);
	con->tag = tag_ref (tag);
	con->id = connection;

	*list = g_list_prepend (*list, con);
}

void
connection_disconnect_all (GList **list)
{
	GList *li;

	g_return_if_fail (list != NULL);

	for (li = *list; li != NULL; li = li->next) {
		Connection *con = li->data;

		li->data = NULL;

		tag_disconnect (con->tag, con->id);
		tag_unref (con->tag);
		con->tag = NULL;
		g_free (con);
	}

	g_list_free (*list);
	*list = NULL;
}

GtkWidget *
pong_gtk_option_menu_get_item (GtkOptionMenu *option_menu, int index)
{
	g_return_val_if_fail (option_menu != NULL, NULL);
	g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), NULL);

	if (option_menu->menu != NULL) {
		return g_list_nth_data (GTK_MENU_SHELL(option_menu->menu)->children,
					index);
	}
	return NULL;
}

int
pong_gtk_option_menu_get_history (GtkOptionMenu *option_menu)
{
	GtkWidget *menu_item;

	g_return_val_if_fail (option_menu != NULL, -1);
	g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), -1);

	if (option_menu->menu != NULL) {
		menu_item = gtk_menu_get_active (GTK_MENU (option_menu->menu));
		return g_list_index (GTK_MENU_SHELL(option_menu->menu)->children,
				     menu_item);
	}
	return -1;
}

gboolean
is_string_in_list (const GList *list, const char *string)
{
	g_return_val_if_fail (string != NULL, FALSE);

	while (list != NULL) {
		if (list->data != NULL &&
		    strcmp (string, list->data) == 0)
			return TRUE;

		list = list->next;
	}

	return FALSE;
}

gboolean
is_string_in_list_case_no_locale (const GList *list, const char *string)
{
	g_return_val_if_fail (string != NULL, FALSE);

	while (list != NULL) {
		if (list->data != NULL &&
		    pong_strcasecmp_no_locale (string, list->data) == 0)
			return TRUE;

		list = list->next;
	}

	return FALSE;
}

GtkCTreeNode *
find_next_in_ctree (GtkCTree *ctree, XMLTag *tag)
{
	XMLTag *next;
	GtkCTreeNode *pnode = NULL;

	g_return_val_if_fail (tag != NULL, NULL);
	g_return_val_if_fail (ctree != NULL, NULL);
	g_return_val_if_fail (GTK_IS_CTREE (ctree), NULL);

	next = tag_ref (tag);

	do {
		XMLTag *new_next = tag_next (next);

		tag_unref (next);

		next = new_next;

		if (next == NULL)
			break;

		pnode = gtk_ctree_find_by_row_data (ctree, NULL, next);
	} while (pnode == NULL);

	return pnode;
}

int
find_next_in_clist (GtkCList *clist, XMLTag *tag)
{
	XMLTag *next;
	int prow = -1;

	g_return_val_if_fail (tag != NULL, -1);
	g_return_val_if_fail (clist != NULL, -1);
	g_return_val_if_fail (GTK_IS_CLIST (clist), -1);

	next = tag_ref (tag);

	do {
		XMLTag *new_next = tag_next (next);

		tag_unref (next);

		next = new_next;

		if (next == NULL)
			break;

		prow = gtk_clist_find_row_from_data (clist, next);
	} while (prow < 0);

	return prow;
}

void
ctree_select_thing (GtkCTree *ctree, XMLTag *tag)
{
	GtkCTreeNode *node;

	node = gtk_ctree_find_by_row_data (ctree, NULL, tag);

	if (node != NULL) {
		gtk_ctree_node_moveto (ctree,
				       node,
				       0 /*column*/,
				       0.5, 0.0);
		gtk_ctree_select (ctree, node);
	}
}

void
clist_select_thing (GtkCList *clist, XMLTag *tag)
{
	int row;

	row = gtk_clist_find_row_from_data (clist, tag);

	if (row >= 0) {
		gtk_clist_moveto (clist,
				  row,
				  0 /*column*/,
				  0.5, 0.0);
		gtk_clist_select_row (clist, row, 0);
	}
}
