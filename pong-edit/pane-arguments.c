/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include <pong/pongpane.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "tree.h"
#include "glade-helper.h"
#include "pong-edit.h"
#include "util.h"
#include "value-entry.h"

#include "pane-arguments.h"

static XMLTag *selected_argument = NULL;
static XMLTag *selected_widget = NULL;

void pane_arguments_clist_select_row	(GtkCList *clist,
					 int row, int column,
					 GdkEvent *event);
void pane_arguments_clist_unselect_row	(GtkCList *clist,
					 int row, int column,
					 GdkEvent *event);

void change_pane_argument_name		(GtkWidget *entry);
void change_pane_argument_value		(GtkWidget *ventry);
void change_pane_argument_translatable	(GtkWidget *cb);

void pane_argument_add			(GtkWidget *w);
void pane_argument_remove		(GtkWidget *w);

static GList *per_list_connections = NULL;

static GtkType widget_type = 0;

static void
pane_argument_name_updated (XMLTag *tag,
			    const char *key,
			    const char *text,
			    gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (pane_arguments_clist), tag);

	if (row >= 0) {
		gtk_clist_set_text (GTK_CLIST (pane_arguments_clist),
				    row, 0, text);
	}
}

static void
pane_argument_value_updated (XMLTag *tag,
			     const char *key,
			     const char *text,
			     gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (pane_arguments_clist), tag);

	if (row >= 0) {
		gtk_clist_set_text (GTK_CLIST (pane_arguments_clist),
				    row, 1, text);
	}
}

static void
pane_argument_translatable_updated (XMLTag *tag,
				    const char *key,
				    const char *text,
				    gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (pane_arguments_clist), tag);

	if (row >= 0) {
		gboolean value;

		if (text != NULL)
			value = pong_bool_from_string (text);
		else
			value = FALSE;

		gtk_clist_set_text (GTK_CLIST (pane_arguments_clist),
				    row, 2, value ? "True" : "False");
	}
}

static void
pane_argument_killed (XMLTag *tag, gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (pane_arguments_clist), tag);

	if (row >= 0) {
		gtk_clist_remove (GTK_CLIST (pane_arguments_clist), row);
	}
}

static int
add_argument_to_clist (XMLTag *argument)
{
	int row;
	char *text[3];
	char *name, *value, *translatable;
	gboolean bool;
	int con_id;

	name = tag_get_text (argument, PONG_S_Name, NULL);
	value = tag_get_text (argument, PONG_S_Value, NULL);

	bool = tag_get_bool (argument, PONG_S_Translatable, FALSE);
	translatable = bool ? "True" : "False";

	text[0] = pong_sure_string (name);
	text[1] = pong_sure_string (value);
	text[2] = pong_sure_string (translatable);

	row = gtk_clist_append (GTK_CLIST (pane_arguments_clist), text);

	g_free (name);
	g_free (value);

	gtk_clist_set_row_data_full (GTK_CLIST (pane_arguments_clist), row,
				     tag_ref (argument),
				     (GtkDestroyNotify)tag_unref);

	con_id = tag_connect_text (argument, PONG_S_Name,
				   pane_argument_name_updated, NULL, NULL);
	connection_add (&per_list_connections, argument, con_id);
	con_id = tag_connect_text (argument, PONG_S_Value,
				   pane_argument_value_updated, NULL, NULL);
	connection_add (&per_list_connections, argument, con_id);
	con_id = tag_connect_text (argument, PONG_S_Translatable,
				   pane_argument_translatable_updated,
				   NULL, NULL);
	connection_add (&per_list_connections, argument, con_id);
	con_id = tag_connect_kill (argument, pane_argument_killed, NULL, NULL);
	connection_add (&per_list_connections, argument, con_id);

	return row;
}

static void
pane_argument_added (XMLTag *root, XMLTag *reference, gpointer data)
{
	const char *name = tag_peek_type (reference);

	if (name != NULL &&
	    pong_strcasecmp_no_locale (name, PONG_S_Argument) == 0) {
		add_argument_to_clist (reference);
	}
}

static void
add_argument_names_to_combo (XMLTag *widget)
{
	const char *type = tag_peek_type (widget);
	GtkWidget *combo, *entry;
	char *gtk_type_string;
	GList *list;
	GtkType gtk_type;
	GtkArg *args;
	guint32 *arg_flags;
	guint nargs;
	int i;
	char *old_text;

	widget_type = 0;

	entry = glade_helper_get (app_xml, "pane_argument_name_entry",
				  GTK_TYPE_ENTRY);
	old_text = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));


	combo = glade_helper_get (app_xml, "pane_argument_name_combo",
				  gtk_combo_get_type ());

	gtk_type_string = tag_get_text (widget, PONG_S_Type, NULL);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, PONG_S_Widget) != 0 ||
	    gtk_type_string == NULL ||
	    pong_string_empty (gtk_type_string)) {
		list = g_list_append (NULL, "");
		gtk_combo_set_popdown_strings (GTK_COMBO (combo), list);
		g_list_free (list);
		g_free (gtk_type_string);
		gtk_entry_set_text (GTK_ENTRY (entry), old_text);
		g_free (old_text);
		return;
	}

	pong_pane_type_from_string (gtk_type_string, NULL, &gtk_type);
	g_free (gtk_type_string);

	if (gtk_type == 0) {
		list = g_list_append (NULL, "");
		gtk_combo_set_popdown_strings (GTK_COMBO (combo), list);
		g_list_free (list);
		gtk_entry_set_text (GTK_ENTRY (entry), old_text);
		g_free (old_text);
		return;
	}

	widget_type = gtk_type;

	list = g_list_prepend (NULL, g_strdup (""));

	while (gtk_type != 0) {
		args = gtk_object_query_args (gtk_type, &arg_flags, &nargs);

		for (i = 0; i < nargs; i++) {
			char *pos = strstr (args[i].name, "::");
			if (pos != NULL &&
			    arg_flags[i] & GTK_ARG_WRITABLE)
				list = g_list_prepend (list, g_strdup (pos+2));
		}
		g_free (args);
		g_free (arg_flags);

		gtk_type = gtk_type_parent (gtk_type);
	}

	list = g_list_reverse (list);

	gtk_combo_set_popdown_strings (GTK_COMBO (combo), list);

	/* hmmm ... should go to utils, and not be in tree I suppose */
	tree_free_string_list (list);

	gtk_entry_set_text (GTK_ENTRY (entry), old_text);
	g_free (old_text);
}

static void
pane_type_changed (XMLTag *tag,
		   const char *key,
		   const char *text,
		   gpointer data)
{
	add_argument_names_to_combo (tag);
}


void
setup_pane_arguments_list (XMLTag *widget)
{
	GList *list, *li;
	int con_id;

	selected_widget = widget;

	add_argument_names_to_combo (widget);

	gtk_clist_freeze (GTK_CLIST (pane_arguments_clist));

	list = tag_get_tags (widget, PONG_S_Argument);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		add_argument_to_clist (tag);
	}

	tree_free_tag_list (list);

	gtk_clist_thaw (GTK_CLIST (pane_arguments_clist));

	con_id = tag_connect_add (widget, pane_argument_added, NULL, NULL);
	connection_add (&per_list_connections, widget, con_id);

	con_id = tag_connect_text (widget, PONG_S_Type,
				   pane_type_changed, NULL, NULL);
	connection_add (&per_list_connections, widget, con_id);
}

void
clear_pane_arguments_list (void)
{
	GtkWidget *w;

	selected_widget = NULL;

	connection_disconnect_all (&per_list_connections);

	gtk_clist_unselect_all (GTK_CLIST (pane_arguments_clist));
	gtk_clist_clear (GTK_CLIST (pane_arguments_clist));

	w = glade_helper_get (app_xml, "pane_argument_edit_table",
			      GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, FALSE);
}

void
change_pane_argument_value (GtkWidget *ventry)
{
	char *text;
	const char *key;

	if (selected_argument == NULL)
		return;

	text = value_entry_get_text (VALUE_ENTRY (ventry));

	if (text != NULL)
		g_strstrip (text);

	if (tag_get_bool (selected_argument, PONG_S_Translatable, FALSE))
		key = "_" PONG_S_Value;
	else
		key = PONG_S_Value;

	if (pong_string_empty (text))
		tag_set_text (selected_argument, key, NULL, NULL);
	else
		tag_set_text (selected_argument, key, text, NULL);

	g_free (text);
}

void
change_pane_argument_translatable (GtkWidget *cb)
{
	gboolean value;
	GList *list, *li;

	if (selected_argument == NULL)
		return;

	value = GTK_TOGGLE_BUTTON (cb)->active;

	tag_set_bool (selected_argument, PONG_S_Translatable, value);
	list = tag_get_tags (selected_argument, PONG_S_Value);
	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		tag_set_translatable (tag, value);
	}
	tree_free_tag_list (list);
}

static void
update_value_entry_type (void)
{
	char *arg_name;
	GtkArgInfo *info;
	char *error;
	GtkWidget *entry;
	GtkWidget *ventry;

	if (selected_argument == NULL)
		return;

	/* if we got this far there HAS to be a selected widget */
	g_assert (selected_widget != NULL);

	entry = glade_helper_get (app_xml, "pane_argument_name_entry",
				  GTK_TYPE_ENTRY);

	arg_name = gtk_entry_get_text (GTK_ENTRY (entry));

	ventry = glade_helper_get (app_xml, "pane_argument_value_entry",
				   TYPE_VALUE_ENTRY);

	if (widget_type == 0) {
		value_entry_set_type (VALUE_ENTRY (ventry), PONG_TYPE_STRING);
		return;
	}

	error = gtk_object_arg_get_info (widget_type, arg_name, &info);
	if (error != NULL) {
		value_entry_set_type (VALUE_ENTRY (ventry), PONG_TYPE_STRING);
		g_free (error);
		return;
	}

	switch (info->type) {
	case GTK_TYPE_INT:
	case GTK_TYPE_ENUM: /* Enums should be by string, but how?? */
		value_entry_set_type (VALUE_ENTRY (ventry), PONG_TYPE_INT);
		break;
	case GTK_TYPE_BOOL:
		value_entry_set_type (VALUE_ENTRY (ventry), PONG_TYPE_BOOL);
		break;
	case GTK_TYPE_FLOAT:
		value_entry_set_type (VALUE_ENTRY (ventry), PONG_TYPE_FLOAT);
		break;
	/* some of these should be spinners, but we need to take
	 * care of range correctly */
	case GTK_TYPE_UINT:
	case GTK_TYPE_CHAR:
	case GTK_TYPE_UCHAR:
	case GTK_TYPE_LONG:
	case GTK_TYPE_ULONG:
	case GTK_TYPE_DOUBLE:
	case GTK_TYPE_STRING:
	default:
		value_entry_set_type (VALUE_ENTRY (ventry), PONG_TYPE_STRING);
	}
}

void
change_pane_argument_name (GtkWidget *entry)
{
	gboolean success;
	char *arg_name;

	if (selected_argument == NULL)
		return;

	/* if we got this far there HAS to be a selected widget */
	g_assert (selected_widget != NULL);

	arg_name = gtk_entry_get_text (GTK_ENTRY (entry));

	success = tree_set_argument_name (selected_widget, selected_argument,
					  arg_name);

	pong_edit_set_red (entry, success ? FALSE : TRUE);

	update_value_entry_type ();
}

static void
setup_pane_argument_edit_area (XMLTag *argument)
{
	GtkWidget *w;
	char *text;
	gboolean value;

	/* The argument name */
	w = glade_helper_get (app_xml, "pane_argument_name_entry",
			      GTK_TYPE_ENTRY);
	text = tag_get_text (argument, PONG_S_Name, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The argument value */
	w = glade_helper_get (app_xml, "pane_argument_value_entry",
			      TYPE_VALUE_ENTRY);
	text = tag_get_text (argument, PONG_S_Value, NULL);
	value_entry_set_text (VALUE_ENTRY (w), pong_sure_string (text));
	value_entry_set_type (VALUE_ENTRY (w), PONG_TYPE_STRING);
	g_free (text);

	/* value translatable */
	w = glade_helper_get (app_xml, "pane_argument_translatable_cb",
			      GTK_TYPE_CHECK_BUTTON);
	value = tag_get_bool (argument, PONG_S_Translatable, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), value);
}

static void
clear_pane_argument_edit_area (void)
{
	GtkWidget *w;

	/* The argument name */
	w = glade_helper_get (app_xml, "pane_argument_name_entry",
			      GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The argument value */
	w = glade_helper_get (app_xml, "pane_argument_value_entry",
			      TYPE_VALUE_ENTRY);
	value_entry_set_text (VALUE_ENTRY (w), "");

	/* value translatable */
	w = glade_helper_get (app_xml, "pane_argument_translatable_cb",
			      GTK_TYPE_CHECK_BUTTON);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), FALSE);
}

void
pane_arguments_clist_select_row (GtkCList *clist, int row, int column,
				 GdkEvent *event)
{
	GtkWidget *w;
	XMLTag *tag = gtk_clist_get_row_data (clist, row);

	selected_argument = NULL;

	w = glade_helper_get (app_xml, "pane_argument_edit_table",
			      GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, TRUE);

	setup_pane_argument_edit_area (tag);
	selected_argument = tag;

	update_value_entry_type ();
}

void
pane_arguments_clist_unselect_row (GtkCList *clist, int row, int column,
				   GdkEvent *event)
{
	GtkWidget *w;

	selected_argument = NULL;
	clear_pane_argument_edit_area ();

	w = glade_helper_get (app_xml, "pane_argument_edit_table",
			      GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, FALSE);

	update_value_entry_type ();
}

void
pane_argument_add (GtkWidget *w)
{
	XMLTag *tag;

	if (selected_widget != NULL)
		tag = tree_widget_add_argument (selected_widget);
	else
		tag = NULL;

	clist_select_thing (GTK_CLIST (pane_arguments_clist), tag);

	tag_unref (tag);
}

void
pane_argument_remove (GtkWidget *w)
{
	if (selected_argument != NULL)
		tag_kill (selected_argument);
}
