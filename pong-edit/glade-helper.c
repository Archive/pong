/* Glade helper routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
/*
 * Note this file requires pong, even though that's not obvious on first
 * look.  But pong has all the gladefile searching logic. */

/*
 * XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 *
 * KEEP THESE FILES IN SYNC:
 *        pong/pong-edit/glade-helper.[ch]
 *        grapevine/src/glade-helper.[ch]
 *
 * XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 */

#include <config.h>
#include <gnome.h>
#include <glade/glade.h>

#include <pong/pong-glade.h>

#include "glade-helper.h"

static void glade_helper_no_interface (const char *filename);
static void glade_helper_bad_interface (const char *filename,
					const char *widget,
					GtkType type);
static void glade_helper_bad_columns (const char *filename,
				      const char *widget,
				      GtkType type,
				      int columns);

GladeXML *
glade_helper_load (const char *file, const char *widget,
		   GtkType expected_type,
		   gboolean dump_on_destroy)
{
	char *f;
	GladeXML *xml;
	GtkWidget *w;

	g_return_val_if_fail (file != NULL, NULL);

	f = pong_find_glade_file (file);

	if (f == NULL) {
		glade_helper_no_interface (file);
		gtk_exit (1);
	}

	xml = glade_xml_new (f, widget);
	/* in case we can't load the interface, bail */
	if (xml == NULL) {
		glade_helper_no_interface (file);
		gtk_exit (1);
	}

	w = glade_xml_get_widget (xml, widget);
	if (w == NULL ||
	    ! GTK_CHECK_TYPE (w, expected_type)) {
		glade_helper_bad_interface (xml->filename != NULL ?
					      xml->filename :
					      _("(memory buffer)"),
					    widget,
					    expected_type);
		gtk_exit (1);
	}

	if (dump_on_destroy) {
		gtk_object_set_data_full (GTK_OBJECT (w),
					  "GladeXML",
					  xml,
					  (GtkDestroyNotify) gtk_object_unref);
	}

	return xml;
}

GtkWidget *
glade_helper_load_widget (const char *file, const char *widget,
			  GtkType expected_type)
{
	GladeXML *xml;
	GtkWidget *w;

	/* this is guaranteed to return non-NULL, otherwise we
	 * would have exited */
	xml = glade_helper_load (file, widget, expected_type, FALSE);

	w = glade_xml_get_widget (xml, widget);
	if (w == NULL ||
	    ! GTK_CHECK_TYPE (w, expected_type)) {
		glade_helper_bad_interface (xml->filename != NULL ?
					      xml->filename :
					      _("(memory buffer"),
					    widget,
					    expected_type);
		gtk_exit (1);
	}

	gtk_object_unref (GTK_OBJECT (xml));
	
	return w;
}

GtkWidget *
glade_helper_get (GladeXML *xml, const char *widget, GtkType expected_type)
{
	GtkWidget *w;

	w = glade_xml_get_widget (xml, widget);

	if (w == NULL ||
	    ! GTK_CHECK_TYPE (w, expected_type)) {
		glade_helper_bad_interface (xml->filename != NULL ?
					      xml->filename :
					      _("(memory buffer"),
					    widget,
					    expected_type);
		gtk_exit (1);
	}

	return w;
}

GtkWidget *
glade_helper_get_clist (GladeXML *xml, const char *widget,
			GtkType expected_type, int expected_columns)
{
	GtkWidget *w;

	w = glade_helper_get (xml, widget, expected_type);

	if (GTK_CLIST (w)->columns != expected_columns) {
		glade_helper_bad_columns (xml->filename != NULL ?
					    xml->filename :
					    _("(memory buffer"),
					  widget,
					  expected_type,
					  expected_columns);
		gtk_exit (1);
	}

	return w;
}

static void
glade_helper_bad_interface (const char *filename, const char *widget,
			    GtkType type)
{
	char *s;
	GtkWidget *w;
	char *typestring;
	const char *typename;

	if (type != 0)
		typename = gtk_type_name (type);
	else
		typename = NULL;

	if (typename == NULL)
		typestring = g_strdup_printf (" (%s)", typename);
	else
		typestring = g_strdup ("");
	
	s = g_strdup_printf (_("An error occured while loading the user "
			       "interface\nelement %s%s from file %s.\n"
			       "Possibly the glade interface description was "
			       "corrupted.\n"
			       "%s cannot continue and will exit now.\n"
			       "You should check your "
			       "installation of %s or reinstall %s."),
			     widget,
			     typestring,
			     filename,
			     GLADE_HELPER_NAME,
			     GLADE_HELPER_OVERALL_NAME,
			     GLADE_HELPER_OVERALL_NAME);
	w = gnome_error_dialog (s);

	g_free (s);
	g_free (typestring);

	g_warning (_("Glade file is on crack! Make sure the correct "
		     "file is installed!\nfile: %s widget: %s"),
		   filename, widget);

	gnome_dialog_run (GNOME_DIALOG (w));
}

static void
glade_helper_bad_columns (const char *filename, const char *widget,
			  GtkType type, int columns)
{
	char *s;
	GtkWidget *w;
	char *typestring;
	const char *typename;

	if (type != 0)
		typename = gtk_type_name (type);
	else
		typename = NULL;

	if (typename == NULL)
		typestring = g_strdup_printf (" (%s)", typename);
	else
		typestring = g_strdup ("");
	
	s = g_strdup_printf (_("An error occured while loading the user "
			       "interface\nelement %s%s from file %s.\n"
			       "CList type widget should have %d columns.\n"
			       "Possibly the glade interface description was "
			       "corrupted.\n"
			       "%s cannot continue and will exit now.\n"
			       "You should check your "
			       "installation of %s or reinstall %s."),
			     widget,
			     typestring,
			     filename,
			     columns,
			     GLADE_HELPER_NAME,
			     GLADE_HELPER_OVERALL_NAME,
			     GLADE_HELPER_OVERALL_NAME);
	w = gnome_error_dialog (s);

	g_free (s);
	g_free (typestring);

	g_warning (_("Glade file is on crack! Make sure the correct "
		     "file is installed!\nfile: %s widget: %s "
		     "expected clist columns: %d"),
		   filename, widget, columns);

	gnome_dialog_run (GNOME_DIALOG (w));
}

static void
glade_helper_no_interface (const char *filename)
{
	char *s;
	GtkWidget *w;
	
	s = g_strdup_printf (_("An error occured while loading the user "
			       "interface\nfrom file %s.\n"
			       "Possibly the glade interface description was "
			       "not found.\n"
			       "%s cannot continue and will exit now.\n"
			       "You should check your "
			       "installation of %s or reinstall %s."),
			     filename,
			     GLADE_HELPER_NAME,
			     GLADE_HELPER_OVERALL_NAME,
			     GLADE_HELPER_OVERALL_NAME);
	w = gnome_error_dialog (s);
	g_free (s);
	g_warning (_("No interface could be loaded, BAD! (file: %s)"),
		   filename);
	gnome_dialog_run (GNOME_DIALOG (w));
}
