/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "tree.h"
#include "glade-helper.h"
#include "pong-edit.h"
#include "util.h"

#include "element-sensitivities.h"

static XMLTag *selected_sensitivity = NULL;
static XMLTag *selected_element = NULL;

static gboolean dialog_in_setup = FALSE;
static GladeXML *sensitivity_edit_xml = NULL;
static GtkWidget *sensitivity_edit_dialog = NULL;


void element_sensitivities_clist_select_row	(GtkCList *clist,
						 int row, int column,
						 GdkEvent *event);
void element_sensitivities_clist_unselect_row	(GtkCList *clist,
						 int row, int column,
						 GdkEvent *event);

void element_sensitivity_add			(GtkWidget *w);
void element_sensitivity_remove			(GtkWidget *w);
void element_sensitivity_edit			(GtkWidget *w);

void sensitivity_edit_values_changed		(GtkWidget *box);
void sensitivity_edit_sensitive_changed		(GtkWidget *box);
void sensitivity_edit_insensitive_changed	(GtkWidget *box);

void sensitivity_edit_select_sensitive		(GtkWidget *w);
void sensitivity_edit_select_insensitive	(GtkWidget *w);

static GHashTable *comparison_hash = NULL;

static GList *per_list_connections = NULL;

#include "../pong/pong-comparisons.cP"

#define NUM_COMPARISONS 6

static const char *comparison_strings[] = {
	"LessThen",
	"LessThenOrEqual",
	"Equal",
	"GreaterThen",
	"GreaterThenOrEqual",
	"NotEqual"
};

static int
comparison_index (PongComparison comparison)
{
	switch (comparison) {
	case PONG_COMP_LT: return 0;
	case PONG_COMP_LE: return 1;
	case PONG_COMP_EQ: return 2;
	case PONG_COMP_GE: return 3;
	case PONG_COMP_GT: return 4;
	case PONG_COMP_NE: return 5;
	default: return 2;
	}
}

static void
init_comparison_hash (void)
{
	int i;
	if (comparison_hash != NULL)
		return;

	comparison_hash = g_hash_table_new (g_str_hash, g_str_equal);

	for (i = 0; comparisons[i].name != NULL; i++) {
		g_hash_table_insert (comparison_hash, comparisons[i].name,
				     GINT_TO_POINTER (comparisons[i].value));
	}
}

static char *
get_value_string (XMLTag *tag)
{
	GList *list, *li;
	GString *str = g_string_new (NULL);
	char *text;
	char *connection;
	int comparison;

	init_comparison_hash ();

	text = tag_get_text (tag, PONG_S_Comparison, NULL);

	if (text != NULL) {
		comparison = GPOINTER_TO_INT (g_hash_table_lookup (comparison_hash, text));

		g_free (text);

		if (comparison == PONG_COMP_INVALID)
			comparison = PONG_COMP_EQ;
	} else {
		comparison = PONG_COMP_EQ;
	}

	connection = tag_get_text (tag, PONG_S_Connection, NULL);
	if (connection == NULL)
		connection = g_strdup ("OR");

	g_strup (connection);

	switch (comparison) {
	case PONG_COMP_EQ:
		g_string_append (str, "[==] ");
		break;
	case PONG_COMP_NE:
		g_string_append (str, "[!=] ");
		break;
	case PONG_COMP_LT:
		g_string_append (str, "[<] ");
		break;
	case PONG_COMP_LE:
		g_string_append (str, "[<=] ");
		break;
	case PONG_COMP_GE:
		g_string_append (str, "[>=] ");
		break;
	case PONG_COMP_GT:
		g_string_append (str, "[>] ");
		break;
	}

	list = tag_get_text_list (tag, PONG_S_Value);

	for (li = list; li != NULL; li = li->next) {
		char *string = li->data;
		if (li != list)
			g_string_sprintfa (str, " %s ", connection);
		if (pong_string_empty (string) ||
		    string[0] == ' ' ||
		    string[0] == '\t' ||
		    string[strlen (string) - 1] == ' ' ||
		    string[strlen (string) - 1] == '\t')
			g_string_sprintfa (str, "\"%s\"",
					   pong_sure_string (string));
		else
			g_string_append (str, string);
	}

	tree_free_string_list (list);

	{
		char *ret = str->str;
		g_string_free (str, FALSE);
		return ret;
	}
}

static char *
get_sensitive_string (XMLTag *tag, gboolean sensitive)
{
	GList *list, *li;
	GString *str = g_string_new (NULL);

	list = tag_get_text_list (tag,
				  sensitive ?
				    PONG_S_Sensitive :
				    PONG_S_Insensitive);

	for (li = list; li != NULL; li = li->next) {
		char *string = li->data;
		if (li != list)
			g_string_append (str, ", ");
		g_string_append (str, string);
	}

	tree_free_string_list (list);

	{
		char *ret = str->str;
		g_string_free (str, FALSE);
		return ret;
	}
}

static void
element_sensitivity_value_updated (XMLTag *tag,
				   const char *key,
				   const char *text,
				   gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (element_sensitivities_clist), tag);

	if (row >= 0) {
		char *value = get_value_string (tag);

		gtk_clist_set_text (GTK_CLIST (element_sensitivities_clist),
				    row, 0, value);

		g_free (value);
	}
}

static void
update_value (XMLTag *sensitivity)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (element_sensitivities_clist),
				      sensitivity);

	if (row >= 0) {
		char *value = get_value_string (sensitivity);

		gtk_clist_set_text (GTK_CLIST (element_sensitivities_clist),
				    row, 0, value);

		g_free (value);
	}
}

static void
update_sensitive (XMLTag *sensitivity, gboolean sensitive)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (element_sensitivities_clist),
				      sensitivity);

	if (row >= 0) {
		char *string = get_sensitive_string (sensitivity, sensitive);

		gtk_clist_set_text (GTK_CLIST (element_sensitivities_clist),
				    row, (sensitive ? 1 : 2), string);

		g_free (string);
	}
}

static void
value_changed (XMLTag *tag, const char *key, const char *value, gpointer data)
{
	XMLTag *sensitivity = data;

	update_value (sensitivity);
}

static void
sensitive_changed (XMLTag *tag, const char *key, const char *value, gpointer data)
{
	XMLTag *sensitivity = data;

	update_sensitive (sensitivity, TRUE);
}

static void
insensitive_changed (XMLTag *tag, const char *key, const char *value, gpointer data)
{
	XMLTag *sensitivity = data;

	update_sensitive (sensitivity, FALSE);
}

static void
element_sensitivity_thing_added (XMLTag *sensitivity, XMLTag *reference, gpointer data)
{
	int con_id;
	const char *name = tag_peek_type (reference);

	if (name == NULL)
		return;

	if (pong_strcasecmp_no_locale (name, PONG_S_Value) == 0) {
		update_value (sensitivity);
		con_id = tag_connect_text (reference, NULL,
					   value_changed,
					   tag_ref (sensitivity),
					   (GDestroyNotify) tag_unref);
		connection_add (&per_list_connections, reference, con_id);
	} else if (pong_strcasecmp_no_locale (name, PONG_S_Sensitive) == 0) {
		update_sensitive (sensitivity, TRUE);
		con_id = tag_connect_text (reference, NULL,
					   sensitive_changed,
					   tag_ref (sensitivity),
					   (GDestroyNotify) tag_unref);
		connection_add (&per_list_connections, reference, con_id);
	} else if (pong_strcasecmp_no_locale (name, PONG_S_Insensitive) == 0) {
		update_sensitive (sensitivity, FALSE);
		con_id = tag_connect_text (reference, NULL,
					   insensitive_changed,
					   tag_ref (sensitivity),
					   (GDestroyNotify) tag_unref);
		connection_add (&per_list_connections, reference, con_id);
	}
}

static void
element_sensitivity_thing_killed (XMLTag *sensitivity, XMLTag *reference, gpointer data)
{
	int row;
	char *string;

	row = pong_edit_find_tag_row (GTK_CLIST (element_sensitivities_clist), sensitivity);

	if (row < 0)
		return;

	string = get_value_string (sensitivity);

	gtk_clist_set_text (GTK_CLIST (element_sensitivities_clist),
			    row, 0, string);

	g_free (string);

	string = get_sensitive_string (sensitivity, TRUE);

	gtk_clist_set_text (GTK_CLIST (element_sensitivities_clist),
			    row, 1, string);

	g_free (string);

	string = get_sensitive_string (sensitivity, FALSE);

	gtk_clist_set_text (GTK_CLIST (element_sensitivities_clist),
			    row, 2, string);

	g_free (string);
}

static void
element_sensitivity_killed (XMLTag *tag, gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (element_sensitivities_clist), tag);

	if (row >= 0) {
		gtk_clist_remove (GTK_CLIST (element_sensitivities_clist), row);
	}
}

static int
add_sensitivity_to_clist (XMLTag *sensitivity)
{
	int row;
	char *text[3];
	char *value, *sensitive, *insensitive;
	int con_id;
	GList *list, *li;

	value = get_value_string (sensitivity);
	sensitive = get_sensitive_string (sensitivity, TRUE);
	insensitive = get_sensitive_string (sensitivity, FALSE);

	text[0] = pong_sure_string (value);
	text[1] = pong_sure_string (sensitive);
	text[2] = pong_sure_string (insensitive);

	row = gtk_clist_append (GTK_CLIST (element_sensitivities_clist), text);

	g_free (value);
	g_free (sensitive);
	g_free (insensitive);

	gtk_clist_set_row_data_full (GTK_CLIST (element_sensitivities_clist),
				     row,
				     tag_ref (sensitivity),
				     (GtkDestroyNotify)tag_unref);

	con_id = tag_connect_text (sensitivity, PONG_S_Connection,
				   element_sensitivity_value_updated,
				   NULL, NULL);
	connection_add (&per_list_connections, sensitivity, con_id);
	con_id = tag_connect_text (sensitivity, PONG_S_Comparison,
				   element_sensitivity_value_updated,
				   NULL, NULL);
	connection_add (&per_list_connections, sensitivity, con_id);
	con_id = tag_connect_add (sensitivity, element_sensitivity_thing_added,
				  NULL, NULL);
	connection_add (&per_list_connections, sensitivity, con_id);
	con_id = tag_connect_kill_child (sensitivity,
					 element_sensitivity_thing_killed,
					 NULL, NULL);
	connection_add (&per_list_connections, sensitivity, con_id);
	con_id = tag_connect_kill (sensitivity, element_sensitivity_killed,
				   NULL, NULL);
	connection_add (&per_list_connections, sensitivity, con_id);

	list = tag_get_tags (sensitivity, PONG_S_Value);
	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		con_id = tag_connect_text (tag, NULL,
					   value_changed,
					   tag_ref (sensitivity),
					   (GDestroyNotify) tag_unref);
		connection_add (&per_list_connections, tag, con_id);
	}
	tree_free_tag_list (list);

	list = tag_get_tags (sensitivity, PONG_S_Sensitive);
	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		con_id = tag_connect_text (tag, NULL,
					   sensitive_changed,
					   tag_ref (sensitivity),
					   (GDestroyNotify) tag_unref);
		connection_add (&per_list_connections, tag, con_id);
	}
	tree_free_tag_list (list);

	list = tag_get_tags (sensitivity, PONG_S_Insensitive);
	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;
		con_id = tag_connect_text (tag, NULL,
					   insensitive_changed,
					   tag_ref (sensitivity),
					   (GDestroyNotify) tag_unref);
		connection_add (&per_list_connections, tag, con_id);
	}
	tree_free_tag_list (list);

	return row;
}

static void
element_sensitivity_added (XMLTag *element, XMLTag *reference, gpointer data)
{
	const char *name = tag_peek_type (reference);

	if (name != NULL &&
	    pong_strcasecmp_no_locale (name, PONG_S_Sensitivity) == 0) {
		add_sensitivity_to_clist (reference);
	}
}

void
setup_element_sensitivities_list (XMLTag *element)
{
	GList *list, *li;
	int con_id;

	selected_element = element;

	gtk_clist_freeze (GTK_CLIST (element_sensitivities_clist));

	list = tag_get_tags (element, PONG_S_Sensitivity);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		add_sensitivity_to_clist (tag);
	}

	tree_free_tag_list (list);

	gtk_clist_thaw (GTK_CLIST (element_sensitivities_clist));

	con_id = tag_connect_add (element, element_sensitivity_added,
				  NULL, NULL);
	connection_add (&per_list_connections, element, con_id);
}

void
clear_element_sensitivities_list (void)
{
	selected_element = NULL;

	connection_disconnect_all (&per_list_connections);

	gtk_clist_unselect_all (GTK_CLIST (element_sensitivities_clist));
	gtk_clist_clear (GTK_CLIST (element_sensitivities_clist));
}

void
element_sensitivities_clist_select_row (GtkCList *clist,
					int row,
					int column,
					GdkEvent *event)
{
	XMLTag *tag = gtk_clist_get_row_data (clist, row);

	selected_sensitivity = tag;
}

void
element_sensitivities_clist_unselect_row (GtkCList *clist,
					  int row,
					  int column,
					  GdkEvent *event)
{
	selected_sensitivity = NULL;

	if (sensitivity_edit_dialog != NULL)
		gnome_dialog_close (GNOME_DIALOG (sensitivity_edit_dialog));
}

void
element_sensitivity_add (GtkWidget *w)
{
	if (selected_element != NULL) {
		XMLTag *tag;
		const char *def_val;
		char *type = tag_get_text (selected_element, PONG_S_Type, NULL);
		if (type != NULL &&
		    (pong_strcasecmp_no_locale (type, PONG_S_Int) == 0 ||
		     pong_strcasecmp_no_locale (type, PONG_S_Float) == 0))
			def_val = "0";
		else if (type != NULL &&
			 pong_strcasecmp_no_locale (type, PONG_S_Bool) == 0)
			def_val = "True";
		else if (type != NULL &&
			 pong_strcasecmp_no_locale (type, PONG_S_String) == 0)
			def_val = "foo";
		else
			def_val = "";
		g_free (type);
		tag = tree_element_add_sensitivity (selected_element, def_val);
		clist_select_thing (GTK_CLIST (element_sensitivities_clist),
				    tag);
		tag_unref (tag);
	}
}

void
element_sensitivity_remove (GtkWidget *w)
{
	if (selected_sensitivity != NULL)
		tag_kill (selected_sensitivity);
}

static char *
get_text_from_list (GList *list)
{
	GString *str = g_string_new (NULL);

	if (list != NULL) {
		g_string_append (str, list->data);
		list = list->next;
	}

	while (list != NULL) {
		g_string_append (str, "\n");
		g_string_append (str, list->data);
		list = list->next;
	}

	{
		char *ret = str->str;
		g_string_free (str, FALSE);
		return ret;
	}
}

static void
set_text_entry (const char *glade_id, const char *xml_id)
{
	GtkWidget *w;
	GList *list;
	char *text;

	w = glade_helper_get (sensitivity_edit_xml, glade_id,
			      GTK_TYPE_TEXT);
	list = tag_get_text_list (selected_sensitivity, xml_id);
	text = get_text_from_list (list);
	tree_free_tag_list (list);
	gtk_text_insert (GTK_TEXT (w),
			 NULL /*font*/,
			 NULL /*fore*/,
			 NULL /*back*/,
			 pong_sure_string (text),
			 -1 /*length*/);
	g_free (text);
}

static GList *
get_list_from_text (GtkWidget *box)
{
	char *text;
	char **vector;
	GList *list;
	int i;

	text = gtk_editable_get_chars (GTK_EDITABLE (box), 0, -1);

	if (text == NULL) {
		g_warning ("Textbox on crack!");
		return NULL;
	}

	vector = g_strsplit (text, "\n", 0);

	list = NULL;
	for (i = 0; vector[i] != NULL; i++) {
		char *text = g_strdup (vector[i]);

		/* XXX: perhaps should be done only for strings! */
		if (text[0] == '"' &&
		    text[1] != '\0' &&
		    text[strlen(text) - 1] == '"') {
			text[strlen(text) - 1] = '\0';
			strcpy (text, &text[1]);
		}

		list = g_list_prepend (list, text);
	}

	g_strfreev (vector);
	g_free (text);

	return list;
}

void
sensitivity_edit_values_changed (GtkWidget *box)
{
	GList *list;

	if (dialog_in_setup ||
	    selected_sensitivity == NULL)
		return;

	list = get_list_from_text (box);

	tag_set_text_list (selected_sensitivity, PONG_S_Value, list);

	tree_free_string_list (list);
}

void
sensitivity_edit_sensitive_changed (GtkWidget *box)
{
	GList *list;

	if (dialog_in_setup ||
	    selected_sensitivity == NULL)
		return;

	list = get_list_from_text (box);

	tag_set_text_list (selected_sensitivity, PONG_S_Sensitive, list);

	tree_free_string_list (list);
}

void
sensitivity_edit_insensitive_changed (GtkWidget *box)
{
	GList *list;

	if (dialog_in_setup ||
	    selected_sensitivity == NULL)
		return;

	list = get_list_from_text (box);

	tag_set_text_list (selected_sensitivity, PONG_S_Insensitive, list);

	tree_free_string_list (list);
}

static void
fill_widget_clist (GtkWidget *clist)
{
	char *text[1];
	GList *names, *li;

	gtk_clist_freeze (GTK_CLIST (clist));

	names = tree_get_names (xml_tree);

	for (li = names; li != NULL; li = li->next) {
		text[0] = li->data;
		gtk_clist_append (GTK_CLIST (clist), text);
	}

	tree_free_string_list (names);

	gtk_clist_thaw (GTK_CLIST (clist));
}

static void
append_widget (GtkWidget *box, const char *widget)
{
	char *begtext;

	gtk_text_set_point (GTK_TEXT (box), 0);

	begtext = gtk_editable_get_chars (GTK_EDITABLE (box), 0, 1);
	gtk_text_insert (GTK_TEXT (box),
			 NULL /*font*/,
			 NULL /*fore*/,
			 NULL /*back*/,
			 pong_sure_string (widget),
			 -1 /*length*/);
	if (begtext != NULL &&
	    begtext[0] != '\n')
		gtk_text_insert (GTK_TEXT (box),
				 NULL /*font*/,
				 NULL /*fore*/,
				 NULL /*back*/,
				 "\n",
				 -1 /*length*/);
	g_free (begtext);

	gtk_text_set_point (GTK_TEXT (box), 0);

	/* BRAINDAMAGE!!!! */
	gtk_signal_emit_by_name (GTK_OBJECT (box), "changed");
}

static void
select_widget_row (GtkCList *clist,
		   int row, int column,
		   GdkEvent *event,
		   gpointer data)
{
	GtkWidget *box = data;
	char *text;

	if (event == NULL ||
	    event->type != GDK_2BUTTON_PRESS)
		return;

	if (gtk_clist_get_text (clist, row, column, &text) &&
	    text != NULL) {
		GtkWidget *dlg = gtk_object_get_data (GTK_OBJECT (box),
						      "Select-Dialog");
		g_assert (dlg != NULL);
		append_widget (box, text);
		gnome_dialog_close (GNOME_DIALOG (dlg));
	}
}

static void
select_widget_clicked (GtkWidget *dlg, int button, gpointer data)
{
	GtkWidget *clist = data;

	if (button == 0 /* OK */) {
		GtkWidget *box;
		int row;

		box = gtk_object_get_data (GTK_OBJECT (dlg), "Select-Box");

		g_assert (box != NULL);

		if (GTK_CLIST (clist)->selection != NULL) {
			char *text;
			row = GPOINTER_TO_INT
				(GTK_CLIST (clist)->selection->data);
	
			if (gtk_clist_get_text (GTK_CLIST (clist),
						row, 0, &text) &&
			    text != NULL) {
				append_widget (box, text);
			}
		}
	}

	gnome_dialog_close (GNOME_DIALOG (dlg));
}


static void
select_dlg_destroyed (GtkWidget *dlg, gpointer data)
{
	GtkWidget *box = data;

	gtk_object_remove_data (GTK_OBJECT (box), "Select-Dialog");

	gtk_object_unref (GTK_OBJECT (box));
}

static void
run_select_widget_dialog (GtkWidget *box)
{
	GladeXML *xml;
	GtkWidget *dlg;
	GtkWidget *clist;

	if (selected_sensitivity == NULL ||
	    dialog_in_setup ||
	    sensitivity_edit_dialog == NULL) {
		return;
	}

	dlg = gtk_object_get_data (GTK_OBJECT (box), "Select-Dialog");

	if (dlg != NULL) {
		gtk_widget_show_all (dlg);
		gdk_window_raise (dlg->window);
		return;
	}

	xml = glade_helper_load ("pong-edit.glade",
				 "select_widget",
				 gnome_dialog_get_type (),
				 FALSE);
	dlg = glade_helper_get (xml,
				"select_widget",
				gnome_dialog_get_type ());
	glade_xml_signal_autoconnect (xml);

	gtk_signal_connect_object_while_alive
		(GTK_OBJECT (box), "destroy",
		 GTK_SIGNAL_FUNC (gnome_dialog_close),
		 GTK_OBJECT (dlg));

	gtk_object_ref (GTK_OBJECT (box));
	gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
			    GTK_SIGNAL_FUNC (select_dlg_destroyed),
			    box);

	gnome_dialog_set_parent (GNOME_DIALOG (dlg),
				 GTK_WINDOW (sensitivity_edit_dialog));

	clist = glade_helper_get_clist (xml,
					"select_widget_clist",
					GTK_TYPE_CLIST, 1);

	fill_widget_clist (clist);

	gtk_signal_connect (GTK_OBJECT (clist), "select_row",
			    GTK_SIGNAL_FUNC (select_widget_row),
			    box);

	gtk_object_set_data (GTK_OBJECT (box), "Select-Dialog", dlg);
	gtk_object_set_data (GTK_OBJECT (dlg), "Select-Box", box);

	gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
			    GTK_SIGNAL_FUNC (select_widget_clicked),
			    clist);

	gtk_widget_show (dlg);

	gtk_object_unref (GTK_OBJECT (xml));
}

void
sensitivity_edit_select_sensitive (GtkWidget *w)
{
	GtkWidget *box = glade_helper_get (sensitivity_edit_xml,
					   "sensitivity_edit_sensitive_text",
					   GTK_TYPE_TEXT);
	run_select_widget_dialog (box);
}

void
sensitivity_edit_select_insensitive (GtkWidget *w)
{
	GtkWidget *box = glade_helper_get (sensitivity_edit_xml,
					   "sensitivity_edit_insensitive_text",
					   GTK_TYPE_TEXT);
	run_select_widget_dialog (box);
}

static void
comparison_changed (GtkWidget *w, gpointer data)
{
	GtkWidget *om = data;
	const char *compr;
	int index;

	if (dialog_in_setup ||
	    selected_sensitivity == NULL)
		return;

	index = pong_gtk_option_menu_get_history (GTK_OPTION_MENU (om));

	if (index < 0 || index >= NUM_COMPARISONS) {
		g_warning ("Option menu on crack!");
		return;
	}

	compr = comparison_strings[index];

	tag_set_text (selected_sensitivity, PONG_S_Comparison, compr, NULL);
}

static void
connection_changed (GtkWidget *w, gpointer data)
{
	GtkWidget *om = data;
	int index;
	const char *strings[] = {
		"And",
		"Or"
	};

	if (dialog_in_setup ||
	    selected_sensitivity == NULL)
		return;

	index = pong_gtk_option_menu_get_history (GTK_OPTION_MENU (om));

	if (index < 0 || index >= 2) {
		g_warning ("Option menu on crack!");
		return;
	}

	tag_set_text (selected_sensitivity, PONG_S_Connection, strings[index], NULL);
}

static void
connect_signals (GladeXML *xml)
{
	int i;
	GtkWidget *om, *item;

	om = glade_helper_get (xml, "sensitivity_edit_comparison_om",
			       GTK_TYPE_OPTION_MENU);
	for (i = 0; i < NUM_COMPARISONS; i++) {
		item = pong_gtk_option_menu_get_item (GTK_OPTION_MENU (om), i);
		if (item == NULL) {
			g_warning ("Option menu on crack!");
			continue;
		}

		gtk_signal_connect_after (GTK_OBJECT (item), "activate",
					  GTK_SIGNAL_FUNC (comparison_changed),
					  om);
	}

	om = glade_helper_get (xml, "sensitivity_edit_connection_om",
			       GTK_TYPE_OPTION_MENU);
	for (i = 0; i < 2; i++) {
		item = pong_gtk_option_menu_get_item (GTK_OPTION_MENU (om), i);
		if (item == NULL) {
			g_warning ("Option menu on crack!");
			continue;
		}

		gtk_signal_connect (GTK_OBJECT (item), "activate",
				    GTK_SIGNAL_FUNC (connection_changed),
				    om);
	}
}

static void
setup_options (GladeXML *xml)
{
	GtkWidget *om, *item;
	char *text;
	int comparison, connection;

	g_return_if_fail (selected_sensitivity != NULL);

	text = tag_get_text (selected_sensitivity, PONG_S_Comparison, NULL);

	comparison = PONG_COMP_INVALID;
	if (text != NULL) {
		comparison = GPOINTER_TO_INT (g_hash_table_lookup (comparison_hash, text));

		g_free (text);
	}

	if (comparison == PONG_COMP_INVALID)
		comparison = PONG_COMP_EQ;

	/* convert to index value */
	comparison = comparison_index (comparison);

	text = tag_get_text (selected_sensitivity, PONG_S_Connection, NULL);

	if (text != NULL &&
	    pong_strcasecmp_no_locale (text, "And") == 0) {
		connection = 0 /* AND */;
	} else {
		connection = 1 /* OR */;
	}

	om = glade_helper_get (xml, "sensitivity_edit_comparison_om",
			       GTK_TYPE_OPTION_MENU);
	gtk_option_menu_set_history (GTK_OPTION_MENU (om), comparison);
	item = pong_gtk_option_menu_get_item (GTK_OPTION_MENU (om), comparison);
	if (item == NULL) {
		g_warning ("Option menu on crack!");
	} else {
		gtk_menu_item_activate (GTK_MENU_ITEM (item));
	}

	om = glade_helper_get (xml, "sensitivity_edit_connection_om",
			       GTK_TYPE_OPTION_MENU);
	gtk_option_menu_set_history (GTK_OPTION_MENU (om), connection);
	item = pong_gtk_option_menu_get_item (GTK_OPTION_MENU (om), connection);
	if (item == NULL) {
		g_warning ("Option menu on crack!");
	} else {
		gtk_menu_item_activate (GTK_MENU_ITEM (item));
	}
}

void
element_sensitivity_edit (GtkWidget *w)
{
	if (selected_sensitivity == NULL)
		return;

	if (sensitivity_edit_dialog == NULL) {
		sensitivity_edit_xml =
			glade_helper_load ("pong-edit.glade",
					   "sensitivity_edit",
					   gnome_dialog_get_type (),
					   TRUE);
		sensitivity_edit_dialog =
			glade_helper_get (sensitivity_edit_xml,
					  "sensitivity_edit",
					  gnome_dialog_get_type ());
		glade_xml_signal_autoconnect (sensitivity_edit_xml);
		gnome_dialog_set_parent (GNOME_DIALOG (sensitivity_edit_dialog),
					 GTK_WINDOW (app));

		connect_signals (sensitivity_edit_xml);
	}

	/* don't notice changes during this time */
	dialog_in_setup = TRUE;

	set_text_entry ("sensitivity_edit_values_text", PONG_S_Value);
	set_text_entry ("sensitivity_edit_sensitive_text", PONG_S_Sensitive);
	set_text_entry ("sensitivity_edit_insensitive_text",
			PONG_S_Insensitive);

	setup_options (sensitivity_edit_xml);

	dialog_in_setup = FALSE;

	gtk_signal_connect (GTK_OBJECT (sensitivity_edit_dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &sensitivity_edit_xml);
	gtk_signal_connect (GTK_OBJECT (sensitivity_edit_dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &sensitivity_edit_dialog);
}
