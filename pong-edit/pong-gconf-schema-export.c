/* PonG-GConf-Schema-Export: Export for GConf schemas
 * Author: George Lebl
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <pong/pong.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "tree.h"

int
main (int argc, char *argv[])
{
	poptContext ctx;
	const char **args;
	const char *file;
	const char *out_file;
	char *found_file;
	XMLTree *tree;

	gnomelib_init ("pong-gconf-schema-export", VERSION);

	ctx = gnomelib_parse_args (argc, argv, 0);

	if ( ! pong_init ())
		g_error (_("PonG init failed!"));

	args = poptGetArgs (ctx);

	if (args == NULL ||
	    args[0] == NULL ||
	    args[1] == NULL ||
	    args[2] != NULL) {
		g_warning (_("You must supply two arguments, "
			     "the .pong file to read and the .schemas file to write."));
		exit (1);
	}

	file = args[0];
	out_file = args[1];

	g_print ("\nPonG GConf Schema Exporter %s\n\n", VERSION);

	found_file = pong_find_file (file, NULL);
	if (found_file == NULL) {
		g_warning (_("Cannot find %s"), file);
		exit (1);
	}

	g_print ("Reading %s ...\n", found_file);

	tree = tree_load (found_file, PONG_S_PongElements);
	if (tree == NULL) {
		g_warning (_("Cannot load %s"), found_file);
		exit (1);
	}

	g_print ("Exporting to %s ...\n", out_file);

	if ( ! tree_write_schemas (tree, out_file)) {
		g_warning (_("Cannot write %s"), out_file);
		exit (1);
	}

	g_print ("Done!\n\n");

	return 0;
}


