/*
 * Translatable strings file generated by Glade.
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("PonG Editor");
gchar *s = N_("_New File");
gchar *s = N_("Import schemas into the pong file and add skeleton entries for them");
gchar *s = N_("_Import schemas...");
gchar *s = N_("Export GConf schema data to a file");
gchar *s = N_("Export schemas...");
gchar *s = N_("_Testing");
gchar *s = N_("Try running this dialog and connect to GConf.  This will test the dialog as it will be used in your application.  Note that this will be changing the live GConf database.");
gchar *s = N_("Test with gconf");
gchar *s = N_("Test the dialog without connecting to GConf.  Note that several things will not be working as some features require connection to the GConf database.  However this is good for testing the layout of widgets.");
gchar *s = N_("Test without gconf");
gchar *s = N_("Manual");
gchar *s = N_("Open");
gchar *s = N_("Save");
gchar *s = N_("Try running this dialog and connect to GConf.  This will test the dialog as it will be used in your application.  Note that this will be changing the live GConf database.");
gchar *s = N_("Test (gconf)");
gchar *s = N_("Test the dialog without connecting to GConf.  Note that several things will not be working as some features require connection to the GConf database.  However this is good for testing the layout of widgets.");
gchar *s = N_("Test");
gchar *s = N_("The prefix for the configuration keys.  This will be prepended to all keys and should end with a slash.  For applications use the standard /apps/application-binary-name/ convention.");
gchar *s = N_("name under which the help file is created (e.g. \"panel\")");
gchar *s = N_("path to the help file (e.g. \"index.html\")");
gchar *s = N_("Help name:");
gchar *s = N_("Configuration prefix:");
gchar *s = N_("Help path:");
gchar *s = N_("Dialog title:");
gchar *s = N_("The title of the preference dialog created.");
gchar *s = N_("The default GConf schema owner to use.  Usually the name of the application binary.");
gchar *s = N_("Schema owner:");
gchar *s = N_("Configuration");
gchar *s = N_("Type");
gchar *s = N_("Widget name");
gchar *s = N_("Configuration path");
gchar *s = N_("Add");
gchar *s = N_("Remove");
gchar *s = N_("GConf path to this key.");
gchar *s = N_("The name of the widget that this key corresponds to.");
gchar *s = N_("If the widget can service several keys, this would select which value this key corresponds to.  Usually can be left blank.");
gchar *s = N_("Default:");
gchar *s = N_("Specifier:");
gchar *s = N_("Widget:");
gchar *s = N_("Configuration path:");
gchar *s = N_("The default value for this configuration key.");
gchar *s = N_("Basic");
gchar *s = N_("Type:");
gchar *s = N_("Int");
gchar *s = N_("Float");
gchar *s = N_("String");
gchar *s = N_("Bool");
gchar *s = N_("List");
gchar *s = N_("Pair");
gchar *s = N_("Type of second pair element:");
gchar *s = N_("Type of first pair element:");
gchar *s = N_("Type of list items:");
gchar *s = N_("Int");
gchar *s = N_("Float");
gchar *s = N_("String");
gchar *s = N_("Bool");
gchar *s = N_("Int");
gchar *s = N_("Float");
gchar *s = N_("String");
gchar *s = N_("Bool");
gchar *s = N_("Int");
gchar *s = N_("Float");
gchar *s = N_("String");
gchar *s = N_("Bool");
gchar *s = N_("Type");
gchar *s = N_("Value");
gchar *s = N_("Sensitive widgets");
gchar *s = N_("Insensitive widgets");
gchar *s = N_("Add...");
gchar *s = N_("Edit...");
gchar *s = N_("Remove");
gchar *s = N_("Sensitivities");
gchar *s = N_("The GConf schema owner for this key, usually the name of the application binary.");
gchar *s = N_("Short description:");
gchar *s = N_("Owner:");
gchar *s = N_("Long description:");
gchar *s = N_("Schema");
gchar *s = N_("Configuration keys");
gchar *s = N_("Add:");
gchar *s = N_("Pane");
gchar *s = N_("Group");
gchar *s = N_("Widget");
gchar *s = N_("Glade");
gchar *s = N_("Bonobo");
gchar *s = N_("Plug");
gchar *s = N_("Move Up");
gchar *s = N_("Move Down");
gchar *s = N_("Remove");
gchar *s = N_("Name:");
gchar *s = N_("Label:");
gchar *s = N_("A label to be set on this widget or prepended in front of it.");
gchar *s = N_("Name that uniquely identifies this widget.  This should correspond to the Widget parameter for some configuration key.");
gchar *s = N_("Basic");
gchar *s = N_("foo!");
gchar *s = N_("label101");
gchar *s = N_("Pane has no other options");
gchar *s = N_("label63");
gchar *s = N_("Expand");
gchar *s = N_("Columns:");
gchar *s = N_("label64");
gchar *s = N_("Expand");
gchar *s = N_("Tooltip: ");
gchar *s = N_("Pong:Check:Group");
gchar *s = N_("Pong:Radio:Group");
gchar *s = N_("Pong:Option:Menu");
gchar *s = N_("Pong:Color:Picker");
gchar *s = N_("Pong:Spin:Button");
gchar *s = N_("Pong:Slider");
gchar *s = N_("Pong:List:Entry");
gchar *s = N_("Gtk:Spin:Button");
gchar *s = N_("Gnome:Number:Entry");
gchar *s = N_("Gnome:Calculator");
gchar *s = N_("Gtk:Entry");
gchar *s = N_("Gtk:Text");
gchar *s = N_("Gtk:Combo");
gchar *s = N_("Gnome:Entry");
gchar *s = N_("Gnome:File:Entry");
gchar *s = N_("Gnome:Pixmap:Entry");
gchar *s = N_("Gnome:Icon:Entry");
gchar *s = N_("Gtk:Toggle:Button");
gchar *s = N_("Gtk:Check:Button");
gchar *s = N_("Gnome:Color:Picker");
gchar *s = N_("Widget Type: ");
gchar *s = N_("Align label");
gchar *s = N_("label65");
gchar *s = N_("Expand");
gchar *s = N_("Path: ");
gchar *s = N_("Specifier: ");
gchar *s = N_("Plug type: ");
gchar *s = N_("Align label");
gchar *s = N_("label66");
gchar *s = N_("Expand");
gchar *s = N_("Query string: ");
gchar *s = N_("Align label");
gchar *s = N_("label67");
gchar *s = N_("Expand");
gchar *s = N_("File:");
gchar *s = N_("Widget:");
gchar *s = N_("Align label");
gchar *s = N_("label68");
gchar *s = N_("<type>");
gchar *s = N_("Label");
gchar *s = N_("Value");
gchar *s = N_("Move Up");
gchar *s = N_("Move Down");
gchar *s = N_("Add");
gchar *s = N_("Remove");
gchar *s = N_("Label:");
gchar *s = N_("Value:");
gchar *s = N_("Options");
gchar *s = N_("Name");
gchar *s = N_("Value");
gchar *s = N_("Translate");
gchar *s = N_("Add");
gchar *s = N_("Remove");
gchar *s = N_("Name:");
gchar *s = N_("Value:");
gchar *s = N_("Value translatable");
gchar *s = N_("The default value for this configuration key.");
gchar *s = N_("Arguments");
gchar *s = N_("All levels");
gchar *s = N_("If no levels are selected the widget will appear in all user levels.");
gchar *s = N_("Levels");
gchar *s = N_("Dialog panes");
gchar *s = N_("Name");
gchar *s = N_("Configuration prefix");
gchar *s = N_("Move Up");
gchar *s = N_("Move Down");
gchar *s = N_("Add");
gchar *s = N_("Remove");
gchar *s = N_("Name:");
gchar *s = N_("Configuration prefix:");
gchar *s = N_("User levels");
gchar *s = N_("(c) 2000,2001 Eazel, Inc.");
gchar *s = N_("PonG XML and GConf schema editor");
gchar *s = N_("Edit Sensitivity");
gchar *s = N_("Values:");
gchar *s = N_("Less then");
gchar *s = N_("Less then or equal to");
gchar *s = N_("Equal to");
gchar *s = N_("Greater then or equal to");
gchar *s = N_("Greater then");
gchar *s = N_("Not equal to");
gchar *s = N_("And");
gchar *s = N_("Or");
gchar *s = N_("Real value is: ");
gchar *s = N_("Connector:");
gchar *s = N_("These widgets become sensitive");
gchar *s = N_("Select...");
gchar *s = N_("These widgets become insensitive");
gchar *s = N_("Select...");
gchar *s = N_("Select Widget");
gchar *s = N_("Select a widget from the below list.  Only the pane interface widgets will be shown however.  To enter other widgets you have to enter them directly.");
gchar *s = N_("label99");
