/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "tree.h"
#include "glade-helper.h"
#include "pong-edit.h"
#include "util.h"

#include "pane-options.h"

static XMLTag *selected_option = NULL;
static XMLTag *selected_widget = NULL;

void pane_options_clist_select_row	(GtkCList *clist,
					 int row, int column,
					 GdkEvent *event);
void pane_options_clist_unselect_row	(GtkCList *clist,
					 int row, int column,
					 GdkEvent *event);

void change_pane_option_label		(GtkWidget *entry);
void change_pane_option_value		(GtkWidget *entry);

void pane_option_add			(GtkWidget *w);
void pane_option_remove			(GtkWidget *w);
void pane_option_move_up		(GtkWidget *w);
void pane_option_move_down		(GtkWidget *w);

static GList *per_list_connections = NULL;

static void
pane_option_label_updated (XMLTag *tag,
			   const char *key,
			   const char *text,
			   gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (pane_options_clist), tag);

	if (row >= 0) {
		gtk_clist_set_text (GTK_CLIST (pane_options_clist),
				    row, 0, text);
	}
}

static void
pane_option_value_updated (XMLTag *tag,
			   const char *key,
			   const char *text,
			   gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (pane_options_clist), tag);

	if (row >= 0) {
		gtk_clist_set_text (GTK_CLIST (pane_options_clist),
				    row, 1, text);
	}
}

static void
pane_option_moved_up (XMLTag *tag, gpointer data)
{
	XMLTag *next;
	const char *type;
	int row;

	next = tag_next (tag);

	if (next == NULL)
		return;

	type = tag_peek_type (next);
	tag_unref (next);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, PONG_S_Option) != 0) {
		return;
	}

	row = pong_edit_find_tag_row (GTK_CLIST (pane_options_clist), tag);

	if (row > 0) {
		gtk_clist_swap_rows (GTK_CLIST (pane_options_clist),
				     row, row - 1);
	}
}

static void
pane_option_moved_down (XMLTag *tag, gpointer data)
{
	XMLTag *prev;
	const char *type;
	int row;

	prev = tag_prev (tag);

	if (prev == NULL)
		return;

	type = tag_peek_type (prev);
	tag_unref (prev);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, PONG_S_Option) != 0) {
		return;
	}

	row = pong_edit_find_tag_row (GTK_CLIST (pane_options_clist), tag);

	if (row >= 0 &&
	    row < GTK_CLIST (pane_options_clist)->rows - 1) {
		gtk_clist_swap_rows (GTK_CLIST (pane_options_clist),
				     row, row + 1);
	}
}

static void
pane_option_killed (XMLTag *tag, gpointer data)
{
	int row;

	row = pong_edit_find_tag_row (GTK_CLIST (pane_options_clist), tag);

	if (row >= 0) {
		gtk_clist_remove (GTK_CLIST (pane_options_clist), row);
	}
}

static int
add_option_to_clist (XMLTag *option, int sibling)
{
	int row;
	char *text[2];
	char *label, *value;
	int con_id;

	label = tag_get_text (option, PONG_S_Label, NULL);
	value = tag_get_text (option, PONG_S_Value, NULL);

	text[0] = pong_sure_string (label);
	text[1] = pong_sure_string (value);

	if (sibling < 0)
		row = gtk_clist_append (GTK_CLIST (pane_options_clist), text);
	else
		row = gtk_clist_insert (GTK_CLIST (pane_options_clist),
					sibling, text);

	g_free (label);
	g_free (value);

	gtk_clist_set_row_data_full (GTK_CLIST (pane_options_clist), row,
				     tag_ref (option),
				     (GtkDestroyNotify)tag_unref);

	con_id = tag_connect_text (option, PONG_S_Label,
				   pane_option_label_updated, NULL, NULL);
	connection_add (&per_list_connections, option, con_id);
	con_id = tag_connect_text (option, PONG_S_Value,
				   pane_option_value_updated, NULL, NULL);
	connection_add (&per_list_connections, option, con_id);
	con_id = tag_connect_move_up (option, pane_option_moved_up, NULL, NULL);
	connection_add (&per_list_connections, option, con_id);
	con_id = tag_connect_move_down (option, pane_option_moved_down,
					NULL, NULL);
	connection_add (&per_list_connections, option, con_id);
	con_id = tag_connect_kill (option, pane_option_killed, NULL, NULL);
	connection_add (&per_list_connections, option, con_id);

	return row;
}

static void
pane_option_added (XMLTag *root, XMLTag *reference, gpointer data)
{
	const char *name = tag_peek_type (reference);

	if (name != NULL &&
	    pong_strcasecmp_no_locale (name, PONG_S_Option) == 0) {
		int next_row =
			find_next_in_clist (GTK_CLIST (pane_options_clist),
					    reference);
		add_option_to_clist (reference, next_row);
	}
}

void
setup_pane_options_list (XMLTag *widget)
{
	GList *list, *li;
	int con_id;

	selected_widget = widget;

	gtk_clist_freeze (GTK_CLIST (pane_options_clist));

	list = tag_get_tags (widget, PONG_S_Option);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		add_option_to_clist (tag, -1);
	}

	tree_free_tag_list (list);

	gtk_clist_thaw (GTK_CLIST (pane_options_clist));

	con_id = tag_connect_add (widget, pane_option_added, NULL, NULL);
	connection_add (&per_list_connections, widget, con_id);
}

void
clear_pane_options_list (void)
{
	GtkWidget *w;

	selected_widget = NULL;

	connection_disconnect_all (&per_list_connections);

	gtk_clist_unselect_all (GTK_CLIST (pane_options_clist));
	gtk_clist_clear (GTK_CLIST (pane_options_clist));

	w = glade_helper_get (app_xml, "pane_option_edit_table",
			      GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, FALSE);
}

static void
set_stripped_text (GtkWidget *entry, const char *key)
{
	char *text;

	if (selected_option == NULL)
		return;

	text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);

	if (text != NULL)
		g_strstrip (text);

	if (pong_string_empty (text))
		tag_set_text (selected_option, key, NULL, NULL);
	else
		tag_set_text (selected_option, key, text, NULL);

	g_free (text);
}

void
change_pane_option_label (GtkWidget *entry)
{
	set_stripped_text (entry, "_" PONG_S_Label);
}

void
change_pane_option_value (GtkWidget *entry)
{
	set_stripped_text (entry, PONG_S_Value);
}

static void
setup_pane_option_edit_area (XMLTag *option)
{
	GtkWidget *w;
	char *text;

	/* The option label */
	w = glade_helper_get (app_xml, "pane_option_label_entry",
			      GTK_TYPE_ENTRY);
	text = tag_get_text (option, PONG_S_Label, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The option value */
	w = glade_helper_get (app_xml, "pane_option_value_entry",
			      GTK_TYPE_ENTRY);
	text = tag_get_text (option, PONG_S_Value, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);
}

static void
clear_pane_option_edit_area (void)
{
	GtkWidget *w;

	/* The option label */
	w = glade_helper_get (app_xml, "pane_option_label_entry",
			      GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The option value */
	w = glade_helper_get (app_xml, "pane_option_value_entry",
			      GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");
}

void
pane_options_clist_select_row (GtkCList *clist, int row, int column,
			       GdkEvent *event)
{
	GtkWidget *w;
	XMLTag *tag = gtk_clist_get_row_data (clist, row);

	selected_option = NULL;

	w = glade_helper_get (app_xml, "pane_option_edit_table",
			      GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, TRUE);

	setup_pane_option_edit_area (tag);
	selected_option = tag;
}

void
pane_options_clist_unselect_row (GtkCList *clist, int row, int column,
				 GdkEvent *event)
{
	GtkWidget *w;

	selected_option = NULL;
	clear_pane_option_edit_area ();

	w = glade_helper_get (app_xml, "pane_option_edit_table",
			      GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, FALSE);
}

void
pane_option_add (GtkWidget *w)
{
	XMLTag *tag;

	if (selected_option != NULL)
		tag = tree_widget_prepend_option (selected_option);
	else if (selected_widget != NULL)
		tag = tree_widget_add_option (selected_widget);
	else
		tag = NULL;

	clist_select_thing (GTK_CLIST (pane_options_clist), tag);

	tag_unref (tag);
}

void
pane_option_remove (GtkWidget *w)
{
	if (selected_option != NULL)
		tag_kill (selected_option);
}

void
pane_option_move_up (GtkWidget *w)
{
	if (selected_option != NULL)
		tree_widget_move_option_up (selected_option);
}

void
pane_option_move_down (GtkWidget *w)
{
	if (selected_option != NULL)
		tree_widget_move_option_down (selected_option);
}
