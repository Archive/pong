/* PonG-Edit: A gui editor for the pong files
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong/pongelement.h>
#include <pong/pongparser.h>
#include <pong/pongutil.h>
#include <pong/pong-type.h>
#include <pong/pong-strings.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "tree.h"
#include "glade-helper.h"
#include "pong-edit.h"
#include "util.h"

/* XXX: ugly utter hack, we already monitor levels here so we cheat and use
 * the monitoring here to tell panes that levels have changed, should be
 * cleaned up later */
#include "panes.h"

#include "levels.h"

static XMLTag *selected_level = NULL;

void levels_clist_select_row	(GtkCList *clist,
				 int row, int column,
				 GdkEvent *event);
void levels_clist_unselect_row	(GtkCList *clist,
				 int row, int column,
				 GdkEvent *event);

void change_level_name		(GtkWidget *entry);
void change_level_base_conf_path(GtkWidget *entry);

void level_add			(GtkWidget *w);
void level_remove		(GtkWidget *w);
void level_move_up		(GtkWidget *w);
void level_move_down		(GtkWidget *w);

static GList *per_list_connections = NULL;

static void
level_name_updated (XMLTag *tag,
		    const char *key,
		    const char *text,
		    gpointer data)
{
	int the_row;

	the_row = pong_edit_find_tag_row (GTK_CLIST (levels_clist), tag);

	if (the_row >= 0) {
		gtk_clist_set_text (GTK_CLIST (levels_clist),
				    the_row, 0, text);
	}

	/* XXX: utter utter hack */
	panes_change_level_list ();
}

static void
level_base_conf_path_updated (XMLTag *tag,
			      const char *key,
			      const char *text,
			      gpointer data)
{
	int the_row;

	the_row = pong_edit_find_tag_row (GTK_CLIST (levels_clist), tag);

	if (the_row >= 0) {
		gtk_clist_set_text (GTK_CLIST (levels_clist),
				    the_row, 1, text);
	}
}

static void
level_moved_up (XMLTag *tag, gpointer data)
{
	XMLTag *next;
	const char *type;
	int row;

	next = tag_next (tag);

	if (next == NULL)
		return;

	type = tag_peek_type (next);
	tag_unref (next);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, PONG_S_Level) != 0) {
		return;
	}

	row = pong_edit_find_tag_row (GTK_CLIST (levels_clist), tag);

	if (row > 0) {
		gtk_clist_swap_rows (GTK_CLIST (levels_clist),
				     row, row - 1);
	}
}

static void
level_moved_down (XMLTag *tag, gpointer data)
{
	XMLTag *prev;
	const char *type;
	int row;

	prev = tag_prev (tag);

	if (prev == NULL)
		return;

	type = tag_peek_type (prev);
	tag_unref (prev);

	if (type == NULL ||
	    pong_strcasecmp_no_locale (type, PONG_S_Level) != 0) {
		return;
	}

	row = pong_edit_find_tag_row (GTK_CLIST (levels_clist), tag);

	if (row >= 0 &&
	    row < GTK_CLIST (levels_clist)->rows - 1) {
		gtk_clist_swap_rows (GTK_CLIST (levels_clist),
				     row, row + 1);
	}
}


static void
level_killed (XMLTag *tag, gpointer data)
{
	int the_row;

	the_row = pong_edit_find_tag_row (GTK_CLIST (levels_clist), tag);

	if (the_row >= 0) {
		gtk_clist_remove (GTK_CLIST (levels_clist), the_row);
	}

	/* XXX: utter utter hack */
	panes_change_level_list ();
}

static int
add_level_to_clist (XMLTag *level, int sibling)
{
	int row;
	char *text[2];
	char *name, *conf_path;
	int con_id;

	name = tag_get_text (level, PONG_S_Name, NULL);
	conf_path = tag_get_text (level, PONG_S_BaseConfPath, NULL);

	text[0] = pong_sure_string (name);
	text[1] = pong_sure_string (conf_path);

	if (sibling < 0)
		row = gtk_clist_append (GTK_CLIST (levels_clist), text);
	else
		row = gtk_clist_insert (GTK_CLIST (levels_clist),
					sibling, text);

	g_free (name);
	g_free (conf_path);

	gtk_clist_set_row_data_full (GTK_CLIST (levels_clist), row,
				     tag_ref (level),
				     (GtkDestroyNotify)tag_unref);

	con_id = tag_connect_text (level, PONG_S_Name,
				   level_name_updated, NULL, NULL);
	connection_add (&per_list_connections, level, con_id);
	con_id = tag_connect_text (level, PONG_S_BaseConfPath,
				   level_base_conf_path_updated, NULL, NULL);
	connection_add (&per_list_connections, level, con_id);
	con_id = tag_connect_move_up (level, level_moved_up, NULL, NULL);
	connection_add (&per_list_connections, level, con_id);
	con_id = tag_connect_move_down (level, level_moved_down, NULL, NULL);
	connection_add (&per_list_connections, level, con_id);
	con_id = tag_connect_kill (level, level_killed, NULL, NULL);
	connection_add (&per_list_connections, level, con_id);

	return row;
}

static void
level_added (XMLTag *root, XMLTag *reference, gpointer data)
{
	const char *name = tag_peek_type (reference);

	if (name != NULL &&
	    pong_strcasecmp_no_locale (name, PONG_S_Level) == 0) {
		int next_row = find_next_in_clist (GTK_CLIST (levels_clist),
						   reference);
		add_level_to_clist (reference, next_row);
	}

	/* XXX: utter utter hack */
	panes_change_level_list ();
}

void
setup_levels_list (XMLTag *root)
{
	GList *list, *li;
	int con_id;

	gtk_clist_freeze (GTK_CLIST (levels_clist));

	list = tree_get_levels (xml_tree);

	for (li = list; li != NULL; li = li->next) {
		XMLTag *tag = li->data;

		add_level_to_clist (tag, -1);
	}

	tree_free_tag_list (list);

	gtk_clist_thaw (GTK_CLIST (levels_clist));

	con_id = tag_connect_add (root, level_added, NULL, NULL);
	connection_add (&per_list_connections, root, con_id);
}

void
clear_levels_list (void)
{
	connection_disconnect_all (&per_list_connections);

	gtk_clist_unselect_all (GTK_CLIST (levels_clist));
	gtk_clist_clear (GTK_CLIST (levels_clist));
}

static void
set_stripped_level_text (GtkWidget *entry, const char *key)
{
	char *text;

	if (selected_level == NULL)
		return;

	text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);

	if (text != NULL)
		g_strstrip (text);

	if (pong_string_empty (text))
		tag_set_text (selected_level, key, NULL, NULL);
	else
		tag_set_text (selected_level, key, text, NULL);

	g_free (text);
}

void
change_level_base_conf_path (GtkWidget *entry)
{
	set_stripped_level_text (entry, PONG_S_BaseConfPath);
}

void
change_level_name (GtkWidget *entry)
{
	gboolean success;

	if (selected_level == NULL)
		return;

	success = tree_set_level_name (xml_tree, selected_level,
				       gtk_entry_get_text (GTK_ENTRY (entry)));

	pong_edit_set_red (entry, success ? FALSE : TRUE);
}

static void
setup_levels_edit_area (XMLTag *level)
{
	GtkWidget *w;
	char *text;

	/* The level name */
	w = glade_helper_get (app_xml, "level_name_entry", GTK_TYPE_ENTRY);
	text = tag_get_text (level, PONG_S_Name, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);

	/* The level base conf path */
	w = glade_helper_get (app_xml, "level_base_conf_path_entry",
			      GTK_TYPE_ENTRY);
	text = tag_get_text (level, PONG_S_BaseConfPath, NULL);
	gtk_entry_set_text (GTK_ENTRY (w), pong_sure_string (text));
	g_free (text);
}

static void
clear_levels_edit_area (void)
{
	GtkWidget *w;

	/* The level name */
	w = glade_helper_get (app_xml, "level_name_entry", GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");

	/* The level name */
	w = glade_helper_get (app_xml, "level_base_conf_path_entry",
			      GTK_TYPE_ENTRY);
	gtk_entry_set_text (GTK_ENTRY (w), "");
}

void
levels_clist_select_row (GtkCList *clist, int row, int column, GdkEvent *event)
{
	GtkWidget *w;
	XMLTag *tag = gtk_clist_get_row_data (clist, row);

	selected_level = NULL;

	w = glade_helper_get (app_xml, "level_edit_table", GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, TRUE);

	setup_levels_edit_area (tag);
	selected_level = tag;
}

void
levels_clist_unselect_row (GtkCList *clist, int row, int column,
			   GdkEvent *event)
{
	GtkWidget *w;

	selected_level = NULL;
	clear_levels_edit_area ();

	w = glade_helper_get (app_xml, "level_edit_table", GTK_TYPE_TABLE);
	gtk_widget_set_sensitive (w, FALSE);
}

void
level_add (GtkWidget *w)
{
	XMLTag *tag;

	if (selected_level != NULL)
		tag = tree_prepend_level (selected_level);
	else if (xml_tree != NULL)
		tag = tree_add_level (xml_tree);
	else
		tag = NULL;

	clist_select_thing (GTK_CLIST (levels_clist), tag);

	tag_unref (tag);
}

void
level_remove (GtkWidget *w)
{
	if (selected_level != NULL)
		tag_kill (selected_level);
}

void
level_move_up (GtkWidget *w)
{
	if (selected_level != NULL)
		tree_move_level_up (selected_level);
}

void
level_move_down (GtkWidget *w)
{
	if (selected_level != NULL)
		tree_move_level_down (selected_level);
}
