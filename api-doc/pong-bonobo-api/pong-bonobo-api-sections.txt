<INCLUDE>pong/pong.h</INCLUDE>
<INCLUDE>pong-bonobo/pong-bonobo.h</INCLUDE>

<SECTION>
<FILE>pong-bonobo-entry</FILE>
PONG_BONOBO_ENTRY_CONST
PongBonoboEntryPrivate
<TITLE>PongBonoboEntry</TITLE>
pong_bonobo_entry_make_control
<SUBSECTION Standard>
PONG_BONOBO_ENTRY
PONG_IS_BONOBO_ENTRY
PONG_TYPE_BONOBO_ENTRY
pong_bonobo_entry_get_type
PONG_BONOBO_ENTRY_CLASS
PONG_BONOBO_ENTRY_GET_CLASS
</SECTION>

<SECTION>
<FILE>pong-ui-plug-bonobo</FILE>
PONG_UI_PLUG_BONOBO_CONST
PongUIPlugBonoboPrivate
<TITLE>PongUIPlugBonobo</TITLE>
pong_ui_plug_bonobo_new
<SUBSECTION Standard>
PONG_UI_PLUG_BONOBO
PONG_IS_UI_PLUG_BONOBO
PONG_TYPE_UI_PLUG_BONOBO
pong_ui_plug_bonobo_get_type
PONG_UI_PLUG_BONOBO_CLASS
PONG_UI_PLUG_BONOBO_GET_CLASS
</SECTION>

<SECTION>
<FILE>pong-bonobo</FILE>
pong_bonobo_init
</SECTION>

