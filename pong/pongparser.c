/* PonG: the parser using SAX interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <gnome.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/parserInternals.h>

#include "pong-i18n.h"

#include "pongpane.h"
#include "pongelement.h"
#include "pongutil.h"
#include "pong-strings.h"

#include "pongparser.h"
#include "pongparser-private.h"

extern GList *pong_directories;

enum {
	IGNORE,
	START,
	FINISH,
	LOOKING_FOR_ELEMENT,

	INSIDE_ELEMENT,

	INSIDE_BASECONFPATH,
	INSIDE_DIALOG_TITLE,
	INSIDE_AUTO_APPLY,
	INSIDE_REVERT,
	INSIDE_HELP,
	INSIDE_HELP_NAME,
	INSIDE_HELP_PATH,
	INSIDE_LEVEL,
	INSIDE_LEVEL_NAME,
	INSIDE_LEVEL_BASECONFPATH,

	INSIDE_ELEMENT_TYPE,
	INSIDE_ELEMENT_CONF_PATH,
	INSIDE_ELEMENT_WIDGET,
	INSIDE_ELEMENT_DEFAULT,
	INSIDE_ELEMENT_SPECIFIER,

	INSIDE_SENSITIVITY,
	INSIDE_SENSITIVITY_CONNECTION,
	INSIDE_SENSITIVITY_VALUE,
	INSIDE_SENSITIVITY_COMPARISON,
	INSIDE_SENSITIVITY_SENSITIVE,
	INSIDE_SENSITIVITY_INSENSITIVE,

	INSIDE_BASE_NAME,
	INSIDE_BASE_LABEL,
	INSIDE_BASE_LEVEL,
	INSIDE_BASE_EXPAND,
	INSIDE_BASE_ALIGN_LABEL,
	INSIDE_BASE_TOOLTIP,

	INSIDE_PANE,

	INSIDE_GLADE,

	INSIDE_BONOBO,

	INSIDE_PLUG,
	INSIDE_PLUG_TYPE,
	INSIDE_PLUG_PATH,
	INSIDE_PLUG_SPECIFIER,

	INSIDE_GROUP,
	INSIDE_GROUP_COLUMNS,

	INSIDE_WIDGET,
	INSIDE_WIDGET_TYPE,

	INSIDE_ARGUMENT,
	INSIDE_ARGUMENT_NAME,
	INSIDE_ARGUMENT_VALUE,
	INSIDE_ARGUMENT_TRANSLATABLE,

	INSIDE_OPTION,
	INSIDE_OPTION_LABEL,
	INSIDE_OPTION_VALUE
};


typedef struct _State State;
struct _State {
	/* stuff that is read */
	PongFile *gf;
	PongSensitivity *sens;

	int state;
	int ignore_last_state; /* state to go into after ignore */
	int ignore_depth; /* how deep are we into ingore */
	GString *buf;
	gboolean reading_characters;
	PongElement *element;

	PongPaneBase *base;
	PongPane *pane;
	PongGroup *group;
	PongWidget *widget;
	PongPlugWidget *plug_widget;

	int state_before_string; /* state to go into after a string */
	int state_before_plug; /* state to go into after a plug */
	int state_before_argopt; /* state to go into after arguments/options */

	int string_lang_level; /* language_level of a string, the
				  lower the better */

	char *arg_name;
	char *arg_value;
	gboolean arg_translatable;

	char *opt_label;
	char *opt_value;

	PongLevel *level;
};

static void startDocument		(gpointer user_data);
static void endDocument			(gpointer user_data);
static void characters			(gpointer user_data,
					 const CHAR *ch,
					 int len);
static void startElement		(gpointer user_data,
					 const CHAR *name,
					 const CHAR **attrs);
static void endElement			(gpointer user_data,
					 const CHAR *name);

static int myXmlSAXParseFile		(xmlSAXHandlerPtr sax,
					 gpointer user_data,
					 const char *filename);
static int myXmlSAXParseMemory		(xmlSAXHandlerPtr sax,
					 gpointer user_data,
					 const char *buffer,
					 int size);

static void pong_file_add_element	(PongFile *gf,
					 PongElement *ge);
static void pong_file_destroy		(PongFile *gf);

static void unref_elements		(gpointer key,
					 gpointer data,
					 gpointer user_data);
static gboolean kill_unused_pongfiles	(gpointer key,
					 gpointer data,
					 gpointer user_data);
static void add_elements_to_list	(gpointer key,
					 gpointer data,
					 gpointer user_data);
static void foreach_elements		(gpointer key,
					 gpointer data,
					 gpointer user_data);

static PongComparison get_comparison	(const char *name);

static xmlSAXHandler handler = {0};
static GHashTable *file_hash = NULL;
static gboolean parser_inited = FALSE;

static GHashTable *comparison_hash = NULL;

#include "pong-comparisons.cP"

static void
init_comparison_hash (void)
{
	int i;
	if (comparison_hash != NULL)
		return;

	comparison_hash = g_hash_table_new (g_str_hash, g_str_equal);

	for (i = 0; comparisons[i].name != NULL; i++) {
		g_hash_table_insert (comparison_hash, comparisons[i].name,
				     GINT_TO_POINTER (comparisons[i].value));
	}
}

static PongComparison
get_comparison (const char *name)
{
	g_return_val_if_fail (name != NULL, PONG_COMP_INVALID);

	if (comparison_hash == NULL)
		init_comparison_hash ();

	/* INVALID == 0, so if it can't find it it will
	 * return INVALID */
	return GPOINTER_TO_INT (g_hash_table_lookup (comparison_hash, name));
}

static PongConnection
get_connection (const char *name)
{
	g_return_val_if_fail (name != NULL, PONG_CONNECTION_INVALID);

	if (pong_strcasecmp_no_locale (name, PONG_S_OR) == 0)
		return PONG_CONNECTION_OR;
	else if (pong_strcasecmp_no_locale (name, PONG_S_AND) == 0)
		return PONG_CONNECTION_AND;
	else
		return PONG_CONNECTION_INVALID;
}

static void
parser_init (void)
{
	if (parser_inited)
		return;

	handler.startDocument = startDocument;
	handler.endDocument = endDocument;
	handler.characters = characters;
	handler.startElement = startElement;
	handler.endElement = endElement;

	file_hash = g_hash_table_new (g_str_hash, g_str_equal);

	parser_inited = TRUE;
}

static void
startDocument (gpointer user_data)
{
	State *state = user_data;

	state->state = START;

	state->buf = g_string_new ("");
	state->reading_characters = FALSE;
}

static void
endDocument (gpointer user_data)
{
	State *state = user_data;

	if (state->sens != NULL) {
		pong_sensitivity_destroy (state->sens);
		state->sens = NULL;
	}

	if (state->buf != NULL) {
		g_string_free (state->buf, TRUE);
		state->buf = NULL;
	}
	state->reading_characters = FALSE;
}

static void
characters (gpointer user_data, const CHAR *ch, int len)
{
	State *state = user_data;
	char *s;

	if ( ! state->reading_characters)
		return;

	/* there must be a better way of doing this */
	s = g_strndup (ch, len);
	g_string_append (state->buf, s);
	g_free (s);
}

static const char *
get_lang (const CHAR **attrs)
{
	int i;

	if (attrs == NULL)
		return NULL;

	i = 0;
	while (attrs[i] != NULL &&
	       attrs[i+1] != NULL) {
		if (strcmp (attrs[i], "xml:lang") == 0) {
			return attrs[i+1];
		}
		i += 2;
	}
	return NULL;
}

static int
lang_level (const char *lang)
{
	static const GList *language_list = NULL;
	const GList *li;
	int i;

	if (lang == NULL)
		return G_MAXINT;

	if (language_list == NULL) {
		language_list = gnome_i18n_get_language_list ("LC_MESSAGES");
	}

	for (i = 0, li = language_list;
	     li != NULL;
	     i++, li = li->next) {
		const char *ll = li->data;

		if (strcmp (ll, lang) == 0)
			return i;
	}

	return -1;
}

#define START_READING_CHARS(element) 				\
	if (cur_lang_level == -1 ||				\
	    cur_lang_level > state->string_lang_level) {	\
		START_IGNORE;					\
	} else {						\
		state->string_lang_level = cur_lang_level;	\
								\
		state->state_before_string = state->state;	\
		state->state = element;				\
								\
		/* clear buffer for attribute */		\
		g_string_assign (state->buf, "");		\
								\
		/* read characters now */			\
		state->reading_characters = TRUE;		\
	}

#define START_IGNORE 					\
	state->ignore_last_state = state->state;	\
	state->state = IGNORE;				\
	state->ignore_depth = 1;

static void
startElement (gpointer user_data, const CHAR *name, const CHAR **attrs)
{
	State *state = user_data;
	const char *lang;
	int cur_lang_level;

	lang = get_lang (attrs);
	if (lang == NULL)
		state->string_lang_level = G_MAXINT;
	cur_lang_level = lang_level (lang);

	switch (state->state) {
	case IGNORE:
		state->ignore_depth++;
		break;

	case START:
		if (pong_tags_equal (name, PONG_S_PongElements)) {
			state->state = LOOKING_FOR_ELEMENT;
		} else {
			START_IGNORE;
		}
		break;

	case FINISH: /* just ingore everything now */
		break;

	case LOOKING_FOR_ELEMENT:
		if (pong_tags_equal (name, PONG_S_Element)) {
			state->state = INSIDE_ELEMENT;
			state->element = pong_element_new ();
		} else if (pong_tags_equal (name, PONG_S_Pane)) {
			state->state = INSIDE_PANE;
			state->pane = pong_pane_new ();
		} else if (pong_tags_equal (name, PONG_S_BaseConfPath)) {
			START_READING_CHARS (INSIDE_BASECONFPATH)
		} else if (pong_tags_equal (name, PONG_S_DialogTitle)) {
				START_READING_CHARS (INSIDE_DIALOG_TITLE)
		} else if (pong_tags_equal (name, PONG_S_AutoApply)) {
			START_READING_CHARS (INSIDE_AUTO_APPLY)
		} else if (pong_tags_equal (name, PONG_S_Revert)) {
			START_READING_CHARS (INSIDE_REVERT)
		} else if (pong_tags_equal (name, PONG_S_Help)) {
			state->state = INSIDE_HELP;
			g_free (state->gf->help_name);
			state->gf->help_name = g_strdup ("pong");
			g_free (state->gf->help_path);
			state->gf->help_path = g_strdup ("generic.html");
		} else if (pong_tags_equal (name, PONG_S_Level)) {
			state->level = g_new0 (PongLevel, 1);
			state->state = INSIDE_LEVEL;
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_HELP:
		if (pong_tags_equal (name, PONG_S_Name)) {
			START_READING_CHARS (INSIDE_HELP_NAME)
		} else if (pong_tags_equal (name, PONG_S_Path)) {
			START_READING_CHARS (INSIDE_HELP_PATH)
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_LEVEL:
		if (pong_tags_equal (name, PONG_S_Name)) {
			START_READING_CHARS (INSIDE_LEVEL_NAME)
		} else if (pong_tags_equal (name, PONG_S_BaseConfPath)) {
			START_READING_CHARS (INSIDE_LEVEL_BASECONFPATH)
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_ELEMENT:
		if (pong_tags_equal (name, PONG_S_Type)) {
			START_READING_CHARS (INSIDE_ELEMENT_TYPE);
		} else if (pong_tags_equal (name, PONG_S_ConfPath)) {
			START_READING_CHARS (INSIDE_ELEMENT_CONF_PATH);
		} else if (pong_tags_equal (name, PONG_S_Widget)) {
			START_READING_CHARS (INSIDE_ELEMENT_WIDGET);
		} else if (pong_tags_equal (name, PONG_S_Default)) {
			START_READING_CHARS (INSIDE_ELEMENT_DEFAULT);
		} else if (pong_tags_equal (name, PONG_S_Specifier)) {
			START_READING_CHARS (INSIDE_ELEMENT_SPECIFIER);
		} else if (pong_tags_equal (name, PONG_S_Sensitivity)) {
			if (state->element->type == PONG_TYPE_INVALID) {
				g_warning (_("Type not set before Sensitivity"));
				state->ignore_last_state = state->state;
				state->state = IGNORE;
				state->ignore_depth = 1;
			} else  {
				state->state = INSIDE_SENSITIVITY;
				state->sens =
					pong_sensitivity_new (state->element);
			}
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_SENSITIVITY:
		if (pong_tags_equal (name, PONG_S_Value)) {
			START_READING_CHARS (INSIDE_SENSITIVITY_VALUE);
		} else if (pong_tags_equal (name, PONG_S_Connection)) {
			START_READING_CHARS (INSIDE_SENSITIVITY_CONNECTION);
		} else if (pong_tags_equal (name, PONG_S_Comparison)) {
			START_READING_CHARS (INSIDE_SENSITIVITY_COMPARISON);
		} else if (pong_tags_equal (name, PONG_S_Sensitive)) {
			START_READING_CHARS (INSIDE_SENSITIVITY_SENSITIVE);
		} else if (pong_tags_equal (name, PONG_S_Insensitive)) {
			START_READING_CHARS (INSIDE_SENSITIVITY_INSENSITIVE);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_PANE:
		if (pong_tags_equal (name, PONG_S_Group)) {
			state->state = INSIDE_GROUP;
			state->group = pong_group_new ();
		} else if (pong_tags_equal (name, PONG_S_Plug)) {
			state->state = INSIDE_PLUG;
			state->state_before_plug = INSIDE_PANE;
			state->plug_widget = pong_plug_widget_new ();
		} else if (pong_tags_equal (name, PONG_S_Glade)) {
			state->state = INSIDE_GLADE;
			state->state_before_plug = INSIDE_PANE;
			state->plug_widget = pong_plug_widget_new ();
			pong_plug_widget_set_plug_type (state->plug_widget, PONG_S_Glade);
		} else if (pong_tags_equal (name, PONG_S_Bonobo)) {
			state->state = INSIDE_BONOBO;
			state->state_before_plug = INSIDE_PANE;
			state->plug_widget = pong_plug_widget_new ();
			pong_plug_widget_set_plug_type (state->plug_widget, PONG_S_Bonobo);
		} else if (pong_tags_equal (name, PONG_S_Name)) {
			state->base = &state->pane->base;
			START_READING_CHARS (INSIDE_BASE_NAME);
		} else if (pong_tags_equal (name, PONG_S_Label)) {
			state->base = &state->pane->base;
			START_READING_CHARS (INSIDE_BASE_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Level)) {
			state->base = &state->pane->base;
			START_READING_CHARS (INSIDE_BASE_LEVEL);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_PLUG:
		if (pong_tags_equal (name, PONG_S_Argument)) {
			state->base = &state->plug_widget->base;
			state->state = INSIDE_ARGUMENT;
			g_free (state->arg_name);
			state->arg_name = NULL;
			g_free (state->arg_value);
			state->arg_value = NULL;
			state->arg_translatable = FALSE;
			state->state_before_argopt = INSIDE_PLUG;
		} else if (pong_tags_equal (name, PONG_S_Option)) {
			state->base = &state->plug_widget->base;
			state->state = INSIDE_OPTION;
			g_free (state->opt_label);
			state->opt_label = NULL;
			g_free (state->opt_value);
			state->opt_value = NULL;
			state->state_before_argopt = INSIDE_PLUG;
		} else if (pong_tags_equal (name, PONG_S_Name)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_NAME);
		} else if (pong_tags_equal (name, PONG_S_Label)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Level)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_LEVEL);
		} else if (pong_tags_equal (name, PONG_S_Expand)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_EXPAND);
		} else if (pong_tags_equal (name, PONG_S_AlignLabel)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_ALIGN_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Tooltip)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_TOOLTIP);
		} else if (pong_tags_equal (name, PONG_S_Type)) {
			START_READING_CHARS (INSIDE_PLUG_TYPE);
		} else if (pong_tags_equal (name, PONG_S_Path)) {
			START_READING_CHARS (INSIDE_PLUG_PATH);
		} else if (pong_tags_equal (name, PONG_S_Specifier)) {
			START_READING_CHARS (INSIDE_PLUG_SPECIFIER);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_GLADE:
		if (pong_tags_equal (name, PONG_S_Argument)) {
			state->base = &state->plug_widget->base;
			state->state = INSIDE_ARGUMENT;
			g_free (state->arg_name);
			state->arg_name = NULL;
			g_free (state->arg_value);
			state->arg_value = NULL;
			state->arg_translatable = FALSE;
			state->state_before_argopt = INSIDE_GLADE;
		} else if (pong_tags_equal (name, PONG_S_Option)) {
			state->base = &state->plug_widget->base;
			state->state = INSIDE_OPTION;
			g_free (state->opt_label);
			state->opt_label = NULL;
			g_free (state->opt_value);
			state->opt_value = NULL;
			state->state_before_argopt = INSIDE_GLADE;
		} else if (pong_tags_equal (name, PONG_S_Name)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_NAME);
		} else if (pong_tags_equal (name, PONG_S_Label)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Level)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_LEVEL);
		} else if (pong_tags_equal (name, PONG_S_Tooltip)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_TOOLTIP);
		} else if (pong_tags_equal (name, PONG_S_Expand)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_EXPAND);
		} else if (pong_tags_equal (name, PONG_S_AlignLabel)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_ALIGN_LABEL);
		} else if (pong_tags_equal (name, PONG_S_File)) {
			START_READING_CHARS (INSIDE_PLUG_PATH);
		} else if (pong_tags_equal (name, PONG_S_Widget)) {
			START_READING_CHARS (INSIDE_PLUG_SPECIFIER);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_BONOBO:
		if (pong_tags_equal (name, PONG_S_Argument)) {
			state->base = &state->plug_widget->base;
			state->state = INSIDE_ARGUMENT;
			g_free (state->arg_name);
			state->arg_name = NULL;
			g_free (state->arg_value);
			state->arg_value = NULL;
			state->arg_translatable = FALSE;
			state->state_before_argopt = INSIDE_BONOBO;
		} else if (pong_tags_equal (name, PONG_S_Option)) {
			state->base = &state->plug_widget->base;
			state->state = INSIDE_OPTION;
			g_free (state->opt_label);
			state->opt_label = NULL;
			g_free (state->opt_value);
			state->opt_value = NULL;
			state->state_before_argopt = INSIDE_BONOBO;
		} else if (pong_tags_equal (name, PONG_S_Name)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_NAME);
		} else if (pong_tags_equal (name, PONG_S_Label)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Level)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_LEVEL);
		} else if (pong_tags_equal (name, PONG_S_Expand)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_EXPAND);
		} else if (pong_tags_equal (name, PONG_S_AlignLabel)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_ALIGN_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Tooltip)) {
			state->base = &state->plug_widget->base;
			START_READING_CHARS (INSIDE_BASE_TOOLTIP);
		} else if (pong_tags_equal (name, PONG_S_Query)) {
			START_READING_CHARS (INSIDE_PLUG_PATH);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_GROUP:
		if (pong_tags_equal (name, PONG_S_Widget)) {
			state->state = INSIDE_WIDGET;
			state->widget = pong_widget_new ();
		} else if (pong_tags_equal (name, PONG_S_Glade)) {
			state->state = INSIDE_GLADE;
			state->state_before_plug = INSIDE_GROUP;
			state->plug_widget = pong_plug_widget_new ();
			pong_plug_widget_set_plug_type (state->plug_widget, PONG_S_Glade);
		} else if (pong_tags_equal (name, PONG_S_Bonobo)) {
			state->state = INSIDE_BONOBO;
			state->state_before_plug = INSIDE_GROUP;
			state->plug_widget = pong_plug_widget_new ();
			pong_plug_widget_set_plug_type (state->plug_widget, PONG_S_Bonobo);
		} else if (pong_tags_equal (name, PONG_S_Name)) {
			state->base = &state->group->base;
			START_READING_CHARS (INSIDE_BASE_NAME);
		} else if (pong_tags_equal (name, PONG_S_Label)) {
			state->base = &state->group->base;
			START_READING_CHARS (INSIDE_BASE_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Level)) {
			state->base = &state->group->base;
			START_READING_CHARS (INSIDE_BASE_LEVEL);
		} else if (pong_tags_equal (name, PONG_S_Expand)) {
			state->base = &state->group->base;
			START_READING_CHARS (INSIDE_BASE_EXPAND);
		} else if (pong_tags_equal (name, PONG_S_Columns)) {
			START_READING_CHARS (INSIDE_GROUP_COLUMNS);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_WIDGET:
		if (pong_tags_equal (name, PONG_S_Argument)) {
			state->base = &state->widget->base;
			state->state = INSIDE_ARGUMENT;
			g_free (state->arg_name);
			state->arg_name = NULL;
			g_free (state->arg_value);
			state->arg_value = NULL;
			state->arg_translatable = FALSE;
			state->state_before_argopt = INSIDE_WIDGET;
		} else if (pong_tags_equal (name, PONG_S_Option)) {
			state->base = &state->widget->base;
			state->state = INSIDE_OPTION;
			g_free (state->opt_label);
			state->opt_label = NULL;
			g_free (state->opt_value);
			state->opt_value = NULL;
			state->state_before_argopt = INSIDE_WIDGET;
		} else if (pong_tags_equal (name, PONG_S_Name)) {
			state->base = &state->widget->base;
			START_READING_CHARS (INSIDE_BASE_NAME);
		} else if (pong_tags_equal (name, PONG_S_Label)) {
			state->base = &state->widget->base;
			START_READING_CHARS (INSIDE_BASE_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Level)) {
			state->base = &state->widget->base;
			START_READING_CHARS (INSIDE_BASE_LEVEL);
		} else if (pong_tags_equal (name, PONG_S_Expand)) {
			state->base = &state->widget->base;
			START_READING_CHARS (INSIDE_BASE_EXPAND);
		} else if (pong_tags_equal (name, PONG_S_AlignLabel)) {
			state->base = &state->widget->base;
			START_READING_CHARS (INSIDE_BASE_ALIGN_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Tooltip)) {
			state->base = &state->widget->base;
			START_READING_CHARS (INSIDE_BASE_TOOLTIP);
		} else if (pong_tags_equal (name, PONG_S_Type)) {
			START_READING_CHARS (INSIDE_WIDGET_TYPE);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_ARGUMENT:
		if (pong_tags_equal (name, PONG_S_Name)) {
			START_READING_CHARS (INSIDE_ARGUMENT_NAME);
		} else if (pong_tags_equal (name, PONG_S_Value)) {
			START_READING_CHARS (INSIDE_ARGUMENT_VALUE);
		} else if (pong_tags_equal (name, PONG_S_Translatable)) {
			START_READING_CHARS (INSIDE_ARGUMENT_TRANSLATABLE);
		} else {
			START_IGNORE;
		}
		break;

	case INSIDE_OPTION:
		if (pong_tags_equal (name, PONG_S_Label)) {
			START_READING_CHARS (INSIDE_OPTION_LABEL);
		} else if (pong_tags_equal (name, PONG_S_Value)) {
			START_READING_CHARS (INSIDE_OPTION_VALUE);
		} else {
			START_IGNORE;
		}
		break;

	default:
		START_IGNORE;
		break;
	}
}

static void
endElement (gpointer user_data, const CHAR *name)
{
	State *state = user_data;
	int i, len;

	switch (state->state) {
	case IGNORE:
		state->ignore_depth--;
		if (state->ignore_depth == 0)
			state->state = state->ignore_last_state;
		break;

	case FINISH: /* just ingore everything now */
		break;

	case LOOKING_FOR_ELEMENT:
		state->state = FINISH;
		break;

	case INSIDE_ELEMENT:
		state->state = LOOKING_FOR_ELEMENT;
		if (state->element->type == PONG_TYPE_INVALID) {
			g_warning (_("Untyped Pong Element found"));
			pong_element_unref (state->element);
			state->element = NULL;
			break;
		}
		if ( ! state->element->conf_path) {
			g_warning (_("Unnamed Pong Element found"));
			pong_element_unref (state->element);
			state->element = NULL;
			break;
		}
		pong_file_add_element (state->gf, state->element);
		state->element = NULL;
		break;

	case INSIDE_BASECONFPATH:
		state->state = LOOKING_FOR_ELEMENT;
		g_free (state->gf->base_conf_path);
		state->gf->base_conf_path = g_strdup (state->buf->str);

		/* chop off trailing '/' */
		len = strlen (state->gf->base_conf_path);
		if (state->gf->base_conf_path[len-1] == '/')
			state->gf->base_conf_path[len-1] = '\0';

		/*FIXME: do further checking on path (gconf_valid_key??) */

		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_DIALOG_TITLE:
		state->state = LOOKING_FOR_ELEMENT;
		g_free (state->gf->dialog_title);
		state->gf->dialog_title = g_strdup (state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_AUTO_APPLY:
		state->state = LOOKING_FOR_ELEMENT;
		state->gf->auto_apply = pong_bool_from_string (state->buf->str);
		if ( ! state->gf->auto_apply) {
			g_warning (_("Non auto apply is NOT currently implemented"));
		}
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_REVERT:
		state->state = LOOKING_FOR_ELEMENT;
		state->gf->auto_apply = pong_bool_from_string (state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_HELP_NAME:
		state->state = INSIDE_HELP;
		g_free (state->gf->help_name);
		state->gf->help_name = g_strdup (state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_HELP_PATH:
		state->state = INSIDE_HELP;
		g_free (state->gf->help_path);
		state->gf->help_path = g_strdup (state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_HELP:
		state->state = LOOKING_FOR_ELEMENT;
		break;

	case INSIDE_LEVEL_NAME:
		state->state = INSIDE_LEVEL;
		g_free (state->level->name);
		state->level->name = g_strdup (state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_LEVEL_BASECONFPATH:
		state->state = INSIDE_LEVEL;
		g_free (state->level->base_conf_path);
		state->level->base_conf_path = g_strdup (state->buf->str);
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_LEVEL:
		if (state->level->name && state->level->base_conf_path) {
			state->gf->levels = g_list_prepend (state->gf->levels, state->level);
		} else {
			g_warning (_("Fields missing from Level tag"));
			g_free (state->level->name);
			g_free (state->level->base_conf_path);
			g_free (state->level);
		}
		state->level = NULL;
		state->state = LOOKING_FOR_ELEMENT;
		break;

	case INSIDE_ELEMENT_TYPE:
		pong_element_set_type_string (state->element,
					      state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_ELEMENT_CONF_PATH:
		pong_element_set_conf_path (state->element,
					   state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_ELEMENT_WIDGET:
		pong_element_set_widget (state->element,
					state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_ELEMENT_DEFAULT:
		pong_element_set_def (state->element,
				      state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_ELEMENT_SPECIFIER:
		pong_element_set_specifier (state->element,
					    state->buf->str);

		/* stop reading characters */
		state->reading_characters = FALSE;

		state->state = INSIDE_ELEMENT;
		break;

	case INSIDE_SENSITIVITY:
		state->state = INSIDE_ELEMENT;
		pong_element_add_sensitivity (state->element, state->sens);
		state->sens = NULL;
		break;

	case INSIDE_SENSITIVITY_VALUE:
		pong_sensitivity_add_value_from_string (state->sens, 
							state->buf->str);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_CONNECTION:
		i = get_connection (state->buf->str);
		if (i == PONG_CONNECTION_INVALID) {
			g_warning (_("Unknown connection '%s' assuming OR"),
				  state->buf->str);
			i = PONG_CONNECTION_OR;
		}
		pong_sensitivity_set_connection (state->sens, i);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_COMPARISON:
		i = get_comparison (state->buf->str);
		if (i == PONG_COMP_INVALID) {
			g_warning (_("Unknown comparison '%s' assuming equals"),
				  state->buf->str);
			i = PONG_COMP_EQ;
		}
		pong_sensitivity_set_comparison (state->sens, i);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_SENSITIVE:
		pong_sensitivity_add_sensitive (state->sens, state->buf->str);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_SENSITIVITY_INSENSITIVE:
		pong_sensitivity_add_insensitive (state->sens, state->buf->str);
		state->state = INSIDE_SENSITIVITY;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_BASE_NAME:
		pong_pane_base_set_name (state->base, state->buf->str);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_BASE_LABEL:
		pong_pane_base_set_label (state->base, state->buf->str);

		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_BASE_LEVEL:
		pong_pane_base_add_level (state->base,
					  state->buf->str);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_BASE_EXPAND:
		if (pong_bool_from_string (state->buf->str))
			pong_pane_base_set_expand (state->base, TRUE);
		else
			pong_pane_base_set_expand (state->base, FALSE);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_BASE_ALIGN_LABEL:
		if (pong_bool_from_string (state->buf->str))
			pong_pane_base_set_align_label (state->base, TRUE);
		else
			pong_pane_base_set_align_label (state->base, FALSE);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_BASE_TOOLTIP:
		pong_pane_base_set_tooltip (state->base, state->buf->str);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_PANE:
		state->gf->panes =
			g_list_append (state->gf->panes, state->pane);
		state->pane = NULL;
		state->state = LOOKING_FOR_ELEMENT;
		break;

	case INSIDE_GLADE:
		if (state->plug_widget->path == NULL ||
		    state->plug_widget->specifier == NULL) {
			g_warning (_("A glade widget needs to have 'File' and "
				     "'Widget' specified"));
		} else {
			if (state->group != NULL)
				pong_group_append_plug_widget (state->group, state->plug_widget);
			else
				pong_pane_append_plug_widget (state->pane, state->plug_widget);
		}
		pong_pane_base_unref (&state->plug_widget->base);
		state->plug_widget = NULL;
		state->state = state->state_before_plug;
		break;

	case INSIDE_BONOBO:
		if (state->plug_widget->path == NULL) {
			g_warning (_("A bonobo widget needs to have 'Query' "
				     "specified"));
		} else {
			if (state->group != NULL)
				pong_group_append_plug_widget (state->group, state->plug_widget);
			else
				pong_pane_append_plug_widget (state->pane, state->plug_widget);
		}
		pong_pane_base_unref (&state->plug_widget->base);
		state->plug_widget = NULL;
		state->state = state->state_before_plug;
		break;

	case INSIDE_PLUG:
		if (state->plug_widget->plug_type == NULL ||
		    state->plug_widget->path == NULL) {
			g_warning (_("A plug widget needs to have 'Type' and "
				     "'Path' specified at least"));
		} else {
			if (state->group != NULL)
				pong_group_append_plug_widget (state->group, state->plug_widget);
			else
				pong_pane_append_plug_widget (state->pane, state->plug_widget);
		}
		pong_pane_base_unref (&state->plug_widget->base);
		state->plug_widget = NULL;
		state->state = state->state_before_plug;
		break;

	case INSIDE_PLUG_TYPE:
		pong_plug_widget_set_plug_type (state->plug_widget,
						state->buf->str);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_PLUG_PATH:
		pong_plug_widget_set_path (state->plug_widget,
					   state->buf->str);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_PLUG_SPECIFIER:
		pong_plug_widget_set_specifier (state->plug_widget,
						state->buf->str);
		state->state = state->state_before_string;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_GROUP:
		pong_pane_append_group (state->pane, state->group);
		pong_pane_base_unref (&state->group->base);
		state->group = NULL;
		state->state = INSIDE_PANE;
		break;

	case INSIDE_GROUP_COLUMNS:
		i = atoi (state->buf->str);
		if (i < 1) {
			g_warning (_("Columns must be at least 1"));
			i = 1;
		}
		pong_group_set_columns (state->group, i);
		state->state = INSIDE_GROUP;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_WIDGET:
		pong_group_append_widget (state->group, state->widget);
		pong_pane_base_unref (&state->widget->base);
		state->widget = NULL;
		state->state = INSIDE_GROUP;
		break;

	case INSIDE_WIDGET_TYPE:
		pong_widget_set_widget_type_string (state->widget,
						    state->buf->str);
		state->state = INSIDE_WIDGET;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_ARGUMENT:

		if ( ! state->arg_name ||
		     ! state->arg_value) {
			g_warning (_("Argument requires both name and value"));
			g_free (state->arg_name);
			g_free (state->arg_value);
			state->arg_name = NULL;
			state->arg_value = NULL;
			state->state = state->state_before_argopt;
			break;
		}

		pong_pane_base_add_argument (state->base,
					     state->arg_name,
					     state->arg_value,
					     state->arg_translatable);
		g_free (state->arg_name);
		g_free (state->arg_value);
		state->arg_name = NULL;
		state->arg_value = NULL;
		state->state = state->state_before_argopt;
		break;

	case INSIDE_ARGUMENT_NAME:
		g_free (state->arg_name);
		state->arg_name = g_strdup (state->buf->str);
		state->state = INSIDE_ARGUMENT;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_ARGUMENT_VALUE:
		g_free (state->arg_value);
		state->arg_value = g_strdup (state->buf->str);
		state->state = INSIDE_ARGUMENT;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_ARGUMENT_TRANSLATABLE:
		if (pong_bool_from_string (state->buf->str))
			state->arg_translatable = TRUE;
		else
			state->arg_translatable = FALSE;
		state->state = INSIDE_ARGUMENT;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_OPTION:
		if (state->opt_value == NULL) {
			g_warning (_("Option requires a value"));
			g_free (state->opt_label);
			state->opt_label = NULL;
			state->opt_value = NULL;
			state->state = state->state_before_argopt;
			break;
		}

		pong_pane_base_add_option (state->base,
					   state->opt_label,
					   state->opt_value);
		state->state = state->state_before_argopt;
		break;

	case INSIDE_OPTION_LABEL:
		g_free (state->opt_label);
		state->opt_label = g_strdup (state->buf->str);
		state->state = INSIDE_OPTION;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	case INSIDE_OPTION_VALUE:
		g_free (state->opt_value);
		state->opt_value = g_strdup (state->buf->str);
		state->state = INSIDE_OPTION;
		/* stop reading characters */
		state->reading_characters = FALSE;
		break;

	default:
		g_warning ("UHH? (state %d)", state->state);
		break;
	}
}


/* stolen from the SAX interface tutorial */
static int
myXmlSAXParseFile (xmlSAXHandlerPtr sax,
		   gpointer user_data,
		   const char *filename)
{
	int ret = 0;                                        
	xmlParserCtxtPtr ctxt;                              

	ctxt = xmlCreateFileParserCtxt (filename);    
	if (ctxt == NULL) return -1;                 
	ctxt->sax = sax;                                    
	ctxt->userData = user_data;    

	xmlParseDocument (ctxt);      

	if (ctxt->wellFormed)        
		ret = 0;                                   
	else                                               
		ret = -1;          
	if (sax != NULL)                                   
		ctxt->sax = NULL;
	xmlFreeParserCtxt (ctxt);     

	return ret;
}

static int
myXmlSAXParseMemory (xmlSAXHandlerPtr sax,
		     gpointer user_data,
		     const char *buffer,
		     int size)
{
	int ret = 0;                                        
	xmlParserCtxtPtr ctxt;                              

	/* XXX: casting to (char *) should be safe */
	ctxt = xmlCreateMemoryParserCtxt ((char *)buffer, size);    
	if (ctxt == NULL) return -1;                 
	ctxt->sax = sax;                                    
	ctxt->userData = user_data;    

	xmlParseDocument (ctxt);      

	if (ctxt->wellFormed)        
		ret = 0;                                   
	else                                               
		ret = -1;          
	if (sax != NULL)                                   
		ctxt->sax = NULL;
	xmlFreeParserCtxt (ctxt);     

	return ret;
}

static void
pong_file_add_element (PongFile *gf, PongElement *ge)
{
	PongElement *old;
	old = g_hash_table_lookup (gf->element_hash,
				   ge->conf_path);
	if (old != NULL) {
		g_hash_table_remove (gf->element_hash,
				     ge->conf_path);
		pong_element_unref (old);
	}
	g_hash_table_insert (gf->element_hash,
			     ge->conf_path, ge);
}

static void
unref_elements (gpointer key, gpointer data, gpointer user_data)
{
	PongElement *ge = data;
	pong_element_unref (ge);
}

static void
pong_file_destroy (PongFile *gf)
{
	GList *li;
	g_hash_table_foreach (gf->element_hash,
			      unref_elements, NULL);
	g_hash_table_destroy (gf->element_hash);
	gf->element_hash = NULL;

	g_free (gf->file_name);
	gf->file_name = NULL;

	g_free (gf->base_conf_path);
	gf->base_conf_path = NULL;

	g_free (gf->dialog_title);
	gf->dialog_title = NULL;

	g_free (gf->help_name);
	gf->help_name = NULL;
	g_free (gf->help_path);
	gf->help_path = NULL;

	g_list_foreach (gf->panes, (GFunc)pong_pane_base_unref, NULL);
	gf->panes = NULL;

	for (li = gf->levels; li != NULL; li = li->next) {
		PongLevel *level = li->data;
		li->data = NULL;
		g_free (level->name);
		g_free (level->base_conf_path);
		g_free (level);
	}
	g_list_free (gf->levels);
	gf->levels = NULL;

	g_free (gf);
}

static void
clear_state (State *state)
{
	if (state->gf != NULL)
		pong_file_unref (state->gf);
	state->gf = NULL;

	if (state->sens != NULL)
		pong_sensitivity_destroy (state->sens);
	state->sens = NULL;

	if (state->element != NULL)
		pong_element_unref (state->element);
	state->element = NULL;

	if (state->pane != NULL)
		pong_pane_base_unref (&state->pane->base);
	state->pane = NULL;

	if (state->group != NULL)
		pong_pane_base_unref (&state->group->base);
	state->group = NULL;

	if (state->widget != NULL)
		pong_pane_base_unref (&state->widget->base);
	state->widget = NULL;

	if (state->plug_widget != NULL)
		pong_pane_base_unref (&state->plug_widget->base);
	state->plug_widget = NULL;

	g_free (state->arg_name);
	state->arg_name = NULL;
	g_free (state->arg_value);
	state->arg_value = NULL;

	g_free (state->opt_label);
	state->opt_label = NULL;
	g_free (state->opt_value);
	state->opt_value = NULL;

	if (state->level != NULL)
		pong_level_destroy (state->level);
	state->level = NULL;
}

/**
 * pong_file_load_xml_no_lookup:
 * @file:  file to load
 *
 * Description:  Tries to load a pong XML file but doesn't
 * try to look in the standard places for the @file.  It just
 * checks for existance.
 *
 * Returns:  a referenced PongFile structure or %NULL
 **/
PongFile *
pong_file_load_xml_no_lookup (const char *file)
{
	State state = {0};
	PongFile *gf;

	g_return_val_if_fail (file != NULL, NULL);

	if ( ! parser_inited)
		parser_init ();

	gf = g_hash_table_lookup (file_hash, file);
	if (gf != NULL) {
		return pong_file_ref (gf);
	}

	if ( ! pong_file_exists (file)) {
		g_warning (_("File '%s' does not exist"), file);
		return NULL;
	}

	state.string_lang_level = G_MAXINT;

	state.gf = g_new0 (PongFile, 1);
	state.gf->refcount = 1;
	state.gf->base_conf_path = g_strdup ("/");
	state.gf->cached = FALSE;
	state.gf->element_hash = g_hash_table_new (g_str_hash, g_str_equal);
	state.gf->auto_apply = TRUE;
	state.gf->revert = TRUE;

	state.gf->file_name = g_strdup (file);

	if (myXmlSAXParseFile (&handler, &state, file) < 0) {
		pong_file_unref (state.gf);
		state.gf = NULL;
		gf = NULL;
	} else {
		g_hash_table_insert (file_hash, g_strdup (file), state.gf);
		/* if cached is on, the object will not get destroyed on the
		 * last unref, but it will stick around in the hash */
		state.gf->cached = TRUE;
		/* ref for the user */
		gf = state.gf;
		state.gf = NULL;
	}

	clear_state (&state);

	return gf;
}

/**
 * pong_file_load_xml:
 * @file:  file to load
 *
 * Description:  Tries to load a pong XML file.  It
 * tries to find the @file in the given directories.
 *
 * Returns:  a referenced PongFile structure or %NULL
 **/
PongFile *
pong_file_load_xml (const char *file)
{
	PongFile *gf;
	char *found_file;

	g_return_val_if_fail (file != NULL, NULL);

	if ( ! parser_inited)
		parser_init ();

	gf = g_hash_table_lookup (file_hash, file);
	if (gf != NULL) {
		return pong_file_ref (gf);
	}

	found_file = pong_find_file (file, pong_directories);

	if (found_file == NULL) {
		g_warning (_("Cannot find file '%s'"), file);
		return NULL;
	}

	gf = pong_file_load_xml_no_lookup (found_file);

	g_free (found_file);

	return gf;
}

/**
 * pong_file_load_xml_from_memory:
 * @buffer:  xml buffer
 * @size:  size of buffer
 *
 * Description:  Tries to load a pong XML file from
 * a memory buffer.
 *
 * Returns:  a referenced PongFile structure or %NULL
 **/
PongFile *
pong_file_load_xml_from_memory (const char *buffer,
				int size)
{
	State state = {0};

	g_return_val_if_fail (buffer != NULL, NULL);
	g_return_val_if_fail (size > 0, NULL);

	if ( ! parser_inited)
		parser_init ();

	state.gf = g_new0 (PongFile, 1);
	state.gf->refcount = 1;
	state.gf->base_conf_path = g_strdup ("/");
	state.gf->cached = FALSE;
	state.gf->element_hash = g_hash_table_new (g_str_hash, g_str_equal);
	state.gf->auto_apply = TRUE;
	state.gf->revert = TRUE;

	if (myXmlSAXParseMemory (&handler, &state, buffer, size) < 0) {
		clear_state (&state);
		return NULL;
	} else {
		PongFile *gf = state.gf;
		state.gf = NULL;
		clear_state (&state);
		return gf;
	}
}

/**
 * pong_file_ref:
 * @gf:  a PongFile structure
 *
 * Description:  Adds a reference to the PongFile structure
 *
 * Returns:  @gf with refcount raised
 **/
PongFile *
pong_file_ref (PongFile *gf)
{
	g_return_val_if_fail (gf != NULL, NULL);

	gf->refcount ++;

	return gf;
}

/**
 * pong_file_unref:
 * @gf:  a PongFile structure
 *
 * Description:  Relenquishes a reference to a PongFile
 **/
void
pong_file_unref (PongFile *gf)
{
	g_return_if_fail (gf != NULL);

	gf->refcount --;

	if (gf->refcount == 0 &&
	    ! gf->cached)
		pong_file_destroy (gf);
}

static gboolean
kill_unused_pongfiles (gpointer key, gpointer data, gpointer user_data)
{
	PongFile *gf = data;
	if (gf->cached && gf->refcount <= 0) {
		gf->cached = FALSE;
		pong_file_destroy (gf);

		/* free the filename */
		g_free (key);
		return TRUE;
	}
	return FALSE;
}

/* kill all unused PongFile structures */
void
pong_parser_free_unused_data (void)
{
	if (parser_inited) {
		g_hash_table_foreach_remove (file_hash, kill_unused_pongfiles, NULL);

		if (g_hash_table_size (file_hash) == 0) {
			g_hash_table_destroy (file_hash);
			file_hash = NULL;
			parser_inited = FALSE;
		}
	}

	if (comparison_hash != NULL) {
		g_hash_table_destroy (comparison_hash);
		comparison_hash = NULL;
	}
}

static void
add_element_keys_to_list (gpointer key, gpointer data, gpointer user_data)
{
	GList **list = user_data;
	*list = g_list_prepend (*list, key);
}

/* list of internal strings, DON'T FREE THEM, just free the list */
GList *
pong_file_get_conf_paths (const PongFile *gf)
{
	GList *list = NULL;
	g_return_val_if_fail (gf != NULL, NULL);

	g_hash_table_foreach (gf->element_hash, add_element_keys_to_list,
			      &list);

	return list;
}

static void
add_elements_to_list (gpointer key, gpointer data, gpointer user_data)
{
	GList **list = user_data;
	*list = g_list_prepend (*list, data);
}

GList *
pong_file_get_elements (const PongFile *gf)
{
	GList *list = NULL;
	g_return_val_if_fail (gf != NULL, NULL);

	g_hash_table_foreach (gf->element_hash, add_elements_to_list, &list);

	return list;
}

PongElement *
pong_file_lookup_element (const PongFile *gf, const char *conf_path)
{
	g_return_val_if_fail (gf != NULL, NULL);
	g_return_val_if_fail (conf_path != NULL, NULL);

	return g_hash_table_lookup (gf->element_hash, conf_path);
}

static void
foreach_elements (gpointer key, gpointer data, gpointer user_data)
{
	gpointer *fedata = user_data;
	PongElementFunc func = fedata[0];

	func ((PongElement *)data, fedata[1]);
}

void
pong_file_foreach_element (PongFile *gf, PongElementFunc func, gpointer data)
{
	gpointer fedata[2];

	g_return_if_fail (gf != NULL);
	g_return_if_fail (func != NULL);

	fedata[0] = func;
	fedata[1] = data;

	g_hash_table_foreach (gf->element_hash, foreach_elements, fedata);
}

PongLevel *
pong_level_new (void)
{
	PongLevel *level = g_new0 (PongLevel, 1);
	return level;
}

void
pong_level_destroy (PongLevel *level)
{
	g_return_if_fail (level != NULL);

	g_free (level->name);
	level->name = NULL;
	g_free (level->base_conf_path);
	level->base_conf_path = NULL;
	g_free (level);
}


GList *
pong_file_get_levels (PongFile *gf)
{
	GList *li, *list = NULL;

	g_return_val_if_fail (gf != NULL, NULL);

	for (li = gf->levels; li != NULL; li = li->next) {
		PongLevel *level = li->data;
		list = g_list_prepend (list, level->name);
	}

	return list;
}

const char *
pong_file_get_level_base_conf_path (PongFile *gf, const char *level_name)
{
	GList *li;

	g_return_val_if_fail (gf != NULL, NULL);
	g_return_val_if_fail (level_name != NULL, NULL);

	for (li = gf->levels; li != NULL; li = li->next) {
		PongLevel *level = li->data;
		if (strcmp (level_name, level->name) == 0)
			return level->base_conf_path;
	}
	return NULL;
}
