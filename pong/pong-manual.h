/* PonG: the manual library interface
 *   Note: may be deprecated if unused
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONG_MANUAL_H
#define PONG_MANUAL_H

#include <glade/glade.h>

#include "pong.h"
#include "pong-glade.h"

/* The standard (non-glade) ui objects */
#include "pong-ui-manual.h"

/* The glade objects */
#include "pong-ui-glade.h"

#endif /* PONG_MANUAL_H */
