/* PonG: String constants
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* This is an internal header only */

#ifndef PONG_STRINGS_H
#define PONG_STRINGS_H

/* String constants here so that misspellings get cought by the
 * compiler */

#define PONG_S_OR			"OR"
#define PONG_S_AND			"AND"
#define PONG_S_PongElements		"PongElements"
#define PONG_S_Element			"Element"
#define PONG_S_Pane			"Pane"
#define PONG_S_BaseConfPath		"BaseConfPath"
#define PONG_S_DialogTitle		"DialogTitle"
#define PONG_S_AutoApply		"AutoApply"
#define PONG_S_Revert			"Revert"
#define PONG_S_Help			"Help"
#define PONG_S_Level			"Level"
#define PONG_S_Name			"Name"
#define PONG_S_Path			"Path"
#define PONG_S_Type			"Type"
#define PONG_S_ConfPath			"ConfPath"
#define PONG_S_Widget			"Widget"
#define PONG_S_Default			"Default"
#define PONG_S_Specifier		"Specifier"
#define PONG_S_Sensitivity		"Sensitivity"
#define PONG_S_Value			"Value"
#define PONG_S_Connection		"Connection"
#define PONG_S_Comparison		"Comparison"
#define PONG_S_Sensitive		"Sensitive"
#define PONG_S_Insensitive		"Insensitive"
#define PONG_S_Group			"Group"
#define PONG_S_Plug			"Plug"
#define PONG_S_Glade			"Glade"
#define PONG_S_Bonobo			"Bonobo"
#define PONG_S_Label			"Label"
#define PONG_S_Expand			"Expand"
#define PONG_S_AlignLabel		"AlignLabel"
#define PONG_S_Columns			"Columns"
#define PONG_S_File			"File"
#define PONG_S_Query			"Query"
#define PONG_S_Argument			"Argument"
#define PONG_S_Option			"Option"
#define PONG_S_Tooltip			"Tooltip"
#define PONG_S_Translatable		"Translatable"

#define PONG_S_PongTranslations		"PongTranslations"
#define PONG_S_Translation		"Translation"
#define PONG_S_UnusedTranslation	"UnusedTranslation"
#define PONG_S_Translated		"Translated"
#define PONG_S_String			"String"
#define PONG_S_Locale			"Locale"

#define PONG_S_Int			"Int"
#define PONG_S_Float			"Float"
#define PONG_S_Bool			"Bool"

#define PONG_S_ListOfStrings		"ListOfStrings"
#define PONG_S_ListOfInts		"ListOfInts"
#define PONG_S_ListOfFloats		"ListOfFloats"
#define PONG_S_ListOfBools		"ListOfBools"

#define PONG_S_PairStringString		"PairStringString"
#define PONG_S_PairStringInt		"PairStringInt"
#define PONG_S_PairStringFloat		"PairStringFloat"
#define PONG_S_PairStringBool		"PairStringBool"

#define PONG_S_PairIntString		"PairIntString"
#define PONG_S_PairIntInt		"PairIntInt"
#define PONG_S_PairIntFloat		"PairIntFloat"
#define PONG_S_PairIntBool		"PairIntBool"

#define PONG_S_PairFloatString		"PairFloatString"
#define PONG_S_PairFloatInt		"PairFloatInt"
#define PONG_S_PairFloatFloat		"PairFloatFloat"
#define PONG_S_PairFloatBool		"PairFloatBool"

#define PONG_S_PairBoolString		"PairBoolString"
#define PONG_S_PairBoolInt		"PairBoolInt"
#define PONG_S_PairBoolFloat		"PairBoolFloat"
#define PONG_S_PairBoolBool		"PairBoolBool"

#define PONG_S_SchemaOwner		"SchemaOwner"
#define PONG_S_SchemaShortDescription	"SchemaShortDescription"
#define PONG_S_SchemaLongDescription	"SchemaLongDescription"

#define PONG_S_gconfschemafile		"gconfschemafile"
#define PONG_S_schemalist		"schemalist"
#define PONG_S_schema			"schema"
#define PONG_S_key			"key"
#define PONG_S_applyto			"applyto"
#define PONG_S_owner			"owner"
#define PONG_S_type			"type"
#define PONG_S_list_type		"list_type"
#define PONG_S_car_type			"car_type"
#define PONG_S_cdr_type			"cdr_type"
#define PONG_S_type			"type"
#define PONG_S_default			"default"
#define PONG_S_locale			"locale"
#define PONG_S_name			"name"
#define PONG_S_short			"short"
#define PONG_S_long			"long"

#define PONG_S_int			"int"
#define PONG_S_string			"string"
#define PONG_S_float			"float"
#define PONG_S_bool			"bool"
#define PONG_S_list			"list"
#define PONG_S_pair			"pair"

#define PONG_S_PongWidgetLabel		"PongWidgetLabel"

#define PONG_S_TopLevel			"TopLevel"

#endif /* PONG_STRINGS_H */
