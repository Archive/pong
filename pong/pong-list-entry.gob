%at{
/* PonG: list entry
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001,2002 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.99.2
%h{
#include "pong-widget-interface.h"
%}
%{
#include "config.h"
#include <gnome.h>
#include <gconf/gconf-value.h>

#include "pong-i18n.h"

#include "pongpane.h"
#include "pongutil.h"

#include "pong-list-entry.h"
#include "pong-list-entry-private.h"

#define MINIMUM_LIST_WIDTH 110
#define MINIMUM_LIST_HEIGHT 160

static gboolean is_string_in_list (const GList *list, const char *string);
static gboolean is_string_in_option_list (const GList *list, const char *string);
static gboolean is_value_in_value_list (const GSList *list, const char *string);
static int find_value_in_clist (GtkCList *clist, GConfValue *value);

%}

class Pong:List:Entry from Gtk:HBox {

	private GtkWidget *origin_clist;
	private GtkWidget *destination_clist;

	private GList *values = NULL
		destroy {
			if (VAR != NULL) {
				g_list_foreach (VAR, (GFunc)g_free, NULL);
				g_list_free (VAR);
			}
		};

	class_init (klass)
	{
		PongWidgetInterface *iface;

		iface = pong_widget_interface_add (GTK_OBJECT_CLASS (klass));
		iface->changed_signal = "changed";
		iface->get_value = self_get_value;
		iface->set_value = self_set_value;
		iface->add_options = self_add_options;
	}

	init (self)
	{
		GtkWidget *sw, *arrow, *button, *list, *a;
		GtkWidget *vbox;
		char *titles[1];

		gtk_box_set_spacing (GTK_BOX (self), GNOME_PAD_SMALL);

		/* The available list */
		sw = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_set_usize (sw,
				      MINIMUM_LIST_WIDTH,
				      MINIMUM_LIST_HEIGHT);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
						GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);
		gtk_box_pack_start (GTK_BOX (self), sw, TRUE, TRUE, 0);
		gtk_widget_show (sw);

		titles[0] = _("Available");
		list = gtk_clist_new_with_titles (1, titles);
		gtk_clist_column_titles_passive (GTK_CLIST (list));
		gtk_clist_set_selection_mode (GTK_CLIST (list),
					      GTK_SELECTION_MULTIPLE);
		gtk_clist_set_column_auto_resize (GTK_CLIST (list), 0, TRUE);
		gtk_container_add (GTK_CONTAINER (sw), list);
		gtk_widget_show (list);

		self->_priv->origin_clist = list;

		/* the buttons */
		a = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
		gtk_widget_show (a);
		gtk_box_pack_start (GTK_BOX (self), a, FALSE, FALSE, 0);

		vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
		gtk_container_add (GTK_CONTAINER (a), vbox);
		gtk_widget_show (vbox);

		button = gtk_button_new ();
		gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
		gtk_widget_show (button);

		gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
					   GTK_SIGNAL_FUNC (self_move_items_in),
					   GTK_OBJECT (self));

		arrow = gtk_arrow_new (GTK_ARROW_RIGHT,
				       GTK_SHADOW_IN);
		gtk_container_add (GTK_CONTAINER (button), arrow);
		gtk_widget_show (arrow);

		button = gtk_button_new ();
		gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
		gtk_widget_show (button);

		gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
					   GTK_SIGNAL_FUNC (self_move_items_out),
					   GTK_OBJECT (self));

		arrow = gtk_arrow_new (GTK_ARROW_LEFT,
				       GTK_SHADOW_IN);
		gtk_container_add (GTK_CONTAINER (button), arrow);
		gtk_widget_show (arrow);

		/* The selected list */
		sw = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_set_usize (sw,
				      MINIMUM_LIST_WIDTH,
				      MINIMUM_LIST_HEIGHT);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
						GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);
		gtk_box_pack_start (GTK_BOX (self), sw, TRUE, TRUE, 0);
		gtk_widget_show (sw);

		titles[0] = _("Selected");
		list = gtk_clist_new_with_titles (1, titles);
		gtk_clist_column_titles_passive (GTK_CLIST (list));
		gtk_clist_set_selection_mode (GTK_CLIST (list),
					      GTK_SELECTION_MULTIPLE);
		gtk_clist_set_column_auto_resize (GTK_CLIST (list), 0, TRUE);
		gtk_container_add (GTK_CONTAINER (sw), list);
		gtk_widget_show (list);

		self->_priv->destination_clist = list;
	}

	property STRING available_label
		(nick = _("Available label"),
		 blurb = _("Label of available column"))
		set {
			if (g_value_get_string (VAL) == NULL) {
				gtk_clist_column_titles_hide (GTK_CLIST (self->_priv->origin_clist));
			} else {
				gtk_clist_column_titles_show (GTK_CLIST (self->_priv->origin_clist));
				gtk_clist_set_column_title (GTK_CLIST (self->_priv->origin_clist),
							    0, g_value_get_string (VAL));
			}
		};
	property STRING selected_label
		(nick = _("Selected label"),
		 blurb = _("Label of selected column"))
		set {
			if (g_value_get_string (VAL) == NULL) {
				gtk_clist_column_titles_hide (GTK_CLIST (self->_priv->destination_clist));
			} else {
				gtk_clist_column_titles_show (GTK_CLIST (self->_priv->destination_clist));
				gtk_clist_set_column_title (GTK_CLIST (self->_priv->destination_clist),
							    0, g_value_get_string (VAL));
			}
		};


	/**
	 * new:
	 *
	 * Description:  Create a new #PongListEntry widget
	 *
	 * Returns:  A new #PongListEntry widget
	 **/
	public
	GtkWidget *
	new (void)
	{
		return (GtkWidget *) GET_NEW;
	}

	/**
	 * new_with_options:
	 * @options:  a list of PongOption structs
	 *
	 * Description:  Create a new #PongListEntry widget and
	 * setup the @options.
	 *
	 * Returns:  A new #PongListEntry widget
	 **/
	public
	GtkWidget *
	new_with_options (GList *pong_options)
	{
		GtkWidget *w = (GtkWidget *) GET_NEW;
		self_add_options (w, pong_options);
		return w;
	}

	private signal NONE (NONE)
	void
	changed (self);

	private gboolean
	get_value (Gtk:Widget *w (check null type),
		   const char *specifier,
		   PongType type,
		   GConfValue **value (check null))
	{
		Self *self = SELF (w);
		GSList *value_list;
		GList *li;

		if (type != PONG_TYPE_LIST_OF_STRINGS &&
		    type != PONG_TYPE_LIST_OF_INTS &&
		    type != PONG_TYPE_LIST_OF_FLOATS &&
		    type != PONG_TYPE_LIST_OF_BOOLS) {
			g_warning (_("Unsupported type on Pong:List:Entry. "
				     "Only list types are supported."));
			return FALSE;
		}

		*value = gconf_value_new (GCONF_VALUE_LIST);

		switch (type) {
		case PONG_TYPE_LIST_OF_STRINGS:
			gconf_value_set_list_type (*value, GCONF_VALUE_STRING);
			break;
		case PONG_TYPE_LIST_OF_INTS:
			gconf_value_set_list_type (*value, GCONF_VALUE_INT);
			break;
		case PONG_TYPE_LIST_OF_FLOATS:
			gconf_value_set_list_type (*value, GCONF_VALUE_FLOAT);
			break;
			/* hmm, do bools even make sense? */
		case PONG_TYPE_LIST_OF_BOOLS:
			gconf_value_set_list_type (*value, GCONF_VALUE_BOOL);
			break;
		default:
			g_assert_not_reached ();
		}

		value_list = NULL;
		for (li = self->_priv->values;
		     li != NULL;
		     li = li->next) {
			char *string = li->data;
			GConfValue *val = NULL;

			switch (type) {
			case PONG_TYPE_LIST_OF_STRINGS:
				val = gconf_value_new (GCONF_VALUE_STRING);
				gconf_value_set_string (val, string);
				break;
			case PONG_TYPE_LIST_OF_INTS:
				val = gconf_value_new (GCONF_VALUE_INT);
				gconf_value_set_int (val, atoi (string));
				break;
			case PONG_TYPE_LIST_OF_FLOATS:
				val = gconf_value_new (GCONF_VALUE_FLOAT);
				pong_i18n_push_c_numeric_locale ();
				gconf_value_set_float (val, atof (string));
				pong_i18n_pop_c_numeric_locale ();
				break;
			case PONG_TYPE_LIST_OF_BOOLS:
				val = gconf_value_new (GCONF_VALUE_BOOL);
				gconf_value_set_int (val,
						     pong_bool_from_string
						     (string));
				break;
			default:
				g_assert_not_reached ();
			}
			value_list = g_slist_prepend (value_list, val);
		}
		value_list = g_slist_reverse (value_list);
		gconf_value_set_list_nocopy (*value, value_list);

		return TRUE;
	}

	private gboolean
	set_value (Gtk:Widget *w (check null type),
		   const char *specifier,
		   GConfValue *value (check null))
	{
		Self *self = SELF (w);
		GtkCList *clist1;
		GtkCList *clist2;
		GList *row_list;
		GList *li;
		GSList *value_list;
		GSList *sli;
		int previous_length, i;

		if (value->type != GCONF_VALUE_LIST) {
			g_warning (_("Unsupported type on Pong:List:Entry. "
				     "Only list types are supported."));
			return FALSE;
		}

		clist1 = GTK_CLIST (self->_priv->origin_clist);
		clist2 = GTK_CLIST (self->_priv->destination_clist);

		previous_length = clist2->rows;

		value_list = gconf_value_get_list (value);

		for (sli = value_list;
		     sli != NULL;
		     sli = sli->next) {
			GConfValue *value = sli->data;
			int row;

			row = find_value_in_clist (clist1, value);
			if (row >= 0) {
				self_move_item (self, clist1, row, clist2,
						TRUE /* in */);
				continue;
			}
		}

		row_list = g_list_copy (clist2->row_list);
		for (i = 0, li = row_list;
		     i < previous_length && li != NULL;
		     i++, li = li->next) {
			GtkCListRow *row = li->data;
			char *string = row->data;

			if ( ! is_value_in_value_list (value_list, string)) {
				self_move_item (self, clist2, i, clist1,
						FALSE /*in*/);
				/* we need to move down one since we just
				 * truncated the list */
				i--;
				previous_length--;
			}
		}
		g_list_free (row_list);

		return TRUE;
	}

	private void
	add_options (Gtk:Widget *w (check null type),
		     GList *pong_options)
	{
		Self *self = SELF (w);
		GList *li, *list;
		GtkCList *clist1;
		GtkCList *clist2;

		clist1 = GTK_CLIST (self->_priv->origin_clist);
		clist2 = GTK_CLIST (self->_priv->destination_clist);

		gtk_clist_freeze (clist1);
		gtk_clist_freeze (clist2);

		gtk_clist_clear (clist1);
		gtk_clist_clear (clist2);

		list = g_list_copy (self->_priv->values);
		for (li = list; li != NULL; li = li->next) {
			char *val = li->data;
			if ( ! is_string_in_option_list (pong_options, val)) {
				li->data = NULL;
				self->_priv->values =
					g_list_remove (self->_priv->values,
						       val);
				g_free (val);
			}
		}
		g_list_free (list);

		for (li = pong_options; li != NULL; li = li->next) {
			PongOption *opt = li->data;
			char *text[1];
			int row;
			GtkCList *clist;

			if (opt->label != NULL)
				text[0] = opt->label;
			else
				text[0] = pong_sure_string (opt->value);

			if (is_string_in_list (self->_priv->values,
					       pong_sure_string (opt->value))) {
				clist = clist2;
			} else {
				clist = clist1;
			}
			row = gtk_clist_append (clist, text);

			gtk_clist_set_row_data_full
				(clist, row,
				 g_strdup (pong_sure_string (opt->value)),
				 (GtkDestroyNotify)g_free);
		}

		gtk_clist_thaw (clist1);
		gtk_clist_thaw (clist2);
	}

	private void
	move_item (self,
		   Gtk:CList *clist1 (check null type),
		   int row,
		   Gtk:CList *clist2 (check null type),
		   gboolean in)
	{
		int new_row;
		char *text[1];
		char *value;

		gtk_clist_get_text (clist1, row, 0, &text[0]);
		value = pong_sure_string
			(gtk_clist_get_row_data (clist1, row));

		/* if this is moving values IN, append it
		 * otherwise, remove */
		if (in) {
			self->_priv->values = g_list_append
				(self->_priv->values, g_strdup (value));
		} else {
			GList *li;
			for (li = self->_priv->values;
			     li != NULL;
			     li = li->next) {
				char *val = li->data;
				if (strcmp (val, value) == 0) {
					li->data = NULL;
					g_free (val);
					self->_priv->values =
						g_list_remove_link
						(self->_priv->values,
						 li);
					g_list_free_1 (li);
					break;
				}
			}
		}

		new_row = gtk_clist_append (clist2, text);
		gtk_clist_set_row_data_full
			(clist2, new_row,
			 g_strdup (pong_sure_string (value)),
			 (GtkDestroyNotify)g_free);

		gtk_clist_remove (clist1, row);

		self_changed (self);
	}

	private void
	move_items (self, Gtk:CList *clist1 (check null type),
		    Gtk:CList *clist2 (check null type),
		    gboolean in)
	{
		while (clist1->selection != NULL) {
			int row = GPOINTER_TO_INT (clist1->selection->data);

			self_move_item (self, clist1, row, clist2, in);
		}
	}

	private void
	move_items_in (self)
	{
		self_move_items (self,
				 GTK_CLIST (self->_priv->origin_clist),
				 GTK_CLIST (self->_priv->destination_clist),
				 TRUE /*in*/);
	}

	private void
	move_items_out (self)
	{
		self_move_items (self,
				 GTK_CLIST (self->_priv->destination_clist),
				 GTK_CLIST (self->_priv->origin_clist),
				 FALSE /*in*/);
	}
}

%{
static gboolean
is_string_in_list (const GList *list, const char *string)
{
	g_return_val_if_fail (string != NULL, FALSE);

	while (list != NULL) {
		if (list->data != NULL &&
		    strcmp (string, list->data) == 0)
			return TRUE;

		list = list->next;
	}

	return FALSE;
}

static gboolean
is_string_in_option_list (const GList *list, const char *string)
{
	g_return_val_if_fail (string != NULL, FALSE);

	while (list != NULL) {
		PongOption *opt = list->data;
		if (opt != NULL &&
		    opt->value != NULL &&
		    strcmp (string, opt->value) == 0)
			return TRUE;

		list = list->next;
	}

	return FALSE;
}

static int
find_value_in_clist (GtkCList *clist, GConfValue *value)
{
	GList *li;
	int i;

	for (i = 0, li = clist->row_list; li != NULL; i++, li = li->next) {
		GtkCListRow *row = li->data;
		char *string = row->data;

		if (pong_string_value_equal (string, value))
			return i;
	}

	return -1;
}

static gboolean
is_value_in_value_list (const GSList *list, const char *string)
{
	const GSList *li;

	for (li = list; li != NULL; li = li->next) {
		GConfValue *value = li->data;

		if (pong_string_value_equal (string, value))
			return TRUE;
	}

	return FALSE;
}

%}
