/* PonG: the elements interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"

/* So that we still support GtkText, in case some app uses it */
#define GTK_ENABLE_BROKEN

#include <gnome.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "pong-i18n.h"

#include "pongwidgetutil.h"
#include "pong-widget-interface.h"

#include "pongutil.h"
#include "pong-strings.h"

#include "pongelement.h"

static void pong_element_destroy (PongElement *ge);

static GHashTable *type_hash = NULL;
static GHashTable *anti_type_hash = NULL;

static void
init_type_hash (void)
{
	if (type_hash != NULL)
		return;

	type_hash = g_hash_table_new (g_str_hash, g_str_equal);
	anti_type_hash = g_hash_table_new (NULL, NULL);

	/* Basic types */
	g_hash_table_insert (type_hash, PONG_S_String,
			     GINT_TO_POINTER (PONG_TYPE_STRING));
	g_hash_table_insert (type_hash, PONG_S_Int,
			     GINT_TO_POINTER (PONG_TYPE_INT));
	g_hash_table_insert (type_hash, PONG_S_Float,
			     GINT_TO_POINTER (PONG_TYPE_FLOAT));
	g_hash_table_insert (type_hash, PONG_S_Bool,
			     GINT_TO_POINTER (PONG_TYPE_BOOL));

	/* List types */
	g_hash_table_insert (type_hash, PONG_S_ListOfStrings,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_STRINGS));
	g_hash_table_insert (type_hash, PONG_S_ListOfInts,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_INTS));
	g_hash_table_insert (type_hash, PONG_S_ListOfFloats,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_FLOATS));
	g_hash_table_insert (type_hash, PONG_S_ListOfBools,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_BOOLS));

	/* Pair types */
	g_hash_table_insert (type_hash, PONG_S_PairStringString,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_STRING));
	g_hash_table_insert (type_hash, PONG_S_PairStringInt,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_INT));
	g_hash_table_insert (type_hash, PONG_S_PairStringFloat,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_FLOAT));
	g_hash_table_insert (type_hash, PONG_S_PairStringBool,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_BOOL));

	g_hash_table_insert (type_hash, PONG_S_PairIntString,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_STRING));
	g_hash_table_insert (type_hash, PONG_S_PairIntInt,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_INT));
	g_hash_table_insert (type_hash, PONG_S_PairIntFloat,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_FLOAT));
	g_hash_table_insert (type_hash, PONG_S_PairIntBool,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_BOOL));

	g_hash_table_insert (type_hash, PONG_S_PairFloatString,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_STRING));
	g_hash_table_insert (type_hash, PONG_S_PairFloatInt,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_INT));
	g_hash_table_insert (type_hash, PONG_S_PairFloatFloat,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_FLOAT));
	g_hash_table_insert (type_hash, PONG_S_PairFloatBool,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_BOOL));

	g_hash_table_insert (type_hash, PONG_S_PairBoolString,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_STRING));
	g_hash_table_insert (type_hash, PONG_S_PairBoolInt,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_INT));
	g_hash_table_insert (type_hash, PONG_S_PairBoolFloat,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_FLOAT));
	g_hash_table_insert (type_hash, PONG_S_PairBoolBool,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_BOOL));

	/* Basic types */
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_STRING),
			     PONG_S_String);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_INT),
			     PONG_S_Int);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_FLOAT),
			     PONG_S_Float);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_BOOL),
			     PONG_S_Bool);

	/* List types */
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_STRINGS),
			     PONG_S_ListOfStrings);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_INTS),
			     PONG_S_ListOfInts);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_FLOATS),
			     PONG_S_ListOfFloats);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_LIST_OF_BOOLS),
			     PONG_S_ListOfBools);

	/* Pair types */
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_STRING),
			     PONG_S_PairStringString);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_INT),
			     PONG_S_PairStringInt);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_FLOAT),
			     PONG_S_PairStringFloat);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_STRING_BOOL),
			     PONG_S_PairStringBool);

	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_STRING),
			     PONG_S_PairIntString);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_INT),
			     PONG_S_PairIntInt);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_FLOAT),
			     PONG_S_PairIntFloat);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_INT_BOOL),
			     PONG_S_PairIntBool);

	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_STRING),
			     PONG_S_PairFloatString);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_INT),
			     PONG_S_PairFloatInt);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_FLOAT),
			     PONG_S_PairFloatFloat);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_FLOAT_BOOL),
			     PONG_S_PairFloatBool);

	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_STRING),
			     PONG_S_PairBoolString);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_INT),
			     PONG_S_PairBoolInt);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_FLOAT),
			     PONG_S_PairBoolFloat);
	g_hash_table_insert (anti_type_hash,
			     GINT_TO_POINTER (PONG_TYPE_PAIR_BOOL_BOOL),
			     PONG_S_PairBoolBool);
}

PongType
pong_type_from_string (const char *type_string)
{
	g_return_val_if_fail (type_string != NULL, 0);

	init_type_hash ();

	return GPOINTER_TO_INT (g_hash_table_lookup (type_hash, type_string));
}

const char *
pong_string_from_type (PongType type)
{
	init_type_hash ();

	return g_hash_table_lookup (anti_type_hash, GINT_TO_POINTER (type));
}

PongElement *
pong_element_new (void)
{
	PongElement *ge;
       
	ge = g_new0 (PongElement, 1);
	ge->refcount = 1;

	return ge;
}

void
pong_element_ref (PongElement *ge)
{
	g_return_if_fail (ge != NULL);

	ge->refcount++;
}

void
pong_element_unref (PongElement *ge)
{
	g_return_if_fail (ge != NULL);

	ge->refcount--;
	if (ge->refcount <= 0)
		pong_element_destroy (ge);
}

static void
pong_element_destroy (PongElement *ge)
{
	g_return_if_fail (ge != NULL);

	/* called before ANY destruction on the object is done so that
	 * the object is still completely intact */
	if (ge->destroy_notify != NULL)
		ge->destroy_notify (ge->data);
	ge->destroy_notify = NULL;
	ge->data = NULL;

	g_free (ge->conf_path);
	ge->conf_path = NULL;

	g_free (ge->widget);
	ge->widget = NULL;

	g_free (ge->def);
	ge->def = NULL;

	g_slist_foreach (ge->sensitivity, (GFunc)pong_sensitivity_destroy, NULL);
	g_slist_free (ge->sensitivity);
	ge->sensitivity = NULL;

	g_free (ge);
}

void
pong_element_set_type (PongElement *el, PongType type)
{
	g_return_if_fail (el != NULL);
	g_return_if_fail (type >= 0);

	el->type = type;
}

void
pong_element_set_type_string (PongElement *el, const char *type)
{
	g_return_if_fail (el != NULL);
	g_return_if_fail (type != NULL);

	el->type = pong_type_from_string (type);
}

void
pong_element_set_data (PongElement *el,
		       gpointer data,
		       GDestroyNotify destroy_notify)
{
	g_return_if_fail (el != NULL);

	if (el->destroy_notify != NULL) {
		el->destroy_notify (el->data);
	}

	el->data = data;
	el->destroy_notify = destroy_notify;
}

void
pong_element_set_widget (PongElement *el, const char *widget)
{
	g_return_if_fail (el != NULL);

	/* Handles NULL well */
	g_free (el->widget);
	el->widget = g_strdup (widget);
}

void
pong_element_set_def (PongElement *el, const char *def)
{
	g_return_if_fail (el != NULL);

	/* Handles NULL well */
	g_free (el->def);
	el->def = g_strdup (def);
}

void
pong_element_set_specifier (PongElement *el, const char *specifier)
{
	g_return_if_fail (el != NULL);

	/* Handles NULL well */
	g_free (el->specifier);
	el->specifier = g_strdup (specifier);
}

void
pong_element_set_conf_path (PongElement *el, const char *conf_path)
{
	g_return_if_fail (el != NULL);
	g_return_if_fail (conf_path != NULL);

	g_free (el->conf_path);
	el->conf_path = g_strdup (conf_path);
}

/* Note: takes ownership of 'sens' */
void
pong_element_add_sensitivity (PongElement *ge, PongSensitivity *sens)
{
	g_return_if_fail (ge != NULL);
	g_return_if_fail (sens != NULL);

	ge->sensitivity = g_slist_append (ge->sensitivity, sens);
}


void
pong_element_free_unused_data (void)
{
	if (type_hash != NULL) {
		g_hash_table_destroy (type_hash);
		type_hash = NULL;
	}
	if (anti_type_hash != NULL) {
		g_hash_table_destroy (anti_type_hash);
		anti_type_hash = NULL;
	}
}

PongRuntimeElement *
pong_runtime_element_new (PongElement *el, PongRuntimeUpdateFunc func,
			  gpointer data)
{
	PongRuntimeElement *gre = g_new0 (PongRuntimeElement, 1);
	gre->el = el;
	gre->gconf_notify = 0;
	gre->updated_idle = 0;
	gre->update_func = func;
	gre->update_data = data;

	/* we let PongXML set up the pointers for us */
	return gre;
}

void
pong_runtime_element_destroy (PongRuntimeElement *gre, GConfClient *client)
{
	g_return_if_fail (gre != NULL);
	g_return_if_fail (client != NULL);
	g_return_if_fail (GCONF_IS_CLIENT (client));

	pong_runtime_element_remove_notify (gre, client);

	if (gre->updated_idle > 0)
		gtk_idle_remove (gre->updated_idle);
	gre->updated_idle = 0;

	g_free (gre);
}

void
pong_runtime_element_add_notify (PongRuntimeElement *gre,
				 GConfClient *client,
				 const char *prefix,
				 GConfClientNotifyFunc func,
				 gpointer user_data,
				 GFreeFunc destroy_notify,
				 GError **err)
{
	char *fullkey;

	g_return_if_fail (gre != NULL);
	g_return_if_fail (client != NULL);
	g_return_if_fail (GCONF_IS_CLIENT (client));

	if (prefix != NULL &&
	    gre->el->conf_path[0] != '/')
		fullkey = g_concat_dir_and_file (prefix, gre->el->conf_path);
	else
		fullkey = g_strdup (gre->el->conf_path);

	pong_runtime_element_remove_notify (gre, client);

	gre->gconf_notify = gconf_client_notify_add (client, fullkey,
						     func, user_data,
						     destroy_notify, err);
	if (gre->gconf_notify != 0 &&
	    gre->el->conf_path[0] == '/') {
		char *dir = g_dirname (gre->el->conf_path);
		if (pong_string_empty (dir))
			gconf_client_add_dir (client, "/",
					      GCONF_CLIENT_PRELOAD_NONE, NULL);
		else
			gconf_client_add_dir (client, dir,
					      GCONF_CLIENT_PRELOAD_NONE, NULL);
		g_free (dir);
	}

	g_free (fullkey);
}

void
pong_runtime_element_remove_notify (PongRuntimeElement *gre,
				    GConfClient *client)
{
	g_return_if_fail (gre != NULL);
	g_return_if_fail (client != NULL);
	g_return_if_fail (GCONF_IS_CLIENT (client));

	if (gre->gconf_notify > 0) {
		gconf_client_notify_remove (client, gre->gconf_notify);
		gre->gconf_notify = 0;

		if (gre->el->conf_path[0] == '/') {
			char *dir = g_dirname (gre->el->conf_path);
			if (pong_string_empty (dir))
				gconf_client_remove_dir (client, "/", NULL);
			else
				gconf_client_remove_dir (client, dir, NULL);
			g_free (dir);
		}
	}
}

static gboolean
widget_change_idle (gpointer data)
{
	PongRuntimeElement *gre = data;

	gre->updated_idle = 0;

	if (gre->update_func != NULL)
		gre->update_func (gre, gre->update_data);

	return FALSE;
}


static void
widget_change_signal (GtkWidget *widget, gpointer data)
{
	PongRuntimeElement *gre = data;

	if (gre->updated_idle == 0)
		gre->updated_idle = gtk_idle_add (widget_change_idle, gre);
}

static void
color_picker_change_signal (GtkWidget *widget, guint r, guint g, guint b, guint a, gpointer data)
{
	PongRuntimeElement *gre = data;

	if (gre->updated_idle == 0)
		gre->updated_idle = gtk_idle_add (widget_change_idle, gre);
}

void
pong_runtime_element_setup_handlers (PongRuntimeElement *gre)
{
	GtkWidget *w;

	g_return_if_fail (gre != NULL);
	g_return_if_fail (gre->widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (gre->widget));
	g_return_if_fail (gre->widget_type != PONG_INVALIDWIDGET);

	switch (gre->widget_type) {
	case PONG_NATIVEWIDGET:
		{
			const char *signal;

			signal = pong_widget_interface_peek_changed_signal (gre->widget);

			g_assert (pong_widget_interface_exists (gre->widget));
			gtk_signal_connect_after
				(GTK_OBJECT (gre->widget), signal,
				 GTK_SIGNAL_FUNC (widget_change_signal),
				 gre);
		}
		break;
	case PONG_GTKSPINBUTTON:
		gtk_signal_connect_after (GTK_OBJECT (gre->widget),
					  "changed",
					  GTK_SIGNAL_FUNC (widget_change_signal),
					  gre);
		break;
	case PONG_GTKENTRY:
	case PONG_GTKTEXT:
	case PONG_GTKEDITABLE:
		gtk_signal_connect_after (GTK_OBJECT (gre->widget),
					  "changed",
					  GTK_SIGNAL_FUNC (widget_change_signal),
					  gre);
		break;
	case PONG_GTKCOMBO:
		gtk_signal_connect_after (GTK_OBJECT (GTK_COMBO (gre->widget)->entry),
					  "changed",
					  GTK_SIGNAL_FUNC (widget_change_signal),
					  gre);
		break;
	case PONG_GNOMEENTRY:
		w = gnome_entry_gtk_entry (GNOME_ENTRY (gre->widget));
		gtk_signal_connect_after (GTK_OBJECT (w),
					  "changed",
					  GTK_SIGNAL_FUNC (widget_change_signal),
					  gre);
		break;
	case PONG_GNOMEFILEENTRY:
		w = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (gre->widget));
		gtk_signal_connect_after (GTK_OBJECT (w),
					 "changed",
					 GTK_SIGNAL_FUNC (widget_change_signal),
					 gre);
		break;
	case PONG_GNOMEPIXMAPENTRY:
		w = gnome_pixmap_entry_gtk_entry (GNOME_PIXMAP_ENTRY (gre->widget));
		gtk_signal_connect_after (GTK_OBJECT (w),
					  "changed",
					  GTK_SIGNAL_FUNC (widget_change_signal),
					  gre);
		break;
	case PONG_GNOMEICONENTRY:
		w = gnome_icon_entry_gtk_entry (GNOME_ICON_ENTRY (gre->widget));
		gtk_signal_connect_after (GTK_OBJECT (w),
					  "changed",
					  GTK_SIGNAL_FUNC (widget_change_signal),
					  gre);
		break;
	case PONG_GTKTOGGLEBUTTON:
		gtk_signal_connect_after (GTK_OBJECT (gre->widget),
					  "toggled",
					  GTK_SIGNAL_FUNC (widget_change_signal),
					  gre);
		break;
	case PONG_GNOMECOLORPICKER:
		gtk_signal_connect_after (GTK_OBJECT (gre->widget),
					  "color_set",
					  GTK_SIGNAL_FUNC (color_picker_change_signal),
					  gre);
		break;
	default:
		g_warning (_("Invalid string widget type"));
		break;
	}
}

gboolean
pong_runtime_element_set_widget (PongRuntimeElement *gre, GtkWidget *widget)
{
	g_return_val_if_fail (gre != NULL, FALSE);
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_WIDGET (widget), FALSE);

	/* Just for sanity really */
	if (gre->widget != NULL)
		gtk_widget_destroy (gre->widget);
	gre->widget = NULL;

	gre->widget_type = PONG_INVALIDWIDGET;

	/* figure out the type of the widget */
	if (pong_widget_interface_exists (widget))
		gre->widget_type = PONG_NATIVEWIDGET;
	else if (GTK_IS_SPIN_BUTTON (widget))
		gre->widget_type = PONG_GTKSPINBUTTON;
	else if (GTK_IS_ENTRY (widget))
		gre->widget_type = PONG_GTKENTRY;
	else if (GTK_IS_TEXT (widget))
		gre->widget_type = PONG_GTKTEXT;
	else if (GTK_IS_TOGGLE_BUTTON (widget))
		gre->widget_type = PONG_GTKTOGGLEBUTTON;
	else if (GNOME_IS_FILE_ENTRY (widget))
		gre->widget_type = PONG_GNOMEFILEENTRY;
	else if (GNOME_IS_PIXMAP_ENTRY (widget))
		gre->widget_type = PONG_GNOMEPIXMAPENTRY;
	else if (GNOME_IS_ICON_ENTRY (widget))
		gre->widget_type = PONG_GNOMEICONENTRY;
	else if (GNOME_IS_ENTRY (widget))
		gre->widget_type = PONG_GNOMEENTRY;
	else if (GTK_IS_COMBO (widget)) /* must be after gnome_entry! */
		gre->widget_type = PONG_GTKCOMBO;
	else if (GNOME_COLOR_PICKER (widget))
		gre->widget_type = PONG_GNOMECOLORPICKER;
	else if (GTK_IS_RANGE (widget))
		gre->widget_type = PONG_GTKRANGE;
	else if (GTK_IS_EDITABLE (widget)) /* must be after entry/text */
		gre->widget_type = PONG_GTKEDITABLE;

	if (gre->widget_type == PONG_INVALIDWIDGET)
		return FALSE;

	gre->widget = widget;
	gtk_signal_connect (GTK_OBJECT (gre->widget), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &gre->widget);

	return TRUE;
}

void
pong_runtime_element_key_to_widget (PongRuntimeElement *gre,
				    GConfClient *client,
				    const char *prefix)
{
	char *fullkey;
	GConfValue *value;

	g_return_if_fail (gre != NULL);
	g_return_if_fail (client != NULL);
	g_return_if_fail (GCONF_IS_CLIENT (client));

	if (prefix != NULL &&
	    gre->el->conf_path[0] != '/')
		fullkey = g_concat_dir_and_file (prefix, gre->el->conf_path);
	else
		fullkey = g_strdup (gre->el->conf_path);

	/* FIXME: handle errors */
	value = gconf_client_get (client, fullkey, NULL);

	/* GConf didn't find a key and didn't find the default,
	 * let's see if we have a default to fall back on */
	if (value == NULL &&
	    gre->el->def != NULL) {
		pong_i18n_push_c_numeric_locale ();
		value = pong_gconf_value_from_string (gre->el->def, gre->el->type);
		pong_i18n_pop_c_numeric_locale ();

		/* Now, force this value unto gconf, thus making app/dialog
		 * consistent */
		/* FIXME: handle errors */
		gconf_client_set (client, fullkey, value, NULL);

		gconf_value_free (value);
	} else if (value != NULL) {
		/* FIXME: check type of key here!!!! and warn if wrong */
		/* This will not set a value if this value is already in the
		 * widget, thus preventing infinite loops */
		pong_runtime_element_set_value (gre, value);
		gconf_value_free (value);
	}

	g_free (fullkey);
}

void
pong_runtime_element_widget_to_key (PongRuntimeElement *gre,
				    GConfClient *client,
				    const char *prefix)
{
	char *fullkey;
	GConfValue *value, *old_value;
	GError *error = NULL;

	g_return_if_fail (gre != NULL);
	g_return_if_fail (client != NULL);
	g_return_if_fail (GCONF_IS_CLIENT (client));

	value = pong_runtime_element_get_value (gre);
	if (value == NULL) {
		g_warning (_("Error while getting value from widget"));
		return;
	}

	if (prefix != NULL &&
	    gre->el->conf_path[0] != '/')
		fullkey = g_concat_dir_and_file (prefix, gre->el->conf_path);
	else
		fullkey = g_strdup (gre->el->conf_path);

	/* Here we check what the value of this key is, and only
	 * reset it in gconf if it's different */
	/* FIXME: handle errors */
	old_value = gconf_client_get (client, fullkey, NULL);
	if (old_value != NULL &&
	    pong_gconf_value_equal (value, old_value)) {
		gconf_value_free (old_value);
		gconf_value_free (value);
		g_free (fullkey);
		return;
	}
	if (old_value != NULL)
		gconf_value_free (old_value);

	gconf_client_set (client, fullkey, value, &error);
	if (error != NULL) {
		/* FIXME: say something/do something */
		g_error_free (error);
		error = NULL;
	}

	gconf_value_free (value);
	g_free (fullkey);
}

static GConfValue *
pong_runtime_element_get_int (PongRuntimeElement *gre)
{
	GConfValue *value = NULL;
	long int_val = 0;

	g_return_val_if_fail (gre != NULL, NULL);

	switch (gre->widget_type) {
	case PONG_GTKRANGE:
		int_val = pong_gtk_range_get_long (GTK_RANGE (gre->widget));
		break;
	case PONG_GTKSPINBUTTON:
		int_val = gtk_spin_button_get_value_as_int
			(GTK_SPIN_BUTTON (gre->widget));
		break;
	case PONG_GTKENTRY:
	case PONG_GTKTEXT:
	case PONG_GTKEDITABLE:
		int_val = pong_gtk_editable_get_long
			(GTK_EDITABLE (gre->widget));
		break;
	case PONG_GTKCOMBO:
		int_val = pong_gtk_editable_get_long
			(GTK_EDITABLE (GTK_COMBO (gre->widget)->entry));
		break;
	case PONG_GNOMEENTRY:
		int_val = pong_gnome_entry_get_long
			(GNOME_ENTRY (gre->widget));
		break;
	case PONG_GNOMECOLORPICKER:
		int_val = pong_gnome_color_picker_get_long
			(GNOME_COLOR_PICKER (gre->widget));
		break;
	default:
		g_warning (_("Invalid integer widget type"));
		return NULL;
	}

	value = gconf_value_new (GCONF_VALUE_INT);
	gconf_value_set_int (value, int_val);
	return value;
}

static GConfValue *
pong_runtime_element_get_float (PongRuntimeElement *gre)
{
	GConfValue *value = NULL;
	double float_val = 0.0;

	g_return_val_if_fail (gre != NULL, NULL);

	switch (gre->widget_type) {
	case PONG_GTKRANGE:
		float_val = pong_gtk_range_get_double 
			(GTK_RANGE (gre->widget));
		break;
	case PONG_GTKSPINBUTTON:
		float_val = gtk_spin_button_get_value_as_float
			(GTK_SPIN_BUTTON (gre->widget));
		break;
	case PONG_GTKENTRY:
	case PONG_GTKTEXT:
	case PONG_GTKEDITABLE:
		float_val = pong_gtk_editable_get_double
			(GTK_EDITABLE (gre->widget));
		break;
	case PONG_GTKCOMBO:
		float_val = pong_gtk_editable_get_double
			(GTK_EDITABLE (GTK_COMBO (gre->widget)->entry));
		break;
	case PONG_GNOMEENTRY:
		float_val = pong_gnome_entry_get_double
			(GNOME_ENTRY (gre->widget));
		break;
	default:
		g_warning (_("Invalid double widget type"));
		return NULL;
	}

	value = gconf_value_new (GCONF_VALUE_FLOAT);
	gconf_value_set_float (value, float_val);
	return value;
}

static GConfValue *
pong_runtime_element_get_string (PongRuntimeElement *gre)
{
	GConfValue *value = NULL;
	char *dup_val = NULL;
	const char *peek_val = NULL;

	g_return_val_if_fail (gre != NULL, NULL);

	switch (gre->widget_type) {
	case PONG_GTKENTRY:
		peek_val = gtk_entry_get_text (GTK_ENTRY (gre->widget));
		break;
	case PONG_GTKTEXT:
	case PONG_GTKEDITABLE:
		dup_val = pong_gtk_editable_get_text
			(GTK_EDITABLE (gre->widget));
		break;
	case PONG_GTKCOMBO:
		peek_val = gtk_entry_get_text
			(GTK_ENTRY (GTK_COMBO (gre->widget)->entry));
		break;
	case PONG_GNOMEENTRY:
		peek_val = pong_gnome_entry_peek_text (GNOME_ENTRY (gre->widget));
		break;
	case PONG_GNOMEFILEENTRY:
		peek_val = pong_gnome_file_entry_peek_text
			(GNOME_FILE_ENTRY (gre->widget));
		break;
	case PONG_GNOMEPIXMAPENTRY:
		peek_val = pong_gnome_pixmap_entry_peek_text
			(GNOME_PIXMAP_ENTRY (gre->widget));
		break;
	case PONG_GNOMEICONENTRY:
		peek_val = pong_gnome_icon_entry_peek_text
			(GNOME_ICON_ENTRY (gre->widget));
		break;
	case PONG_GNOMECOLORPICKER:
		dup_val = pong_gnome_color_picker_get_text
			(GNOME_COLOR_PICKER (gre->widget));
		break;
	default:
		g_warning (_("Invalid string widget type"));
		return NULL;
	}

	value = gconf_value_new (GCONF_VALUE_STRING);
	if (peek_val != NULL)
		gconf_value_set_string (value, peek_val);
	else
		gconf_value_set_string (value, dup_val);
	g_free (dup_val);

	return value;
}

static GConfValue *
pong_runtime_element_get_bool (PongRuntimeElement *gre)
{
	GConfValue *value = NULL;
	gboolean bool_val = FALSE;

	g_return_val_if_fail (gre != NULL, NULL);

	switch (gre->widget_type) {
	case PONG_GTKTOGGLEBUTTON:
		bool_val = GTK_TOGGLE_BUTTON (gre->widget)->active != FALSE;
		break;
	default:
		g_warning (_("Invalid boolean widget type"));
		return NULL;
	}

	value = gconf_value_new (GCONF_VALUE_BOOL);
	gconf_value_set_bool (value, bool_val);
	return value;
}

static GConfValue *
pong_runtime_element_get_weird (PongRuntimeElement *gre)
{
	GConfValue *value;
	char *s;
	g_return_val_if_fail (gre != NULL, NULL);

	switch (gre->widget_type) {
	case PONG_GTKENTRY:
	case PONG_GTKEDITABLE:
		s = pong_gtk_editable_get_text (GTK_EDITABLE (gre->widget));
		value = pong_gconf_value_from_string (s, gre->el->type);
		g_free (s);
		break;
	case PONG_GTKTEXT:
		s = pong_gtk_editable_get_text (GTK_EDITABLE (gre->widget));
		value = pong_gconf_value_from_text (s, gre->el->type);
		g_free (s);
		break;
	case PONG_GTKCOMBO:
		value = pong_gconf_value_from_string
			(gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (gre->widget)->entry)),
			 gre->el->type);
		break;
	case PONG_GNOMEENTRY:
		value = pong_gconf_value_from_string
			(pong_gnome_entry_peek_text (GNOME_ENTRY (gre->widget)),
			 gre->el->type);
		break;
	default:
		g_warning (_("Invalid list/pair widget type"));
		value = NULL;
		break;
	}

	return value;
}

GConfValue *
pong_runtime_element_get_value (PongRuntimeElement *gre)
{
	GConfValue *value;

	g_return_val_if_fail (gre != NULL, NULL);

	if (gre->widget_type == PONG_NATIVEWIDGET) {
		value = NULL;
		if ( ! pong_widget_interface_get_value (gre->widget,
							gre->el->specifier,
							gre->el->type,
							&value))
			g_warning (_("Error getting string from widget"));
		return value;
	}

	/* Non native widgets, only supported types need to be here */
	switch (gre->el->type) {
	case PONG_TYPE_STRING:
		value = pong_runtime_element_get_string (gre);
		break;
	case PONG_TYPE_INT:
		value = pong_runtime_element_get_int (gre);
		break;
	case PONG_TYPE_FLOAT:
		value = pong_runtime_element_get_float (gre);
		break;
	case PONG_TYPE_BOOL:
		value = pong_runtime_element_get_bool (gre);
		break;
	default:
		value = pong_runtime_element_get_weird (gre);
		break;
	}

	return value;
}

static void
pong_runtime_element_set_int (PongRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail (gre != NULL);
	g_return_if_fail (value != NULL);
	g_return_if_fail (value->type == GCONF_VALUE_INT);

	switch (gre->widget_type) {
	case PONG_GTKRANGE:
		pong_gtk_range_set_long (GTK_RANGE (gre->widget),
					 gconf_value_get_int (value));
		break;
	case PONG_GTKSPINBUTTON:
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (gre->widget),
					   gconf_value_get_int (value));
		break;
	case PONG_GTKENTRY:
	case PONG_GTKTEXT:
	case PONG_GTKEDITABLE:
		pong_gtk_editable_set_long (GTK_EDITABLE (gre->widget),
					    gconf_value_get_int (value));
		break;
	case PONG_GTKCOMBO:
		pong_gtk_editable_set_long
			(GTK_EDITABLE (GTK_COMBO (gre->widget)->entry),
			 gconf_value_get_int (value));
		break;
	case PONG_GNOMEENTRY:
		pong_gnome_entry_set_long (GNOME_ENTRY (gre->widget),
					   gconf_value_get_int (value));
		break;
	case PONG_GNOMECOLORPICKER:
		pong_gnome_color_picker_set_long
			(GNOME_COLOR_PICKER (gre->widget),
			 gconf_value_get_int (value));
		break;
	default:
		g_warning (_("Invalid integer widget type"));
		break;
	}
}

static void
pong_runtime_element_set_float (PongRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail (gre != NULL);
	g_return_if_fail (value != NULL);
	g_return_if_fail (value->type == GCONF_VALUE_FLOAT);

	switch (gre->widget_type) {
	case PONG_GTKRANGE:
		pong_gtk_range_set_double (GTK_RANGE (gre->widget),
					   gconf_value_get_float (value));
		break;
	case PONG_GTKSPINBUTTON:
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (gre->widget),
					   gconf_value_get_float (value));
		break;
	case PONG_GTKENTRY:
	case PONG_GTKTEXT:
	case PONG_GTKEDITABLE:
		pong_gtk_editable_set_double (GTK_EDITABLE (gre->widget),
					      gconf_value_get_float (value));
		break;
	case PONG_GTKCOMBO:
		pong_gtk_editable_set_double
			(GTK_EDITABLE (GTK_COMBO (gre->widget)->entry),
			 gconf_value_get_float (value));
		break;
	case PONG_GNOMEENTRY:
		pong_gnome_entry_set_double (GNOME_ENTRY (gre->widget),
					     gconf_value_get_float (value));
		break;
	default:
		g_warning (_("Invalid double widget type"));
		break;
	}
}

static void
pong_runtime_element_set_string (PongRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail (gre != NULL);
	g_return_if_fail (value != NULL);
	g_return_if_fail (value->type == GCONF_VALUE_STRING);

	switch (gre->widget_type) {
	case PONG_GTKENTRY:
	case PONG_GTKTEXT:
	case PONG_GTKEDITABLE:
		pong_gtk_editable_set_text (GTK_EDITABLE (gre->widget),
					    gconf_value_get_string (value));
		break;
	case PONG_GTKCOMBO:
		gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (gre->widget)->entry),
				    gconf_value_get_string (value));
		break;
	case PONG_GNOMEENTRY:
		pong_gnome_entry_set_text (GNOME_ENTRY (gre->widget),
					   gconf_value_get_string (value));
		break;
	case PONG_GNOMEFILEENTRY:
		pong_gnome_file_entry_set_text (GNOME_FILE_ENTRY (gre->widget),
						gconf_value_get_string (value));
		break;
	case PONG_GNOMEPIXMAPENTRY:
		pong_gnome_pixmap_entry_set_text (GNOME_PIXMAP_ENTRY (gre->widget),
						  gconf_value_get_string (value));
		break;
	case PONG_GNOMEICONENTRY:
		gnome_icon_entry_set_icon (GNOME_ICON_ENTRY (gre->widget),
					   gconf_value_get_string (value));
		break;
	case PONG_GNOMECOLORPICKER:
		pong_gnome_color_picker_set_text
			(GNOME_COLOR_PICKER (gre->widget),
			 gconf_value_get_string (value));
		break;
	default:
		g_warning (_("Invalid string widget type"));
		break;
	}
}

static void
pong_runtime_element_set_bool (PongRuntimeElement *gre, GConfValue *value)
{
	g_return_if_fail (gre != NULL);
	g_return_if_fail (value != NULL);
	g_return_if_fail (value->type == GCONF_VALUE_BOOL);

	switch (gre->widget_type) {
	case PONG_GTKTOGGLEBUTTON:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gre->widget),
					      gconf_value_get_bool (value));
		break;
	default:
		g_warning (_("Invalid boolean widget type"));
		break;
	}
}

static void
pong_runtime_element_set_weird (PongRuntimeElement *gre, GConfValue *value)
{
	char *s;

	g_return_if_fail (gre != NULL);
	g_return_if_fail (value != NULL);

	switch (gre->widget_type) {
	case PONG_GTKEDITABLE:
	case PONG_GTKENTRY:
		s = pong_string_from_gconf_value (value);
		pong_gtk_editable_set_text (GTK_EDITABLE (gre->widget), s);
		g_free (s);
		break;
	case PONG_GTKTEXT:
		s = pong_text_from_gconf_value (value);
		pong_gtk_editable_set_text (GTK_EDITABLE (gre->widget), s);
		g_free (s);
		break;
	case PONG_GTKCOMBO:
		s = pong_string_from_gconf_value (value);
		gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (gre->widget)->entry),
				    s);
		g_free (s);
		break;
	case PONG_GNOMEENTRY:
		s = pong_string_from_gconf_value (value);
		pong_gnome_entry_set_text (GNOME_ENTRY (gre->widget), s);
		g_free (s);
		break;
	default:
		g_warning (_("Invalid list/pair widget type"));
		break;
	}
}

void
pong_runtime_element_set_value (PongRuntimeElement *gre, GConfValue *value)
{
	GConfValue *old_value;

	g_return_if_fail (gre != NULL);
	g_return_if_fail (value != NULL);

	old_value = pong_runtime_element_get_value (gre);
	if (old_value != NULL &&
	    pong_gconf_value_equal (value, old_value)) {
		gconf_value_free (old_value);
		return;
	}
	if (old_value != NULL)
		gconf_value_free (old_value);

	if (gre->widget_type == PONG_NATIVEWIDGET) {
		if ( ! pong_widget_interface_set_value (gre->widget,
							gre->el->specifier,
							value))
			g_warning (_("Error setting widget value"));
		return;
	}

	/* Non native widgets, only supported types need to be here */
	switch (gre->el->type) {
	case PONG_TYPE_STRING:
		pong_runtime_element_set_string (gre, value);
		break;
	case PONG_TYPE_INT:
		pong_runtime_element_set_int (gre, value);
		break;
	case PONG_TYPE_FLOAT:
		pong_runtime_element_set_float (gre, value);
		break;
	case PONG_TYPE_BOOL:
		pong_runtime_element_set_bool (gre, value);
		break;
	default:
		pong_runtime_element_set_weird (gre, value);
		break;
	}
}

PongSensitivity	*
pong_sensitivity_new (PongElement *ge)
{
	PongSensitivity *sens = g_new0 (PongSensitivity, 1);

	sens->type = ge->type;
	sens->values = NULL;
	sens->comparison = PONG_COMP_EQ;
	sens->connection = PONG_CONNECTION_OR;
	sens->sensitive = NULL;
	sens->insensitive = NULL;

	return sens;
}

void
pong_sensitivity_destroy (PongSensitivity *sens)
{
	g_list_foreach (sens->values, (GFunc) gconf_value_free, NULL);
	g_list_free (sens->values);
	sens->values = NULL;

	g_slist_foreach (sens->sensitive, (GFunc) g_free, NULL);
	g_slist_free (sens->sensitive);
	sens->sensitive = NULL;

	g_slist_foreach (sens->insensitive, (GFunc) g_free, NULL);
	g_slist_free (sens->insensitive);
	sens->insensitive = NULL;

	g_free (sens);
}

void
pong_sensitivity_set_connection (PongSensitivity *sens,
				 PongConnection connection)
{
	g_return_if_fail (sens != NULL);
	g_return_if_fail (connection > PONG_CONNECTION_INVALID &&
			  connection < PONG_CONNECTION_LAST);

	sens->connection = connection;
}

void
pong_sensitivity_set_comparison (PongSensitivity *sens,
				PongComparison comparison)
{
	g_return_if_fail (sens != NULL);
	g_return_if_fail (comparison > PONG_COMP_INVALID &&
			  comparison < PONG_COMP_LAST);

	sens->comparison = comparison;
}

void
pong_sensitivity_add_sensitive (PongSensitivity *sens, const char *widget)
{
	g_return_if_fail (sens != NULL);
	g_return_if_fail (widget != NULL);

	sens->sensitive = g_slist_append (sens->sensitive, g_strdup (widget));
}

void
pong_sensitivity_add_insensitive (PongSensitivity *sens, const char *widget)
{
	g_return_if_fail (sens != NULL);
	g_return_if_fail (widget != NULL);

	sens->insensitive = g_slist_append (sens->insensitive, g_strdup (widget));
}

void
pong_sensitivity_add_value (PongSensitivity *sens, GConfValue *value)
{
	g_return_if_fail (sens != NULL);
	g_return_if_fail (value != NULL);

	g_return_if_fail (sens->type == pong_type_from_gconf_value (value));

	sens->values = g_list_prepend (sens->values, gconf_value_copy (value));
}

void
pong_sensitivity_add_value_from_string (PongSensitivity *sens,
					const char *str)
{
	GConfValue *value;

	g_return_if_fail (sens != NULL);
	g_return_if_fail (str != NULL);

	pong_i18n_push_c_numeric_locale ();
	value = pong_gconf_value_from_string (str, sens->type);
	pong_i18n_pop_c_numeric_locale ();
	if (value == NULL) {
		g_warning (_("Unsupported sensitivity value type"));
		return;
	}

	sens->values = g_list_prepend (sens->values, value);
}

void
pong_sensitivity_remove_value (PongSensitivity *sens, GConfValue *value)
{
	GList *li;

	g_return_if_fail (sens != NULL);
	g_return_if_fail (value != NULL);

	g_return_if_fail (sens->type == pong_type_from_gconf_value (value));

	for (li = sens->values; li != NULL; li = li->next) {
		GConfValue *valiter = li->data;
		if (pong_gconf_value_equal (value, valiter)) {
			sens->values = g_list_remove_link (sens->values, li);
			li->data = NULL;
			g_list_free_1 (li);
			gconf_value_free (valiter);
			return;
		}
	}
}
