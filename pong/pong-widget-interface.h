/* PonG: Standard widget interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONG_WIDGET_INTERFACE_H
#define PONG_WIDGET_INTERFACE_H

#include <gtk/gtk.h>
#include <gconf/gconf-value.h>
#include "pong-type.h" /* PongType */

#ifdef __cplusplus
extern "C" {
#endif

/* This is used in add_options */
/* This is protected as it's also in gnoppane.h */
#ifndef TYPEDEF_PONG_OPTION
#define TYPEDEF_PONG_OPTION
typedef struct _PongOption PongOption;
struct _PongOption {
	char *label;
	char *value;
};
#endif /* TYPEDEF_PONG_OPTION */

/*
 * Pong Widgets should have this structure set as the data of name
 * "pong_widget_interface"
 */

typedef struct _PongWidgetInterface PongWidgetInterface;
struct _PongWidgetInterface {
	/* this is the name of the signal that should be bound to get changes
	 * on the widget, this signal should not have any extra function
	 * arguments (handler should look like
	 * "void foo(GtkWidget *widget, gpointer data)" ) */
	char *		changed_signal;

	gboolean	(*get_value)	(GtkWidget *w,
					 const char *specifier,
					 PongType type,
					 GConfValue **value);
	gboolean	(*set_value)	(GtkWidget *w,
					 const char *specifier,
					 GConfValue *value);

	void		(*add_options)	(GtkWidget *w,
					 GList *pong_options);

	gboolean	(*set_label)	(GtkWidget *w,
					 const char *label);

	/* If this is unimplemented, pong will set GtkArgs */
	void		(*set_arg)	(GtkWidget *w,
					 const char *name,
					 const char *value);
};

/* This will create or get the current interface of a class, so derived
 * classes can easily override implementations of methods */
PongWidgetInterface *
		pong_widget_interface_add	(GtkObjectClass *klass);

/* check for the existance of the interface on a widget */
gboolean	pong_widget_interface_exists	(GtkWidget *w);

const char *	pong_widget_interface_peek_changed_signal(GtkWidget *w);

gboolean	pong_widget_interface_get_value	(GtkWidget *w,
						 const char *specifier,
						 GConfValueType type,
						 GConfValue **value);
gboolean	pong_widget_interface_set_value	(GtkWidget *w,
						 const char *specifier,
						 GConfValue *value);

gboolean	pong_widget_interface_implements_add_options(GtkWidget *w);
void		pong_widget_interface_add_options(GtkWidget *w,
						  GList *pong_options);

gboolean	pong_widget_interface_implements_set_label(GtkWidget *w);
gboolean	pong_widget_interface_set_label	(GtkWidget *w,
						 const char *label);

gboolean	pong_widget_interface_implements_set_arg(GtkWidget *w);
void		pong_widget_interface_set_arg	(GtkWidget *w,
						 const char *name,
						 const char *value);

#ifdef __cplusplus
}
#endif

#endif /* PONG_WIDGET_INTERFACE_H */
