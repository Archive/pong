/* PonG: Utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001,2002 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONGUTIL_H
#define PONGUTIL_H

#include <glib.h>
#include <glib-object.h>
#include <unistd.h>
#include <gconf/gconf-value.h>
#include "pong-type.h"

#ifdef __cplusplus
extern "C" {
#endif

gboolean	pong_bool_from_string			(const char *s);
gboolean	pong_gconf_value_equal			(GConfValue *v1,
							 GConfValue *v2);
/* only supports primitives */
gboolean	pong_string_value_equal			(const char *string,
							 GConfValue *value);


GConfValueType	pong_gconf_value_type_from_pong_type	(PongType type) G_GNUC_CONST;
GConfValueType	pong_list_gconf_value_type_from_pong_type(PongType type) G_GNUC_CONST;
GConfValueType	pong_car_gconf_value_type_from_pong_type(PongType type) G_GNUC_CONST;
GConfValueType	pong_cdr_gconf_value_type_from_pong_type(PongType type) G_GNUC_CONST;
PongType	pong_type_from_gconf_value		(GConfValue *value);
PongType	pong_type_from_gconf_types		(GConfValueType type,
							 GConfValueType list_type,
							 GConfValueType car_type,
							 GConfValueType cdr_type) G_GNUC_CONST;


/* Unlike the gconf equivalents, these will try to do "something".  They use
 * the gnome_config style vector stuff using spaces. */
GConfValue *	pong_gconf_value_from_string		(const char *s,
							 PongType type);
char *		pong_string_from_gconf_value		(GConfValue *value);

/* These are for text widgets, they work by separating on newline */
GConfValue *	pong_gconf_value_from_text		(const char *s,
							 PongType type);
char *		pong_text_from_gconf_value		(GConfValue *value);


/* Do strcasecmp but ignore locale, usefull for parsing where locale would
 * just do damage */
int		pong_strcasecmp_no_locale		(const char *s1,
							 const char *s2);
/* Compare tags ignoring a leading underscore */
gboolean	pong_tags_equal				(const char *tag1,
							 const char *tag2);

/* Find a file using the specified list of directories, an absolute path is
 * just checked, whereas a relative path is search in the given directories,
 * gnome_datadir, g_get_prgname() subdirectory, in PONG_PATH, in GNOME_PATH
 * under /share/ and /share/<g_get_prgname()> */
char *		pong_find_file				(const char *filename,
							 const GList *directories);

void		pong_i18n_push_c_numeric_locale		(void);
void		pong_i18n_pop_c_numeric_locale		(void);

#define		pong_gtk_object_set_int_data(object,name,data) \
			(gtk_object_set_data (GTK_OBJECT (object), name, \
					      GINT_TO_POINTER (data)))
#define		pong_gtk_object_get_int_data(object,name) \
			(GPOINTER_TO_INT (gtk_object_get_data \
					  (GTK_OBJECT (object), name)))

#define		pong_string_empty(string) \
			((string) == NULL || (string)[0] == '\0')

#define		pong_sure_string(string) \
			((string) == NULL ? "" : (string))

/* like g_file_exists, but lots faster, at least for gnome 1.x */
#define		pong_file_exists(file)	(access (file, F_OK) == 0)
			
void		_pong_signal_connect_while_alive (GObject     *object,
						  const gchar *signal,
						  GCallback    func,
						  gpointer     func_data,
						  GObject     *alive_object);
void		_pong_signal_connect_object_while_alive (GObject      *object,
							 const gchar  *signal,
							 GCallback     func,
							 GObject      *alive_object);

#define		_pong_signal_disconnect_by_data(object,data)	\
				(g_signal_handlers_disconnect_matched	\
					((object), G_SIGNAL_MATCH_DATA, \
					 0, 0, NULL, NULL, (data)))

#ifdef __cplusplus
}
#endif

#endif /* PONGUTIL_H */
