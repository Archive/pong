/* PonG: the parser using SAX interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONGPARSER_PRIVATE_H
#define PONGPARSER_PRIVATE_H

#include "pongelement.h"
#include "pongparser.h"

#ifdef __cplusplus
extern "C" {
#endif

struct _PongFile {
	int refcount;
	gboolean cached;
	char *file_name;
	char *base_conf_path;

	char *dialog_title;
	char *help_name;
	char *help_path;

	gboolean auto_apply;
	gboolean revert;

	GHashTable *element_hash;
	GList *panes;
	GList *levels;
};

typedef struct _PongLevel PongLevel;
struct _PongLevel {
	char *name;
	char *base_conf_path;
};

typedef void (*PongElementFunc) (PongElement *element, gpointer data);

/* list of internal strings, DON'T FREE THEM, just free the list */
GList *		pong_file_get_conf_paths	(const PongFile *gf);

GList *		pong_file_get_elements		(const PongFile *gf);
PongElement *	pong_file_lookup_element	(const PongFile *gf,
						 const char *conf_path);

void		pong_file_foreach_element	(PongFile *gf,
						 PongElementFunc func,
						 gpointer data);

/* list of internal strings, DON'T FREE THEM, just free the list */
GList *		pong_file_get_levels		(PongFile *gf);
const char *	pong_file_get_level_base_conf_path(PongFile *gf,
						   const char *level_name);

/* Really for internal use only */
PongLevel *	pong_level_new			(void);
void		pong_level_destroy		(PongLevel *level);

/* kill all unused PongFile structures */
void		pong_parser_free_unused_data	(void);

#ifdef __cplusplus
}
#endif


#endif /* PONGPARSER_PRIVATE_H */
