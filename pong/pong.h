/* PonG: the library interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONG_H
#define PONG_H

#include "pong-xml.h"

/* The standard (non-glade) ui objects */
/* For glade stuff look in pong-glade.h */
#include "pong-ui.h"
#include "pong-ui-manual.h"
#include "pong-ui-plug.h"
#include "pong-ui-internal.h"

#ifdef __cplusplus
extern "C" {
#endif

/* initialize, you need to call this to use pong */
gboolean	pong_init			(void);
gboolean	pong_is_initialized		(void);

/* To be called at any time, no need in user code,
 * this init is called by pong itself during first function
 * that needs it */
void		pong_lazy_init			(void);

/* free any non essential data */
void		pong_free_unused_data		(void);

/* add a directory to the path in which to look for pong files */
void		pong_add_directory		(const char *directory);

/* Pure sugar functions, this way adding a config dialog is a 1-liner */
GtkWidget *	pong_run_simple_dialog		(const char *file);
GtkWidget *	pong_run_simple_dialog_from_memory(const char *buffer,
						   int len);

/* This is mostly for internal use, or for further extentions, that have their
 * own plugs.  This will be used in pong_run_simple_dialog* functions, and
 * also in pong_xml_new and pong_xml_new_from_memory */
void		pong_add_plug_creator		(PongUIPlugCreator creator);

#ifdef __cplusplus
}
#endif

#endif /* PONG_H */
