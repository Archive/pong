/* PonG: The types enum
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONG_TYPE_H
#define PONG_TYPE_H

/* These correspond to GConf Types */
typedef enum {
	PONG_TYPE_INVALID,

	/* Basic types */
	PONG_TYPE_STRING,
	PONG_TYPE_INT,
	PONG_TYPE_FLOAT,
	PONG_TYPE_BOOL,

	/* List types */
	PONG_TYPE_LIST_OF_STRINGS,
	PONG_TYPE_LIST_OF_INTS,
	PONG_TYPE_LIST_OF_FLOATS,
	PONG_TYPE_LIST_OF_BOOLS,

	/* Pair types */
	PONG_TYPE_PAIR_STRING_STRING,
	PONG_TYPE_PAIR_STRING_INT,
	PONG_TYPE_PAIR_STRING_FLOAT,
	PONG_TYPE_PAIR_STRING_BOOL,

	PONG_TYPE_PAIR_INT_STRING,
	PONG_TYPE_PAIR_INT_INT,
	PONG_TYPE_PAIR_INT_FLOAT,
	PONG_TYPE_PAIR_INT_BOOL,

	PONG_TYPE_PAIR_FLOAT_STRING,
	PONG_TYPE_PAIR_FLOAT_INT,
	PONG_TYPE_PAIR_FLOAT_FLOAT,
	PONG_TYPE_PAIR_FLOAT_BOOL,

	PONG_TYPE_PAIR_BOOL_STRING,
	PONG_TYPE_PAIR_BOOL_INT,
	PONG_TYPE_PAIR_BOOL_FLOAT,
	PONG_TYPE_PAIR_BOOL_BOOL,

	PONG_TYPE_LAST
} PongType;

#endif /* PONG_TYPE_H */
