/* PonG: widget utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONGWIDGETUTIL_H
#define PONGWIDGETUTIL_H

#ifdef __cplusplus
extern "C" {
#endif

void		pong_gtk_editable_set_double	(GtkEditable *entry,
						 double number);
void		pong_gtk_editable_set_long	(GtkEditable *entry,
						 long number);
void		pong_gtk_editable_set_text	(GtkEditable *entry,
						 const char *text);

void		pong_gnome_entry_set_double	(GnomeEntry *gentry,
						 double number);
void		pong_gnome_entry_set_long	(GnomeEntry *gentry,
						 long number);
void		pong_gnome_entry_set_text	(GnomeEntry *gentry,
						 const char *text);

void		pong_gtk_range_set_double	(GtkRange *range,
						 double number);
void		pong_gtk_range_set_long		(GtkRange *range,
						 long number);

void		pong_gnome_file_entry_set_text	(GnomeFileEntry *gentry,
						 const char *text);

void		pong_gnome_pixmap_entry_set_text(GnomePixmapEntry *gentry,
						 const char *text);
void		pong_gnome_color_picker_set_text(GnomeColorPicker *picker,
						 const char *text);
/* The long here is a 24bit rgb value */
void		pong_gnome_color_picker_set_long(GnomeColorPicker *picker,
						 long number);

double		pong_gtk_range_get_double	(GtkRange *range);
long		pong_gtk_range_get_long		(GtkRange *range);

double		pong_gtk_editable_get_double	(GtkEditable *entry);
long		pong_gtk_editable_get_long	(GtkEditable *entry);
char *		pong_gtk_editable_get_text	(GtkEditable *entry);

double		pong_gnome_entry_get_double	(GnomeEntry *gentry);
long		pong_gnome_entry_get_long	(GnomeEntry *gentry);
const char *	pong_gnome_entry_peek_text	(GnomeEntry *gentry);

const char *	pong_gnome_file_entry_peek_text	(GnomeFileEntry *gentry);

const char *	pong_gnome_pixmap_entry_peek_text(GnomePixmapEntry *gentry);

const char *	pong_gnome_icon_entry_peek_text	(GnomeIconEntry *gentry);

char *		pong_gnome_color_picker_get_text(GnomeColorPicker *picker);
/* The long here is a 24bit rgb value */
long		pong_gnome_color_picker_get_long(GnomeColorPicker *picker);


/* if the argument is of a floating point type, the value should be
 * in a syntax of the "C" locale.  Such as using 1.2 and not 1,2 in non-C
 * locale. */
gboolean	pong_set_object_argument	(GtkObject *object,
						 const char *arg_name,
						 const char *value);

/* connect this to spin buttons to make them behave sanely */
void		pong_spin_button_mapped		(GtkWidget *w);

#ifdef __cplusplus
}
#endif

#endif /* PONGWIDGETUTIL_H */
