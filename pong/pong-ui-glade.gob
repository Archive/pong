%at{
/* PonG: the GLADE UI object
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2002 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.99.2

%{
#include "config.h"
#include <gnome.h>
#include <glade/glade-xml.h>

#include "pong-i18n.h"

#include "pongparser.h"
#include "pongutil.h"

#include "pong-ui.h"

#include "pong-ui-glade.h"
#include "pong-ui-glade-private.h"
%}

class Pong:UI:Glade from Pong:UI {

	/* the glade file to use */
	private char *glade_file = NULL
		destroywith g_free;
	property STRING glade_file
		(nick = _("Glade file"),
		 blurb = _("Glade file"),
		 link);

	/* name of the propertybox, used when loading a new dialog box
	 * from glade, can just be NULL for hand created dialogs */
	private char *dialog_name = NULL
		destroywith g_free;
	property STRING dialog_name
		(nick = _("Dialog name"),
		 blurb = _("Dialog name"),
		 link);

	/* the gettext domain to use */
	private char *gettext_domain = NULL
		destroywith g_free;
	property STRING gettext_domain
		(nick = _("Gettext domain"),
		 blurb = _("Gettext domain"),
		 link);

	/* truly private */
	private GladeXML *gladexml = NULL
		destroywith g_object_unref;

	override (Pong:UI)
	GtkWidget *
	get_widget(Pong:UI *gi (check null type),
		   const char *name (check null))
	{
		PongUIGlade *self = SELF(gi);

		if (self->_priv->gladexml != NULL)
			return glade_xml_get_widget (self->_priv->gladexml,
						     name);
		return NULL;
	}

	override (Pong:UI)
	void
	load_dialog (Pong:UI *gi (check null type), PongFile *gf (check null),
		     const char *level)
	{
		GtkWidget *dialog;
		PongUIGlade *self = SELF(gi);

		if (self->_priv->gladexml != NULL)
			g_object_unref (G_OBJECT (self->_priv->gladexml));
		self->_priv->gladexml =
			glade_xml_new_with_domain (self->_priv->glade_file,
						   self->_priv->dialog_name,
						   self->_priv->gettext_domain);

		if (self->_priv->gladexml == NULL) {
			g_warning (_("Cannot load glade file: '%s'"),
				   self->_priv->glade_file);
			return;
		}

		_pong_signal_connect_while_alive
			(G_OBJECT(self->_priv->gladexml),
			 "destroy",
			 G_CALLBACK (gtk_widget_destroyed),
			 &self->_priv->gladexml,
			 G_OBJECT (self));

		dialog = glade_xml_get_widget (self->_priv->gladexml,
					       self->_priv->dialog_name);

		pong_ui_set_dialog_widget (gi, dialog);

		_pong_signal_connect_object_while_alive
			(G_OBJECT (gi->dialog_widget),
			 "destroy",
			 G_CALLBACK (g_object_unref),
			 G_OBJECT (self->_priv->gladexml));
	}

	public
	PongUI *
	new(void)
	{
		return (PongUI *) GET_NEW;
	}

	public
	PongUI *
	new_from_file(const char *file (check null),
		      const char *dialog_name (check null),
		      const char *gettext_domain (check null)) onerror NULL
	{
		PongUIGlade *gig = GET_NEW;

		gig->_priv->glade_file = g_strdup (file);
		gig->_priv->dialog_name = g_strdup (dialog_name);
		gig->_priv->gettext_domain = g_strdup (gettext_domain);
		
		return PONG_UI(gig);
	}
}
