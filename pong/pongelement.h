/* PonG: the elements interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONGELEMENT_H
#define PONGELEMENT_H

#include <glib.h>
#include <gtk/gtk.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "pong-type.h"

typedef struct _PongElement PongElement; 
typedef struct _PongSensitivity PongSensitivity; 

#ifdef __cplusplus
extern "C" {
#endif

/* Widge types */
typedef enum {
	/* the invalid widget */
	PONG_INVALIDWIDGET,

	/* nothing set */
	PONG_NOTHING,

	/* meta type for native widget,
	 * widgets using the pong_widget_interface,
	 * used instead of specific types */
	PONG_NATIVEWIDGET,

	/* meta type for widgets that are outside any scope,
	 * that is widgets not connected to anything */
	PONG_EXTRAWIDGET,

	/* Some extra widgets that have some special
	 * support code, same as EXTRAWIDGET in that it
	 * is not to be connected to anything */
	PONG_GTKLABEL,

	/* handled types only, we can't natively create these, but
	 * we know these to support generic widgets like it */
	PONG_GTKRANGE,
	PONG_GTKEDITABLE,

	/* number widgets */
	PONG_GTKSPINBUTTON,

	/* string widgets */
	PONG_GTKENTRY, /* can also be used for numbers */
	PONG_GTKTEXT,
	PONG_GTKCOMBO,
	PONG_GNOMEENTRY,
	PONG_GNOMEFILEENTRY,
	PONG_GNOMEPIXMAPENTRY,
	PONG_GNOMEICONENTRY,

	/* boolean */
	PONG_GTKTOGGLEBUTTON,
	PONG_GTKCHECKBUTTON, /* the above is used for getting information */

	/* misc/multiple widgets */
	PONG_GNOMECOLORPICKER,

	PONG_LASTWIDGET
} PongWidgetType;

/* type of comparison for sensitivity */
typedef enum {
	PONG_COMP_INVALID = 0,
	PONG_COMP_LT,
	PONG_COMP_LE,
	PONG_COMP_EQ,
	PONG_COMP_NE,
	PONG_COMP_GE,
	PONG_COMP_GT,
	PONG_COMP_LAST
} PongComparison;

typedef enum {
	PONG_CONNECTION_INVALID = 0,
	PONG_CONNECTION_OR,
	PONG_CONNECTION_AND,
	PONG_CONNECTION_LAST
} PongConnection;


struct _PongElement {
	PongType type;

	int refcount;

	/* configuration path to store setting at, this is also
	   used to index the pong elements, and does not have to be
	   a complete path as the pong-xml object can store the base
	   path */
	char *conf_path;

	/* user data */
	gpointer data;
	GDestroyNotify destroy_notify;

	/* the widget associated */
	char *widget;

	/* The default default.  When GConf finds no default
	 * use this */
	char *def;

	/* the specifier to use when contacting widgets,
	 * used in cases where a widget can handle more then
	 * one key in different ways */
	char *specifier;

	/* sensitivity elements */
	GSList *sensitivity;
};

/* for a single widget, when the value "value" is given the following
 * things should be set sensitive or insensitive */
struct _PongSensitivity {
	PongType type;
	GList *values;
	PongConnection connection;
	PongComparison comparison;
	GSList *sensitive;
	GSList *insensitive;
};


/* stores actual runtime information about a PongElement */
typedef struct _PongRuntimeElement PongRuntimeElement;

typedef void (*PongRuntimeUpdateFunc)(PongRuntimeElement *, gpointer);

struct _PongRuntimeElement {
	PongElement *el;
	GtkWidget *widget;
	PongWidgetType widget_type;

	guint gconf_notify;
	guint updated_idle;

	PongRuntimeUpdateFunc update_func;
	gpointer update_data;
};

PongType	pong_type_from_string		(const char *type_string);
const char *	pong_string_from_type		(PongType type);

/*
 * Element
 */
PongElement *	pong_element_new		(void);

void		pong_element_set_type		(PongElement *el,
						 PongType type);
void		pong_element_set_type_string	(PongElement *el,
						 const char *type);
void		pong_element_set_data		(PongElement *el,
						 gpointer data,
						 GDestroyNotify destroy_notify);
void		pong_element_set_widget		(PongElement *el,
						 const char *widget);
void		pong_element_set_def		(PongElement *el,
						 const char *def);
void		pong_element_set_specifier	(PongElement *el,
						 const char *specifier);
void		pong_element_set_conf_path	(PongElement *el,
						 const char *conf_path);

void		pong_element_ref		(PongElement *ge);
void		pong_element_unref		(PongElement *ge);

/* Note: takes ownership of 'sens' */
void		pong_element_add_sensitivity	(PongElement *ge,
						 PongSensitivity *sens);

/*
 * Sensitiviy
 */
PongSensitivity	*pong_sensitivity_new		(PongElement *ge);
void		pong_sensitivity_destroy	(PongSensitivity *sens);

void		pong_sensitivity_add_value	(PongSensitivity *sens,
						 GConfValue *value);
void		pong_sensitivity_add_value_from_string(PongSensitivity *sens,
						       const char *str);
void		pong_sensitivity_remove_value	(PongSensitivity *sens,
						 GConfValue *value);
void		pong_sensitivity_set_connection	(PongSensitivity *sens,
						 PongConnection connection);
void		pong_sensitivity_set_comparison	(PongSensitivity *sens,
						 PongComparison comparison);
void		pong_sensitivity_add_sensitive	(PongSensitivity *sens,
						 const char *widget);
void		pong_sensitivity_add_insensitive(PongSensitivity *sens,
						 const char *widget);

/*
 * Runtime Element
 */
PongRuntimeElement *
		pong_runtime_element_new	(PongElement *el,
						 PongRuntimeUpdateFunc func,
						 gpointer data);
void		pong_runtime_element_destroy	(PongRuntimeElement *gre,
						 GConfClient *client);

void		pong_runtime_element_add_notify	(PongRuntimeElement *gre,
						 GConfClient *client,
						 const char *prefix,
						 GConfClientNotifyFunc func,
						 gpointer user_data,
						 GFreeFunc destroy_notify,
						 GError **err);
void		pong_runtime_element_remove_notify(PongRuntimeElement *gre,
						   GConfClient *client);

/* set widget and autodetect its type, returns FALSE if the
 * widget is not a valid widget */
gboolean	pong_runtime_element_set_widget	(PongRuntimeElement *el,
						 GtkWidget *widget);
/* setup the change handlers for the widget, not done in _set_widget since
 * we want to do _key_to_widget before setting the handlers */
void		pong_runtime_element_setup_handlers(PongRuntimeElement *gre);

/* Gets the value in the correct type according to the type of the PongElement */
GConfValue *	pong_runtime_element_get_value	(PongRuntimeElement *gre);
void		pong_runtime_element_set_value	(PongRuntimeElement *gre, GConfValue *value);

/* sync widget to the key or the key to the widget */
void		pong_runtime_element_widget_to_key(PongRuntimeElement *gre,
						   GConfClient *client,
						   const char *prefix);
void		pong_runtime_element_key_to_widget(PongRuntimeElement *gre,
						   GConfClient *client,
						   const char *prefix);

/*
 * General functions
 */


/* remove any data that is not really needed to keep */
void		pong_element_free_unused_data	(void);

#ifdef __cplusplus
}
#endif

#endif /* PONGELEMENT_H */
