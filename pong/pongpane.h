/* PonG: the pane interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONGPANE_H
#define PONGPANE_H

#include <glib.h>
#include <gtk/gtk.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "pongparser.h"
#include "pong-type.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ui internal type */
#ifndef __TYPEDEF_PONG_UI_INTERNAL__
#define __TYPEDEF_PONG_UI_INTERNAL__
typedef struct _PongUIInternal PongUIInternal;
#endif /* __TYPEDEF_PONG_UI_INTERNAL__ */

/* config stuff */
typedef struct _PongPaneBase PongPaneBase; 
typedef struct _PongPane PongPane; 
typedef struct _PongGroup PongGroup; 
typedef struct _PongWidget PongWidget; 
typedef struct _PongPlugWidget PongPlugWidget; 

enum {
	PONG_PANE_INVALID = 0,
	PONG_PANE,
	PONG_GROUP,
	PONG_WIDGET,
	PONG_PLUG_WIDGET,
	PONG_PANE_LAST
};

typedef struct _PongArgument PongArgument; 

/* runtime stuff */
typedef struct _PongDialog PongDialog; 
typedef struct _PongWidgetRuntime PongWidgetRuntime;

/* This is protected as it's also in gnop-widget-interface.h */
#ifndef TYPEDEF_PONG_OPTION
#define TYPEDEF_PONG_OPTION
typedef struct _PongOption PongOption;
struct _PongOption {
	char *label;
	char *value;
};
#endif /* TYPEDEF_PONG_OPTION */

struct _PongPaneBase {
	int type;
	int refcount;

	char *name;
	char *label;
	char **levels;

	GDestroyNotify destructor;
};

struct _PongPane {
	PongPaneBase base;

	GList *groups;
};

struct _PongGroup {
	PongPaneBase base;

	gboolean expand;
	int columns;

	GList *widgets;
};

struct _PongArgument {
	char *name;
	char *value;
	gboolean value_translatable;
};

struct _PongWidget {
	PongPaneBase base;

	char *tooltip;

	gboolean expand;
	gboolean align_label;

	int widget_type;
	GtkType widget_gtktype;

	GList *arguments;
	GList *options;
};

/*
 * Plug widget can be substituted for a group or
 * a widget
 */
struct _PongPlugWidget {
	PongPaneBase base;

	char *tooltip;

	char *plug_type;

	char *path;
	char *specifier;

	gboolean expand;
	gboolean align_label;

	GList *arguments;
	GList *options;
};

struct _PongWidgetRuntime {
	PongWidget *pongwid;
	GtkWidget *wid;
};

struct _PongDialog {
	int refcount;

	GtkWidget *wid;
	GtkWidget *pane_wid;
	GtkTooltips *tooltips;

	PongUIInternal *ui;

	GList *panes;
	GHashTable *widgets;
	GHashTable *gtk_widgets;
};

/*
 * PaneBase
 */
void		pong_pane_base_init		(PongPaneBase *base,
						 int type,
						 GDestroyNotify destructor);

void		pong_pane_base_ref		(PongPaneBase *base);
void		pong_pane_base_unref		(PongPaneBase *base);

void		pong_pane_base_set_name		(PongPaneBase *base,
						 const char *name);
void		pong_pane_base_set_label	(PongPaneBase *base,
						 const char *label);
void		pong_pane_base_set_levels	(PongPaneBase *base,
						 const char **levels);
void		pong_pane_base_add_level	(PongPaneBase *base,
						 const char *level);

/*
 * These three only work on PongWidget and PongPlugWidget
 */
void		pong_pane_base_add_argument	(PongPaneBase *base,
						 const char *arg_name,
						 const char *arg_value,
						 gboolean value_translatable);
void		pong_pane_base_add_option	(PongPaneBase *base,
						 const char *opt_label,
						 const char *opt_value);
void		pong_pane_base_set_tooltip     	(PongPaneBase *base,
						 const char *tooltip);

/*
 * This only works on PongGroup, PongWidget and PongPlugWidget
 */
void		pong_pane_base_set_expand	(PongPaneBase *base,
						 gboolean expand);
/*
 * This only works on PongWidget and PongPlugWidget
 */
void		pong_pane_base_set_align_label	(PongPaneBase *base,
						 gboolean align_label);

/*
 * Pane
 */
PongPane *	pong_pane_new			(void);

void		pong_pane_append_group		(PongPane *pane,
						 PongGroup *group);
void		pong_pane_append_plug_widget	(PongPane *pane,
						 PongPlugWidget *plug_widget);

/*
 * Group
 */
PongGroup *	pong_group_new			(void);

void		pong_group_set_columns		(PongGroup *group,
						 int columns);
void		pong_group_append_widget	(PongGroup *group,
						 PongWidget *widget);
void		pong_group_append_plug_widget	(PongGroup *group,
						 PongPlugWidget *plug_widget);

/*
 * Widget
 */
PongWidget *	pong_widget_new			(void);

void		pong_widget_set_widget_type	(PongWidget *widget,
						 int widget_type);
void		pong_widget_set_widget_gtktype	(PongWidget *widget,
						 GtkType widget_gtktype);

void		pong_widget_set_widget_type_string(PongWidget *widget,
						   const char *widget_type);

/*
 * Plug Widget
 */
PongPlugWidget *pong_plug_widget_new		(void);

void		pong_plug_widget_set_plug_type	(PongPlugWidget *plug_widget,
						 const char *plug_type);
void		pong_plug_widget_set_path	(PongPlugWidget *plug_widget,
						 const char *path);
void		pong_plug_widget_set_specifier	(PongPlugWidget *plug_widget,
						 const char *specifier);

/*
 * Dialog
 */
PongDialog *	pong_dialog_new			(PongUIInternal *ui,
						 PongFile *file,
						 const char *level);
void		pong_dialog_ref			(PongDialog *dialog);
void		pong_dialog_unref		(PongDialog *dialog);
PongWidget *	pong_dialog_lookup_widget	(PongDialog *dialog,
						 const char *name);
GtkWidget *	pong_dialog_lookup_gtk_widget	(PongDialog *dialog,
						 const char *name);

void		pong_pane_type_from_string	(const char *string,
						 PongType *type,
						 GtkType *gtk_type);


/* Free all absolutely non essential stuff */
void		pong_pane_free_unused_data	(void);

#ifdef __cplusplus
}
#endif

#endif /* PONGPANE_H */
