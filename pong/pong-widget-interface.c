/* PonG: Standard widget interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include "pong-i18n.h"

#include "pong-widget-interface.h"

/**
 * pong_widget_interface_add:
 * @klass:  The #GtkObjectClass to add an interface to
 *
 * Description:  The PonG Interface hack.  Before GTK+2.0 arrives we have
 * our own simple interface implementation.  This will add the interface to
 * a class.  You will have to do it for all the subclasses as well.  This
 * should go in your class init function and you should fill out the returned
 * structure as appropriate.
 *
 * Returns:  The PongWidgetInterface structure belonging to this class
 **/
PongWidgetInterface *
pong_widget_interface_add (GtkObjectClass *klass)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail (klass != NULL, NULL);
	g_return_val_if_fail (GTK_IS_OBJECT_CLASS (klass), NULL);

	iface = g_dataset_get_data (klass, "pong_widget_interface");
	if (iface == NULL) {
		gpointer parent_class = gtk_type_parent_class (klass->type);
		if (parent_class != NULL)
			iface = g_dataset_get_data (parent_class,
						    "pong_widget_interface");
		if (iface == NULL)
			iface = g_new0 (PongWidgetInterface, 1);

		g_dataset_set_data (klass, "pong_widget_interface", iface);
	}

	return iface;
}

/* check for the existance of the interface on a widget */
static PongWidgetInterface *
pong_widget_interface_get (GtkWidget *w)
{
	g_return_val_if_fail (w != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_WIDGET (w), FALSE);

	return g_dataset_get_data (GTK_OBJECT (w)->klass,
				   "pong_widget_interface");
}

/**
 * pong_widget_interface_exists:
 * @w:  a GTK+ widget
 *
 * Description:  Checks if this widget implements the Pong interface
 *
 * Returns:  %TRUE or %FALSE
 **/
gboolean
pong_widget_interface_exists (GtkWidget *w)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail (w != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_WIDGET (w), FALSE);

	iface = pong_widget_interface_get (w);

	if (iface != NULL)
		return TRUE;
	else
		return FALSE;
}

const char *
pong_widget_interface_peek_changed_signal(GtkWidget *w)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail (w != NULL, NULL);
	g_return_val_if_fail (GTK_IS_WIDGET (w), NULL);

	iface = pong_widget_interface_get (w);
	g_return_val_if_fail (iface != NULL, NULL);

	if (iface->changed_signal == NULL) {
		g_warning(_("Widget specified no changed signal, "
			    "trying \"changed\""));
		return "changed";
	}

	return iface->changed_signal;
}

gboolean
pong_widget_interface_get_value	(GtkWidget *w,
				 const char *specifier,
				 PongType type,
				 GConfValue **value)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(value != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = pong_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, FALSE);

	*value = NULL;

	if(iface->get_value == NULL) {
		g_warning(_("get_value not implemented in widget"));
		return FALSE;
	} else
		return iface->get_value(w, specifier, type, value);
}


gboolean
pong_widget_interface_set_value	(GtkWidget *w,
				 const char *specifier,
				 GConfValue *value)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(value != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = pong_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, FALSE);

	if(iface->set_value == NULL) 
		g_warning(_("set_value not implemented in widget"));
	else
		return iface->set_value(w, specifier, value);
	return FALSE;
}

gboolean
pong_widget_interface_implements_add_options(GtkWidget *w)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail(w != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(w), FALSE);

	iface = pong_widget_interface_get(w);
	g_return_val_if_fail(iface != NULL, FALSE);

	if(iface->add_options)
		return TRUE;
	else
		return FALSE;
}

void
pong_widget_interface_add_options(GtkWidget *w, GList *pong_options)
{
	PongWidgetInterface *iface;

	g_return_if_fail(w != NULL);
	g_return_if_fail(GTK_IS_WIDGET(w));

	iface = pong_widget_interface_get(w);
	g_return_if_fail(iface != NULL);

	if(iface->add_options == NULL)
		g_warning(_("add_options not implemented in widget"));
	else
		iface->add_options(w, pong_options);
}

gboolean
pong_widget_interface_implements_set_label (GtkWidget *w)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail (w != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_WIDGET (w), FALSE);

	iface = pong_widget_interface_get (w);
	g_return_val_if_fail (iface != NULL, FALSE);

	if (iface->set_label != NULL)
		return TRUE;
	else
		return FALSE;
}

gboolean
pong_widget_interface_set_label	(GtkWidget *w, const char *label)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail (w != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_WIDGET (w), FALSE);

	iface = pong_widget_interface_get (w);
	g_return_val_if_fail (iface != NULL, FALSE);

	if (iface->set_label == NULL) {
		g_warning (_("set_label not implemented in widget"));
		return FALSE;
	} else {
		return iface->set_label (w, label);
	}
}

gboolean
pong_widget_interface_implements_set_arg (GtkWidget *w)
{
	PongWidgetInterface *iface;

	g_return_val_if_fail (w != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_WIDGET (w), FALSE);

	iface = pong_widget_interface_get (w);
	g_return_val_if_fail (iface != NULL, FALSE);

	if (iface->set_label != NULL)
		return TRUE;
	else
		return FALSE;
}

void
pong_widget_interface_set_arg (GtkWidget *w,
			       const char *name,
			       const char *value)
{
	PongWidgetInterface *iface;

	g_return_if_fail (w != NULL);
	g_return_if_fail (GTK_IS_WIDGET (w));
	g_return_if_fail (name != NULL);

	iface = pong_widget_interface_get (w);
	g_return_if_fail (iface != NULL);

	if (iface->set_arg == NULL)
		g_warning (_("set_arg not implemented in widget"));
	else
		iface->set_arg (w, name, value);
}
