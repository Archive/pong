%at{
/* PonG: option menu
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001,2002 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.99.2
%h{
#include "pong-widget-interface.h"
%}
%{
#include "config.h"
#include <gnome.h>
#include <gconf/gconf-value.h>

#include "pong-i18n.h"

#include "pongpane.h"
#include "pongutil.h"

#include "pong-option-menu.h"
#include "pong-option-menu-private.h"

static void free_menu_item (MenuItem *item);
static void item_activated (GtkWidget *w, gpointer data);

%}

%ph{
typedef struct _MenuItem MenuItem;
struct _MenuItem {
	GtkWidget *w;
	char *value;
};
%}


class Pong:Option:Menu from Gtk:Option:Menu {

	private GList *menu_items = NULL
		destroy {
			g_list_foreach (VAR, (GFunc)free_menu_item, NULL);
			g_list_free (VAR);
		};

	private gboolean allow_arbitrary = FALSE;
	property BOOLEAN allow_arbitrary
		(nick = _("Allow arbitrary"),
		 blurb = _("Allow arbitrary values"),
		 link);

	private MenuItem *arbit_item = NULL;

	class_init (klass)
	{
		PongWidgetInterface *iface;

		iface = pong_widget_interface_add (GTK_OBJECT_CLASS (klass));
		iface->changed_signal = "changed";
		iface->get_value = self_get_value;
		iface->set_value = self_set_value;
		iface->add_options = self_add_options;
	}

	init (self)
	{
		self_add_options ((GtkWidget *)self, NULL);
	}

	/**
	 * new:
	 *
	 * Description:  Create a new #PongOptionMenu widget
	 *
	 * Returns:  A new #PongOptionMenu widget
	 **/
	public
	GtkWidget *
	new (void)
	{
		return (GtkWidget *)GET_NEW;
	}

	/**
	 * new_with_options:
	 * @options:  a list of PongOption structs
	 *
	 * Description:  Create a new #PongOptionMenu widget and
	 * setup the @options.
	 *
	 * Returns:  A new #PongOptionMenu widget
	 **/
	public
	GtkWidget *
	new_with_options (GList *pong_options)
	{
		GtkWidget *w = (GtkWidget *)GET_NEW;
		self_add_options (w, pong_options);
		return w;
	}

	private signal NONE(NONE)
	void
	changed (self);

	private char *
	get_cur_value (self)
	{
		GtkOptionMenu *option_menu = GTK_OPTION_MENU (self);

		if (option_menu->menu != NULL) {
			GtkWidget *active_widget;
			GList *li;

			active_widget = gtk_menu_get_active
				(GTK_MENU (option_menu->menu));

			for (li = self->_priv->menu_items;
			     li != NULL;
			     li = li->next) {
				MenuItem *item = li->data;
				if (item->w == active_widget)
					return item->value;
			}
		}

		return NULL;
	}

	private void
	activate_item (self, MenuItem *item (check null))
	{
		gtk_menu_item_activate (GTK_MENU_ITEM (item->w));
		gtk_option_menu_set_history
			(GTK_OPTION_MENU (self),
			 g_list_index (self->_priv->menu_items, item));
	}

	/* if all else fails ... */
	/* FIXME: support no-value-state, rather then
	 * activating the first */
	private void
	activate_first (self)
	{
		if (self->_priv->menu_items != NULL) {
			MenuItem *item = self->_priv->menu_items->data;
			self_activate_item (self, item);
		}
	}

	private gboolean
	get_value (Gtk:Widget *w (check null type),
		   const char *specifier,
		   PongType type,
		   GConfValue **value (check null))
		onerror FALSE
	{
		Self *self = SELF(w);
		char *cur_value;

		*value = NULL;

		/* FIXME: add radio button like specifier behaviour */

		if (type != PONG_TYPE_STRING &&
		    type != PONG_TYPE_INT &&
		    type != PONG_TYPE_FLOAT &&
		    type != PONG_TYPE_BOOL) {
			g_warning (_("Unsupported type on Pong:Option:Menu"));
			return FALSE;
		}

		cur_value = self_get_cur_value (self);
		if (cur_value == NULL)
			return FALSE;

		switch (type) {
		case PONG_TYPE_STRING:
			*value = gconf_value_new (GCONF_VALUE_STRING);
			gconf_value_set_string (*value, cur_value);
			break;
		case PONG_TYPE_INT:
			{
				int ret;
				*value = gconf_value_new (GCONF_VALUE_INT);
				sscanf (cur_value, "%d", &ret);
				gconf_value_set_int (*value, ret);
				break;
			}
		case PONG_TYPE_FLOAT:
			{
				double ret;
				*value = gconf_value_new (GCONF_VALUE_FLOAT);
				pong_i18n_push_c_numeric_locale ();
				sscanf (cur_value, "%lf", &ret);
				pong_i18n_pop_c_numeric_locale ();
				gconf_value_set_float (*value, ret);
				break;
			}
		case PONG_TYPE_BOOL:
			*value = gconf_value_new (GCONF_VALUE_BOOL);
			gconf_value_set_bool (*value,
					      pong_bool_from_string (cur_value));
			break;
		default:
			g_assert_not_reached ();
			break;
		}

		return TRUE;
	}

	private void
	whack_arbitrary (self, MenuItem *cur)
	{
		if (self->_priv->arbit_item != NULL &&
		    self->_priv->arbit_item != cur) {
			gtk_widget_destroy (self->_priv->arbit_item->w);
			self->_priv->arbit_item->w = NULL;
			self->_priv->menu_items =
				g_list_remove (self->_priv->menu_items,
					       self->_priv->arbit_item);
			free_menu_item (self->_priv->arbit_item);
		}
	}

	private void
	add_arbitrary (self, GConfValue *value)
	{
		MenuItem *item;

		if (self->_priv->arbit_item == NULL) {
			self->_priv->arbit_item = g_new0 (MenuItem, 1);
			self->_priv->arbit_item->w = NULL;
			self->_priv->arbit_item->value =
				pong_string_from_gconf_value (value);

			self->_priv->menu_items = 
				g_list_prepend (self->_priv->menu_items,
						self->_priv->arbit_item);
		}

		item = self->_priv->arbit_item;

		if (item->w != NULL)
			gtk_widget_destroy (item->w);

		item->w = gtk_menu_item_new_with_label
			(pong_sure_string (item->value));

		gtk_signal_connect (GTK_OBJECT (item->w), "activate",
				    GTK_SIGNAL_FUNC (item_activated),
				    self);

		gtk_widget_show (item->w);

		gtk_menu_prepend (GTK_MENU (GTK_OPTION_MENU (self)->menu),
				  item->w);
	}

	private gboolean
	set_value (Gtk:Widget *w (check null type),
		   const char *specifier,
		   GConfValue *value (check null))
		onerror FALSE
	{
		Self *self = SELF(w);
		GList *li;

		/* FIXME: add radio button like specifier behaviour */

		if (value->type != GCONF_VALUE_STRING &&
		    value->type != GCONF_VALUE_INT &&
		    value->type != GCONF_VALUE_FLOAT &&
		    value->type != GCONF_VALUE_BOOL) {
			g_warning (_("Unsupported type on Pong:Option:Menu"));
			return FALSE;
		}

		for (li = self->_priv->menu_items;
		     li != NULL;
		     li = li->next) {
			MenuItem *item = li->data;
			switch (value->type) {
			case GCONF_VALUE_STRING:
				if (strcmp (gconf_value_get_string (value), item->value) == 0) {
					self_whack_arbitrary (self, item);
					self_activate_item (self, item);
					return TRUE;
				}
				break;
			case GCONF_VALUE_INT:
				if (gconf_value_get_int (value) == atoi (item->value)) {
					self_whack_arbitrary (self, item);
					self_activate_item (self, item);
					return TRUE;
				}
				break;
			case GCONF_VALUE_FLOAT:
				{
					double foo;

					pong_i18n_push_c_numeric_locale ();
					foo = atof (item->value);
					pong_i18n_pop_c_numeric_locale ();

					/* FIXME: EEEEK! float comparison !!!! */
					if (gconf_value_get_float (value) == foo) {
						self_whack_arbitrary (self, item);
						self_activate_item (self, item);
						return TRUE;
					}
				}
				break;
			case GCONF_VALUE_BOOL:
				/* The reason for the ?: is that comparing bools
				 * directly is evil */
				if ((gconf_value_get_bool (value) ? 1 : 0) ==
				   (pong_bool_from_string (item->value) ? 1 : 0)) {
					self_whack_arbitrary (self, item);
					self_activate_item (self, item);
					return TRUE;
				}
				break;
			default:
				g_assert_not_reached ();
				break;
			}
		}

		if (self->_priv->allow_arbitrary) {
			self_add_arbitrary (self, value);
		} else {
			/* FIXME: support no-value-state */
			self_activate_first (self);
		}
		return FALSE;
	}

	private void
	add_options (Gtk:Widget *w (check null type),
		     GList *pong_options)
	{
		Self *self = SELF (w);
		GtkOptionMenu *option_menu = GTK_OPTION_MENU (self);
		GList *li;
		GtkWidget *menu;

		if (option_menu->menu != NULL) {
			GList *children;

			menu = option_menu->menu;

			children = gtk_container_children (GTK_CONTAINER (menu));

			g_list_free (children);
		} else {
			menu = gtk_menu_new ();

			gtk_option_menu_set_menu (option_menu, menu);
		}

		for(li = pong_options; li != NULL; li = li->next) {
			PongOption *opt = li->data;
			MenuItem *item = g_new0 (MenuItem, 1);

			item->value = g_strdup (opt->value);

			item->w = gtk_menu_item_new_with_label (opt->label != NULL?
								opt->label :
								_("???"));

			gtk_signal_connect (GTK_OBJECT (item->w), "activate",
					    GTK_SIGNAL_FUNC (item_activated),
					    self);

			gtk_widget_show (item->w);

			gtk_menu_append (GTK_MENU (menu), item->w);

			self->_priv->menu_items =
				g_list_append (self->_priv->menu_items, item);
		}
	}
}

%{
static void
free_menu_item (MenuItem *item)
{
	g_return_if_fail (item != NULL);

	g_free (item->value);
	item->value = NULL; /* paranoia */
	g_free (item);
}

static void
item_activated (GtkWidget *w, gpointer data)
{
	self_changed (SELF (data));
}
%}
