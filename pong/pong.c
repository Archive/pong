/* PonG: the library interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include "pong-i18n.h"

#include "pongelement.h"
#include "pongparser.h"
#include "pongparser-private.h"
#include "pongpane.h"

#include "pong-native-widgets.h"

#include "pong.h"

GList *pong_directories = NULL;

GList *pong_plug_creators = NULL;

static gboolean pong_initialized = FALSE;

/**
 * pong_is_initialized:
 *
 * Description:  Check if pong has been initialized
 *
 * Returns:  TRUE if pong_init has been called already, FALSE otherwise
 **/
gboolean
pong_is_initialized (void)
{
	return pong_initialized;
}

/**
 * pong_init:
 *
 * Description:  Initialize the pong library.  Should
 * be called in your main function.  Or any time before
 * you use the pong library in any way.
 *
 * Returns:  TRUE if all went well, FALSE on a fatal error.
 **/
gboolean
pong_init (void)
{
	/* Allow multiple calls to pong_init */
	if (pong_initialized)
		return TRUE;

	/* Init goes here, when I get some,
	 * apps should still call this, even though it's
	 * currently a no-op, but it just may be that
	 * it will be needed in the future. */

	pong_initialized = TRUE;

	return TRUE;
}

/* An init that can be run at any time */
/**
 * pong_lazy_init:
 *
 * Description:  Initialization that can be run at any time.  You do
 * not have to call this as it is called automatically from pong.
 **/
void
pong_lazy_init (void)
{
	static gboolean lazy_init_run = FALSE;

	if (lazy_init_run)
		return;

	/* Make sure these native widget classes are created just for the sake
	 * of non-gmodule supporting systems */
	pong_radio_group_get_type ();
	pong_option_menu_get_type ();
	pong_check_group_get_type ();
	pong_spin_button_get_type ();
	pong_color_picker_get_type ();
	pong_slider_get_type ();
	pong_list_entry_get_type ();
}

/**
 * pong_free_unused_data:
 *
 * Description:  Free any unused data.  You can call this after you know
 * you will not be using pong for a while and pong will free some data
 * which it doesn't need to resume operation.  This is completely
 * transaprent and can be called at any time.
 **/
void
pong_free_unused_data (void)
{
	pong_element_free_unused_data ();
	pong_parser_free_unused_data ();
	pong_pane_free_unused_data ();
}

/**
 * pong_add_directory:
 * @directory:  Directory to add
 *
 * Description:  Add a directory a list of directories where
 * pong will search for .pong files.  They will be searched in
 * the order you give them.  Pong will always check for the file
 * in the current directory, and then these directories.  And only
 * after that will it try to locate files by other means.
 **/
void
pong_add_directory (const char *directory)
{
	pong_directories = g_list_append (pong_directories, g_strdup (directory));
}

/* Pure sugar functions, this way adding a config dialog is a 1-liner */
/**
 * pong_run_simple_dialog:
 * @file:  .pong file to use.
 *
 * Description:  This will load the file, and show the dialog for
 * you.  It will return a pointer to the dialog widget or %NULL
 * if no dialog could be created.
 *
 * Returns:  A pointer to the dialog widget created.
 **/
GtkWidget *
pong_run_simple_dialog (const char *file)
{
	GtkWidget *dlg;
	PongXML *xml;

	g_return_val_if_fail (pong_is_initialized (), NULL);

	xml = pong_xml_new (file);
	if (xml == NULL) {
		g_warning (_("Cannot load config dialog"));
		return NULL;
	}
	pong_xml_show_dialog (xml);
	dlg = pong_xml_get_dialog_widget (xml);

	gtk_object_unref (GTK_OBJECT (xml));

	if (dlg == NULL) {
		g_warning (_("Cannot run config dialog"));
		return NULL;
	}
	return dlg;
}

/**
 * pong_run_simple_dialog_from_memory:
 * @buffer:  buffer with the .pong file to use.
 * @len:  the length of @buffer
 *
 * Description:  This will parse the buffer, and show the dialog for
 * you.  It will return a pointer to the dialog widget or %NULL
 * if no dialog could be created.
 *
 * Returns:  A pointer to the dialog widget created.
 **/
GtkWidget *
pong_run_simple_dialog_from_memory (const char *buffer, int len)
{
	GtkWidget *dlg;
	PongXML *xml;

	g_return_val_if_fail (pong_is_initialized (), NULL);

	xml = pong_xml_new_from_memory (buffer, len);
	if (xml == NULL) {
		g_warning (_("Cannot load config dialog"));
		return NULL;
	}
	pong_xml_show_dialog (xml);
	dlg = pong_xml_get_dialog_widget (xml);

	gtk_object_unref (GTK_OBJECT (xml));

	if (dlg == NULL) {
		g_warning (_("Cannot run config dialog"));
		return NULL;
	}
	return dlg;
}

/**
 * pong_add_plug_creator:
 * @creator:  plug creator function
 *
 * Description:  This function is used by pong UI addons called
 * Plugs.  They can register a plug creator here that will add
 * the "Plug" to every dialog created afterwards.  The plug creator
 * function should return a PongUIPlug object when called.
 **/
void
pong_add_plug_creator (PongUIPlugCreator creator)
{
	g_return_if_fail (creator != NULL);

	if (g_list_find (pong_plug_creators, creator) != NULL)
		return;

	pong_plug_creators = g_list_append (pong_plug_creators, creator);
}
