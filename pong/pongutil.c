/* PonG: Utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001,2002 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <stdlib.h>
#include <popt.h> /* poptParseArgvString () */
#include <locale.h>

#include "pong-i18n.h"

#include "pongutil.h"

gboolean
pong_bool_from_string (const char *s)
{
	if (s == NULL)
		return FALSE;
	if (s[0] == 'T' || s[0] == 't' ||
	    s[0] == 'Y' || s[0] == 'y' ||
	    atoi (s) != 0)
		return TRUE;
	else
		return FALSE;
}

gboolean
pong_gconf_value_equal (GConfValue *v1, GConfValue *v2)
{
	if (v1->type != v2->type)
		return FALSE;
	switch (v1->type) {
	case GCONF_VALUE_STRING:
		if (strcmp (gconf_value_get_string (v1), gconf_value_get_string (v2)) == 0)
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_INT:
		if (gconf_value_get_int (v1) == gconf_value_get_int (v2))
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_FLOAT:
		/* Eeek, float comparison, might be ok in this case though I guess */
		if (gconf_value_get_float (v1) == gconf_value_get_float (v2))
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_BOOL:
		if ((gconf_value_get_bool (v1) ? 1 : 0) ==
		    (gconf_value_get_bool (v2) ? 1 : 0))
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_LIST:
		{
			GConfValueType type = gconf_value_get_list_type (v1);
			GSList *li1, *li2;
			if (type != gconf_value_get_list_type (v2))
				return FALSE;

			for (li1 = gconf_value_get_list (v1), li2 = gconf_value_get_list (v2);
			     li1 != NULL && li2 != NULL;
			     li1 = li1->next, li2 = li2->next) {
				if ( ! pong_gconf_value_equal (li1->data, li2->data))
					return FALSE;
			}
			/* At least one of them is NULL, so we're effectively
			 * testing for NULL of both */
			if (li1 != li2)
				return FALSE;
			return TRUE;
		}
	case GCONF_VALUE_PAIR:
		if ( ! pong_gconf_value_equal (gconf_value_get_car (v1),
					      gconf_value_get_car (v2)))
			return FALSE;
		if ( ! pong_gconf_value_equal (gconf_value_get_cdr (v1),
					      gconf_value_get_cdr (v2)))
			return FALSE;
		return TRUE;
	default:
		g_warning (_("Unknown GConfValue type to compare"));
		break;
	}
	return FALSE;
}

GConfValueType
pong_gconf_value_type_from_pong_type (PongType type)
{
	switch (type) {

	case PONG_TYPE_STRING:
		return GCONF_VALUE_STRING;

	case PONG_TYPE_INT:
		return GCONF_VALUE_INT;

	case PONG_TYPE_FLOAT:
		return GCONF_VALUE_FLOAT;

	case PONG_TYPE_BOOL:
		return GCONF_VALUE_BOOL;

	case PONG_TYPE_LIST_OF_STRINGS:
	case PONG_TYPE_LIST_OF_INTS:
	case PONG_TYPE_LIST_OF_FLOATS:
	case PONG_TYPE_LIST_OF_BOOLS:
		return GCONF_VALUE_LIST;

	case PONG_TYPE_PAIR_STRING_STRING:
	case PONG_TYPE_PAIR_STRING_INT:
	case PONG_TYPE_PAIR_STRING_FLOAT:
	case PONG_TYPE_PAIR_STRING_BOOL:
	case PONG_TYPE_PAIR_INT_STRING:
	case PONG_TYPE_PAIR_INT_INT:
	case PONG_TYPE_PAIR_INT_FLOAT:
	case PONG_TYPE_PAIR_INT_BOOL:
	case PONG_TYPE_PAIR_FLOAT_STRING:
	case PONG_TYPE_PAIR_FLOAT_INT:
	case PONG_TYPE_PAIR_FLOAT_FLOAT:
	case PONG_TYPE_PAIR_FLOAT_BOOL:
	case PONG_TYPE_PAIR_BOOL_STRING:
	case PONG_TYPE_PAIR_BOOL_INT:
	case PONG_TYPE_PAIR_BOOL_FLOAT:
	case PONG_TYPE_PAIR_BOOL_BOOL:
		return GCONF_VALUE_PAIR;

	default:
		return GCONF_VALUE_INVALID;
	}
}

GConfValueType
pong_list_gconf_value_type_from_pong_type (PongType type)
{
	switch (type) {

	case PONG_TYPE_LIST_OF_STRINGS:
		return GCONF_VALUE_STRING;

	case PONG_TYPE_LIST_OF_INTS:

		return GCONF_VALUE_INT;
	case PONG_TYPE_LIST_OF_FLOATS:
		return GCONF_VALUE_FLOAT;

	case PONG_TYPE_LIST_OF_BOOLS:
		return GCONF_VALUE_BOOL;

	default:
		return GCONF_VALUE_INVALID;
	}
}

GConfValueType
pong_car_gconf_value_type_from_pong_type (PongType type)
{
	switch (type) {

	case PONG_TYPE_PAIR_STRING_STRING:
	case PONG_TYPE_PAIR_STRING_INT:
	case PONG_TYPE_PAIR_STRING_FLOAT:
	case PONG_TYPE_PAIR_STRING_BOOL:
		return GCONF_VALUE_STRING;

	case PONG_TYPE_PAIR_INT_STRING:
	case PONG_TYPE_PAIR_INT_INT:
	case PONG_TYPE_PAIR_INT_FLOAT:
	case PONG_TYPE_PAIR_INT_BOOL:
		return GCONF_VALUE_INT;

	case PONG_TYPE_PAIR_FLOAT_STRING:
	case PONG_TYPE_PAIR_FLOAT_INT:
	case PONG_TYPE_PAIR_FLOAT_FLOAT:
	case PONG_TYPE_PAIR_FLOAT_BOOL:
		return GCONF_VALUE_FLOAT;

	case PONG_TYPE_PAIR_BOOL_STRING:
	case PONG_TYPE_PAIR_BOOL_INT:
	case PONG_TYPE_PAIR_BOOL_FLOAT:
	case PONG_TYPE_PAIR_BOOL_BOOL:
		return GCONF_VALUE_BOOL;

	default:
		return GCONF_VALUE_INVALID;
	}
}

GConfValueType
pong_cdr_gconf_value_type_from_pong_type (PongType type)
{
	switch (type) {

	case PONG_TYPE_PAIR_STRING_STRING:
	case PONG_TYPE_PAIR_INT_STRING:
	case PONG_TYPE_PAIR_FLOAT_STRING:
	case PONG_TYPE_PAIR_BOOL_STRING:
		return GCONF_VALUE_STRING;

	case PONG_TYPE_PAIR_STRING_INT:
	case PONG_TYPE_PAIR_INT_INT:
	case PONG_TYPE_PAIR_FLOAT_INT:
	case PONG_TYPE_PAIR_BOOL_INT:
		return GCONF_VALUE_INT;

	case PONG_TYPE_PAIR_STRING_FLOAT:
	case PONG_TYPE_PAIR_INT_FLOAT:
	case PONG_TYPE_PAIR_FLOAT_FLOAT:
	case PONG_TYPE_PAIR_BOOL_FLOAT:
		return GCONF_VALUE_FLOAT;

	case PONG_TYPE_PAIR_STRING_BOOL:
	case PONG_TYPE_PAIR_INT_BOOL:
	case PONG_TYPE_PAIR_FLOAT_BOOL:
	case PONG_TYPE_PAIR_BOOL_BOOL:
		return GCONF_VALUE_BOOL;

	default:
		return GCONF_VALUE_INVALID;
	}
}

PongType
pong_type_from_gconf_types (GConfValueType type,
			    GConfValueType list_type,
			    GConfValueType car_type,
			    GConfValueType cdr_type)
{
	switch (type) {

	case GCONF_VALUE_STRING:
		return PONG_TYPE_STRING;
	case GCONF_VALUE_INT:
		return PONG_TYPE_INT;
	case GCONF_VALUE_FLOAT:
		return PONG_TYPE_FLOAT;
	case GCONF_VALUE_BOOL:
		return PONG_TYPE_BOOL;

	case GCONF_VALUE_LIST:
		{
			switch (list_type) {
			case GCONF_VALUE_STRING:
				return PONG_TYPE_LIST_OF_STRINGS;
			case GCONF_VALUE_INT:
				return PONG_TYPE_LIST_OF_INTS;
			case GCONF_VALUE_FLOAT:
				return PONG_TYPE_LIST_OF_FLOATS;
			case GCONF_VALUE_BOOL:
				return PONG_TYPE_LIST_OF_BOOLS;
			default:
				return PONG_TYPE_INVALID;
			}
		}

	case GCONF_VALUE_PAIR:
		{
			switch (car_type) {
			case GCONF_VALUE_STRING:
				switch (cdr_type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_STRING_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_STRING_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_STRING_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_STRING_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			case GCONF_VALUE_INT:
				switch (cdr_type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_INT_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_INT_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_INT_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_INT_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			case GCONF_VALUE_FLOAT:
				switch (cdr_type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_FLOAT_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_FLOAT_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_FLOAT_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_FLOAT_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			case GCONF_VALUE_BOOL:
				switch (cdr_type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_BOOL_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_BOOL_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_BOOL_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_BOOL_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			default:
				return PONG_TYPE_INVALID;
			}
		}

	default:
		return PONG_TYPE_INVALID;
	}
}

PongType
pong_type_from_gconf_value (GConfValue *value)
{
	g_return_val_if_fail (value != NULL, PONG_TYPE_INVALID);

	switch (value->type) {

	case GCONF_VALUE_STRING:
		return PONG_TYPE_STRING;
	case GCONF_VALUE_INT:
		return PONG_TYPE_INT;
	case GCONF_VALUE_FLOAT:
		return PONG_TYPE_FLOAT;
	case GCONF_VALUE_BOOL:
		return PONG_TYPE_BOOL;

	case GCONF_VALUE_LIST:
		{
			GConfValueType type = gconf_value_get_list_type (value);
			switch (type) {
			case GCONF_VALUE_STRING:
				return PONG_TYPE_LIST_OF_STRINGS;
			case GCONF_VALUE_INT:
				return PONG_TYPE_LIST_OF_INTS;
			case GCONF_VALUE_FLOAT:
				return PONG_TYPE_LIST_OF_FLOATS;
			case GCONF_VALUE_BOOL:
				return PONG_TYPE_LIST_OF_BOOLS;
			default:
				return PONG_TYPE_INVALID;
			}
		}

	case GCONF_VALUE_PAIR:
		{
			GConfValue *car = gconf_value_get_car (value);
			GConfValue *cdr = gconf_value_get_cdr (value);
			switch (car->type) {
			case GCONF_VALUE_STRING:
				switch (cdr->type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_STRING_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_STRING_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_STRING_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_STRING_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			case GCONF_VALUE_INT:
				switch (cdr->type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_INT_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_INT_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_INT_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_INT_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			case GCONF_VALUE_FLOAT:
				switch (cdr->type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_FLOAT_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_FLOAT_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_FLOAT_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_FLOAT_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			case GCONF_VALUE_BOOL:
				switch (cdr->type) {
				case GCONF_VALUE_STRING:
					return PONG_TYPE_PAIR_BOOL_STRING;
				case GCONF_VALUE_INT:
					return PONG_TYPE_PAIR_BOOL_INT;
				case GCONF_VALUE_FLOAT:
					return PONG_TYPE_PAIR_BOOL_FLOAT;
				case GCONF_VALUE_BOOL:
					return PONG_TYPE_PAIR_BOOL_BOOL;
				default:
					return PONG_TYPE_INVALID;
				}
			default:
				return PONG_TYPE_INVALID;
			}
		}

	default:
		return PONG_TYPE_INVALID;
	}

}

static void
set_list (GConfValue *value, const char *s, PongType type)
{
	char **argv;
	int argc, i;
	GSList *list;
	GConfValueType gconf_type = pong_gconf_value_type_from_pong_type (type);

	gconf_value_set_list_type (value, gconf_type);

	if ( ! g_shell_parse_argv (s, &argc, &argv, NULL /* error */)) {
		argc = 0;
		argv = NULL;
	}

	if (argc < 1) {
		if (argv != NULL)
			g_strfreev (argv);

		/* If we fail, then just make ONE value, this is
		 * utterly wrong IMO, it should TRY to parse the string and
		 * return a part parsed string.  Oh well, not a huge deal. */

		argc = 1;
		argv = g_new0 (char *, 2);
		argv[0] = g_strdup (s);
		argv[1] = NULL;
	}

	list = NULL;

	for (i = 0; i < argc; i++) {
		GConfValue *v;

		v = pong_gconf_value_from_string (argv[i], type);

		/* Just for sanity */
		if (v == NULL)
			v = gconf_value_new (gconf_type);

		list = g_slist_prepend (list, v);
	}

	list = g_slist_reverse (list);

	gconf_value_set_list_nocopy (value, list);

	g_strfreev (argv);
}

static void
set_pair (GConfValue *value, const char *s,
	  PongType type_car, PongType type_cdr)
{
	char **argv;
	int argc;
	GConfValue *v;

	GConfValueType gconf_type_car = pong_gconf_value_type_from_pong_type (type_car);
	GConfValueType gconf_type_cdr = pong_gconf_value_type_from_pong_type (type_cdr);

	if ( ! g_shell_parse_argv (s, &argc, &argv, NULL /* error */)) {
		argc = 0;
		argv = NULL;
	}

	if (argc < 1) {
		if (argv != NULL)
			g_strfreev (argv);

		/* If we fail, then just make ONE value, this is
		 * utterly wrong IMO, it should TRY to parse the string and
		 * return a part parsed string.  Oh well, not a huge deal. */

		argc = 1;
		argv = g_new0 (char *, 2);
		argv[0] = g_strdup (s);
		argv[1] = NULL;
	}

	/* Note: we are guaranteed argc to be higher then 0 */
	g_assert (argc > 0);

	/* CAR */
	v = pong_gconf_value_from_string (argv[0], type_car);

	/* Just for sanity */
	if (v == NULL)
		v = gconf_value_new (gconf_type_car);

	gconf_value_set_car_nocopy (value, v);

	/* CDR */
	if (argc > 1)
		v = pong_gconf_value_from_string (argv[1], type_cdr);
	else
		v = NULL;

	if (v == NULL)
		v = gconf_value_new (gconf_type_cdr);

	gconf_value_set_cdr_nocopy (value, v);

	g_strfreev (argv);
}

static void
set_newline_list (GConfValue *value, const char *s, PongType type)
{
	char **strings;
	int i;
	GSList *list;
	GConfValueType gconf_type = pong_gconf_value_type_from_pong_type (type);

	gconf_value_set_list_type (value, gconf_type);

	strings = g_strsplit (s, "\n", 0);

	list = NULL;
	for (i = 0; strings != NULL && strings[i] != NULL; i++) {
		GConfValue *v;

		v = pong_gconf_value_from_string (strings[i], type);

		/* Just for sanity */
		if (v == NULL)
			v = gconf_value_new (gconf_type);

		list = g_slist_prepend (list, v);
	}

	list = g_slist_reverse (list);

	gconf_value_set_list_nocopy (value, list);

	g_strfreev (strings);
}

static void
set_newline_pair (GConfValue *value, const char *s,
	  PongType type_car, PongType type_cdr)
{
	char **strings;
	GConfValue *v;

	GConfValueType gconf_type_car = pong_gconf_value_type_from_pong_type (type_car);
	GConfValueType gconf_type_cdr = pong_gconf_value_type_from_pong_type (type_cdr);

	strings = g_strsplit (s, "\n", 0);

	/* CAR */
	if (strings != NULL && strings[0] != NULL)
		v = pong_gconf_value_from_string (strings[0], type_car);
	else
		v = NULL;

	/* Just for sanity */
	if (v == NULL)
		v = gconf_value_new (gconf_type_car);

	gconf_value_set_car_nocopy (value, v);

	/* CDR */
	if (strings != NULL && strings[0] != NULL && strings[1] != NULL)
		v = pong_gconf_value_from_string (strings[1], type_cdr);
	else
		v = NULL;

	if (v == NULL)
		v = gconf_value_new (gconf_type_cdr);

	gconf_value_set_cdr_nocopy (value, v);

	g_strfreev (strings);
}

GConfValue *
pong_gconf_value_from_string (const char *s, PongType type)
{
	GConfValue *value;
	GConfValueType gconf_type;

	g_return_val_if_fail (s != NULL, NULL);

	gconf_type = pong_gconf_value_type_from_pong_type (type);

	if (gconf_type == GCONF_VALUE_INVALID) {
		g_warning (_("Invalid type"));
		return NULL;
	}

	value = gconf_value_new (gconf_type);

	switch (type) {
	case PONG_TYPE_STRING:
		gconf_value_set_string (value, s);
		break;
	case PONG_TYPE_INT:
		gconf_value_set_int (value, atoi (s));
		break;
	case PONG_TYPE_FLOAT:
		gconf_value_set_float (value, atof (s));
		break;
	case PONG_TYPE_BOOL:
		if (s[0] == 't' || s[0] == 'T' ||
		    s[0] == 'y' || s[0] == 'Y' ||
		    atoi (s) != 0)
			gconf_value_set_bool (value, TRUE);
		else
			gconf_value_set_bool (value, FALSE);
		break;

	case PONG_TYPE_LIST_OF_STRINGS:
		set_list (value, s, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_LIST_OF_INTS:
		set_list (value, s, PONG_TYPE_INT);
		break;
	case PONG_TYPE_LIST_OF_FLOATS:
		set_list (value, s, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_LIST_OF_BOOLS:
		set_list (value, s, PONG_TYPE_BOOL);
		break;

	case PONG_TYPE_PAIR_STRING_STRING:
		set_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_STRING_INT:
		set_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_STRING_FLOAT:
		set_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_STRING_BOOL:
		set_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_BOOL);
		break;
	case PONG_TYPE_PAIR_INT_STRING:
		set_pair (value, s, PONG_TYPE_INT, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_INT_INT:
		set_pair (value, s, PONG_TYPE_INT, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_INT_FLOAT:
		set_pair (value, s, PONG_TYPE_INT, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_INT_BOOL:
		set_pair (value, s, PONG_TYPE_INT, PONG_TYPE_BOOL);
		break;
	case PONG_TYPE_PAIR_FLOAT_STRING:
		set_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_FLOAT_INT:
		set_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_FLOAT_FLOAT:
		set_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_FLOAT_BOOL:
		set_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_BOOL);
		break;
	case PONG_TYPE_PAIR_BOOL_STRING:
		set_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_BOOL_INT:
		set_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_BOOL_FLOAT:
		set_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_BOOL_BOOL:
		set_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_BOOL);
		break;

	default:
		/* This should never really happen */
		gconf_value_free (value);
		return NULL;
	}

	return value;
}

GConfValue *
pong_gconf_value_from_text (const char *s, PongType type)
{
	GConfValue *value;
	GConfValueType gconf_type;

	g_return_val_if_fail (s != NULL, NULL);

	gconf_type = pong_gconf_value_type_from_pong_type (type);

	if (gconf_type == GCONF_VALUE_INVALID) {
		g_warning (_("Invalid type"));
		return NULL;
	}

	value = gconf_value_new (gconf_type);

	switch (type) {
	case PONG_TYPE_STRING:
	case PONG_TYPE_INT:
	case PONG_TYPE_FLOAT:
	case PONG_TYPE_BOOL:
		gconf_value_free (value);
		value = pong_gconf_value_from_string (s, type);
		break;

	case PONG_TYPE_LIST_OF_STRINGS:
		set_newline_list (value, s, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_LIST_OF_INTS:
		set_newline_list (value, s, PONG_TYPE_INT);
		break;
	case PONG_TYPE_LIST_OF_FLOATS:
		set_newline_list (value, s, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_LIST_OF_BOOLS:
		set_newline_list (value, s, PONG_TYPE_BOOL);
		break;

	case PONG_TYPE_PAIR_STRING_STRING:
		set_newline_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_STRING_INT:
		set_newline_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_STRING_FLOAT:
		set_newline_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_STRING_BOOL:
		set_newline_pair (value, s, PONG_TYPE_STRING, PONG_TYPE_BOOL);
		break;
	case PONG_TYPE_PAIR_INT_STRING:
		set_newline_pair (value, s, PONG_TYPE_INT, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_INT_INT:
		set_newline_pair (value, s, PONG_TYPE_INT, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_INT_FLOAT:
		set_newline_pair (value, s, PONG_TYPE_INT, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_INT_BOOL:
		set_newline_pair (value, s, PONG_TYPE_INT, PONG_TYPE_BOOL);
		break;
	case PONG_TYPE_PAIR_FLOAT_STRING:
		set_newline_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_FLOAT_INT:
		set_newline_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_FLOAT_FLOAT:
		set_newline_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_FLOAT_BOOL:
		set_newline_pair (value, s, PONG_TYPE_FLOAT, PONG_TYPE_BOOL);
		break;
	case PONG_TYPE_PAIR_BOOL_STRING:
		set_newline_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_STRING);
		break;
	case PONG_TYPE_PAIR_BOOL_INT:
		set_newline_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_INT);
		break;
	case PONG_TYPE_PAIR_BOOL_FLOAT:
		set_newline_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_FLOAT);
		break;
	case PONG_TYPE_PAIR_BOOL_BOOL:
		set_newline_pair (value, s, PONG_TYPE_BOOL, PONG_TYPE_BOOL);
		break;

	default:
		/* This should never really happen */
		gconf_value_free (value);
		return NULL;
	}

	return value;
}

static gboolean
no_quoting_needed (const char *s)
{
	int i;

	for (i = 0; s[i] != '\0'; i++) {
		/* if the character is not a normal alphanumeric one,
		 * use quoting */
		if ( ! ((s[i] >= 'a' && s[i] <= 'z') ||
			(s[i] >= 'A' && s[i] <= 'Z') ||
			(s[i] >= '0' && s[i] <= '9')))
			return FALSE;
	}
	return TRUE;
}

/* Escape the ' character by '\'' and append things to the given GString,
 * also put the string in signle quotes */
static void
add_escaped_string (GString *string, const char *from)
{
	const char *p;

	if (no_quoting_needed (from)) {
		g_string_append (string, from);
		return;
	}

	g_string_append_c (string, '\'');
	for (p = from; *p != '\0'; p++) {
		if (*p == '\'') {
			g_string_append (string, "'\\''");
		} else {
			g_string_append_c (string, *p);
		}
	}
	g_string_append_c (string, '\'');
}

char *
pong_string_from_gconf_value (GConfValue *value)
{
	g_return_val_if_fail (value != NULL, NULL);

	switch (value->type) {
	case GCONF_VALUE_STRING:
		return g_strdup (gconf_value_get_string (value));
	case GCONF_VALUE_INT:
		return g_strdup_printf ("%d", gconf_value_get_int (value));
	case GCONF_VALUE_FLOAT:
		return g_strdup_printf ("%g", gconf_value_get_float (value));
	case GCONF_VALUE_BOOL:
		return g_strdup (gconf_value_get_bool (value) ? "True" : "False");

	case GCONF_VALUE_LIST:
		{
			char *ret, *sep;
			GSList *li;
			GString *gs;
			gs = g_string_new (NULL);
			sep = "";

			for (li = gconf_value_get_list (value); li != NULL; li = li->next) {
				char *s = pong_string_from_gconf_value (li->data);

				g_string_append (gs, sep);
				add_escaped_string (gs, s);

				sep = " ";

				g_free (s);
			}

			ret = gs->str;
			g_string_free (gs, FALSE);
			return ret;
		}
	case GCONF_VALUE_PAIR:
		{
			char *s1, *s2;
			char *ret;
			GString *gs = g_string_new (NULL);

			s1 = pong_string_from_gconf_value (gconf_value_get_car (value));
			s2 = pong_string_from_gconf_value (gconf_value_get_cdr (value));

			add_escaped_string (gs, s1);
			g_string_append (gs, " ");
			add_escaped_string (gs, s1);

			g_free (s1);
			g_free (s2);

			ret = gs->str;
			g_string_free (gs, FALSE);
			return ret;
		}
	default:
		g_warning (_("Unsupported gconf value type to convert to string"));
		return NULL;
	}

}

char *
pong_text_from_gconf_value (GConfValue *value)
{
	g_return_val_if_fail (value != NULL, NULL);

	switch (value->type) {
	case GCONF_VALUE_STRING:
	case GCONF_VALUE_INT:
	case GCONF_VALUE_FLOAT:
	case GCONF_VALUE_BOOL:
		return pong_string_from_gconf_value (value);

	case GCONF_VALUE_LIST:
		{
			char *ret, *sep;
			GSList *li;
			GString *gs;
			gs = g_string_new (NULL);
			sep = "";

			for (li = gconf_value_get_list (value); li != NULL; li = li->next) {
				char *s = pong_string_from_gconf_value (li->data);

				g_string_append (gs, sep);
				g_string_append (gs, s);

				sep = "\n";

				g_free (s);
			}

			ret = gs->str;
			g_string_free (gs, FALSE);
			return ret;
		}
	case GCONF_VALUE_PAIR:
		{
			char *s1, *s2;
			char *ret;
			GString *gs = g_string_new (NULL);

			s1 = pong_string_from_gconf_value (gconf_value_get_car (value));
			s2 = pong_string_from_gconf_value (gconf_value_get_cdr (value));

			g_string_append (gs, s1);
			g_string_append (gs, " ");
			g_string_append (gs, s2);

			g_free (s1);
			g_free (s2);

			ret = gs->str;
			g_string_free (gs, FALSE);
			return ret;
		}
	default:
		g_warning (_("Unsupported gconf value type to convert to text"));
		return NULL;
	}

}

/* Do strcasecmp but ignore locale */
int
pong_strcasecmp_no_locale (const char *s1, const char *s2)
{
	int i;

	/* Error, but don't make them equal then */
	g_return_val_if_fail (s1 != NULL, G_MAXINT);
	g_return_val_if_fail (s2 != NULL, G_MININT);

	for (i = 0; s1[i] != '\0' && s2[i] != '\0'; i++) {
		char a = s1[i];
		char b = s2[i];

		if (a >= 'A' && a <= 'Z')
			a -= 'A' - 'a';
		if (b >= 'A' && b <= 'Z')
			b -= 'A' - 'a';

		if (a < b)
			return -1;
		else if (a > b)
			return 1;
	}

	/* find out which string is smaller */
	if (s2[i] != '\0')
		return -1; /* s1 is smaller */
	else if (s1[i] != '\0')
		return 1; /* s2 is smaller */
	else
		return 0; /* equal */
}

/* Compare tags ignoring a leading underscore */
gboolean
pong_tags_equal (const char *tag1, const char *tag2)
{
	if (tag1 == NULL && tag2 == NULL)
		return TRUE;
	else if (tag1 == NULL || tag2 == NULL)
		return FALSE;

	g_assert (tag1 != NULL && tag2 != NULL);

	/* skip a leading underscore */
	if (tag1[0] == '_')
		tag1++;
	if (tag2[0] == '_')
		tag2++;

	if (pong_strcasecmp_no_locale (tag1, tag2) == 0)
		return TRUE;
	else
		return FALSE;
}

char *
pong_find_file (const char *filename, const GList *directories)
{
	const char *path;
	char *s, *ss;
	char **pathv;
	int i;
	const GList *li;

	if (pong_file_exists (filename))
		return g_strdup (filename);

	/* an absolute path is just checked */
	if (g_path_is_absolute (filename))
		return NULL;

	for (li = directories; li != NULL; li = li->next) {
		s = g_concat_dir_and_file (li->data, filename);
		if (pong_file_exists (s))
			return s;
		g_free (s);
	}

	s = gnome_datadir_file (filename);
	if (s != NULL)
		return s;

	ss = g_concat_dir_and_file (g_get_prgname (), filename);
	s = gnome_datadir_file (ss);
	g_free (ss);
	if (s != NULL)
		return s;

	path = g_getenv ("PONG_PATH");
	if (path != NULL) {
		pathv = g_strsplit (path, ":", 0);
		for (i = 0; pathv[i] != NULL; i++) {
			s = g_concat_dir_and_file (pathv[i], filename);
			if (pong_file_exists (s)) {
				g_strfreev (pathv);
				return s;
			}
			g_free (s);
		}
		g_strfreev (pathv);
	}

	path = g_getenv ("GNOME_PATH");
	if (path != NULL) {
		pathv = g_strsplit (path, ":", 0);
		for (i = 0; pathv[i] != NULL; i++) {
			s = g_strconcat (pathv[i], "/share/", filename, NULL);
			if (pong_file_exists (s)) {
				g_strfreev (pathv);
				return s;
			}
			g_free (s);
			s = g_strconcat (pathv[i], "/share/",
					 g_get_prgname (), "/",
					filename, NULL);
			if (pong_file_exists (s)) {
				g_strfreev (pathv);
				return s;
			}
			g_free (s);
		}
		g_strfreev (pathv);
	}

	return NULL;
}

static GList *numeric_locale_stack = NULL;
 
/**
 * pong_i18n_push_c_numeric_locale:
 *
 * Description:  Pushes the current LC_NUMERIC locale onto a stack, then 
 * sets LC_NUMERIC to "C".  This way you can safely read write flaoting
 * point numbers all in the same format.  You should make sure that
 * code between #pong_i18n_push_c_numeric_locale and
 * #pong_i18n_pop_c_numeric_locale doesn't do any setlocale calls or locale
 * may end up in a strange setting.  Also make sure to always pop the
 * c numeric locale after you've pushed it.
 **/
void
pong_i18n_push_c_numeric_locale (void)
{
	char *current;

	current = g_strdup (setlocale (LC_NUMERIC, NULL));
	numeric_locale_stack = g_list_prepend (numeric_locale_stack,
					       current);
	setlocale (LC_NUMERIC, "C");
}

/**
 * pong_i18n_pop_c_numeric_locale:
 *
 * Description:  Pops the last LC_NUMERIC locale from the stack (where
 * it was put with #pong_i18n_push_c_numeric_locale).  Then resets the
 * current locale to that one.
 **/
void
pong_i18n_pop_c_numeric_locale (void)
{
	char *old;

	if (numeric_locale_stack == NULL)
		return;

	old = numeric_locale_stack->data;

	setlocale (LC_NUMERIC, old);

	numeric_locale_stack = g_list_remove (numeric_locale_stack, old);

	g_free (old);
}

/* only supports primitives */
gboolean
pong_string_value_equal (const char *string, GConfValue *value)
{
	switch(value->type) {
	case GCONF_VALUE_STRING:
		if (strcmp (gconf_value_get_string (value), string) == 0)
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_INT:
		if (gconf_value_get_int (value) == atoi (string))
			return TRUE;
		else
			return FALSE;
	case GCONF_VALUE_FLOAT:
		{
			double foo;

			pong_i18n_push_c_numeric_locale ();
			foo = atof (string);
			pong_i18n_pop_c_numeric_locale ();

			/* FIXME: EEEEK! float comparison !!!! */
			if (gconf_value_get_float (value) == foo)
				return TRUE;
			else
				return FALSE;
		}
	case GCONF_VALUE_BOOL:
		/* The reason for the ?: is that comparing bools
		 * directly is evil */
		if ((gconf_value_get_bool (value) ? 1 : 0) ==
		    (pong_bool_from_string (string) ? 1 : 0))
			return TRUE;
		else
			return FALSE;
	default:
		g_assert_not_reached();
		break;
	}
	return FALSE;
}

void
_pong_signal_connect_while_alive (GObject     *object,
				  const gchar *signal,
				  GCallback    func,
				  gpointer     func_data,
				  GObject     *alive_object)
{
	GClosure *closure;

	g_return_if_fail (G_IS_OBJECT (object));

	closure = g_cclosure_new (func, func_data, NULL);
	g_object_watch_closure (G_OBJECT (alive_object), closure);
	g_signal_connect_closure_by_id (object,
					g_signal_lookup (signal, G_OBJECT_TYPE (object)), 0,
					closure,
					FALSE);
}

void
_pong_signal_connect_object_while_alive (GObject      *object,
					 const gchar  *signal,
					 GCallback     func,
					 GObject      *alive_object)
{
  g_return_if_fail (G_IS_OBJECT (object));
  
  g_signal_connect_closure_by_id (object,
				  g_signal_lookup (signal, G_OBJECT_TYPE (object)), 0,
				  g_cclosure_new_object_swap (func, G_OBJECT (alive_object)),
				  FALSE);
}

