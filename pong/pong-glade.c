/* PonG: Glade support
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>

#include "pong-i18n.h"

#include "pongutil.h"

#include "pong-xml.h"
#include "pong-ui-internal.h"
#include "pong-ui-plug-glade.h"

#include "pong-glade.h"

/* directories to look up glade files in */
GList *pong_glade_directories = NULL;

/* function that gets new GladeXML objects */
PongGetGladeXML pong_glade_default_getter = NULL;
gpointer pong_glade_default_getter_data = NULL;
GDestroyNotify pong_glade_default_getter_data_destroy = NULL;

/* the gettext domain for glade */
char *pong_glade_gettext_domain = NULL;

/**
 * pong_glade_init:
 *
 * Description:  Initializes pong and adds the pong plug, also
 * initializes glade with gnome support.
 *
 * Returns:  %TRUE on success or %FALSE on failiure
 **/
gboolean
pong_glade_init (void)
{
	static gboolean initialized = FALSE;

	if ( ! pong_init ())
		return FALSE;

	if ( ! initialized) {
		pong_add_plug_creator (pong_ui_plug_glade_new);

		glade_gnome_init ();

		initialized = TRUE;
	}

	return TRUE;
}

/**
 * pong_add_glade_directory:
 * @directory:  directory with glade files
 *
 * Description:  Add a standard glade directory to search for
 * glade files.
 **/
void
pong_add_glade_directory (const char *directory)
{
	pong_glade_directories = g_list_append (pong_glade_directories,
						g_strdup (directory));
}

/**
 * pong_find_glade_file:
 * @file:  glade filename
 *
 * Description:  Finds a glade file in the same directories that
 * pong would look in.  A utility if you use glade elsewhere in your
 * application to make using glade simpler.
 *
 * Returns:  Newly allocated string with the full path, or %NULL
 * if not found.
 **/
char *
pong_find_glade_file (const char *file)
{
	g_return_val_if_fail (file != NULL, NULL);

	return pong_find_file (file, pong_glade_directories);
}

/**
 * pong_glade_set_gettext_domain:
 * @gettext_domain:  gettext domain
 *
 * Description:  Sets the default gettext domain that PonG should use for
 * loading glade files.
 **/
void
pong_glade_set_gettext_domain (const char *gettext_domain)
{
	g_free (pong_glade_gettext_domain);
	pong_glade_gettext_domain = g_strdup (gettext_domain);
}

/* 
 * Glade files in memory, support
 */

/**
 * pong_set_default_glade_getter:
 * @getter:  getter function
 * @data:  data to getter function
 * @destroy_notify:  destroy notification for data
 *
 * Description:  Set a function that gets a glade file XML,
 * this way you can get glade files from memory or wherever
 * you wish instead of having pong look for them and load them.
 * This can be overriden on a per pong object basis.
 **/
void
pong_set_default_glade_getter (PongGetGladeXML getter,
			       gpointer data,
			       GDestroyNotify destroy_notify)
{
	if (pong_glade_default_getter_data_destroy != NULL)
		pong_glade_default_getter_data_destroy
			(pong_glade_default_getter_data);

	pong_glade_default_getter = getter;
	pong_glade_default_getter_data = data;
	pong_glade_default_getter_data_destroy = destroy_notify;
}
