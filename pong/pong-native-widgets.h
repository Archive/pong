/* PonG: include all native widgets
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONG_NATIVE_WIDGETS_H
#define PONG_NATIVE_WIDGETS_H

#include "pong-radio-group.h"
#include "pong-option-menu.h"
#include "pong-check-group.h"
#include "pong-spin-button.h"
#include "pong-color-picker.h"
#include "pong-slider.h"
#include "pong-list-entry.h"

#endif /* PONG_NATIVE_WIDGETS_H */
