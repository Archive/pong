/* PonG: the parser using SAX interface
 * Author: George Lebl
 * (c) 1999 Free Software Foundation
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONGPARSER_H
#define PONGPARSER_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _PongFile PongFile;

PongFile *	pong_file_load_xml		(const char *file);
PongFile *	pong_file_load_xml_no_lookup	(const char *file);
PongFile *	pong_file_load_xml_from_memory	(const char *buffer,
						 int size);
PongFile *	pong_file_ref			(PongFile *gf);
void		pong_file_unref			(PongFile *gf);

#ifdef __cplusplus
}
#endif


#endif /* PONGPARSER_H */
