%at{
/* PonG: the Manual UI object
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.99.2

%{
#include "config.h"
#include <gnome.h>

#include "pong-i18n.h"

#include "pong-ui.h"

#include "pong-ui-manual.h"
#include "pong-ui-manual-private.h"

%}

class Pong:UI:Manual from Pong:UI {

	private GHashTable *hash_table = 
		{g_hash_table_new (g_str_hash, g_str_equal)}
	destroy {
		g_hash_table_foreach (VAR, (GHFunc)g_free, NULL);
		g_hash_table_destroy (VAR);
	};

	override (Pong:UI)
	GtkWidget *
	get_widget (Pong:UI *gi (check null type),
		    const char *name (check null))
	{
		PongUIManual *self = SELF(gi);

		return g_hash_table_lookup (self->_priv->hash_table, name);
	}

	public
	PongUI *
	new (void)
	{
		return PONG_UI(GET_NEW);
	}

	/**
	 * add_widget:
	 * @self: the #PongUIManual object
	 * @name: the name of the widget being added
	 * @widget: #GtkWidget pointer of the widget
	 *
	 * Description: Add a pointer to a widget and a name under
	 * which it is stored and will be refered to later.  All widgets
	 * that pong needs will need a name.
	 **/
	public
	void
	add_widget (self, const char *name (check null),
	            Gtk:Widget *widget (check null type))
	{
		g_hash_table_insert (self->_priv->hash_table,
				     g_strdup (name),
				     widget);
	}
}
