/* PonG: Glade support
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef PONG_GLADE_H
#define PONG_GLADE_H

#include "pong.h"

#include <glade/glade.h>

/* This is the "obscoleted" way to do glade based dialogs, use ui-internal and
 * ui-plug-glade,  You can still use it but it's NotSoNice(tm) */
#include "pong-ui-glade.h"

#include "pong-ui-plug-glade.h"

#ifdef __cplusplus
extern "C" {
#endif

/* init glade support, all new PongXML's will be created
 * with glade support on by default after this */
gboolean	pong_glade_init			(void);

/* add a directory to the path in which to look for glade files */
void		pong_add_glade_directory	(const char *directory);

/* Find a glade file in the same directories as pong looks in, utility
 * for glade using audience, a newly allocated path is returned or NULL
 * if not found. */
char *		pong_find_glade_file		(const char *file);


/*
 * This is the interface for using PongUIPlugGlade with glade files in memory.
 * This function will be used to first query all glade file elements from .pong
 * files (using the panel interface).  If the function is NULL it will try to
 * look them up on disk as normally.  This function can return NULL in which
 * case they will again be loaded from disk.
 */
void		pong_set_default_glade_getter	(PongGetGladeXML getter,
						 gpointer data,
						 GDestroyNotify destroy_notify);

/* Set the domain with which glade files are loaded */
void		pong_glade_set_gettext_domain	(const char *gettext_domain);

#ifdef __cplusplus
}
#endif

#endif /* PONG_GLADE_H */
