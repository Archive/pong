%at{
/* PonG: The pane widget
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2002 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.99.2
%{
#include "config.h"
#include <gnome.h>

#include "pong-i18n.h"

#include "pong-pane-widget.h"
#include "pong-pane-widget-private.h"

static void clist_select_row(GtkCList *clist, int row, int column, GdkEvent *event, gpointer data);

/* the number of panes we are still a notebook in automatic mode */
#define NBOOK_PANES 5

%}

enum PONG_PANE_WIDGET_MODE {
	NOTEBOOK,
	LIST,
	AUTOMATIC, /* switches between notebook and list
		    * depending on the number of panes
		    * if more then 5 panes uses list */
	LAST
} Pong:Pane:Widget:Mode;

class Pong:Pane:Widget from Gtk:HBox {

	private GtkWidget *notebook = NULL;
	private GtkWidget *list = NULL;
	private int n_children = 0;

	private PongPaneWidgetMode pane_mode;

	property ENUM pane_mode
		(nick = _("Pane mode"),
		 blurb = _("Pane mode"),
		 enum_type = PONG_TYPE_PANE_WIDGET_MODE)
		set {
			self_set_pane_mode (self, g_value_get_enum (VAL));
		} get {
			g_value_set_enum (VAL, self->_priv->pane_mode);
		};

	init(self)
	{
		self->_priv->pane_mode = PONG_PANE_WIDGET_MODE_AUTOMATIC;

		self->_priv->list = gtk_clist_new(1);
		gtk_clist_set_column_auto_resize(GTK_CLIST(self->_priv->list),
						 0, TRUE);
		gtk_clist_set_selection_mode(GTK_CLIST(self->_priv->list),
					     GTK_SELECTION_BROWSE);
		gtk_signal_connect(GTK_OBJECT(self->_priv->list), "select_row",
				   GTK_SIGNAL_FUNC(clist_select_row), self);
		gtk_box_pack_start(GTK_BOX(self), self->_priv->list,
				   FALSE, FALSE, 0);

		self->_priv->notebook = gtk_notebook_new();
		gtk_notebook_set_show_border(GTK_NOTEBOOK(self->_priv->notebook),
					     TRUE);
		gtk_notebook_set_show_tabs(GTK_NOTEBOOK(self->_priv->notebook),
					   TRUE);
		gtk_widget_show(self->_priv->notebook);
		gtk_box_pack_start(GTK_BOX(self), self->_priv->notebook,
				   TRUE, TRUE, 0);
		gtk_box_set_spacing(GTK_BOX(self), GNOME_PAD_SMALL);
	}

	public
	GtkWidget *
	new(void)
	{
		return GTK_WIDGET(GET_NEW);
	}

	private
	void
	setup_internal_pane_mode (self, PongPaneWidgetMode pane_mode)
	{
		switch (pane_mode) {
		case PONG_PANE_WIDGET_MODE_NOTEBOOK:
			gtk_widget_hide(self->_priv->list);
			gtk_notebook_set_show_tabs(GTK_NOTEBOOK(self->_priv->notebook),
						   TRUE);
			gtk_notebook_set_show_border(GTK_NOTEBOOK(self->_priv->notebook),
						     TRUE);
			break;
		case PONG_PANE_WIDGET_MODE_LIST:
			gtk_widget_show(self->_priv->list);
			gtk_notebook_set_show_tabs(GTK_NOTEBOOK(self->_priv->notebook),
						   FALSE);
			gtk_notebook_set_show_border(GTK_NOTEBOOK(self->_priv->notebook),
						     FALSE);
			break;
		default:
			g_assert_not_reached();
			break;
		}
	}

	public
	void
	set_pane_mode (self, PongPaneWidgetMode pane_mode)
	{
		g_return_if_fail(pane_mode >= 0);
		g_return_if_fail(pane_mode < PONG_PANE_WIDGET_MODE_LAST);

		self->_priv->pane_mode = pane_mode;
		if (pane_mode == PONG_PANE_WIDGET_MODE_AUTOMATIC) {
			if (self->_priv->n_children > NBOOK_PANES) {
				self_setup_internal_pane_mode
					(self, PONG_PANE_WIDGET_MODE_LIST);
			} else {
				self_setup_internal_pane_mode
					(self, PONG_PANE_WIDGET_MODE_NOTEBOOK);
			}
		} else {
			self_setup_internal_pane_mode (self, pane_mode);
		}
	}

	public
	void
	append_page(self, Gtk:Widget *child (check null type),
		    const char *label (check null))
	{
		GtkNotebook *nbook = GTK_NOTEBOOK(self->_priv->notebook);
		GtkCList *clist = GTK_CLIST(self->_priv->list);
		GtkWidget *l, *ml;
		char *text[1];

		l = gtk_label_new(label);
		gtk_widget_show(l);
		ml = gtk_label_new(label);
		gtk_widget_show(ml);
		gtk_notebook_append_page_menu(nbook, child, l, ml);

		text[0] = (char *)label;
		gtk_clist_append(clist, text);

		self->_priv->n_children ++;

		/* if the mode is ripe to change */
		if (self->_priv->pane_mode == PONG_PANE_WIDGET_MODE_AUTOMATIC &&
		    self->_priv->n_children == NBOOK_PANES + 1) {
			self_setup_internal_pane_mode
				(self, PONG_PANE_WIDGET_MODE_LIST);
		}
	}

}

%{
static void
clist_select_row(GtkCList *clist, int row, int column,
		 GdkEvent *event, gpointer data)
{
	Self *self = SELF(data);
	GtkNotebook *nbook = GTK_NOTEBOOK(self->_priv->notebook);

	gtk_notebook_set_page(nbook, row);
}
%}
