/* PonG: widget utility routines
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include "pong-i18n.h"
#include "pong-strings.h"

#include "pongutil.h"

#include "pongwidgetutil.h"

void
pong_gtk_editable_set_double (GtkEditable *entry, double number)
{
	char *s = g_strdup_printf ("%g", number);
	pong_gtk_editable_set_text (entry, s);
	g_free (s);
}

void
pong_gnome_entry_set_double (GnomeEntry *gentry, double number)
{
	GtkWidget *entry = gnome_entry_gtk_entry (gentry);
	pong_gtk_editable_set_double (GTK_EDITABLE (entry), number);
}

void
pong_gtk_editable_set_long (GtkEditable *entry, long number)
{
	char *s = g_strdup_printf ("%ld", number);
	pong_gtk_editable_set_text (entry, s);
	g_free (s);
}

void
pong_gnome_entry_set_long (GnomeEntry *gentry, long number)
{
	GtkWidget *entry = gnome_entry_gtk_entry (gentry);
	pong_gtk_editable_set_long (GTK_EDITABLE  (entry), number);
}

void
pong_gtk_editable_set_text (GtkEditable *entry, const char *text)
{
	int pos;
	gtk_editable_delete_text (entry, 0, -1);
	pos = 0;
	gtk_editable_insert_text (entry, text, strlen (text), &pos);
}

void
pong_gnome_entry_set_text (GnomeEntry *gentry, const char *text)
{
	GtkWidget *entry = gnome_entry_gtk_entry (gentry);
	pong_gtk_editable_set_text (GTK_EDITABLE  (entry), text);
}

void
pong_gnome_file_entry_set_text (GnomeFileEntry *gentry, const char *text)
{
	GtkWidget *entry = gnome_file_entry_gtk_entry (gentry);
	pong_gtk_editable_set_text (GTK_EDITABLE (entry), text);
}

void
pong_gnome_pixmap_entry_set_text (GnomePixmapEntry *gentry, const char *text)
{
	GtkWidget *entry = gnome_pixmap_entry_gtk_entry (gentry);
	pong_gtk_editable_set_text (GTK_EDITABLE (entry), text);
}

void
pong_gnome_color_picker_set_text (GnomeColorPicker *picker,
				  const char *text)
{
	int r, g, b, a;
	GdkColor color = {0};

	if (sscanf (text, "#%02x%02x%02x%02x", &r, &g, &b, &a) == 4) {
		gnome_color_picker_set_i8 (picker, r, g, b, a);
	} else if (sscanf (text, "#%02x%02x%02x", &r, &g, &b) == 3) {
		gnome_color_picker_set_i8 (picker, r, g, b, 255);
	} else if (gdk_color_parse (text, &color)) {
		gnome_color_picker_set_i16 (picker, color.red,
					    color.green, color.blue,
					    65535);
	} else {
		g_warning (_("Can't set color to '%s'\n"), text);
	}
}

void
pong_gnome_color_picker_set_long (GnomeColorPicker *picker,
				  long number)
{
	/* FIXME: make sure this is correct */
	if (gnome_color_picker_get_use_alpha (picker)) {
		gnome_color_picker_set_i8 (picker,
					   (0x255<<24) & number,
					   (0x255<<16) & number,
					   (0x255<<8) & number,
					   (0x255) & number);
	} else {
		gnome_color_picker_set_i8 (picker,
					   (0x255<<16) & number,
					   (0x255<<8) & number,
					   (0x255) & number,
					   255);
	}
}

char *
pong_gtk_editable_get_text (GtkEditable *entry)
{
	return gtk_editable_get_chars (entry, 0, -1);
}

double
pong_gtk_editable_get_double (GtkEditable *entry)
{
	double ret;
	char *s = pong_gtk_editable_get_text (entry);
	ret = atof (s);
	g_free (s);
	return ret;
}

long
pong_gtk_editable_get_long (GtkEditable *entry)
{
	long ret;
	char *s = pong_gtk_editable_get_text (entry);
	ret = atol (s);
	g_free (s);
	return ret;
}

double
pong_gnome_entry_get_double (GnomeEntry *gentry)
{
	GtkWidget *entry = gnome_entry_gtk_entry (gentry);
	return pong_gtk_editable_get_double (GTK_EDITABLE (entry));
}

long
pong_gnome_entry_get_long (GnomeEntry *gentry)
{
	GtkWidget *entry = gnome_entry_gtk_entry (gentry);
	return pong_gtk_editable_get_long (GTK_EDITABLE (entry));
}

const char *
pong_gnome_entry_peek_text (GnomeEntry *gentry)
{
	GtkWidget *entry = gnome_entry_gtk_entry (gentry);
	return gtk_entry_get_text (GTK_ENTRY (entry));
}

const char *
pong_gnome_file_entry_peek_text (GnomeFileEntry *gentry)
{
	GtkWidget *entry = gnome_file_entry_gtk_entry (gentry);
	return gtk_entry_get_text (GTK_ENTRY (entry));
}

const char *
pong_gnome_pixmap_entry_peek_text (GnomePixmapEntry *gentry)
{
	GtkWidget *entry = gnome_pixmap_entry_gtk_entry (gentry);
	return gtk_entry_get_text (GTK_ENTRY (entry));
}

const char *
pong_gnome_icon_entry_peek_text (GnomeIconEntry *gentry)
{
	GtkWidget *entry = gnome_icon_entry_gtk_entry (gentry);
	return gtk_entry_get_text (GTK_ENTRY (entry));
}

char *
pong_gnome_color_picker_get_text (GnomeColorPicker *picker)
{
	guint8 r, g, b, a;

	gnome_color_picker_get_i8 (picker, &r, &g, &b, &a);

	if (gnome_color_picker_get_use_alpha (picker))
		return g_strdup_printf ("#%02x%02x%02x%02x",
					(guint)r, (guint)g, (guint)b,
					(guint)a);
	else
		return g_strdup_printf ("#%02x%02x%02x",
					(guint)r, (guint)g, (guint)b);
}

long
pong_gnome_color_picker_get_long (GnomeColorPicker *picker)
{
	guint8 r, g, b, a;

	gnome_color_picker_get_i8 (picker, &r, &g, &b, &a);

	if (gnome_color_picker_get_use_alpha (picker))
		return (((int)r)<<24) + (((int)g)<<16) +
			(((int)b)<<8) + (int)a;
	else
		return (((int)r)<<16) + (((int)g)<<8) + (int)b;
}

void
pong_gtk_range_set_double (GtkRange *range, double number)
{
	GtkAdjustment *adj = gtk_range_get_adjustment (range);
	if (adj != NULL)
		gtk_adjustment_set_value (adj, number);
}

void
pong_gtk_range_set_long (GtkRange *range, long number)
{
	GtkAdjustment *adj = gtk_range_get_adjustment (range);
	if (adj != NULL)
		gtk_adjustment_set_value (adj, number);
}

double
pong_gtk_range_get_double (GtkRange *range)
{
	GtkAdjustment *adj = gtk_range_get_adjustment (range);
	if (adj != NULL)
		return adj->value;
	else
		return 0.0;
}

long
pong_gtk_range_get_long (GtkRange *range)
{
	GtkAdjustment *adj = gtk_range_get_adjustment (range);
	if (adj != NULL)
		return adj->value;
	else
		return 0;
}

gboolean
pong_set_object_argument (GtkObject *object,
			  const char *arg_name,
			  const char *value)
{
	GtkArgInfo *info;
	char *error;
	GtkArg arg;

	g_return_val_if_fail (object != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_OBJECT (object), FALSE);
	g_return_val_if_fail (arg_name != NULL, FALSE);
	g_return_val_if_fail (value != NULL, FALSE);

	error = gtk_object_arg_get_info (GTK_OBJECT_TYPE (object),
					 arg_name, &info);
	if (error != NULL) {
		g_warning (_("Cannot set object argument: %s"), error);
		g_free (error);
		return FALSE;
	}

	arg.type = info->type;
	/* It is ok to cast here */
	arg.name = (char *)arg_name;

	switch (info->type) {
	case GTK_TYPE_CHAR:
		if (!value[0]) {
			g_warning (_("Invalid value being set"));
			return FALSE;
		}
		arg.d.char_data = value[0];
		break;
	case GTK_TYPE_UCHAR:
		if (!value[0]) {
			g_warning (_("Invalid value being set"));
			return FALSE;
		}
		arg.d.uchar_data = *((guchar *)value);
		break;
	case GTK_TYPE_BOOL:
		if (value[0] == 't' || value[0] == 'T' ||
		    value[0] == 'y' || value[0] == 'Y' ||
		    atoi (value) != 0)
			arg.d.bool_data = TRUE;
		else
			arg.d.bool_data = FALSE;
		break;
	case GTK_TYPE_INT:
		arg.d.int_data = atoi (value);
		break;
	case GTK_TYPE_UINT:
		arg.d.uint_data = strtoul (value, NULL, 10);
		break;
	case GTK_TYPE_LONG:
		arg.d.long_data = strtol (value, NULL, 10);
		break;
	case GTK_TYPE_ULONG:
		arg.d.ulong_data = strtoul (value, NULL, 10);
		break;
	case GTK_TYPE_FLOAT:
		pong_i18n_push_c_numeric_locale ();
		arg.d.float_data = atof (value);
		pong_i18n_pop_c_numeric_locale ();
		break;
	case GTK_TYPE_DOUBLE:
		pong_i18n_push_c_numeric_locale ();
		arg.d.double_data = atof (value);
		pong_i18n_pop_c_numeric_locale ();
		break;
	case GTK_TYPE_STRING:
		arg.d.string_data = (char *)value;
		break;
	case GTK_TYPE_ENUM:
		/* XXX: this is wrong, we may need to understand some basic
		 * enums I guess, but even that is somewhat wrong, and
		 * opens a pandora's box. */
		arg.d.int_data = atoi (value);
		break;
	
	default:
		g_warning (_("Unsupported argument type being set"));
		return FALSE;
	}

	gtk_object_arg_set (object, &arg, info);

	return TRUE;
}

static void
object_destroy_notify (gpointer data)
{
	if (data != NULL) {
		gtk_object_unref (data);
	}
}

static void
dialog_destroyed (GtkWidget *w, gpointer data)
{
	GtkWidget *spin = data;
	gtk_spin_button_update (GTK_SPIN_BUTTON (spin));
}

/* connect this to spin buttons to make them behave sanely */
void
pong_spin_button_mapped (GtkWidget *w)
{
	GtkWidget *toplevel;
	GtkWidget *oldtoplevel = gtk_object_get_data (GTK_OBJECT (w),
						      PONG_S_TopLevel);


	/* On map check toplevel and rebind ourselves if it changed */
	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (w));

	if (toplevel == oldtoplevel)
		return;

	if (oldtoplevel != NULL) {
		gtk_signal_disconnect_by_data
			(GTK_OBJECT (oldtoplevel), w);
	}

	gtk_object_ref (GTK_OBJECT (toplevel));
	gtk_object_set_data_full (GTK_OBJECT (w), PONG_S_TopLevel,
				  toplevel, object_destroy_notify);

	gtk_signal_connect_while_alive
		(GTK_OBJECT (toplevel), "destroy",
		 GTK_SIGNAL_FUNC (dialog_destroyed),
		 w,
		 GTK_OBJECT (w));
}

