%at{
/* PonG: the Internal Glade GOB UI object
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2002 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.99.2

%{
#include "config.h"
#include <gnome.h>

#include <glade/glade-xml.h>

#include "pong-i18n.h"

#include "pongpane.h"
#include "pongutil.h"
#include "pong-strings.h"

#include "pong-xml.h"
#include "pong-ui.h"
#include "pong-ui-plug.h"

#include "pong-ui-plug-glade.h"
#include "pong-ui-plug-glade-private.h"

extern GList *pong_glade_directories;

extern PongGetGladeXML pong_glade_default_getter;
extern gpointer pong_glade_default_getter_data;
extern GDestroyNotify pong_glade_default_getter_data_destroy;

extern char *pong_glade_gettext_domain;

typedef struct _File File;
struct _File {
	char *name;
	GladeXML *gladexml;
};

static void file_destroy (File *file);

%}

%h{
/* If you need to load files from memory, you need to set up this function
 * to get the correct GladeXML */
typedef GladeXML * (* PongGetGladeXML) (const char *filename,
					const char *root,
					gpointer data);
%}

class Pong:UI:Plug:Glade from Pong:UI:Plug {

	private PongGetGladeXML glade_getter = NULL;
	private gpointer glade_getter_data = NULL
		destroy {
			if (self->_priv->glade_getter_data_destroy != NULL) {
				self->_priv->glade_getter_data_destroy (VAR);
			}
		};
	private GDestroyNotify glade_getter_data_destroy = NULL;

	private GList *files = NULL
		destroy {
			g_list_foreach (VAR, (GFunc)file_destroy, NULL);
			g_list_free (VAR);
		};

	/* the gettext domain to use */
	private char *gettext_domain = NULL
		destroywith g_free;
	private GList *directories = NULL
		destroy {
			g_list_foreach (VAR, (GFunc)g_free, NULL);
			g_list_free (VAR);
		};

	override (Pong:UI:Plug)
	GtkWidget *
	get_widget (Pong:UI:Plug *ppp (check null type),
		    const char *name (check null))
		onerror NULL
	{
		Self *self = SELF (ppp);
		GtkWidget *widget = NULL;
		GList *li;
		char *s = g_strdup (name);
		char *widgetname;
		char *gladename = NULL;

		widgetname = strchr (s, '#');
		if (widgetname != NULL) {
			gladename = s;
			*widgetname = '\0';
			widgetname++;
		} else {
			widgetname = s;
		}

		for (li = self->_priv->files; li != NULL; li = li->next) {
			File *file = li->data;
			if (gladename == NULL ||
			    strcmp (file->name, gladename) == 0) {
				widget = glade_xml_get_widget (file->gladexml, widgetname);
				if (widget != NULL)
					break;
			}
		}

		g_free (s);

		return widget;
	}

	override (Pong:UI:Plug)
	gboolean
	supports (Pong:UI:Plug *ppp (check null type), const char *plug_name (check null))
		onerror FALSE
	{
		if (pong_strcasecmp_no_locale (plug_name, PONG_S_Glade) == 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	override (Pong:UI:Plug)
	GtkWidget *
	make_widget (Pong:UI:Plug *ppp (check null type),
		     const char *name (check null),
		     const char *file (check null),
		     const char *widget)
		onerror NULL
	{
		Self *self = SELF (ppp);
		File *f;
		GladeXML *xml;
		char *found_file = NULL;

		if (widget == NULL) {
			g_warning (_("No widget specified for glade interface '%s'"),
				   name);
			return NULL;
		}

		xml = NULL;

		if (self->_priv->glade_getter != NULL) {
			xml = self->_priv->glade_getter (file, widget, self->_priv->glade_getter_data);
		}

		if (xml == NULL &&
		    pong_glade_default_getter != NULL) {
			xml = pong_glade_default_getter (file, widget, pong_glade_default_getter_data);
		}

		if (xml == NULL) {
			char *gettext_domain;

			found_file = pong_find_file (file, pong_glade_directories);

			if (found_file == NULL) {
				g_warning(_("Cannot find glade file '%s'"), file);
				return NULL;
			}

			if (self->_priv->gettext_domain != NULL)
				gettext_domain = self->_priv->gettext_domain;
			else
				gettext_domain = pong_glade_gettext_domain;

			xml = glade_xml_new_with_domain (found_file, widget,
							 gettext_domain);
		}

		if (xml == NULL) {
			if (found_file != NULL) {
				g_warning (_("Cannot load glade file: %s (%s)"),
					   file, found_file);
			} else {
				g_warning (_("Cannot load glade file: %s"),
					   file);
			}
			g_free (found_file);
			return NULL;
		}

		g_free (found_file);

		f = g_new0 (File, 1);

		f->name = g_strdup (name);
		f->gladexml = xml;

		self->_priv->files = g_list_prepend (self->_priv->files, f);

		return glade_xml_get_widget (xml, widget);
	}

	public
	void
	set_glade_getter (self,
			  PongGetGladeXML getter,
			  gpointer data,
			  GDestroyNotify destroy_notify)
	{
		if (self->_priv->glade_getter_data_destroy != NULL)
			self->_priv->glade_getter_data_destroy
				(self->_priv->glade_getter_data);

		self->_priv->glade_getter = getter;
		self->_priv->glade_getter_data = data;
		self->_priv->glade_getter_data_destroy = destroy_notify;
	}

	public
	void
	set_glade_gettext_domain (self,
				  const char *gettext_domain)
	{
		g_free (self->_priv->gettext_domain);
		self->_priv->gettext_domain = g_strdup (gettext_domain);
	}

	public
	void
	set_directories (self, const GList *directories)
	{
		GList *li;

		g_list_foreach (self->_priv->directories, (GFunc)g_free, NULL);
		g_list_free (self->_priv->directories);

		/* bad bad glib, we need to cast */
		self->_priv->directories =
			g_list_copy ((GList *)directories);

		for (li = self->_priv->directories;
		     li != NULL;
		     li = li->next) {
			li->data = g_strdup (li->data);
		}
	}
		
	public
	PongUIPlug *
	new (void)
	{
		return (PongUIPlug *) GET_NEW;
	}
}

%{
static void
file_destroy (File *file)
{
	g_return_if_fail (file != NULL);

	g_free (file->name);
	file->name = NULL;

	if (file->gladexml != NULL)
		g_object_unref (G_OBJECT (file->gladexml));
	file->gladexml = NULL;

	g_free (file);
}
%}
