/* PonG: the pane interface
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "config.h"

/* So that we still support GtkText, in case some app uses it */
#define GTK_ENABLE_BROKEN

#include <gnome.h>
#include <gmodule.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "pong-i18n.h"

#include "pong.h"
#include "pongelement.h"
#include "pongparser.h"
#include "pongparser-private.h"
#include "pongwidgetutil.h"
#include "pongutil.h"

#include "pong-ui.h"
#include "pong-ui-plug.h"
#include "pong-ui-internal.h"

#include "pong-widget-interface.h"
#include "pong-pane-widget.h"
#include "pong-native-widgets.h"
#include "pong-strings.h"

#include "pongpane.h"

/*
 * Type translation
 */
static struct {
	char *name;
	PongWidgetType type;
} type_table[] = {
	/* This is not really an entry/property widget, but one
	 * we support by default anyway, to allow simple looking
	 * label widgets */
	{ "GtkLabel", PONG_GTKLABEL },

	{ "GtkSpinButton", PONG_GTKSPINBUTTON },
	{ "GtkEntry", PONG_GTKENTRY },
	{ "GtkText", PONG_GTKTEXT },
	{ "GtkCombo", PONG_GTKCOMBO },
	{ "GnomeEntry", PONG_GNOMEENTRY },
	{ "GnomeFileEntry", PONG_GNOMEFILEENTRY },
	{ "GnomePixmapEntry", PONG_GNOMEPIXMAPENTRY },
	{ "GnomeIconEntry", PONG_GNOMEICONENTRY },
	{ "GtkToggleButton", PONG_GTKTOGGLEBUTTON },
	{ "GtkCheckButton", PONG_GTKCHECKBUTTON },
	{ "GnomeColorPicker", PONG_GNOMECOLORPICKER },
	{ NULL, 0 }
};

static GHashTable *type_hash = NULL;
static GModule *allsymbols = NULL;

/* Gob functions for types */
static char *
remove_sep (const char *base)
{
	char *p;
	char *s = g_strdup (base);
	while ((p = strchr (s, ':')) != NULL)
		strcpy (p, p+1);
	return s;
}

static char *
replace_sep (const char *base, char r)
{
	char *p;
	char *s = g_strdup (base);
	while ((p = strchr (s, ':')) != NULL)
		*p = r;
	if (*s == r) {
		p = g_strdup (s+1);
		g_free (s);
		return p;
	}
	return s;
}

static void
type_hash_init (void)
{
	int i;

	type_hash = g_hash_table_new(g_str_hash, g_str_equal);

	for(i = 0; type_table[i].name != NULL; i++) {
		g_hash_table_insert (type_hash, type_table[i].name,
				     GINT_TO_POINTER (type_table[i].type));
	}
}

void
pong_pane_type_from_string (const char *string,
			    PongType *type,
			    GtkType *gtk_type)
{
	char *typestring;
	char *initfunc;
	guint (*get_type)(void);
	gpointer klass;
	GtkType lgtk_type;

	g_return_if_fail (string != NULL);

	if (type != NULL)
		*type = 0;
	if (gtk_type != NULL)
		*gtk_type = 0;

	/* make sure we have the native widget classes in gtk type table */
	pong_lazy_init ();

	typestring = remove_sep (string);

	if (type != NULL) {
		if( ! type_hash)
			type_hash_init ();

		/* this will give 0, which is the invalid widget, if we don't
		 * find the string */
		*type = GPOINTER_TO_INT (g_hash_table_lookup (type_hash,
							      typestring));
		if (*type > 0) {
			g_free (typestring);
			return;
		}
	}

	lgtk_type = gtk_type_from_name (typestring);

	g_free (typestring);

	if (lgtk_type == 0) {
		if( ! g_module_supported ()) {
			g_warning (_("GModule is not supported and a widget "
				     "of type '%s' has not yet been "
				     "initialized!"), string);
			return;
		}
		typestring = replace_sep (string, '_');
		initfunc = g_strconcat (typestring, "_get_type", NULL);
		g_free(typestring);
		g_strdown(initfunc);

		if( ! allsymbols)
			allsymbols = g_module_open(NULL, 0);

		if( ! g_module_symbol(allsymbols, initfunc,
				      (gpointer *)&get_type)) {
			g_warning(_("Unknown widget %s found"), string);
			g_free(initfunc);
			return;
		}
		g_free(initfunc);

		g_assert(get_type != NULL);

		lgtk_type = get_type();
	}

	if (lgtk_type == 0) {
		g_warning (_("Unknown widget '%s' found"), string);
		return;
	}

	/* we use gtk_type_name as a checking thing to see if this type
	 * actually exists */
	if( ! gtk_type_name (lgtk_type)) {
		g_warning (_("Strange widget type '%s' found"), string);
		return;
	}
	
	if( ! gtk_type_is_a (lgtk_type, gtk_widget_get_type())) {
		g_warning (_("Non-widget object '%s' found"), string);
		return;
	}

	if (type != NULL) {
		klass = gtk_type_class(lgtk_type);

		if (g_dataset_get_data (klass,
					"pong_widget_interface") != NULL) {
			*type = PONG_NATIVEWIDGET;
		} else {
			*type = PONG_EXTRAWIDGET;
		}
	}

	if (gtk_type != NULL)
		*gtk_type = lgtk_type;
}

/*
 * PaneBase
 */
void
pong_pane_base_init (PongPaneBase *base,
		     int type,
		     GDestroyNotify destructor)
{
	static int foo = 1;

	g_return_if_fail (base != NULL);
	g_return_if_fail (type > PONG_PANE_INVALID && type < PONG_PANE_LAST);

	base->refcount = 1;
	base->type = type;
	base->name = g_strdup_printf("__thingie_%d", foo++);
	base->label = NULL;
	base->levels = NULL;

	base->destructor = destructor;
}

void
pong_pane_base_ref (PongPaneBase *base)
{
	g_return_if_fail (base != NULL);

	base->refcount ++;
}

void
pong_pane_base_unref (PongPaneBase *base)
{
	g_return_if_fail (base != NULL);

	base->refcount --;

	if (base->refcount == 0) {
		if (base->destructor != NULL)
			base->destructor (base);
		base->destructor = NULL;

		g_free (base->label);
		base->label = NULL;

		g_free (base->name);
		base->name = NULL;

		g_strfreev (base->levels);
		base->levels = NULL;

		g_free (base);
	}
}

void
pong_pane_base_set_name (PongPaneBase *base, const char *name)
{
	g_return_if_fail (base != NULL);
	g_return_if_fail (name != NULL);

	g_free (base->name);
	base->name = g_strdup (name);
}

void
pong_pane_base_set_label (PongPaneBase *base, const char *label)
{
	g_return_if_fail (base != NULL);

	base->label = label != NULL ? g_strdup (label) : NULL;
}

void
pong_pane_base_set_levels (PongPaneBase *base, const char **levels)
{
	int i;

	g_return_if_fail (base != NULL);

	g_strfreev (base->levels);
	base->levels = NULL;

	if (levels == NULL)
		return;

	for (i = 0; levels[i] != NULL; i++)
		;
	base->levels = g_new0 (char *, i+1);

	for (i = 0; levels[i] != NULL; i++)
		base->levels[i] = g_strdup (levels[i]);
	base->levels[i] = NULL;
}

void
pong_pane_base_add_level (PongPaneBase *base, const char *level)
{
	g_return_if_fail (base != NULL);

	if (base->levels == NULL) {
		base->levels = g_new0 (char *, 2);
		base->levels[0] = g_strdup (level);
		base->levels[1] = NULL;
	} else {
		int i;
		for (i = 0; base->levels[i]; i++)
			;
		base->levels = g_renew (char *, base->levels, i+2);
		base->levels[i] = g_strdup (level);
		base->levels[i+1] = NULL;
	}
}

void
pong_pane_base_add_argument (PongPaneBase *base,
			     const char *arg_name,
			     const char *arg_value,
			     gboolean value_translatable)
{
	PongArgument *a;

	g_return_if_fail (base != NULL);
	g_return_if_fail (base->type == PONG_WIDGET ||
			  base->type == PONG_PLUG_WIDGET);
	g_return_if_fail (arg_name != NULL);
	g_return_if_fail (arg_value != NULL);

	a = g_new0 (PongArgument, 1);
	a->name = g_strdup (arg_name);
	a->value = g_strdup (arg_value);
	a->value_translatable = value_translatable;

	if (base->type == PONG_WIDGET) {
		PongWidget *widget = (PongWidget *)base;
		widget->arguments = g_list_append (widget->arguments, a);
	} else if (base->type == PONG_PLUG_WIDGET) {
		PongPlugWidget *plug_widget = (PongPlugWidget *)base;
		plug_widget->arguments =
			g_list_append (plug_widget->arguments, a);
	}
}

void
pong_pane_base_add_option (PongPaneBase *base,
			   const char *opt_label,
			   const char *opt_value)
{
	PongOption *o;

	g_return_if_fail (base != NULL);
	g_return_if_fail (base->type == PONG_WIDGET ||
			  base->type == PONG_PLUG_WIDGET);
	g_return_if_fail (opt_value != NULL);

	o = g_new0 (PongOption, 1);
	o->label = g_strdup (opt_label);
	o->value = g_strdup (opt_value);

	if (base->type == PONG_WIDGET) {
		PongWidget *widget = (PongWidget *)base;
		widget->options = g_list_append (widget->options, o);
	} else if (base->type == PONG_PLUG_WIDGET) {
		PongPlugWidget *plug_widget = (PongPlugWidget *)base;
		plug_widget->options = g_list_append (plug_widget->options, o);
	}
}

void
pong_pane_base_set_tooltip (PongPaneBase *base, const char *tooltip)
{
	g_return_if_fail (base != NULL);
	g_return_if_fail (base->type == PONG_WIDGET ||
			  base->type == PONG_PLUG_WIDGET);

	if (base->type == PONG_WIDGET) {
		PongWidget *widget = (PongWidget *)base;
		/* handles NULLs ok */
		g_free (widget->tooltip);
		widget->tooltip = g_strdup (tooltip);

	} else if (base->type == PONG_PLUG_WIDGET) {
		PongPlugWidget *plug_widget = (PongPlugWidget *)base;
		/* handles NULLs ok */
		g_free (plug_widget->tooltip);
		plug_widget->tooltip = g_strdup (tooltip);
	}

}

void
pong_pane_base_set_expand (PongPaneBase *base, gboolean expand)
{
	g_return_if_fail (base != NULL);
	g_return_if_fail (base->type == PONG_GROUP ||
			  base->type == PONG_WIDGET ||
			  base->type == PONG_PLUG_WIDGET);

	if (base->type == PONG_WIDGET) {
		PongWidget *widget = (PongWidget *)base;
		widget->expand = expand;
	} else if (base->type == PONG_PLUG_WIDGET) {
		PongPlugWidget *plug_widget = (PongPlugWidget *)base;
		plug_widget->expand = expand;
	} else if (base->type == PONG_GROUP) {
		PongGroup *group = (PongGroup *)base;
		group->expand = expand;
	}
}

void
pong_pane_base_set_align_label (PongPaneBase *base, gboolean align_label)
{
	g_return_if_fail (base != NULL);
	g_return_if_fail (base->type == PONG_WIDGET ||
			  base->type == PONG_PLUG_WIDGET);

	if (base->type == PONG_WIDGET) {
		PongWidget *widget = (PongWidget *)base;
		widget->align_label = align_label;
	} else if (base->type == PONG_PLUG_WIDGET) {
		PongPlugWidget *plug_widget = (PongPlugWidget *)base;
		plug_widget->align_label = align_label;
	}
}

/*
 * Pane
 */
static void
pong_pane_destructor(gpointer data)
{
	PongPane *pane = data;

	g_return_if_fail (pane != NULL);
	g_return_if_fail (pane->base.type == PONG_PANE);

	g_list_foreach (pane->groups, (GFunc)pong_pane_base_unref, NULL);
	g_list_free (pane->groups);
	pane->groups = NULL;
}

PongPane *
pong_pane_new(void)
{
	static int foo = 1;
	PongPane *pane = g_new0 (PongPane, 1);

	pong_pane_base_init (&pane->base,
			     PONG_PANE,
			     pong_pane_destructor);

	pane->base.label = g_strdup_printf (_("Pane %d"), foo++);
	pane->groups = NULL;

	return pane;
}

void
pong_pane_append_group (PongPane *pane, PongGroup *group)
{
	g_return_if_fail (pane != NULL);
	g_return_if_fail (pane->base.type == PONG_PANE);
	g_return_if_fail (group != NULL);
	g_return_if_fail (group->base.type == PONG_GROUP);

	if (g_list_find (pane->groups, group) != NULL)
		return;

	pong_pane_base_ref (&group->base);
	pane->groups = g_list_append (pane->groups, group);
}

void
pong_pane_append_plug_widget (PongPane *pane, PongPlugWidget *plug_widget)
{
	g_return_if_fail (pane != NULL);
	g_return_if_fail (pane->base.type == PONG_PANE);
	g_return_if_fail (plug_widget != NULL);
	g_return_if_fail (plug_widget->base.type == PONG_PLUG_WIDGET);

	if (g_list_find (pane->groups, plug_widget) != NULL)
		return;

	pong_pane_base_ref (&plug_widget->base);
	pane->groups = g_list_append (pane->groups, plug_widget);
}

/*
 * Group
 */
static void
pong_group_destructor (gpointer data)
{
	PongGroup *group = data;

	g_return_if_fail (group != NULL);
	g_return_if_fail (group->base.type == PONG_GROUP);

	g_list_foreach (group->widgets, (GFunc)pong_pane_base_unref, NULL);
	g_list_free (group->widgets);
	group->widgets = NULL;
}

PongGroup *
pong_group_new (void)
{
	static int foo = 1;
	PongGroup *group = g_new0 (PongGroup, 1);

	pong_pane_base_init (&group->base,
			     PONG_GROUP,
			     pong_group_destructor);

	group->base.label = g_strdup_printf (_("Group %d"), foo++);

	group->columns = 1;
	group->expand = FALSE;
	group->widgets = NULL;

	return group;
}

void
pong_group_set_columns (PongGroup *group, int columns)
{
	g_return_if_fail (group != NULL);
	g_return_if_fail (group->base.type == PONG_GROUP);
	g_return_if_fail (columns > 0);

	group->columns = columns;
}

void
pong_group_append_widget(PongGroup *group, PongWidget *widget)
{
	g_return_if_fail (group != NULL);
	g_return_if_fail (group->base.type == PONG_GROUP);
	g_return_if_fail (widget != NULL);
	g_return_if_fail (widget->base.type == PONG_WIDGET);

	if (g_list_find (group->widgets, widget) != NULL)
		return;

	pong_pane_base_ref (&widget->base);
	group->widgets = g_list_append (group->widgets, widget);
}

void
pong_group_append_plug_widget (PongGroup *group, PongPlugWidget *plug_widget)
{
	g_return_if_fail (group != NULL);
	g_return_if_fail (group->base.type == PONG_GROUP);
	g_return_if_fail (plug_widget != NULL);
	g_return_if_fail (plug_widget->base.type == PONG_PLUG_WIDGET);

	if (g_list_find (group->widgets, plug_widget) != NULL)
		return;

	pong_pane_base_ref (&plug_widget->base);
	group->widgets = g_list_append (group->widgets, plug_widget);
}

/*
 * Widget
 */
static void
pong_widget_destructor(gpointer data)
{
	PongWidget *widget = data;
	GList *li;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (widget->base.type == PONG_WIDGET);

	for(li = widget->arguments; li != NULL; li = li->next) {
		PongArgument *a = li->data;
		g_free (a->name);
		a->name = NULL;
		g_free (a->value);
		a->value = NULL;
		g_free (a);
		li->data = NULL;
	}
	g_list_free (widget->arguments);
	widget->arguments = NULL;

	for(li = widget->options; li != NULL; li = li->next) {
		PongOption *o = li->data;
		g_free (o->label);
		o->label = NULL;
		g_free (o->value);
		o->value = NULL;
		g_free (o);
	}
	g_list_free (widget->options);
	widget->options = NULL;
}

PongWidget *
pong_widget_new (void)
{
	PongWidget *widget = g_new0 (PongWidget, 1);

	pong_pane_base_init (&widget->base,
			     PONG_WIDGET,
			     pong_widget_destructor);

	widget->expand = FALSE;
	widget->align_label = TRUE;
	widget->widget_type = PONG_NOTHING;
	widget->widget_gtktype = 0;
	widget->arguments = NULL;
	widget->options = NULL;

	return widget;
}

void
pong_widget_set_widget_type (PongWidget *widget, int widget_type)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (widget->base.type == PONG_WIDGET);
	g_return_if_fail (widget_type >= PONG_INVALIDWIDGET);
	g_return_if_fail (widget_type < PONG_LASTWIDGET);

	widget->widget_type = widget_type;
}

void
pong_widget_set_widget_gtktype (PongWidget *widget, GtkType widget_gtktype)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (widget->base.type == PONG_WIDGET);

	widget->widget_gtktype = widget_gtktype;
}

void
pong_widget_set_widget_type_string (PongWidget *widget, const char *widget_type)
{
	PongType type = 0;
	GtkType gtk_type = 0;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (widget->base.type == PONG_WIDGET);
	g_return_if_fail (widget_type != NULL);

	pong_pane_type_from_string (widget_type, &type, &gtk_type);

	pong_widget_set_widget_type (widget, type);
	pong_widget_set_widget_gtktype (widget, gtk_type);
}

/*
 * Plug Widget
 */
static void
pong_plug_widget_destructor (gpointer data)
{
	PongPlugWidget *plug_widget = data;

	g_return_if_fail (plug_widget != NULL);
	g_return_if_fail (plug_widget->base.type == PONG_PLUG_WIDGET);

	g_free (plug_widget->plug_type);
	plug_widget->plug_type = NULL;

	g_free (plug_widget->path);
	plug_widget->path = NULL;

	g_free (plug_widget->specifier);
	plug_widget->specifier = NULL;
}

PongPlugWidget *
pong_plug_widget_new (void)
{
	PongPlugWidget *plug_widget = g_new0 (PongPlugWidget, 1);

	pong_pane_base_init (&plug_widget->base,
			     PONG_PLUG_WIDGET,
			     pong_plug_widget_destructor);

	plug_widget->expand = FALSE;
	plug_widget->align_label = TRUE;
	plug_widget->plug_type = NULL;
	plug_widget->path = NULL;
	plug_widget->specifier = NULL;

	return plug_widget;
}

void
pong_plug_widget_set_plug_type (PongPlugWidget *plug_widget,
				const char *plug_type)
{
	g_return_if_fail (plug_widget != NULL);
	g_return_if_fail (plug_widget->base.type == PONG_PLUG_WIDGET);
	g_return_if_fail (plug_type != NULL);

	g_free (plug_widget->plug_type);
	plug_widget->plug_type = g_strdup (plug_type);
}

void
pong_plug_widget_set_path (PongPlugWidget *plug_widget,
			   const char *path)
{
	g_return_if_fail (plug_widget != NULL);
	g_return_if_fail (plug_widget->base.type == PONG_PLUG_WIDGET);
	g_return_if_fail (path != NULL);

	g_free (plug_widget->path);
	plug_widget->path = g_strdup (path);
}

void
pong_plug_widget_set_specifier (PongPlugWidget *plug_widget,
				const char *specifier)
{
	g_return_if_fail (plug_widget != NULL);
	g_return_if_fail (plug_widget->base.type == PONG_PLUG_WIDGET);

	/* handles NULL correctly */
	g_free (plug_widget->specifier);
	plug_widget->specifier = g_strdup (specifier);
}

/*
 * Dialog
 */

static void
free_widget_runtime(gpointer key, gpointer data, gpointer user_data)
{
	PongWidgetRuntime *gwr = data;

	if (gwr->wid != NULL)
		gtk_widget_unref (gwr->wid);
	gwr->wid = NULL;
	gwr->pongwid = NULL;

	g_free(gwr);
}

static void
unref_widget(gpointer key, gpointer data, gpointer user_data)
{
	GtkWidget *wid = data;
	gtk_widget_unref (wid);
}

static void
pong_dialog_destroy(PongDialog *dialog)
{
	if (dialog->wid != NULL)
		gtk_widget_unref (dialog->wid);
	dialog->wid = NULL;

	if (dialog->wid != NULL)
		gtk_widget_unref (dialog->pane_wid);
	dialog->pane_wid = NULL;

	if (dialog->tooltips != NULL)
		gtk_object_unref (GTK_OBJECT(dialog->tooltips));
	dialog->tooltips = NULL;

	if (dialog->panes != NULL) {
		g_list_foreach (dialog->panes, (GFunc)pong_pane_base_unref, NULL);
		g_list_free (dialog->panes);
	}
	dialog->panes = NULL;

	if (dialog->widgets != NULL) {
		g_hash_table_foreach (dialog->widgets, free_widget_runtime, NULL);
		g_hash_table_destroy (dialog->widgets);
	}
	dialog->widgets = NULL;

	if (dialog->gtk_widgets != NULL) {
		g_hash_table_foreach (dialog->gtk_widgets, unref_widget, NULL);
		g_hash_table_destroy (dialog->gtk_widgets);
	}
	dialog->gtk_widgets = NULL;

	g_free(dialog);
}

static GtkWidget *
label_widget (GtkWidget *w, const char *label, GtkWidget **labelw)
{
	GtkWidget *hbox, *l;

	l = gtk_label_new (label);
	gtk_misc_set_alignment (GTK_MISC (l),
				0.0,
				0.0);
	gtk_widget_show (l);

	if (labelw != NULL) {
		*labelw = l;

		gtk_widget_ref (l);
		gtk_object_set_data_full (GTK_OBJECT (w),
					  PONG_S_PongWidgetLabel,
					  l,
					  (GtkDestroyNotify) gtk_widget_unref);

		return w;
	} else {
		hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
		gtk_widget_show (hbox);

		gtk_box_pack_start (GTK_BOX (hbox), l, FALSE, FALSE, 0);

		gtk_box_pack_start (GTK_BOX (hbox), w, TRUE, TRUE, 0);

		return hbox;
	}
}

static GtkWidget *
scroll_widget(GtkWidget *w)
{
	GtkWidget *sw;

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (sw);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (sw), w);

	return sw;
}

static void
add_options_to_combo (GtkCombo *combo, GList *options)
{
	GList *list, *li;

	list = NULL;
	for(li = options; li != NULL; li = li->next) {
		PongOption *opt = li->data;

		list = g_list_append(list, opt->value);
	}

	gtk_combo_set_popdown_strings (combo, list);
	g_list_free (list);
}

static void
pong_gtk_widget_set_arguments (GtkWidget *w, GList *arguments)
{
	if (arguments != NULL) {
		GList *li;
		gboolean use_interface = FALSE;

		use_interface =
			pong_widget_interface_exists (w) &&
			pong_widget_interface_implements_set_arg (w);


		for(li = arguments; li != NULL; li = li->next) {
			PongArgument *a = li->data;
			if (use_interface)
				pong_widget_interface_set_arg (w,
							       a->name,
							       a->value);
			else
				pong_set_object_argument (GTK_OBJECT (w),
							  a->name, a->value);
		}
	}
}

static GtkWidget *
pong_gtk_widget_setup_native (GtkWidget *widget,
			      const char *label,
			      GList *arguments,
			      GList *options,
			      GtkWidget **labelw)
{
	GtkWidget *w;

	w = widget;

	if (labelw != NULL)
		*labelw = NULL;

	pong_gtk_widget_set_arguments (widget, arguments);
	gtk_object_default_construct (GTK_OBJECT (widget));

	gtk_widget_show (widget);
	if (label != NULL) {
		/* if it doesn't implement or can't set the label, set it
		 * ourselves */
		if ( ! pong_widget_interface_implements_set_label (widget) ||
		     ! pong_widget_interface_set_label (widget, label))
			w = label_widget (widget, label, labelw);
	}
	if (options != NULL &&
	    pong_widget_interface_implements_add_options (widget))
		pong_widget_interface_add_options (widget, options);

	return w;
}

static GtkWidget *
pong_gtk_widget_setup_extra (GtkWidget *widget,
			     const char *label,
			     GList *arguments,
			     GtkWidget **labelw)
{
	GtkWidget *w;

	w = widget;

	if (labelw != NULL)
		*labelw = NULL;

	pong_gtk_widget_set_arguments (widget, arguments);
	gtk_object_default_construct (GTK_OBJECT (widget));

	gtk_widget_show (widget);

	if (label != NULL)
		w = label_widget (widget, label, labelw);

	return w;
}

static GtkWidget *
pong_dialog_setup_tooltip (PongDialog *dialog, GtkWidget *widget, const char *tooltip)
{
	GtkWidget *w;

	w = widget;

	if (tooltip != NULL) {
		if (GTK_WIDGET_NO_WINDOW (w)) {
			w = gtk_event_box_new ();
			gtk_widget_show (w);
			gtk_container_add (GTK_CONTAINER (w), widget);
			gtk_widget_show (widget);
		}

		gtk_tooltips_set_tip (dialog->tooltips, w, tooltip, NULL);
	}

	return w;
}


static GtkWidget *
pong_dialog_make_widget (PongDialog *dialog, PongWidget *widget,
			 GtkWidget **labelw)
{
	GtkWidget *w, *realwid = NULL;
	PongWidgetRuntime *gwr;

	if (labelw != NULL)
		*labelw = NULL;

	gwr = g_hash_table_lookup (dialog->widgets, widget->base.name);

	/* this is a name clash, so we ignore this widget */
	if( ! gwr ||
	   gwr->pongwid != widget)
		return NULL;

	switch (widget->widget_type) {
	case PONG_INVALIDWIDGET:
		g_warning(_("Invalid widget type found while creating dialog"));
		realwid = w = NULL;
		break;

	case PONG_NOTHING:
		if (widget->base.label != NULL) {
			realwid = w = gtk_label_new (widget->base.label);
			pong_gtk_widget_set_arguments (realwid, widget->arguments);
		} else {
			realwid = w = NULL;
		}
		break;


	case PONG_NATIVEWIDGET:
		realwid = w = gtk_type_new (widget->widget_gtktype);
		if (w == NULL || ! GTK_IS_WIDGET (w)) {
			g_warning(_("Cannot create native widget!"));
			realwid = w = NULL;
			break;
		}
		w = pong_gtk_widget_setup_native (realwid,
						  widget->base.label,
						  widget->arguments,
						  widget->options,
						  labelw);
		break;

	case PONG_EXTRAWIDGET:
		realwid = w = gtk_type_new (widget->widget_gtktype);
		if (w == NULL || ! GTK_IS_WIDGET (w)) {
			g_warning(_("Cannot create native widget!"));
			realwid = w = NULL;
			break;
		}
		w = pong_gtk_widget_setup_extra (realwid,
						 widget->base.label,
						 widget->arguments,
						 labelw);
		break;

	case PONG_GTKLABEL:
		realwid = w = gtk_label_new (widget->base.label);
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		break;

	case PONG_GTKSPINBUTTON:
		realwid = w = gtk_spin_button_new (NULL, 1, 0);
		gtk_signal_connect (GTK_OBJECT (w), "map",
				    GTK_SIGNAL_FUNC (pong_spin_button_mapped),
				    NULL);
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GTKENTRY:
		realwid = w = gtk_entry_new ();
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GTKTEXT:
		realwid = w = gtk_text_new (NULL, NULL);
		gtk_text_set_editable (GTK_TEXT (w), TRUE);
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show (w);
		w = scroll_widget (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GTKCOMBO:
		realwid = w = gtk_combo_new ();
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		add_options_to_combo (GTK_COMBO (w), widget->options);
		gtk_widget_show (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GNOMEENTRY:
		realwid = w = gnome_entry_new (widget->base.name);
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GNOMEFILEENTRY:
		realwid = w = gnome_file_entry_new (widget->base.name,
						    _("Browse"));
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GNOMEPIXMAPENTRY:
		realwid = w = gnome_pixmap_entry_new (widget->base.name,
						      _("Browse"), TRUE);
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GNOMEICONENTRY:
		realwid = w = gnome_icon_entry_new(widget->base.name,
						   _("Browse"));
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show(w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	case PONG_GTKTOGGLEBUTTON:
		realwid = w =
			gtk_toggle_button_new_with_label (widget->base.label);
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		break;
	case PONG_GTKCHECKBUTTON:
		realwid = w =
			gtk_check_button_new_with_label (widget->base.label);
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		break;
	case PONG_GNOMECOLORPICKER:
		realwid = w = gnome_color_picker_new ();
		pong_gtk_widget_set_arguments (realwid, widget->arguments);
		gtk_widget_show (w);
		if (widget->base.label != NULL)
			w = label_widget (w, widget->base.label, labelw);
		break;
	default:
		g_warning(_("Unsupported widget type found while creating dialog"));
		realwid = w = NULL;
		break;
	}
	
	if (w == NULL)
		return NULL;

	gtk_widget_show (w);

	gwr->wid = realwid;
	gtk_widget_ref (realwid);

	w = pong_dialog_setup_tooltip (dialog, w, widget->tooltip);

	return w;
}

static gboolean
is_in_level (char **levels, const char *level)
{
	int i;

	if ( ! levels ||
	     ! level)
		return TRUE;

	for (i = 0; levels[i]; i++) {
		if (strcmp (levels[i], level) == 0)
			return TRUE;
	}
	return FALSE;
}

static GtkWidget *
pong_dialog_make_plug_widget (PongDialog *dialog, PongPlugWidget *plug_widget)
{
	GtkWidget *widget;

	if (plug_widget->plug_type == NULL) {
		/* this should never happen really */
		g_warning (_("No plug_type specified for a plug widget"));
		return NULL;
	}

	if (plug_widget->path == NULL) {
		g_warning (_("No path specified for a '%s' widget"),
			   plug_widget->plug_type);
		return NULL;
	}

	if (dialog->ui == NULL) {
		/* can this even happen? */
		g_warning (_("No UI object.  Skipping '%s' widgets."),
			   plug_widget->plug_type);
		return NULL;
	}

	if (pong_ui_internal_supports (dialog->ui, plug_widget->plug_type)) {
		widget = pong_ui_internal_make_plug_widget (dialog->ui, 
							    plug_widget->plug_type,
							    plug_widget->base.name,
							    plug_widget->path,
							    plug_widget->specifier);
	} else {
		g_warning (_("No '%s' support in UI object.  "
			     "Skipping '%s' widgets."),
			   plug_widget->plug_type,
			   plug_widget->plug_type);
		return NULL;
	}

	if (widget == NULL)
		g_warning (_("Cannot create a '%s' widget %s %s"),
			   plug_widget->plug_type,
			   plug_widget->path,
			   plug_widget->specifier);

	return widget;
}

static GtkWidget *
pong_dialog_create_plug (PongDialog *dialog, PongPlugWidget *plug_widget,
			 GtkWidget **labelw)
{
	GtkWidget *widget;

	if (labelw != NULL)
		*labelw = NULL;

	g_assert (plug_widget->base.type == PONG_PLUG_WIDGET);

	widget = pong_dialog_make_plug_widget (dialog,
					       plug_widget);
	if (widget != NULL &&
	    g_hash_table_lookup (dialog->gtk_widgets,
				 plug_widget->base.name) == NULL) {
		gtk_widget_ref (widget);
		g_hash_table_insert (dialog->gtk_widgets,
				     plug_widget->base.name, widget);
	}
	/* This is AFTER it's been added to gtk_widgets
	 * hash table, as we need the original widget there */
	if (widget != NULL) {
		if (pong_widget_interface_exists (widget))
			widget = pong_gtk_widget_setup_native
				(widget,
				 plug_widget->base.label,
				 plug_widget->arguments,
				 plug_widget->options,
				 labelw);
		else
			widget = pong_gtk_widget_setup_extra
				(widget,
				 plug_widget->base.label,
				 plug_widget->arguments,
				 labelw);
		widget = pong_dialog_setup_tooltip (dialog,
						    widget,
						    plug_widget->tooltip);
	}

	return widget;
}

static GtkWidget *
pong_dialog_make_group (PongDialog *dialog, PongGroup *group, const char *level)
{
	GtkWidget *table;
	GtkWidget *label = NULL;
	int row, column;
	GList *li;

	table = gtk_table_new (0, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
	gtk_widget_show (table);

	row = 0;
	column = 0;
	for (li = group->widgets; li != NULL; li = li->next) {
		PongPaneBase *base = li->data;
		GtkWidget *widget;
		gboolean expand = FALSE;

		label = NULL;

		/* By now we can assert, this should never fail */
		g_assert (base->type == PONG_WIDGET ||
			  base->type == PONG_PLUG_WIDGET);

		if ( ! is_in_level (base->levels, level))
			continue;

		/* FIXME: UGLY, this should be unified with
		 * PongWidget really, now we only support plug
		 * widgets to be like NATIVE or EXTRA, anyway
		 * needs to be generalized and streamlined */
		if (base->type == PONG_PLUG_WIDGET) {
			PongPlugWidget *plug_widget =
				(PongPlugWidget *)base;

			expand = plug_widget->expand;

			if (plug_widget->align_label)
				widget = pong_dialog_create_plug (dialog,
								  plug_widget,
								  &label);
			else
				widget = pong_dialog_create_plug (dialog,
								  plug_widget,
								  NULL);
		} else /* if (base->type == PONG_WIDGET) */ {
			PongWidget *pong_widget = (PongWidget *)base;

			expand = pong_widget->expand;

			if (pong_widget->align_label)
				widget = pong_dialog_make_widget (dialog,
								  pong_widget,
								  &label);
			else
				widget = pong_dialog_make_widget (dialog,
								  pong_widget,
								  NULL);
		}

		if (widget != NULL) {
			int left_attach;
			if (label != NULL) {
				left_attach = column*2 + 1;
				gtk_table_attach (GTK_TABLE (table),
						  label,
						  column*2 /* left_attach */,
						  column*2 + 1 /* right_attach */,
						  row /* top_attach */,
						  row + 1 /* bottom_attach */,
						  GTK_FILL /* xoptions */,
						  GTK_FILL /* yoptions */,
						  0 /* xpadding */,
						  0 /* ypadding */);
			} else {
				left_attach = column*2;
			}
			gtk_table_attach (GTK_TABLE (table),
					  widget,
					  left_attach /* left_attach */,
					  column*2 + 2 /* right_attach */,
					  row /* top_attach */,
					  row + 1 /* bottom_attach */,
					  GTK_EXPAND | GTK_FILL /* xoptions */,
					  (expand ? GTK_EXPAND | GTK_FILL : 0) /* yoptions */,
					  0 /* xpadding */,
					  0 /* ypadding */);
		}
		column ++;
		if (column == group->columns) {
			column = 0;
			row ++;
		}
	}

	return table;
}

static GtkWidget *
pong_dialog_make_pane (PongDialog *dialog,
		       PongPane *pane,
		       const char *level)
{
	GtkWidget *vbox;
	GList *li;

	vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD_SMALL);
	gtk_widget_show (vbox);

	for(li = pane->groups; li != NULL; li = li->next) {
		PongPaneBase *base = li->data;
		GtkWidget *widget, *topwidget;
		gboolean expand;

		/* By now we can assert, this should never fail */
		g_assert (base->type == PONG_GROUP ||
			  base->type == PONG_PLUG_WIDGET);

		if( ! is_in_level (base->levels, level))
			continue;

		/* FIXME: UGLY, this should be unified with
		 * PongWidget really, now we only support plug
		 * widgets to be like NATIVE or EXTRA, anyway
		 * needs to be generalized and streamlined */
		if (base->type == PONG_PLUG_WIDGET) {
			PongPlugWidget *plug_widget =
				(PongPlugWidget *)base;

			expand = plug_widget->expand;

			topwidget = pong_dialog_create_plug (dialog,
							     plug_widget,
							     NULL);
		} else /* if (base->type == PONG_GROUP) */ {
			PongGroup *group = (PongGroup *)base;
			
			expand = group->expand;

			topwidget = gtk_frame_new (base->label);
			gtk_widget_show (topwidget);

			widget = pong_dialog_make_group (dialog, group, level);
			gtk_container_add (GTK_CONTAINER (topwidget), widget);

			if (g_hash_table_lookup (dialog->gtk_widgets,
						 base->name) == NULL) {
				gtk_widget_ref (widget);
				g_hash_table_insert (dialog->gtk_widgets,
						     base->name, widget);
			}
		}

		if (topwidget != NULL)
			gtk_box_pack_start (GTK_BOX (vbox), topwidget,
					    expand, expand, 0);

	}

	return vbox;
}

static void
insert_widget(PongDialog *dialog, GHashTable *gtk_widgets, PongWidget *widget)
{
	PongWidgetRuntime *gwr;

	if (g_hash_table_lookup(dialog->widgets, widget->base.name) != NULL) {
		g_warning(_("Widget of name '%s' is already added!"),
			  widget->base.name);
		return;
	}

	if (g_hash_table_lookup(gtk_widgets, widget->base.name) != NULL) {
		g_warning(_("Widget of name '%s' is already added!"),
			  widget->base.name);
		return;
	}

	gwr = g_new0 (PongWidgetRuntime, 1);
	gwr->wid = NULL;
	gwr->pongwid = widget;
	g_hash_table_insert(dialog->widgets, widget->base.name, gwr);
}

static void
insert_gtk_widget(PongDialog *dialog, GHashTable *gtk_widgets, char *name)
{
	if (g_hash_table_lookup(dialog->widgets, name) != NULL) {
		g_warning(_("Widget of name '%s' is already added!"), name);
		return;
	}

	if (g_hash_table_lookup(gtk_widgets, name) != NULL) {
		g_warning(_("Widget of name '%s' is already added!"), name);
		return;
	}

	g_hash_table_insert(gtk_widgets, name, GINT_TO_POINTER(1));
}

static void
dialog_setup_hash(PongDialog *dialog, GList *panes)
{
	GList *lip, *lig, *liw;
	GHashTable *gtk_widgets;

	/* temporary hash table just to find duplicates */
	gtk_widgets = g_hash_table_new(g_str_hash, g_str_equal);

	for(lip = panes; lip != NULL; lip = lip->next) {
		PongPane *pane = lip->data;

		insert_gtk_widget(dialog, gtk_widgets,
				  pane->base.name);

		for(lig = pane->groups; lig != NULL; lig = lig->next) {
			PongPaneBase *baseg = lig->data;
			PongGroup *group;

			insert_gtk_widget(dialog, gtk_widgets,
					  baseg->name);

			if (baseg->type == PONG_PLUG_WIDGET)
				continue;

			group = (PongGroup *)baseg;

			for (liw = group->widgets;
			     liw != NULL;
			     liw = liw->next) {
				PongPaneBase *basew = liw->data;

				if (basew->type == PONG_WIDGET)
					insert_widget(dialog, gtk_widgets, 
						      (PongWidget *)basew);
				else
					insert_gtk_widget(dialog, gtk_widgets,
							  basew->name);
			}
		}
	}

	g_hash_table_destroy(gtk_widgets);
}

static void
dialog_setup_unknowns (PongElement *element, gpointer data)
{
	PongDialog *dialog = data;
	PongWidget *widget = NULL;

	if (element->widget != NULL)
		widget = pong_dialog_lookup_widget (dialog, element->widget);
	if (widget == NULL)
		widget = pong_dialog_lookup_widget (dialog, element->conf_path);
	if (widget == NULL)
		return;

	if (widget->widget_type == PONG_INVALIDWIDGET ||
	    widget->widget_type == PONG_NOTHING) {
		switch(element->type) {
		case PONG_TYPE_BOOL:
			widget->widget_type = PONG_GTKCHECKBUTTON;
			break;
		case PONG_TYPE_INT:
			widget->widget_type = PONG_NATIVEWIDGET;
			widget->widget_gtktype = pong_spin_button_get_type ();
			break;
		case PONG_TYPE_FLOAT:
			/* FIXME: Hmmm, limits, and digits and all that
			 * aren't set right ... are they? */
			widget->widget_type = PONG_NATIVEWIDGET;
			widget->widget_gtktype = pong_spin_button_get_type ();
			break;
		case PONG_TYPE_STRING:
			widget->widget_type = PONG_GTKENTRY;
			break;

		/* all list/pair types become gtktext */
		case PONG_TYPE_LIST_OF_STRINGS:
		case PONG_TYPE_LIST_OF_INTS:
		case PONG_TYPE_LIST_OF_FLOATS:
		case PONG_TYPE_LIST_OF_BOOLS:

		case PONG_TYPE_PAIR_STRING_STRING:
		case PONG_TYPE_PAIR_STRING_INT:
		case PONG_TYPE_PAIR_STRING_FLOAT:
		case PONG_TYPE_PAIR_STRING_BOOL:
		case PONG_TYPE_PAIR_INT_STRING:
		case PONG_TYPE_PAIR_INT_INT:
		case PONG_TYPE_PAIR_INT_FLOAT:
		case PONG_TYPE_PAIR_INT_BOOL:
		case PONG_TYPE_PAIR_FLOAT_STRING:
		case PONG_TYPE_PAIR_FLOAT_INT:
		case PONG_TYPE_PAIR_FLOAT_FLOAT:
		case PONG_TYPE_PAIR_FLOAT_BOOL:
		case PONG_TYPE_PAIR_BOOL_STRING:
		case PONG_TYPE_PAIR_BOOL_INT:
		case PONG_TYPE_PAIR_BOOL_FLOAT:
		case PONG_TYPE_PAIR_BOOL_BOOL:
			widget->widget_type = PONG_GTKTEXT;
			break;
		default:
			g_assert_not_reached ();
			break;
		}
	}
}

static void
dialog_clicked (GtkWidget *widget, int button, gpointer data)
{
	PongUIInternal *ui = data;
	/*int ok = pong_gtk_object_get_int_data (widget, "ok");*/
	int help = pong_gtk_object_get_int_data (widget, "help");
	int cancel = pong_gtk_object_get_int_data (widget, "cancel");

	if (button == help) {
		pong_ui_show_help (PONG_UI (ui));
	} else if (button == cancel) {
		pong_ui_revert (PONG_UI (ui));
		gnome_dialog_close (GNOME_DIALOG (widget));
		/* note we have to do the revert before the close, because
		 * note that 1) the revert change set has the lifetime of
		 * the dialog 2) xml object might be dead by now */
	} else {
		gnome_dialog_close (GNOME_DIALOG (widget));
	}
}

static void
dialog_destroyed (GtkWidget *widget, gpointer data)
{
	PongUIInternal *ui = data;

	/* We still keep a weak reference in dialog->ui,
	 * we just drop the dialog's hard ref here */
	gtk_object_unref (GTK_OBJECT (ui));
}

static void
ui_destroyed (GtkObject *ui, gpointer data)
{
	PongDialog *dialog = data;

	dialog->ui = NULL;
}

PongDialog *
pong_dialog_new (PongUIInternal *ui,
		 PongFile *file,
		 const char *level)
{
	PongDialog *dialog;
	GList *li;

	g_return_val_if_fail (ui != NULL, NULL);
	g_return_val_if_fail (PONG_IS_UI_INTERNAL (ui), NULL);
	g_return_val_if_fail (file != NULL, NULL);

	if (file->panes == NULL) {
		g_warning (_("No file or panes to make dialog from"));
		return NULL;
	}
       
	dialog = g_new0 (PongDialog, 1);

	dialog->ui = ui;

	gtk_signal_connect (GTK_OBJECT (ui), "destroy",
			    GTK_SIGNAL_FUNC (ui_destroyed),
			    dialog);

	dialog->tooltips = gtk_tooltips_new ();

	/* FIXME: if auto_apply is FALSE, then use gnome_property_box */
	dialog->refcount = 1;
	if (file->help_name != NULL &&
	    ! file->revert) {
		dialog->wid = gnome_dialog_new (file->dialog_title, 
						GNOME_STOCK_BUTTON_HELP,
						GNOME_STOCK_BUTTON_OK,
						NULL);
		pong_gtk_object_set_int_data (dialog->wid, "ok", 1);
		pong_gtk_object_set_int_data (dialog->wid, "help", 0);
		pong_gtk_object_set_int_data (dialog->wid, "cancel", 999);
		/* 999 is invalid, however, it can't be -1, as that's
		 * given on dialog being closed by WM */
	} else if (file->help_name != NULL &&
		   file->revert) {
		dialog->wid = gnome_dialog_new (file->dialog_title, 
						GNOME_STOCK_BUTTON_HELP,
						GNOME_STOCK_BUTTON_CANCEL,
						GNOME_STOCK_BUTTON_OK,
						NULL,
						/* Evil huh, but I want to
						 * keep the Revert
						 * translations so I don't want
						 * to just delete this line */
						N_("Revert"));
		pong_gtk_object_set_int_data (dialog->wid, "ok", 2);
		pong_gtk_object_set_int_data (dialog->wid, "help", 0);
		pong_gtk_object_set_int_data (dialog->wid, "cancel", 1);
	} else if (file->help_name == NULL &&
		   file->revert) {
		dialog->wid = gnome_dialog_new (file->dialog_title, 
						GNOME_STOCK_BUTTON_CANCEL,
						GNOME_STOCK_BUTTON_OK,
						NULL);
		pong_gtk_object_set_int_data (dialog->wid, "ok", 1);
		pong_gtk_object_set_int_data (dialog->wid, "help", 999);
		pong_gtk_object_set_int_data (dialog->wid, "cancel", 0);
		/* 999 is invalid, however, it can't be -1, as that's
		 * given on dialog being closed by WM */
	} else {
		dialog->wid = gnome_dialog_new (file->dialog_title, 
						GNOME_STOCK_BUTTON_OK,
						NULL);
		pong_gtk_object_set_int_data (dialog->wid, "ok", 0);
		pong_gtk_object_set_int_data (dialog->wid, "help", 999);
		pong_gtk_object_set_int_data (dialog->wid, "cancel", 999);
		/* 999 is invalid, however, it can't be -1, as that's
		 * given on dialog being closed by WM */
	}

	/* In this case we ref the UI, so that basically
	 * the "dialog refs the ui" */
	gtk_object_ref (GTK_OBJECT (ui));
	gtk_signal_connect (GTK_OBJECT (dialog->wid), "clicked",
			    GTK_SIGNAL_FUNC (dialog_clicked),
			    ui);
	gtk_signal_connect (GTK_OBJECT (dialog->wid), "destroy",
			    GTK_SIGNAL_FUNC (dialog_destroyed),
			    ui);

	gtk_widget_ref (dialog->wid);

	dialog->panes = g_list_copy (file->panes);
	g_list_foreach (dialog->panes, (GFunc)pong_pane_base_ref, NULL);

	dialog->widgets = g_hash_table_new (g_str_hash, g_str_equal);
	dialog->gtk_widgets = g_hash_table_new (g_str_hash, g_str_equal);

	/* setup our name to gwr hash */
	dialog_setup_hash (dialog, dialog->panes);

	/* setup any invalid widgets (unknowns) to be defaults for
	 * that particular type */
	pong_file_foreach_element (file, dialog_setup_unknowns, dialog);

	/*FIXME: somehow read preferences and make it possible to change
	 * the style (with set_pane_mode) */
	dialog->pane_wid = pong_pane_widget_new ();
	gtk_widget_ref (dialog->pane_wid);
	gtk_widget_show (dialog->pane_wid);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog->wid)->vbox),
			    dialog->pane_wid, TRUE, TRUE, 0);

	for (li = dialog->panes; li != NULL; li = li->next) {
		PongPane *pane = li->data;
		GtkWidget *w;

		if ( ! is_in_level (pane->base.levels, level))
			continue;

		w = pong_dialog_make_pane (dialog, pane, level);

		if (g_hash_table_lookup (dialog->gtk_widgets,
					 pane->base.name) == NULL) {
			gtk_widget_ref (w);
			g_hash_table_insert (dialog->gtk_widgets,
					     pane->base.name, w);
		}
		pong_pane_widget_append_page (PONG_PANE_WIDGET(dialog->pane_wid),
					      w, pane->base.label);
		gtk_widget_map (w);
	}

	gtk_widget_show (dialog->wid);

	return dialog;
}

void
pong_dialog_ref(PongDialog *dialog)
{
	g_return_if_fail(dialog != NULL);

	dialog->refcount ++;
}

void
pong_dialog_unref(PongDialog *dialog)
{
	g_return_if_fail(dialog != NULL);

	dialog->refcount --;

	if(dialog->refcount == 0)
		pong_dialog_destroy(dialog);
}

PongWidget *
pong_dialog_lookup_widget(PongDialog *dialog, const char *name)
{
	PongWidgetRuntime *gwr;

	g_return_val_if_fail(dialog != NULL, NULL);
	g_return_val_if_fail(name != NULL, NULL);

	gwr = g_hash_table_lookup (dialog->widgets, name);
	if (gwr != NULL)
		return gwr->pongwid;
	else
		return NULL;
}

GtkWidget *
pong_dialog_lookup_gtk_widget(PongDialog *dialog, const char *name)
{
	PongWidgetRuntime *gwr;

	g_return_val_if_fail(dialog != NULL, NULL);
	g_return_val_if_fail(name != NULL, NULL);

	gwr = g_hash_table_lookup (dialog->widgets, name);
	if (gwr != NULL)
		return gwr->wid;
	else
		return g_hash_table_lookup (dialog->gtk_widgets, name);
}

void
pong_pane_free_unused_data(void)
{
	if(type_hash) {
		g_hash_table_destroy(type_hash);
		type_hash = NULL;
	}
	if(allsymbols)
		g_module_close(allsymbols);
}

