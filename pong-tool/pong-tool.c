/* pong-tool: pong tool
 * Author: George Lebl
 * (c) 1999 the Free Software Foundation
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "config.h"
#include <string.h>
#include <gnome.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include "../bonobo/pong-bonobo.h"

static PongXML *config = NULL;

static char *prefix = NULL;
static char *level = NULL;

static const struct poptOption options[] = {
	{ "level", 0, POPT_ARG_STRING, &level, 0, N_("Pong user level to use"), N_("LEVEL") },
	{ "prefix", 0, POPT_ARG_STRING, &prefix, 0, N_("Pong prefix to use"), N_("PREFIX") },
	{ NULL } 
};

int
main (int argc, char *argv[])
{
	CORBA_ORB orb;
	poptContext ctx;
	GtkWidget *dialog;
	GError* error = NULL;
	const char **args;
	const char *file;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	gnome_init_with_popt_table ("pong-tool", VERSION,
				    argc, argv, options, 0, &ctx);

	/* Initialize components and libraries we use */
	orb = oaf_init (argc, argv);
	bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);

	if ( ! gconf_init (argc, argv, &error)) {
		g_assert (error != NULL);
		g_warning (_("GConf init failed:\n  %s"), error->message);
		g_error_free (error);
		error = NULL;
		return 1;
	}

	if ( ! pong_init ())
		g_error (_("PonG init failed!"));
	if ( ! pong_glade_init ())
		g_error (_("PonG glade init failed!"));
	if ( ! pong_bonobo_init ())
		g_error (_("PonG bonobo init failed!"));

	args = poptGetArgs (ctx);

	if (args == NULL ||
	    args[0] == NULL ||
	    args[1] != NULL) {
		fprintf (stderr,
			 _("You must supply a single argument with the "
			   ".pong file to use\n"));
		return 1;
	}

	file = args[0];

	/* Add the dirname from file to the path */
	/* For the sake of glade files */
	{
		char *dir = g_dirname (file);

		if (dir != NULL) {
			pong_add_directory (dir);
			pong_add_glade_directory (dir);

			g_free (dir);
		}
	}

	config = pong_xml_new (file);
	if (config == NULL) {
		fprintf (stderr, _("Can't load pong file: %s\n"), file);
		return 1;
	}

	if (level != NULL)
		pong_xml_set_level (config, level);

	if (prefix != NULL)
		pong_xml_set_prefix (config, prefix);

	pong_xml_show_dialog (config);

	dialog = pong_xml_get_dialog_widget (config);
	if (dialog == NULL) {
		gtk_object_unref (GTK_OBJECT (config));
		fprintf (stderr, _("Can't run dialog\n"));
		return 1;
	}

	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    GTK_SIGNAL_FUNC (gtk_main_quit),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_false),
			    NULL);

	gtk_main ();

	gtk_object_unref (GTK_OBJECT (config));

	return 0;
}
